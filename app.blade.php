<!DOCTYPE html>
<html>
<head>
	<!-- set the encoding of your site -->
	<meta charset="utf-8">
	<!-- set the viewport width and initial-scale on mobile devices -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Brandeden</title>
	<!-- include the Montserrat Font stylesheet -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<!-- include the bootstrap stylesheet -->
	<link rel="stylesheet" href="<?php echo asset('css/bootstrap.css')?>">
	<!-- include the site stylesheet -->
	<link rel="stylesheet" href="<?php echo asset('css/main.css')?>">
    <!-- include the datapicker stylesheet -->
	<link rel="stylesheet" type="text/css" href="/css/jquery-ui.min.css">
   	<!-- include jQuery library -->
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript">window.jQuery || document.write('<script src="/js/jquery-1.11.2.min.js"><\/script>')</script>
	<!-- include Bootstrap JavaScript -->
	<script src="/js/bootstrap.min.js"></script>
	<!-- include custom JavaScript -->
	<script src="/js/jquery.main.js"></script>
    <!-- include datepicker JavaScript -->
	<script type="text/javascript" src="/js/jq.ui.datepicker.js"></script>
	<!-- include custom JavaScript for sidebar -->
	<script src="/js/jquery.main.sidebar.js"></script>

    <!--include money.js-->
    <script src="/js/money.js"></script>
    <script src="/js/accounting.min.js"></script>
	<!-- include utility JavaScript -->
	<script type="text/javascript">
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
			document.createTextNode(
			  '@-ms-viewport{width:auto!important}'
			)
			)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>
    <script type="text/javascript">
		(function() {
			if (typeof window.janrain !== 'object') window.janrain = {};
			if (typeof window.janrain.settings !== 'object') window.janrain.settings = {};
			
			janrain.settings.tokenUrl = '<?php echo config('app.url'); ?>/login';
			janrain.settings.type = 'modal';
			janrain.settings.appId = 'idbpadndmkmpcnggmojf';
			janrain.settings.appUrl = 'https://zendomain.rpxnow.com/';
			janrain.settings.providers = ['googleplus', 'yahoo'];//['facebook', 'googleplus', 'blogger', 'aol', 'yahoo', 'openid'];
			janrain.settings.actionText = 'Choose a provider!';
			janrain.settings.showAttribution = false;
			janrain.settings.fontColor = '#a020f0';
			janrain.settings.fontFamily = 'impact, playbill, fantasy';		
			function isReady() { janrain.ready = true; };
			if (document.addEventListener) {
			  document.addEventListener("DOMContentLoaded", isReady, false);
			} else {
			  window.attachEvent('onload', isReady);
			}
		
			var e = document.createElement('script');
			e.type = 'text/javascript';
			e.id = 'janrainAuthWidget';
		
			if (document.location.protocol === 'https:') {
			  e.src = 'https://rpxnow.com/js/lib/zendomain/engage.js';
			} else {
			  e.src = 'http://widget-cdn.rpxnow.com/js/lib/zendomain/engage.js';
			}
		
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(e, s);
		})();

		$(document).ready(function(){			

			//Trigger a click on load to adjust screen heights.
			if($('html').width() > 1399)
				$('.sidebar-opener').click();

			//Click boxes and letsgo
			$('a.btn-save, a.btn-buy').hover(
			  	function() {
					$(this).parent().parent().removeClass('box-link');
			  	},
			  	function() {
					$(this).parent().parent().addClass('box-link');
			  	}
			);
			$('body').on('click', 'div.box-link', function(){
			  	var link = $(this).find('h2>a').attr('href');
				if($(this).hasClass('box-link'))
					window.location.href = link;
			});

			$('body').on("click", 'a.btn-save', function(e){
			
				$link = $(this);
				$.ajax({
					type: "POST",
					url: $link.data('href'),
					success: function(r) {
						/*if($link.closest('article').hasClass('save'))
							$link.closest('article').removeClass('save');
						else
							$link.closest('article').addClass('save');*/
					},
					error: function(xhr, exception){
						alert('Something went wrong!! Please try later.');
					}
				});
				return false;
			});

			$('body').on("click", 'a.btn-buy', function(){
				$('#buyNowModal').modal();
				$('.box').removeClass('buyNowActiveBox');
				$(this).parent().parent().addClass('buyNowActiveBox');
				return false;
			});

			$('#buyNowModal').on('shown.bs.modal', function (event) {
				$('#enq-domainName').val($('.buyNowActiveBox h2>a').text());
				$('#enq-domainOffer').val($('.buyNowActiveBox input[name=offer_pricedata]').val());
				$('#enq-domain-id').val($('.buyNowActiveBox input[name=domainId]').val());
				$('#enq-email').val('');
				$('#enq-message').val('');

				if($('#enq-domainOffer').val() > 0)
					$('#enq-domainOffer').parent().show();
				else $('#enq-domainOffer').parent().hide();

				$('#enq-email').focus();
			});

			$('#buyNowModal').on('hidden.bs.modal', function (event) {
				$('#buyNowModal .status-message').remove();
			});

			$('#send-enquiry').click(function(){
	
				var $btn = $(this);
				$btn.button('loading');

				$('#buyNowModal .status-message').remove();

				// business logic...
				$.post("/enquire", $("#enquiryForm").serialize()).done(function(json){

					var r = $.parseJSON(json);
					var status = r.status.toLowerCase();
					var message = r.message;

					console.log(r);

					$('<p class="status-message text-center bg-'+status+' text-'+status+' col-lg-12" >'+message+'</p>').prependTo('#buyNowModal .modal-body');

					$btn.button('reset');
					return false;
				});

				return false;
			});
		});
</script>
<script type="text/javascript">
var fxMoney;
var $currency_symbol = '$';
$(document).ready(function() {
    $('.currency').click(function(e){
		// Load exchange rates data via AJAX:
		var $currency = $(this).text();
		$currency_symbol = $(this).data('symbol');
		$('#btn_currency').text($currency);
		$('#overlay-wrapper').show();
		$.ajax({
				type: "GET",
				url: "/currency",
				dataType: 'json',
				success: function(data) {
					if(data.result_code == 0)
					{
						// Check money.js has finished loading:
						if ( typeof fx !== "undefined" && fx.rates ) {
							fx.rates = data.rates;
							fx.base = data.base;
						} else {
							// If not, apply to fxSetup global:
							var fxSetup = {
								rates : data.rates,
								base : data.base
							}
						}
						fx.settings = { from: 'USD', to: $currency };
						fxMoney = fx;
						_changeprice(fxMoney, $currency_symbol);
					}
					else
						alert(data.response_details);
					$('#overlay-wrapper').hide();
				}
		});	
		e.preventDefault();	
	});	
});
function _changeprice(fx, $currency_symbol)
{
	$('input[name=pricedata]').each(function(index, element) {
		var amount = fx.convert($(this).val());
		amount = accounting.formatMoney(amount, {
			symbol: $currency_symbol,
			format: "%s%v"
		});
		$(this).siblings('.price').text(amount);
	});
}
</script>
    <style>
	.close {
    color: #000;
    float: right;
    font-size: 21px;
    font-weight: 700;
    line-height: 1;
    opacity: 0.2;
    text-shadow: 0 1px 0 #fff;
}
.close:focus, .close:hover {
    color: #000;
    cursor: pointer;
    opacity: 0.5;
    text-decoration: none;
}
button.close {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: 0 none;
    cursor: pointer;
    padding: 0;
}
.modal-open {
    overflow: hidden;
}
.modal {
    bottom: 0;
    display: none;
    left: 0;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    right: 0;
    top: 0;
    z-index: 1050;
}
.modal.fade .modal-dialog {
    transform: translate(0px, -25%);
    transition: transform 0.3s ease-out 0s;
}
.modal.in .modal-dialog {
    transform: translate(0px, 0px);
}
.modal-open .modal {
    overflow-x: hidden;
    overflow-y: auto;
}
.modal-dialog {
    margin: 10px;
    position: relative;
    width: auto;
}
.modal-content {
    background-clip: padding-box;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 6px;
    box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
    outline: 0 none;
    position: relative;
}
.modal-backdrop {
    background-color: #000;
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    z-index: 1040;
}
.modal-backdrop.fade {
    opacity: 0;
}
.modal-backdrop.in {
    opacity: 0.5;
}
.modal-header {
    border-bottom: 1px solid #e5e5e5;
    padding: 15px;
}
.modal-header .close {
    margin-top: -2px;
}
.modal-title {
    line-height: 1.42857;
    margin: 0;
}
.modal-body {
    padding: 15px;
    position: relative;
}
.modal-footer {
    border-top: 1px solid #e5e5e5;
    padding: 15px;
    text-align: right;
}
.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 5px;
}
.modal-footer .btn-group .btn + .btn {
    margin-left: -1px;
}
.modal-footer .btn-block + .btn-block {
    margin-left: 0;
}
.modal-scrollbar-measure {
    height: 50px;
    overflow: scroll;
    position: absolute;
    top: -9999px;
    width: 50px;
}
@media (min-width: 768px) {
.modal-dialog {
    margin: 30px auto;
    width: 600px;
}
.modal-content {
    box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);
}
.modal-sm {
    width: 300px;
}
}
@media (min-width: 992px) {
.modal-lg {
    width: 900px;
}
}
.box-link{
	cursor:pointer;
}
</style>
</head>
<body>
	<!-- main container of all the page elements -->
	<div id="wrapper">
		<!-- inner wrapper -->
		<div class="w1">
			<!-- topbar -->
			<div class="topbar clearfix container-fluid add">
				<a href="#" class="topbar-opener"><i class="glyphicon glyphicon-remove"></i></a>
				<!-- user-info -->
				<div class="user-info pull-right">
					<!-- nav-signup -->
<?php
/*
                    @if(Session::has('id_buyer'))
                    <nav class="nav-signup pull-left">
						<ul class="list-inline">
							<li><a href="/myaccount">My Account <i class="icon-user"></i></a></li>
						</ul>
					</nav>
                    @else
					<nav class="nav-signup pull-left">
						<ul class="list-inline">
							<li><a href="#" data-toggle="modal" data-target="#regModal">Signup</a></li>
							<li><a href="#" data-toggle="modal" data-target="#myModal">Login</a></li>
						</ul>
					</nav>
                    @endif
*/
?>
					<ul class="info-links list-inline pull-left">
<?php
/*
						<li class="" >
							<a href="#"><i class="icon-chat"></i></a>
							<span class="items-counter chat">4</span>
						</li>
						<li class="" >
							<a href="#"><i class="icon-cart"></i></a>
							<span class="items-counter">2</span>
						</li>
						<li><a href="#"><img src="/images/flag.jpg" alt="country description"></a></li>
*/
?>
						<li><a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $currency }} {{ $currency_tag }}</a>
<?php /*
                        <ul class="dropdown-menu" role="menu" >
                          <li role="presentation"><a href="/setlocale/en_US" >$ USD</a></li>
                          <li role="presentation"><a href="/setlocale/nl_NL" >€ EUR</a></li>
                          <li role="presentation"><a href="/setlocale/en_GB" >£ GBP</a></li>
                          <li role="presentation"><a href="/setlocale/zh_CN" >¥ CNY</a></li>
                        </ul>
*/ ?>
                        <ul class="dropdown-menu" role="menu" >
                          <li role="presentation"><a href="/setlocale/en_US" >English</a></li>
                          <li role="presentation"><a href="/setlocale/de_DE" >German</a></li>
                          <li role="presentation"><a href="/setlocale/es_ES" >Espanol</a></li>
                          <li role="presentation"><a href="/setlocale/fr_FR" >French</a></li>
                          <li role="presentation"><a href="/setlocale/it_IT" >Italian</a></li>
                          <li role="presentation"><a href="/setlocale/zh_CN" >Chinese</a></li>
                          <li role="presentation"><a href="/setlocale/ru_RU" >Russian</a></li>
                        </ul>
                        </li>
					</ul>
				</div>
				<!-- social-networks -->
				<ul class="social-networks list-inline pull-left">
					<li><a href="#"><i class="icon-facebook"></i></a></li>
					<li><a href="#"><i class="icon-twitter"></i></a></li>
					<li><a href="#"><i class="icon-linkedin"></i></a></li>
					<li><a href="#"><i class="icon-google"></i></a></li>
					<li><a href="#"><i class="icon-youtube"></i></a></li>
				</ul>
<?php //				<a href="tel:18001231234" class="helpline pull-left"><i class="icon-phone"></i> +1 800 123 1234</a> ?>
			</div>
			<!-- header of the page -->
			<header id="header">
				<div class="container-fluid">
					<div class="header-holder">
						<!-- topbar-opener -->
						<a href="#" class="topbar-opener"><i class="icon-list"></i></a>
						<!-- navbar-toggle -->
						<button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- page logo -->
						<strong class="logo pull-left"><a href="/"><img src="/images/logo.png" alt="Brandeden"></a></strong>
					</div>
					<nav class="navbar navbar-default">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<i class="glyphicon glyphicon-remove"></i>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active"><a href="#">{{ trans('messages.HOME') }}</a></li>
								<li><a href="#">{{ trans('messages.BRANDING BASICS') }}</a></li>
								<li><a href="#">{{ trans('messages.BUYING PROCESS') }}</a></li>
								<li><a href="#">{{ trans('messages.INDUSTRY SALES') }}</a></li>
								<li><a href="#">{{ trans('messages.BLOG') }}</a></li>
							</ul>
						</div>
					</nav>
				</div>
			</header>
			<!-- page-header -->
			<div class="page-header text-center">
				<div class="container">
					<h1>{{ trans('messages.BETTER BRANDS, BETTER BRANDING, BETTER BUSINESS') }}</h1>
					<p>{{ trans('messages.Brandable domain names give you instant credibility') }}</p>
				</div>
			</div>
			
				@yield('content')
			
		</div>
		<!-- footer of the page -->
		<div class="bottom-container">
			<!-- footer-aside -->
			<aside class="footer-aside">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12 col-lg-4">
							<strong class="logo"><a href="#"><img src="/images/logo2.png" alt="Brandeden"></a></strong>
							<div class="text-holder">
								<p>Sed ornare cras donec litora integer curabit ur orci at nullam aliquam libero nam himen aeos amet massa amet. ...<a href="#">More</a></p>
							</div>
							<!-- social-area -->
							<div class="social-area">
								<h3>{{ trans('messages.Follow Us') }}</h3>
								<!-- social-networks -->
								<ul class="social-networks list-inline">
									<li><a href="#"><i class="icon-facebook"></i></a></li>
									<li><a href="#"><i class="icon-twitter"></i></a></li>
									<li><a href="#"><i class="icon-linkedin"></i></a></li>
									<li><a href="#"><i class="icon-google"></i></a></li>
									<li><a href="#"><i class="icon-youtube"></i></a></li>
								</ul>
							</div>
						</div>
						<nav class="col-xs-12 col-sm-6 col-lg-2">
							<h3>{{ trans('messages.Product & Services') }}</h3>
							<ul class="list-unstyled">
								<li><a href="/brandable">{{ trans('messages.Brandable Domains') }}</a></li>
								<li><a href="/premium">{{ trans('messages.Premium Domains') }}</a></li>
								<li><a href="/under1k">{{ trans('messages.Under $1,000 Domains') }}</a></li>
								<li><a href="#">{{ trans('messages.Websites') }}</a></li>
							</ul>
						</nav>
						<nav class="col-xs-12 col-sm-6 col-lg-2">
							<h3>{{ trans('messages.Resources') }}</h3>
							<ul class="list-unstyled">
								<li><a href="#">{{ trans('messages.Branding Basics') }}</a></li>
								<li><a href="#">{{ trans('messages.Buying Process') }}</a></li>
								<li><a href="#">{{ trans('messages.Industry Sales') }}</a></li>
								<li><a href="#">{{ trans('messages.Blog') }}</a></li>
							</ul>
						</nav>
						<nav class="col-xs-12 col-sm-6 col-lg-2">
							<h3>{{ trans('messages.Company Info') }} </h3>
							<ul class="list-unstyled">
								<li><a href="#">{{ trans('messages.About Us') }}</a></li>
								<li><a href="#">{{ trans('messages.Privacy Policy') }}</a></li>
								<li><a href="#">{{ trans('messages.Terms of Service') }}</a></li>
								<li><a href="#">{{ trans('messages.TradeMark Issues') }}</a></li>
							</ul>
						</nav>
						<div class="col-xs-12 col-sm-6 col-lg-2">
							<h3>{{ trans('messages.Contact Us') }} </h3>
							<!-- social-links -->
							<ul class="social-links list-unstyled">
								<li><a href="#"><i class="icon-question"></i>{{ trans('messages.FAQ') }}</a></li>
								<li><a href="#"><i class="icon-chat_bubble_outline"></i>{{ trans('messages.Live chat') }}</a></li>
								<li><a href="mailto:sales@brandeden.com"><i class="icon-envelope2"></i>sales@brandeden.com</a></li>
								<li><a href="tel:18001231234"><i class="icon-phone"></i> +1 800 123 1234</a></li>
							</ul>
						</div>
					</div>
				</div>
			</aside>
			<!-- footer of the page -->
			<footer id="footer">
				<div class="container-fluid text-center">
					<p>&copy; 2015 <a href="#">BrandEden</a>. {{ trans('messages.All Rights Reserved') }}.</p>
				</div>
			</footer>
		</div>
	</div>
    <!--Registration Modal-->
    <div class="modal fade" id="regModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('messages.Sign Up') }}</h4>
        </div>
        <div class="modal-body">
          <form id="regForm" method="POST" action="/signup" novalidate>
              <div class="form-group">
              <label for="first_name" class="control-label">{{ trans('messages.First name') }}</label>
                  <input type="text" class="form-control" id="first_name" name="first_name" value="" required="" title="{{ trans('messages.Please enter your first name') }}" placeholder="John">
                  <span class="help-block"></span>
              </div>
              <div class="form-group">
              <label for="password" class="control-label">{{ trans('messages.Last name') }}</label>
                  <input type="text" class="form-control" id="last_name" name="last_name" value="" required="" title="{{ trans('messages.Please enter your lsast name') }}" placeholder="Doe">
                  <span class="help-block"></span>
              </div>
              <div class="form-group">
              <label for="email" class="control-label">{{ trans('messages.Email') }}</label>
                  <input type="email" class="form-control" id="email" name="email" value="" required="" title="{{ trans('messages.Please enter your email') }}" placeholder="example@gmail.com">
                  <span class="help-block"></span>
              </div>
              <div class="form-group">
                  <label for="password" class="control-label">{{ trans('messages.Password') }}</label>
                  <input type="password" class="form-control" id="password" name="password" value="" required="" title="{{ trans('messages.Please enter your password') }}">
                  <span class="help-block"></span>
              </div>
              <div id="loginErrorMsg" class="alert alert-error hide">{{ trans('messages.Wrong username or password') }}</div>
              
              <button type="submit" class="btn btn-default">{{ trans('messages.Sign Up') }}</button>
              
              <input type="hidden" name="action" value="signup" />
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.Close') }}</button>
        </div>
      </div>
      
    </div>
  </div>
    <!--Login Modal-->
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('messages.Login') }}</h4>
        </div>
        <div class="modal-body">
          <form id="loginForm" method="POST" action="/login/" novalidate>
                              <div class="form-group">
                                  <input type="email" class="form-control" id="email" name="email" value="" required="" title="{{ trans('messages.Please enter your email') }}" placeholder="example@gmail.com">
                                  <span class="help-block"></span>
                              </div>
                              <div class="form-group">
                                  <label for="password" class="control-label">{{ trans('messages.Password') }}</label>
                                  <input type="password" class="form-control" id="password" name="password" value="" required="" title="{{ trans('messages.Please enter your password') }}">
                                  <span class="help-block"></span>
                              </div>
                              <div id="loginErrorMsg" class="alert alert-error hide">{{ trans('messages.Wrong username og password') }}</div>
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember" id="remember"> {{ trans('messages.Remember login') }}
                                  </label>
                                  <p class="help-block">({{ trans('messages.if this is a private computer') }})</p>
                              </div>
                              <button type="submit" class="btn btn-default">{{ trans('messages.Login') }}</button>
                              <a href="#" class="btn btn-default janrainEngage"  data-dismiss="modal">{{ trans('messages.Social Login') }}</a>
                              <input type="hidden" name="action" value="login" />
                          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.Close') }}</button>
        </div>
      </div>
    </div>
  </div>

<!--Login Modal-->
    <div class="modal fade" id="buyNowModal" role="dialog">
    <div class="modal-dialog">
    
		<div class="modal-content">
          <form id="enquiryForm" method="POST" action="/enquire">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">{{ trans('messages.Buy now') }}</h4>
			  </div>
			  <div class="modal-body">
				<form>
				  <div class="form-group">
				    <label for="enq-email" class="control-label">{{ trans('messages.Your Email') }}</label>
				    <input type="text" class="form-control" id="enq-email" name="enq-email" placeholder="Xyz@internet.com" >
				  </div>
				  <div class="form-group">
				    <label for="enq-message" class="control-label">{{ trans('messages.Message') }}</label>
				    <textarea class="form-control" id="enq-message" name="enq-message" ></textarea>
				  </div>
				  <div class="form-group">
				    <label for="enq-domainName" class="control-label">{{ trans('messages.Selected domain') }}</label>
				    <input type="text" class="form-control" id="enq-domainName" name="enq-domainName" placeholder="Brandeden.com" readonly >
				  </div>
				  <div class="form-group">
				    <label for="enq-domainOffer" class="control-label">{{ trans('messages.Your offer price (in USD)') }}</label>
				    <input type="text" class="form-control" id="enq-domainOffer" name="enq-domainOffer" placeholder="{{ trans('messages.Offer price (in USD)') }}" >
				  </div>

				  <input type="hidden" value="" id="enq-domain-id" name="enq-domain-id" >
				</form>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.Close') }}</button>
				<button type="button" class="btn btn-primary" id="send-enquiry" data-loading-text="{{ trans('messages.Sending') }}..." autocomplete="off" >{{ trans('messages.Send enquiry') }}</button>
			  </div>
			</form>
		</div>
	</div>
	</div>
</body>
</html>
