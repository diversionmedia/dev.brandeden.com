<?php

namespace App\Http\Controllers;

use Mail;
use Request;
use Session;
//use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;

use App\Models\APICall;

class AccountController extends Controller
{

	/*
	public function __construct(){	
		$this->logged_in_buyer = session('id_buyer');
	}
	*/
	
    public function index()	{
		if( !$this->is_user_login() )	{
			return redirect('/');
		}
		else	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
			
				if( $id_buyer < 1 )	{
					return redirect('/');
				}
				else	{
					
					$data['premiumprice'] = 1000;
					$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
					if( count($usersetting) > 0 )	{
						$data['premiumprice'] = $usersetting->setting->premium_price;
					}
					
					$inquiry = APICall::api_call('/inquiries/getlatestmsgfromseller/'.$id_buyer);
					if( count($inquiry) > 0 )	{
						$seller = APICall::api_call('users/'.$inquiry->friendly);
						$data['inq_message'] = $inquiry->message;
						$data['inq_domain'] = $inquiry->domain;
						$data['inq_offer'] = $inquiry->offer;
						$data['inq_time'] = date("F d Y g:i a", strtotime($inquiry->received_on));
						//$data['inq_fullname'] = $seller->info->first_name.' '.$seller->info->last_name;
						$data['inq_fullname'] = '';
					}
					else	{
						$data['inq_message'] = '';
						$data['inq_domain'] = '';
						$data['inq_offer'] = '';
						$data['inq_offer'] = '';
						$data['inq_time'] = '';
						$data['inq_fullname'] = '';
					}
					$user = APICall::api_call('users/'.$friendly);
					
					$data['currency'] = $this->c_symbol;
					$data['currency_tag'] = $this->c_tag;

					$data['buyer_id'] = $user->id;
					$data['first_name'] = $user->info->first_name;
					$data['last_name'] = $user->info->last_name;
					$data['email'] = $user->info->email;
					$data['usertype'] = $user->info->type;
					$data['friendly'] = $friendly;
					
					return view('myaccount')->with($data);
				}
			}
			else	{
				return '<h1>Coming soon. For now this is only for Buyer View</h1>';
			}
		}
	}
	
	public function logout()
	{
		session_start();
		session_destroy();
		return redirect('/');
	}
	
	public function redirectmain()
	{
		return redirect('/');
	}

	public function mainlogin($message=null,$status=null)
	{
		$data['status'] = $status;
		$data['message'] = $message;
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		return view('login')->with($data);
	}
	
	public function mainsignup($message=null)
	{
		$data['message'] = $message;
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		return view('signup')->with($data);
	}
	
	public function industry_sales($message=null)
	{
		session_start();
		$data['message'] = $message;
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		$data['premiumprice'] = 1000;
		return view('industrysales')->with($data);
	}
	
	public function get_industry_sales($topdata)
	{
		session_start();
		if($topdata == 'top20')
			return view('industrysalestop20');
		if($topdata == 'top100')
			return view('industrysalestop100');
		if($topdata == 'top500')
			return view('industrysalestop500');
	}	
	
	public function login()
	{
		$input = Request::all();
		
		if(isset($input['token'])){
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, config('app.JANRAIN_AUTH_URL'));
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, config('app.JANRAIN_SSL_VERIFYPEER'));
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, array('token' =>  $input['token'], 'apiKey' => config('app.JANRAIN_API_KEY')));
			curl_setopt($curl, CURLOPT_FAILONERROR, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$profileString = curl_exec($curl);
			curl_close($curl);
			$profile = json_decode($profileString);
			
			$_data = array('profile' => $profile, 'user_type'=> 'B');
		  	
		   	$res = APICall::api_call('users/socialSignin', $_data, 'POST');
			if($res->result_code == 0)	{
				// session(['id_buyer' => $res->id_user]);	// session laravel doesn't work properly
				
				// temporary code
				session_start();
				$_SESSION['id_buyer'] = $res->id_user;
				$_SESSION['friendly'] = $res->friendly;
				$_SESSION['email'] = $res->email;
				$_SESSION['user_type'] = $res->user_type;

				return redirect('myaccount');
			}
		}
		elseif(isset($input['action']) && $input['action'] == 'login')	{
			$input['frontend'] = 1;
			$user_login = APICall::api_call('users/login', $input, 'POST');
			if($user_login->error == 200){
				// session(['id_buyer' => $user_login->id_user]); // session laravel doesn't work properly

				// temporary code
				session_start();
				$_SESSION['id_buyer'] = $user_login->id_user;
				$_SESSION['friendly'] = $user_login->friendly;
				$_SESSION['email'] = $user_login->email;
				$_SESSION['user_type'] = $user_login->user_type;
				/*
				echo '<pre>';
				print_r($_SESSION);
				echo '</pre>';				
				*/
				//return redirect('myaccount');
				
				$json['status'] = 'success';
				$json['message'] = $user_login->errormsg.' Please wait will redirect you the account page.';
				return json_encode($json);
			}
			else	{
				$json['status'] = 'danger';
				$json['message'] = $user_login->errormsg;
				return json_encode($json);
				//return $this->mainlogin($user_login->errormsg,'danger');
			}
		}
		else
		{
			//return redirect('/');
			$json['status'] = 'danger';
			$json['message'] = 'Unknown Login Method';
			return json_encode($json);
		}
	}
	
	public function signup($processCaptcha = true)
	{
		$input = Request::all();
		$json = array();
		
		if(!trim($input['g-recaptcha-response']) && $processCaptcha)	{	
    	    $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfr5xkTAAAAAGjugdzxM1McR1TGWzJWiP9A1GLY&response=".$input['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
	        if($response['success'] == false)	{	
				$message = "Google captcha response is invalid.";
				$json['status'] = 'danger';
				$json['message'] = $message;
				return json_encode($json);
			}
		}
		
		$_data = array('first_name' => $input['reg_first_name'], 'last_name' => $input['reg_last_name'], 'email' => $input['reg_email'], 'password' => $input['reg_password'], 'identifier' => '', 'type'=> 3);
		$res = APICall::api_call('users', $_data, 'POST');
				
		if($res->error == 0)	{
			
			$data_user_info = (array) $res;
			$data_user_info =  $input;
			$data_user_info['url'] = url('/');
			
			$data_user_info['id_user'] = $res->id_user;
			$data_user_info['friendly'] = $res->friendly;
			$data_user_info['user_type'] = $res->user_type;

			$api_res = APICall::api_call('users/send_email_confirmation', $data_user_info, 'POST');
			//$api_res = APICall::api_call('users/send_email_confirmation_general', $data_user_info, 'POST');
			
			//$this->send_email_confirmation($res,$input);
			$json['status'] = 'success';
			$json['message'] = 'Please check your mail to verify your email address.';
			//$json['message'] = $res->errormsg;
			//session(['id_buyer' => $res->id_user]);
			//return redirect('myaccount');
		}
		else	{
			$json['status'] = 'danger';
			$json['message'] = $res->errormsg;
		}
		
		return json_encode($json);
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			June 20, 2016
		@description: 	send email confirmation
		
		// this function has been moved to API - 10-11-16
	**/
	public function send_email_confirmation($res=null,$input=null)
	{
		$key = $res->friendly.'*_***_*'.$res->id_user.'*_***_*'.$res->user_type.'*_***_*'.$input['reg_email'];
		$key = base64_encode($key);
		
		$to  = $input['reg_email'];
		$subject = 'Brandeden Registration - Email Confirmation';

		$message = '
		<html>
		<head>
		  <title>Brandeden Registration - Email Confirmation</title>
		</head>
		<body>
		  <h2>Please verify your email address</h2>
		  <p>
			Hi '.$input['reg_first_name'].' '.$input['reg_last_name'].',
			<br><br><br><br>
			Please verify your email address so we know that it\'s really you! 
			<br><br>
			Just click the link below to verify your email address.
			<br><br><br>
			<a href="'. url('/').'/process_email?key='.$key.'">'. url('/').'/process_email?key='.$key.'</a>
			<br><br><br><br>
			Cheers,<br>
			Brandeden
		  </p>
		</body>
		</html>
		';
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		mail($to, $subject, $message, $headers);
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			June 20, 2016
		@description: 	check key and process  email confirmation
	**/
	public function process_email()	{
		if( isset($_REQUEST['key']) )	{
			$key = $_REQUEST['key'];
			$key = base64_decode($key);
			$vars = explode('*_***_*',$key);
			
			$def_err_msg = 'Can\'t confirm your email. Invalid key.';

			if( count($vars) < 4 )	{
				return $this->mainlogin($def_err_msg,'danger');
			}

			$user_info = APICall::api_call('users/'.$vars[0]);
			
			if( $vars[0] == $user_info->friendly && $vars[1] == $user_info->id && $vars[3] == $user_info->info->email && $user_info->info->type == 3 )	{
				$_data = array('emailconfirm' => '1', 'id_user'=> $user_info->id);
				
				$result = APICall::api_call('users/emailconfirm', $_data, 'POST');
				return $this->mainlogin($result->response_details,'success');
			}
			else
				return $this->mainlogin($def_err_msg,'danger');
		}
		else
			return redirect('/');
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			July 19, 2017
		@description: 	check key and process email confirmation with auto creation of escrow transaction
	**/
	public function process_email_escrow()	{
		if( isset($_REQUEST['key']) )	{
			$key = $_REQUEST['key'];
			$key = base64_decode($key);
			$vars = explode('*_***_*',$key);
			
			$def_err_msg = 'Can\'t confirm your email. Invalid key.';

			if( count($vars) < 5 )	{
				return $this->mainlogin($def_err_msg,'danger');
			}

			$user_info = APICall::api_call('users/'.$vars[0]);
			if( $vars[0] == $user_info->friendly && $vars[1] == $user_info->id && $vars[3] == $user_info->info->email && $user_info->info->type == 3 )	{
				$_data = array('emailconfirm' => '1', 'id_user'=> $user_info->id, 'escrow_inq_id'=> $vars[4], 'url'=> url('/'));
				
				$result = APICall::api_call('users/emailconfirm_escrow', $_data, 'POST');
				return $this->mainlogin( $result->response_details, strtolower($result->response_bg) );
				//return $this->mainlogin($result->response_details,'success');
			}
			else
				return $this->mainlogin($def_err_msg,'danger');
		}
		else
			return redirect('/');
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			July 29, 2017
		@description: 	check key and process email confirmation with auto creation of escrow transaction
	**/
	public function process_email_makeoffer()	{
		if( isset($_REQUEST['key']) )	{
			$key = $_REQUEST['key'];
			$key = base64_decode($key);
			$vars = explode('*_***_*',$key);
			
			$def_err_msg = 'Can\'t confirm your email. Invalid key.';

			if( count($vars) < 5 )	{
				return $this->mainlogin($def_err_msg,'danger');
			}

			$user_info = APICall::api_call('users/'.$vars[0]);
			if( $vars[0] == $user_info->friendly && $vars[1] == $user_info->id && $vars[3] == $user_info->info->email && $user_info->info->type == 3 )	{
				$_data = array('emailconfirm' => '1', 'id_user'=> $user_info->id, 'offer_inq_id'=> $vars[4], 'url'=> url('/'));
				
				$result = APICall::api_call('users/emailconfirm_makeoffer', $_data, 'POST');
				return $this->mainlogin( $result->response_details, strtolower($result->response_bg) );
				//return $this->mainlogin($result->response_details,'success');
			}
			else
				return $this->mainlogin($def_err_msg,'danger');
		}
		else
			return redirect('/');
	}
	
	
	/**
		@auhtor: 		James Bolongan
		@date: 			August 22, 2016
		@description: 	send email confirmation for forgot password
		
		// this function has been moved to API - 10-10-16
	**/
	public function send_email_confirmation_forgot_password($res)
	{
		$key = $res->friendly.'*_***_*'.$res->id_user.'*_***_*'.$res->user_type.'*_***_*'.$res->email;
		$key = base64_encode($key);
		
		$to  = $res->email;
		$subject = 'Brandeden Forgot Password';

		$message = '
		<html>
		<head>
		  <title>Brandeden Forgot Password</title>
		</head>
		<body>
		  <p>
			Hi '.$res->first_name.' '.$res->last_name.',
			<br><br><br><br>
			If you want to reset your password just click the link below so we know that it\'s really you!
			<br><br><br>
			<a href="'. url('/').'/process_forgot_password?key='.$key.'">'. url('/').'/process_forgot_password?key='.$key.'</a>
			<br><br><br><br>
			Cheers,<br>
			Brandeden
		  </p>
		</body>
		</html>
		';
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$result = mail($to, $subject, $message, $headers);
		return $result;
	}
	
	/**
		@auhtor: 		James Bolongan
		@date: 			August 22, 2016
		@description: 	check key and process email forgotpassword
	**/
	public function process_forgot_password()	{
		if( isset($_REQUEST['key']) )	{
			$key = $_REQUEST['key'];
			$key = base64_decode($key);
			$vars = explode('*_***_*',$key);
			
			$def_err_msg = 'Can\'t reset your password. Invalid key.';

			if( count($vars) < 4 )	{
				return $this->mainlogin($def_err_msg,'danger');
			}
			
			$user_info = APICall::api_call('users/'.$vars[0]);

			if( $vars[0] == $user_info->friendly && $vars[1] == $user_info->id && $vars[3] == $user_info->info->email && $user_info->info->type == 3 )	{
				$randomPassword = $this->randomPassword();
				$_data = array('id_user' => $user_info->id, 'password'=> $randomPassword);
				$api_res = APICall::api_call('users/changepassword', $_data, 'POST');

				if( $api_res->result == 'SUCCESS' )	{
					
					$data_user_info = (array) $user_info;
					$data_user_info['url'] = url('/');
					$data_user_info['randomPassword'] = $randomPassword;
					$api_res = APICall::api_call('users/send_email_new_password', $data_user_info, 'POST');
					 
					/*
					
					 // this email code has been moved to API - 10-10-16
					
					$to  = $user_info->info->email;
					$subject = 'Brandeden Your New Password';

					$message = '
					<html>
					<head>
					  <title>Brandeden Your New Password</title>
					</head>
					<body>
					  <p>
						Hi '.$user_info->info->first_name.' '.$user_info->info->last_name.',
						<br><br><br><br>
						Here is your new password
						<br><br><br>
						<b>  '.$randomPassword.' </b> <br><br>
						'.url('/').'<br>
						email/username: '.$user_info->info->email.' <br>
						password: '.$randomPassword.' <br>
						<br><br><br><br>
						Cheers,<br>
						Brandeden
					  </p>
					</body>
					</html>
					';
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					mail($to, $subject, $message, $headers);
					*/
					
					//return $this->mainlogin($api_res->response_details,'success');
					//return $this->mainlogin($api_res->response_details.'. New password has been sent to your email.','success');
					return $this->mainlogin('New password has been sent to your email.','success');
				}
				else	{
					return $this->mainlogin($api_res->response_details,'danger');
				}
			}
			else
				return $this->mainlogin($def_err_msg,'danger');
		}
		else
			return redirect('/');
	}
	
	function randomPassword() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^&*()-=+";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 16; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}

	public function save_new_password()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
			
				//$api = APICall::api_call('users/account', $_POST, 'PUT');	// not working
				$api = APICall::api_call('users/changepassword', $_POST, 'POST');
				echo json_encode($api);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function forgot_password()	{
		$_POST['email'] = $_POST['email_fp'];
		$api = APICall::api_call('users/get_user_by_email', $_POST, 'POST');
		if( count($api->user) > 0 )	{
			
			//$this->send_email_confirmation_forgot_password($api->user);
			
			$api_user =  (array) $api->user;
			$api_user['url'] = url('/');
			$send_email = APICall::api_call('users/send_email_confirmation_forgot_password', $api_user, 'POST');

			//if( $this->send_email_confirmation_forgot_password($api->user) )	{
			if( $send_email->result == 'SUCCESS' )	{
				$api->result = 'SUCCESS';
				$api->response_text = 'Info';
				$api->response_details = 'Please check your email for instructions on resetting your password.';
				echo json_encode($api);
			}
			else	{
				$api->result = 'ERROR';
				$api->response_text = 'MAIL ERROR';
				$api->response_details = 'Something went wrong when sending the confirmation email.';
				echo json_encode($api);
			}
		}
		else	{
			echo json_encode($api);
		}
	}
	
	
	public function save_account_settings()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				
				$_POST['id_user'] = $id_buyer;
				if( $_POST['birthdate'] != '' )	{
					$birthdate = explode('/',$_POST['birthdate']);
					$_POST['birthdate'] = $birthdate[2].'-'.$birthdate[1].'-'.$birthdate[0];
				}
				$result = APICall::api_call('userprofile/save', $_POST, 'POST');
				echo json_encode($result);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function get_my_profile($buyer_id)	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];

				$result = APICall::api_call('userprofile/get/'.$buyer_id);
				echo json_encode($result);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function upload_avatar_test()	{
	}
	
	public function upload_avatar()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];

				// Only accept files with these extensions
				$whitelist = array('jpg', 'jpeg', 'png', 'gif');
				$name      = null;
				$error     = 'No file uploaded.';
				
				if (isset($_FILES)) {
					if (isset($_FILES['file'])) {
						$tmp_name = $_FILES['file']['tmp_name'];
						$name     = basename($_FILES['file']['name']);
						$error    = $_FILES['file']['error'];
						
						if ($error === UPLOAD_ERR_OK) {
							$extension = pathinfo($name, PATHINFO_EXTENSION);

							if (!in_array($extension, $whitelist)) {
								$error = 'Invalid file type uploaded.';
							} 
							else {
								
								$args['file']['id_user'] = $id_buyer;
								
								$link = '/users/upload_avatar';
								if (strpos($link, '?') !== false) {	$seperator = '&';	}
								else {	$seperator = '?';	}
								$curl_url = config('app.API_URL') . $link . $seperator . 'api_key=' . config('app.API_KEY');
								$curl = curl_init();
								curl_setopt($curl, CURLOPT_URL,$curl_url);
								curl_setopt($curl, CURLOPT_POST,1);
								$args['file'] = new \CurlFile($_FILES['file']['tmp_name'], $_FILES['file']['type'], $id_buyer.'.'.$extension);
								curl_setopt($curl, CURLOPT_POSTFIELDS, $args);
								$curl_response = curl_exec($curl);
								
								if ( $curl_response === false || $curl_response == '' ) {
									$info = curl_getinfo($curl);
									curl_close($curl);
									//die('error occured during curl exec. Additioanl info: ' . var_export($info));
									$error =  'Error occured during curl exec.';
								}
								else	{
									curl_close($curl);
									$error =  'Avatar successfully uploaded.';
								}
								//echo json_encode($result);
								//$result = APICall::api_call('/users/upload_avatar', $_FILES, 'POST');
								//echo json_encode($result);
								
								
								//die();
							}
						}
						else	{	$error = 'Something went error';	}
					}
					else	{	$error = 'No file being uploaded'; }
				}
				else	{	$error = 'No file being uploaded'; }
		
				echo json_encode(array(
					'name'  => $name,
					'error' => $error,
				));
				die();
		
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
		
	}
	
}
