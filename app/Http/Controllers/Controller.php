<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Session;
use App;
use Response;
use Request;
use Redirect;
use Input;
use Cookie;
use Config;
use DB;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

        protected $loc_errors = array();
	protected $loc_service = 'api.ipinfodb.com';
	protected $loc_version = 'v3';
	protected $loc_apiKey = '5670542334da0901f392728b2c8e60388440e5924d8accffbf584c33a7d315c6';

	public function __construct(){

		/*
		*
		*	Set locale values
		*/

		/* Set language */
		$locale = Cookie::get('locale');
		if(!$locale)
			$locale = App::getLocale();
		App::setLocale($locale);
		setlocale(LC_ALL, $locale.'.UTF8');
		$this->c_language = $this->getLanguageLable($locale);

		/* Set currency as selected or by locale selected */
		$currencylocale = Cookie::get('currency');
		if(!$currencylocale)
			$currencylocale = Config::get('app.locale');
		setlocale(LC_MONETARY, $currencylocale.'.UTF8');

		$localeconv = localeconv();
		$currency_tag = $localeconv['int_curr_symbol'];
		$currecny_symbol = $localeconv['currency_symbol'];
		if(!trim($currecny_symbol) || !trim($currency_tag))
			$currencylocale = Config::get('app.locale');
		setlocale(LC_MONETARY, $currencylocale.'.UTF8');
		$this->c_locale = $currencylocale;

		/*
		//Set currrency values for controllers
		$conv_json = DB::select('SELECT * FROM `currency_conversion` WHERE `id` = (SELECT MAX(`id`) FROM `currency_conversion`)');
		if(!$conv_json)
			$conv_json = $this->refreshCurrencyApi();
		else{
			$conv_json = $conv_json[0];
			$diff_days = intval((abs(strtotime($conv_json->createdate) - time()))/86400);
			if($diff_days > 1){
				$conv_json = $this->refreshCurrencyApi();
				$conv_json = $conv_json[0];
			}
		}
		$currency = json_decode($conv_json->json, true);
		*/
		$currency['rates']['USD'] = 1;
		$localeconv = localeconv();

		$this->c_tag = trim($localeconv['int_curr_symbol']);
		$this->c_symbol = trim($localeconv['currency_symbol']);
		$this->c_multiplier = $currency['rates'][$this->c_tag];
		
		$this->visitor_stats = $this->getIpResult($this->getUserIP());
		$this->visitor_stats['browser'] = @$_SERVER['HTTP_USER_AGENT'];
		
		$timezone = '+05:30';
		$sign = substr($timezone, 0, 1) == '+'? '+': (substr($timezone, 0, 1) =='-'? '-': null);
		$hour = (int) substr($timezone, 1, 2);
		$minutes = (int) substr($timezone, -2) + ($hour * 60);
                $localtime = gmdate("Y-m-d H:i:s", time());
                if($sign){
                        //means no GMT location (Its London my friend)
                        $formatter = $sign.$minutes.' minutes';
                        $localtime = date("Y-m-d H:i:s", strtotime($formatter));
                }
                $this->visitor_stats['localTime'] = $localtime;
	}

	public function refreshCurrencyApi(){

		$curl = curl_init('http://api.fixer.io/latest?base=USD');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, false);
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . print_r($info));
		}
		curl_close($curl);

		$api_res = array();
		$api_res['createdate'] = date('Y-m-d H:i:s', time());
		$api_res['json'] = $curl_response;
		$api_res['uri'] = 'http://api.fixer.io/latest?base=USD&symbols=USD,GBP,EUR,CNY';
		DB::table('currency_conversion')->insert($api_res);

		return DB::select('SELECT * FROM `currency_conversion` WHERE `id` = (SELECT MAX(`id`) FROM `currency_conversion`)');
	}

	public function de_format_currency($price = 0){

		$price = floatval($price) / $this->c_multiplier;
		$price = number_format($price, 2, '.', '');
		return $price;
	}

	public function format_currency($price = 0, $formatted = true){

		$price = $this->c_multiplier * floatval($price);

		if($price != round($price))
			$price = number_format($price, 2, '.', '');

		if($formatted == true)
			if($price != round($price))
					$price = money_format('%.2n', $price);
			else  	$price = money_format('%.0n', $price);

		return $price;
		return utf8_encode($price);
	}
	
	public function getLanguageLable($locale){

		$lang = 'English';

		switch($locale){
			default: 
			case 'en_US': $lang = 'English';
			case 'en_GB': $lang = 'English'; break;
			case 'de_DE': $lang = 'German'; break;
			case 'es_ES': $lang = 'Espanaol'; break;
			case 'fr_FR': $lang = 'French'; break;
			case 'it_IT': $lang = 'Italian'; break;
			case 'zh_CH': $lang = 'Chinese'; break;
			case 'ru_RU': $lang = 'Russian'; break;
		}
		return $lang;
	}
	
        public function getUserIP(){
        
            $client  = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote  = $_SERVER['REMOTE_ADDR'];

            if(filter_var($client, FILTER_VALIDATE_IP))
                $ip = $client;
            elseif(filter_var($forward, FILTER_VALIDATE_IP))
                $ip = $forward;
            else
                $ip = $remote;

            //temporary setting
            $ip = '103.40.203.25';
                
            return $ip;
        }

	public function getIpResult($host){
        	
        	if(@$_COOKIE["BrandEden_Geo_Location"])
        	        return unserialize(base64_decode($_COOKIE["BrandEden_Geo_Location"]));

		$ip = @gethostbyname($host);
		if(preg_match('/^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/', $ip)){
			$loc_api_path = 'http://' . $this->loc_service . '/' . $this->loc_version . '/ip-city/?key=' . $this->loc_apiKey . '&ip=' . $ip . '&format=json';
			$json = @file_get_contents($loc_api_path);
			try{
				$response = json_decode($json, true);
				
				$data = base64_encode(serialize($response));
                                setcookie("BrandEden_Geo_Location", $data, time()+3600*24*7); //set cookie for 1 week
                                return $response;
			}
			catch(Exception $e){
				$this->loc_errors[] = $e->getMessage();
			}
		}
		$this->loc_errors[] = '"' . $host . '" is not a valid IP address or hostname.';
		return;
	}
	
	/**
		@auhtor: 		James Bolongan
		@datecreate: 	June 21, 2016
		@param: 		$status (int)
		@description:	return the status label (string)

	**/
	public function get_status_label($status = 1)	{
		if($status == 1)
			$str_status = 'New Inquiries';
		else if($status == 2)
			$str_status = 'Ongoing Negotiations';
		else if($status == 3)
			$str_status = 'Pending Sale';
		else if($status == 4)
			$str_status = 'Sold';
		else if($status == 5)
			$str_status = 'Archive';
		
		return $str_status;
	}

	/**
		@auhtor: 		James Bolongan
		@datecreate: 	June 21, 2016
		@description: 	check if user login through session
	**/	
	public function is_user_login()	{
		if (version_compare(phpversion(), '5.4.0', '<')) {
			 if(session_id() == '') {
				session_start();
			 }
		 }
		 else	{
			if (session_status() == PHP_SESSION_NONE) {
				session_start();
			}
		 }
		
		if( isset($_SESSION['friendly']) && $_SESSION['id_buyer'] )
			return true;
		else
			return false;
	}
}