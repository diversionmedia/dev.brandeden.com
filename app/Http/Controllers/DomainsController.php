<?php

namespace App\Http\Controllers;

use Mail;

use App;
use Request;
use Response;
use Session;
use Cookie;
use DB;
use App\Http\Controllers\Controller;
use App\Models\APICall;
use View;

class DomainsController extends Controller
{
	//public $id_user = 1;
    //public function index($tab = 'brandable')
	public function index($tab = 'brandable')
	{
		session_start();
		
		//if tab is a domain
		if(preg_match("/^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/", $tab))	{
			$id = $tab;
			$tab = '';
			$name = '';
			$domainData = APICall::api_call('domain/'.$id);
			if( !isset($domainData->domain) )	{
				return redirect('/');
				//return $this->detailserror($tab, $id, $tab, true);
			}
			else	{
				if($domainData->domain->id_logo)	{	$tab = 'brandable'; 	}
				elseif($domainData->domain->price > 1000)	{	$tab = 'premium';	}
				else	{	$tab = 'under1k';	}
				return $this->details($tab, $id, $tab, true);
			}
		}
		
		$id_user = 1;
		$input = Request::all();
		$data['category']  = 0;
		$data['search']  = '';
		$data['tld']  = '';
		$data['length']  = 0;
		$data['price']  = 0;
		$data['tab'] = $tab;
		$default_order = 'order_no';
		$default_order_by = 'asc';
		$limit = config('app.records_per_page');
		$param = '&order=order_no&order_type=asc';
		if($tab != 'brandable')
			$limit = $limit*3;
		if($tab == 'under1k')
			$tab = 'underprice';
		if(isset($input['search']) AND trim($input['search']) <> '')
		{
			$param .= '&search='.trim($input['search']);
			$data['search']  = trim($input['search']);
		}
		if(isset($input['category']) && trim($input['category']) > 0)
		{
			$param .= '&category='.trim($input['category']);
			$data['category']  = trim($input['category']);
		}
		if(isset($input['tld']) AND is_array($input['tld']))
		{
			$param .= '&tld='.implode(',', $input['tld']);
			$data['tld'] = implode(',', $input['tld']);
		}
		elseif(isset($input['tld'])){
			$param .= '&tld='.trim($input['tld']);
			$data['tld'] = trim($input['tld']);
		}
		if(isset($input['length']) AND trim($input['length']))
		{
			$param .= '&length='.trim($input['length']);
			$data['length'] = trim($input['length']);
		}
		if(isset($input['price']) && trim($input['price']))
		{
			if(explode(',',$input['price'])){
				$prices = explode(',',$input['price']);
				$prices = array($this->de_format_currency(min($prices)), $this->de_format_currency(max($prices)));
				$input['price'] = implode(',', $prices);
			}

			$param .= '&price='.trim($input['price']);
			$data['price'] = trim($input['price']);
		}
		//$user_domains = DB::select('select id_domain from user_domains where id_user = ?', array($id_user));
		
		//$tab = 'underprice';
		if($tab == 'premium')
			$domainData = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?limit='.$limit.'&subtab=brandablepending&tab='.$tab.$param);
		else
			$domainData = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?limit='.$limit.'&tab='.$tab.$param);
		
		//echo (config('app.API_DOMAIN_SEARCH').'?limit='.$limit.'&tab='.$tab.$param);
		// domain/search?limit=16&tab=brandable&order=order_no&order_type=asc&length=4,17&price=1.00,36000.00 
		/*
		echo '<pre>';
		print_r($domainData);
		echo '<pre>';
		exit;
		*/
		
		$premiumprice = 0;
		if( isset($domainData->premium) )	{
			$premiumprice = $domainData->premium;		
			if( $domainData->premium < 0 )	{
				$premiumprice = 0;
			}
		}
		$data['premiumprice'] = $premiumprice;
		$data['premiumprice_formatted'] = $this->format_currency($premiumprice, true);
		/*
		echo config('app.API_KEY');
		echo '<br>';
		echo config('app.JANRAIN_API_KEY');
		echo '<br>';
		echo (config('app.API_DOMAIN_SEARCH').'?limit='.$limit.'&tab='.$tab.$param);
		echo '<pre>';
		print_r($domainData);
		echo '</pre>';
		exit;
		*/
		
		$seller_settings = APICall::api_call('/fesettings/getsettings/'.$domainData->id_user);
		$data['cnt_seller_settings'] = count($seller_settings);
		if( count($seller_settings) > 0 )	{
			$data['seller_settings'] = $seller_settings;
		}
		
		if($domainData->rows > 0){
			foreach($domainData->record as $k => $v){
				$domainData->record[$k]->price_formatted = $this->format_currency($v->price, true);
				$domainData->record[$k]->price = $this->format_currency($v->price, false);
			}
		}

		$savedData = array();

		if($domainData->rows <= 0)
			$domainData->record = array();

		$saved = Cookie::get('BrandEden_favourite_domains');
		$data['fav'] = explode(',', $saved);

		foreach($domainData->record as $d){
			if(count($data['fav']) > 0){
				//if ($this->in_array_r($d->id_domain, $user_domains, 'id_domain'))
				if(in_array($d->id_domain, $data['fav']))
						$savedData['class'][$d->id_domain] = 'save';
				else	$savedData['class'][$d->id_domain] = '';
			}
			else
				$savedData['class'][$d->id_domain] = '';
		}

		$data['domainData'] = $domainData;
		$data['class'] = @$savedData['class'];
		//$data['categories'] = APICall::api_call('domains/category?tab='.$tab);
		$data['categories'] = APICall::api_call('domains/categorywithdomaincount');
		$data['tlds'] = APICall::api_call('domain/tld?tab='.$tab);
		$data['url'] = config('app.API_URL');
		$data['base_url'] = config('app.BASE_URL');
		$data['records_per_page'] = $limit;
		$total_page = ceil((int)$domainData->rows/$limit);
		$data['total_page'] = $total_page;
		//dd($data);

		$data['max_price'] = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?tab='.$tab.'&limit=1&order=price&order_type=desc');
		//$data['min_price'] = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?tab='.$tab.'&limit=1&order=price&order_type=asc');
		//$data['min_price'] = $this->format_currency( 0 );
		$data['max_price'] = ceil($this->format_currency(@$data['max_price']->record[0]->price > 0? @$data['max_price']->record[0]->price: 5000, false));
		//$data['max_price'] = $this->format_currency( $data['max_price'] );
		//$data['min_price'] = floor($this->format_currency(@$data['min_price']->record[0]->price > 0? @$data['min_price']->record[0]->price: 0, false));
		$data['min_price'] = (float) 0;
		//$data['min_price'] = floor($this->format_currency(@$data['min_price']->record[0]->price > 0? @$data['min_price']->record[0]->price: 1, false));


		
		$data['max_length'] = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?tab='.$tab.'&limit=1&order=length&order_type=desc');
		$data['min_length'] = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?tab='.$tab.'&limit=1&order=length&order_type=asc');
		$data['max_length'] = @$data['max_length']->record[0]->length > 0? @$data['max_length']->record[0]->length: 50;
		$data['min_length'] = @$data['min_length']->record[0]->length > 0? @$data['min_length']->record[0]->length: 0;
		//$data['min_length'] = @$data['min_length']->record[0]->length > 0? @$data['min_length']->record[0]->length: 1;

		$data['max_price_selected'] = round(@$input['price']? @max(explode(',',$input['price'])): $data['max_price']);
		$data['min_price_selected'] = round(@$input['price']? @min(explode(',',$input['price'])): $data['min_price']);
		$data['max_length_selected'] = round(@$input['length']? @max(explode(',',$input['length'])): $data['max_length']);
		$data['min_length_selected'] = round(@$input['length']? @min(explode(',',$input['length'])): $data['min_length']);

		$data['tld_selected'] = [];
		if(!isset($data['tlds']->record))
		        $data['tlds']->record = array();
		
                foreach($data['tlds']->record  as $k => $d){

			        if(in_array($d->tld, explode(',',$data['tld'])))
					        $data['tld_selected'][$d->tld] = 'checked';
			        else	$data['tld_selected'][$d->tld] = '';
		}

		$data['cats_selected'] = [];
		foreach($data['categories']  as $k => $d){

			if($d->id_category == $data['category'])
					$data['cats_selected'][$d->id_category] = 'checked';
			else	$data['cats_selected'][$d->id_category] = '';
		}

		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		$data['currency_locale'] = $this->c_locale;
		$data['locale_language'] = $this->c_language;

		$data['friendly'] = '';
		$data['id_buyer'] = 0;
		
		// load the buyer info on views including the orders
		if( $this->is_user_login() )	{
			$friendly = $_SESSION['friendly'];
			$id_buyer = $_SESSION['id_buyer'];
			$data['friendly'] = $friendly;
			$data['id_buyer'] = $id_buyer;
			if( $id_buyer > 0 )	{
				
				// get all the order transaction
				$ord_tran = $this->get_all_order_transactions();
				$orders = $ord_tran['orders'];
				$order_domainIDs = $ord_tran['order_domainIDs'];
				$order_domainStatus = $ord_tran['order_domainStatus'];
				$order_IPNstatus_EscrowUniqueIdentifier = $ord_tran['order_IPNstatus_EscrowUniqueIdentifier'];
				$order_IPNstatus_Description = $ord_tran['order_IPNstatus_Description'];
				$order_IPNstatus_Code = $ord_tran['order_IPNstatus_Code'];
				$order_IPNstatus = $ord_tran['order_IPNstatus'];
				$order_IPNstatus_result = $ord_tran['order_IPNstatus_result'];
				
				// overwrite some existing order transaction 
				
				$orders = APICall::api_call('/ordertransactions/'.$id_buyer);
				//$order_domainIDs = array();
				//$order_domainStatus = array();
				//$order_IPNstatus_EscrowUniqueIdentifier = array();
				//$order_IPNstatus_Description = array();
				//$order_IPNstatus_Code = array();
				$display_status = '';
				if( count($orders) > 0 )	{
					foreach($orders as $ind => $order)	{
						$order_domainIDs[$order->id_domain] = $order->id_domain;
						//$order_domainStatus[] = $order->IPNstatus;
						$IPNstatus = json_decode($order->IPNstatus);
						if($IPNstatus)	{
							$IPNstatus_EscrowUniqueIdentifier = $IPNstatus->EscrowUniqueIdentifier;
							$IPNstatus_Description = $IPNstatus->Description;
							$IPNstatus_Code = $IPNstatus->Code;
						}
						else	{
							$IPNstatus_EscrowUniqueIdentifier = '';
							$IPNstatus_Description = '';
							$IPNstatus_Code = '';
						}
						if( $IPNstatus_Code == 0 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Cancelled </span>';
						}
						if( $IPNstatus_Code == 5 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Pending </span>';
						}
						if( $IPNstatus_Code == 15 )	{
							$display_status = '<span class="text-danger" style="color:#ff5652;" alt="Your Attention Is Required!" title="Your Attention Is Required!"> Pay Now ! </span>';
						}
						if( $IPNstatus_Code == 20 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Submitted </span>';
						}
						if( $IPNstatus_Code == 25 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Accepted </span>';
						}
						$order_domainStatus[$order->id_domain] = $display_status;
						$order_IPNstatus_EscrowUniqueIdentifier[$order->id_domain] = $IPNstatus_EscrowUniqueIdentifier;
						$order_IPNstatus_Description[$order->id_domain] = $IPNstatus_Description;
						$order_IPNstatus_Code[$order->id_domain] = $IPNstatus_Code;
						
					}
				}
				$data['orders'] = $orders;
				$data['order_domainIDs'] = $order_domainIDs;
				$data['order_domainStatus'] = $order_domainStatus;
				$data['order_IPNstatus_EscrowUniqueIdentifier'] = $order_IPNstatus_EscrowUniqueIdentifier;
				$data['order_IPNstatus_Description'] = $order_IPNstatus_Description;
				$data['order_IPNstatus_Code'] = $order_IPNstatus_Code;
				
				// Favorites Data
				$api_passdata['pageindex'] = 0; 
				$api_passdata['per_page'] = 0;
				$api_passdata['tab'] = 'all';
				$api_passdata['id_buyer'] = $id_buyer;
				$api_passdata['favsearchtxt'] = '';
				$favorites = APICall::api_call('/favorites/getfavdomains', $api_passdata, 'POST');
				
				$fav_domainIDs = array();
				if( count($favorites) > 0 )	{
					foreach($favorites as $fav)	{
						$fav_domainIDs[] = $fav->id_domain;
					}
				}
				$data['fav_domainIDs'] = $fav_domainIDs;
				
			}
		}
		else	{

			$ord_tran = $this->get_all_order_transactions();

			$data['orders'] = $ord_tran['orders'];
			$data['order_domainIDs'] = $ord_tran['order_domainIDs'];
			$data['order_domainStatus'] = $ord_tran['order_domainStatus'];
			$data['order_IPNstatus_EscrowUniqueIdentifier'] = $ord_tran['order_IPNstatus_EscrowUniqueIdentifier'];
			$data['order_IPNstatus_Description'] = $ord_tran['order_IPNstatus_Description'];
			$data['order_IPNstatus_Code'] = $ord_tran['order_IPNstatus_Code'];
			$data['order_IPNstatus'] = $ord_tran['order_IPNstatus'];
			$data['order_IPNstatus_result'] = $ord_tran['order_IPNstatus_result'];
			
		}
		
		return view('index')->with($data);
		//return view('indexstatic')->with($data);
	}
	
	public function showFilter($tab = 'brandable', $curr_page = 1)
	{
		
		$input = Request::all();
		$input_search = $input['search'];
		
		$invalid_characters = array( '#','%', '&' );
		$found_invalid_character = false;
		$invalid_character = '';
		for(  $m=0; $m < strlen( $input_search ); $m++ )	{
			if (in_array( $input_search[$m], $invalid_characters ) ) {
				$found_invalid_character = true;
				$invalid_character = $input_search[$m];
			}
		}
		
		if( $found_invalid_character == true )	{
			return 'Invalid input character ('.$invalid_character.') found. Please remove this '.$invalid_character.' from your search.';
			exit;
		}
		
		$id_user = 1;
		$default_order = 'order_no';
		$default_order_by = 'asc';
		$limit = config('app.records_per_page');
		$param = '';
		$data['tab'] = $tab;
		if($tab != 'brandable')
			$limit = $limit*3;
		if($tab == 'under1k')
			$tab = 'underprice';
		//$input = Request::all();
		
		//$param .= '&search_type=E';
		if(isset($input['search']) AND trim($input['search']) <> '')
		{
			$param .= '&search='.trim($input['search']);
		}
		if(isset($input['category']) AND trim($input['category']) > 0)
		{
			$param .= '&category='.trim($input['category']);
		}

		if(isset($input['tld']) AND is_array($input['tld']))
		{
			$param .= '&tld='.implode(',', $input['tld']);
		}
		elseif(isset($input['tld'])){
			$param .= '&tld='.trim($input['tld']);
		}
		if(isset($input['length']) AND trim($input['length']))
		{
			$param .= '&length='.trim($input['length']);
		}
		if(isset($input['price']) && trim($input['price']))
		{
			if(explode(',',$input['price'])){
				$prices = explode(',',$input['price']);

				$prices = array($this->de_format_currency(min($prices)), $this->de_format_currency(max($prices)));
				$input['price'] = implode(',', $prices);
			}

			$param .= '&price='.trim($input['price']);
		}
		if(isset($input['order_by']) AND trim($input['order_by']) <> '')
		{
			$param .= '&order_by='.trim($input['order_by']);
		}
		if(isset($input['order']) AND trim($input['order']) <> '')
		{
			$default_order = trim($input['order']);
		}
		if(isset($input['order_by']) AND trim($input['order_by']) <> '')
		{
			$default_order_by = trim($input['order_by']);
		}
		$param .= '&order='.$default_order.'&order_type='.$default_order_by;

		//$user_domains = DB::select('select id_domain from user_domains where id_user = ?', array($id_user));
		// echo config('app.API_DOMAIN_SEARCH'); domain/search
		
		//$domains = api('domain/list?tab='.$tab.'&id_portfolio='.((int) @$_GET['id_portfolio']).'&page='.$page.'&limit='.$limit.'&search='.$search);
		
		//$id_portfolio = -1;
		//$page = 1;
		//$page = 25;
		
		//$domainData = APICall::api_call('domain/list?tab='.$tab.'&id_portfolio='.$id_portfolio.'&page='.$page.'&limit='.$limit.'&search='.$search);
		
		//echo config('app.API_DOMAIN_SEARCH').'?page='.$curr_page.'&limit='.$limit.'&tab='.$tab.$param;
		//echo config('app.API_DOMAIN_SEARCH').'?page='.$curr_page.'&limit='.$limit.'&subtab=brandablepending&tab='.$tab.$param;
		
		//echo (config('app.API_DOMAIN_SEARCH').'?page='.$curr_page.'&limit='.$limit.'&tab='.$tab.$param);
		//return ;
		//domain/search?page=1&limit=16&tab=brandable&search=mecc&tld=com,io,tv&length=4,16&price=0.00,56000.00&order=order_no&order_type=asc
		//domain/search?page=1&limit=48&tab=premium&search=mecc&tld=com,org,me,tv,net,bz,us,io&length=1,30&price=0.00,4200000.00&order=order_no&order_type=asc
		//domain/search?page=1&limit=48&tab=underprice&search=mecc&tld=com,net,org,tv,info,me,cc,io&length=4,26&price=0.00,995.00&order=order_no&order_type=asc
		
		if($tab == 'premium')	{
			$domainData = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?page='.$curr_page.'&limit='.$limit.'&subtab=brandablepending&tab='.$tab.$param);
			//echo (config('app.API_DOMAIN_SEARCH').'?limit='.$limit.'&subtab=brandablepending&tab='.$tab.$param);
		}
		else	{
			$domainData = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?page='.$curr_page.'&limit='.$limit.'&tab='.$tab.$param);
			//echo (config('app.API_DOMAIN_SEARCH').'?limit='.$limit.'&tab='.$tab.$param);
		}
		
		//$domainData = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?page='.$curr_page.'&limit='.$limit.'&tab='.$tab.$param);
		//echo 'domain/search?page=1&limit=16&tab=brandable&tld=&length=4,17&price=0.00,36000.00&order_by=asc&order=domain&order_type=asc';
		
		//echo 'domain/search?page='.$curr_page.'&limit='.$limit.'&tab='.$tab.$param;
		//echo '<br>';
		
		
		
		//echo '<pre>';
		//print_r($domainData);
		//echo '</pre>';
		//exit;
		
		//$total_page = ceil((int)$domainData->rows/$limit);
		//$data['total_page'] = $total_page;
		
		if($domainData->result == 'SUCCESS')	{
			//if($domainData->rows > 0)	{
			if( count($domainData->record) > 0	)	{
				foreach($domainData->record as $k => $v){
					$domainData->record[$k]->price_formatted = $this->format_currency($v->price, true);
					$domainData->record[$k]->price = $this->format_currency($v->price, false);
				}
			}
		}

		$savedData = array();
		$savedData['class'] = array();
		
		$saved = Cookie::get('BrandEden_favourite_domains');
		$data['fav'] = explode(',', $saved);
		
		if($domainData->result == 'SUCCESS')	{
			if($domainData->rows > 0)	{
				foreach($domainData->record as $d)	{

					if(count($data['fav']) > 0){
						//if ($this->in_array_r($d->id_domain, $user_domains, 'id_domain'))
						if(in_array($d->id_domain, $data['fav']))
								$savedData['class'][$d->id_domain] = 'save';
						else	$savedData['class'][$d->id_domain] = '';
					}
					else
						$savedData['class'][$d->id_domain] = '';
				}
			}
		}
		
		$data['url'] = config('app.API_URL');
		$data['show_more'] = true;
		$total_page = ceil((int)$domainData->rows/$limit);
		$data['total_page'] = $total_page;
		$data['records_per_page'] = $limit;
		
		if($curr_page == $total_page)
			$data['show_more'] = false;
		$curr_page++;
		
		//@if($domainData->total_rows > $records_per_page && $domainData->rows > 0)
		
		//echo $data['total_page'].' - '.$curr_page.' - '.$data['show_more'];
		
		$data['domainData'] = $domainData;
		$data['class'] = $savedData['class'];
		$data['curr_page'] = $curr_page;
		//return json_encode($domainData);

		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		$data['currency_locale'] = $this->c_locale;
		$data['locale_language'] = $this->c_language;

		$data['friendly'] = '';
		$data['id_buyer'] = 0;
		
		// load the buyer info on views including the orders
		if( $this->is_user_login() )	{
			$friendly = $_SESSION['friendly'];
			$id_buyer = $_SESSION['id_buyer'];
			$data['friendly'] = $friendly;
			$data['id_buyer'] = $id_buyer;
			if( $id_buyer > 0 )	{
				
				// get all the order transaction
				$ord_tran = $this->get_all_order_transactions();
				$orders = $ord_tran['orders'];
				$order_domainIDs = $ord_tran['order_domainIDs'];
				$order_domainStatus = $ord_tran['order_domainStatus'];
				$order_IPNstatus_EscrowUniqueIdentifier = $ord_tran['order_IPNstatus_EscrowUniqueIdentifier'];
				$order_IPNstatus_Description = $ord_tran['order_IPNstatus_Description'];
				$order_IPNstatus_Code = $ord_tran['order_IPNstatus_Code'];
				$order_IPNstatus = $ord_tran['order_IPNstatus'];
				$order_IPNstatus_result = $ord_tran['order_IPNstatus_result'];
				
				// overwrite some existing order transaction 
				$orders = APICall::api_call('/ordertransactions/'.$id_buyer);
				//$order_domainIDs = array();
				//$order_domainStatus = array();
				//$order_IPNstatus_EscrowUniqueIdentifier = array();
				//$order_IPNstatus_Description = array();
				//$order_IPNstatus_Code = array();
				$display_status = '';
				if( count($orders) > 0 )	{
					foreach($orders as $ind => $order)	{
						$order_domainIDs[$order->id_domain] = $order->id_domain;
						//$order_domainStatus[] = $order->IPNstatus;
						
						$IPNstatus = json_decode($order->IPNstatus);
						if($IPNstatus)	{
							$IPNstatus_EscrowUniqueIdentifier = $IPNstatus->EscrowUniqueIdentifier;
							$IPNstatus_Description = $IPNstatus->Description;
							$IPNstatus_Code = $IPNstatus->Code;
						}
						else	{
							$IPNstatus_EscrowUniqueIdentifier = '';
							$IPNstatus_Description = '';
							$IPNstatus_Code = '';
						}
						if( $IPNstatus_Code == 0 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Cancelled </span>';
						}
						if( $IPNstatus_Code == 5 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Pending </span>';
						}
						if( $IPNstatus_Code == 15 )	{
							$display_status = '<span class="text-danger" style="color:#ff5652;" alt="Your Attention Is Required!" title="Your Attention Is Required!"> Pay Now ! </span>';
						}
						if( $IPNstatus_Code == 20 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Submitted </span>';
						}
						if( $IPNstatus_Code == 25 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Accepted </span>';
						}
						$order_domainStatus[$order->id_domain] = $display_status;
						$order_IPNstatus_EscrowUniqueIdentifier[$order->id_domain] = $IPNstatus_EscrowUniqueIdentifier;
						$order_IPNstatus_Description[$order->id_domain] = $IPNstatus_Description;
						$order_IPNstatus_Code[$order->id_domain] = $IPNstatus_Code;
						
					}
				}

			
				//echo '<pre>';
				//print_r($orders);
				//echo '</pre>';
				//echo '<pre>';
				//print_r($order_domainIDs);
				//echo '</pre>';
			
				
				$data['orders'] = $orders;
				$data['order_domainIDs'] = $order_domainIDs;
				$data['order_domainStatus'] = $order_domainStatus;
				$data['order_IPNstatus_EscrowUniqueIdentifier'] = $order_IPNstatus_EscrowUniqueIdentifier;
				$data['order_IPNstatus_Description'] = $order_IPNstatus_Description;
				$data['order_IPNstatus_Code'] = $order_IPNstatus_Code;
				
				// Favorites Data
				$api_passdata['pageindex'] = 0; 
				$api_passdata['per_page'] = 0;
				$api_passdata['tab'] = 'all';
				$api_passdata['id_buyer'] = $id_buyer;
				$api_passdata['favsearchtxt'] = '';
				$favorites = APICall::api_call('/favorites/getfavdomains', $api_passdata, 'POST');
				
				$fav_domainIDs = array();
				if( count($favorites) > 0 )	{
					foreach($favorites as $fav)	{
						$fav_domainIDs[] = $fav->id_domain;
					}
				}
				$data['fav_domainIDs'] = $fav_domainIDs;

			}
		}
		else	{

			$ord_tran = $this->get_all_order_transactions();

			$data['orders'] = $ord_tran['orders'];
			$data['order_domainIDs'] = $ord_tran['order_domainIDs'];
			$data['order_domainStatus'] = $ord_tran['order_domainStatus'];
			$data['order_IPNstatus_EscrowUniqueIdentifier'] = $ord_tran['order_IPNstatus_EscrowUniqueIdentifier'];
			$data['order_IPNstatus_Description'] = $ord_tran['order_IPNstatus_Description'];
			$data['order_IPNstatus_Code'] = $ord_tran['order_IPNstatus_Code'];
			$data['order_IPNstatus'] = $ord_tran['order_IPNstatus'];
			$data['order_IPNstatus_result'] = $ord_tran['order_IPNstatus_result'];
		}
		
		return view('loadmore')->with($data);
	}
	
	public function get_all_order_transactions()	{
		
		$orders = APICall::api_call('/allordertransactions');
		$order_domainIDs = array();
		$order_domainStatus = array();
		$order_IPNstatus_EscrowUniqueIdentifier = array();
		$order_IPNstatus_Description = array();
		$order_IPNstatus_Code = array();
		$order_IPNstatus = array();
		$order_IPNstatus_result = array();
		$display_status = '';
		if( count($orders) > 0 )	{
			foreach($orders as $ind => $order)	{
				$order_domainIDs[$order->id_domain] = $order->id_domain;
				//$order_domainStatus[] = $order->IPNstatus;
				
				$IPNstatus = json_decode($order->IPNstatus);
				if($IPNstatus)	{
					$IPNstatus_EscrowUniqueIdentifier = $IPNstatus->EscrowUniqueIdentifier;
					$IPNstatus_Description = $IPNstatus->Description;
					$IPNstatus_Code = $IPNstatus->Code;
				}
				else	{
					$IPNstatus_EscrowUniqueIdentifier = '';
					$IPNstatus_Description = '';
					$IPNstatus_Code = '';
				}

				if( $IPNstatus_Code == 0 )	{
					$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Cancelled </span>';
				}
				if( $IPNstatus_Code == 5 )	{
					$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Pending </span>';
				}
				if( $IPNstatus_Code == 15 )	{
					$display_status = '<span class="text-danger" style="color:#ff5652;" alt="Pay Now!" title="Pay Now!"> Pending </span>';
					//$display_status = '<span class="text-danger" style="color:#ff5652;" alt="Your Attention Is Required!" title="Your Attention Is Required!"> Pay Now ! </span>';
				}
				if( $IPNstatus_Code == 20 )	{
					$display_status = '<span class="text-primary" style="color:#3c9ffb;" alt="Payment Submitted" title="Payment Submitted" > Pending </span>';
					//$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Submitted </span>';
				}
				if( $IPNstatus_Code == 25 )	{
					$display_status = '<span class="text-primary" style="color:#3c9ffb;" alt="Payment Accepted" title="Payment Accepted"> Pending </span>';
					//$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Accepted </span>';
				}
				$order_domainStatus[$order->id_domain] = $display_status;
				$order_IPNstatus_EscrowUniqueIdentifier[$order->id_domain] = $IPNstatus_EscrowUniqueIdentifier;
				$order_IPNstatus_Description[$order->id_domain] = $IPNstatus_Description;
				$order_IPNstatus_Code[$order->id_domain] = $IPNstatus_Code;
				$order_IPNstatus[$order->id_domain] = $order->IPNstatus;
				$order_IPNstatus_result[$order->id_domain] = $IPNstatus;
				
			}
		}
		
		$return_data = array();
		$return_data['orders'] = $orders;
		$return_data['order_domainIDs'] = $order_domainIDs;
		$return_data['order_domainStatus'] = $order_domainStatus;
		$return_data['order_IPNstatus_EscrowUniqueIdentifier'] = $order_IPNstatus_EscrowUniqueIdentifier;
		$return_data['order_IPNstatus_Description'] = $order_IPNstatus_Description;
		$return_data['order_IPNstatus_Code'] = $order_IPNstatus_Code;
		$return_data['order_IPNstatus'] = $order_IPNstatus;
		$return_data['order_IPNstatus_result'] = $order_IPNstatus_result;
		return $return_data;
	}
	
	public function currency()
	{
		$currency = APICall::api_call('currency/openexchange');
		return json_encode($currency);
	}
	
	public function detailserror($tab, $id, $name, $landing_page)
	{
		$data['hideheader'] = false;
		$data['currency'] = $this->c_symbol;
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		$premiumprice = 0;
		if( isset($domainData->premium) )	{
			$premiumprice = $domainData->premium;		
			if( $domainData->premium < 0 )	{
				$premiumprice = 0;
			}
		}
		$data['premiumprice'] = $premiumprice;
		$data['premiumprice_formatted'] = $this->format_currency($premiumprice, true);
		return view('detailserror')->with($data);
	}
	
	public function details($tab, $id, $name, $landing_page)
	{
		$data['categories'] = APICall::api_call('domains/category');
		$data['tlds'] = APICall::api_call('domain/tld');
		$data['url'] = config('app.API_URL');
		$data['tab'] = $tab;
		$domainDetails = APICall::api_call('domain/list/'.$id);
		$domain_name = $id;
		$tldArr = explode('.',$domain_name);
		$length = strlen($tldArr[0]);
		$tld = str_replace($tldArr[0].".", "", $domain_name);
		
		$domainDetails->domain->tld = $tld;
		$domainDetails->domain->length = $length;
		
		if( $domainDetails->result = 'SUCCESS' )	{
			$p_data = array( 'id_domain' => $domainDetails->domain->id_domain, 'domain' => $domainDetails->domain->domain );
			$whois = APICall::api_call('domain/updatewhois', $p_data, 'POST');

			if( $whois )	{
				if( isset($whois->result_json->regrinfo->domain->created) )
					$domainDetails->domain->reg_date = $whois->result_json->regrinfo->domain->created;
				else
					$domainDetails->domain->reg_date = '';
				
				if( isset($whois->whoisinfo->tld) )
					$domainDetails->domain->tld = $whois->whoisinfo->tld;
				else
					$domainDetails->domain->tld = '';
					
			}
		}

		$domainDetails->domain->price_formatted = $this->format_currency($domainDetails->domain->price, true);
		$domainDetails->domain->price = $this->format_currency($domainDetails->domain->price, false);
		$data['details'] = $domainDetails;//dd($domainDetails);

		$related_domains = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?not_domain_id='.$id.'&category='.$domainDetails->domain->category.'&limit=3&tab=brandable&order=rand&order_type=asc');
		$data['related_domains'] = $related_domains;//dd($domainDetails);
		
		$data['premiumprice'] = 1000;
		//$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
		if( count($related_domains) > 0 )	{
			$data['premiumprice'] = $related_domains->premium;
		}
		
		$related_prem_domains = APICall::api_call(config('app.API_DOMAIN_SEARCH').'?not_domain_id='.$id.'&category='.$domainDetails->domain->category.'&limit=4&tab=premium&order=rand&order_type=asc');
		$data['related_prem_domains'] = $related_prem_domains;//dd($domainDetails);

		$saved = Cookie::get('BrandEden_favourite_domains');
		$data['fav'] = explode(',', $saved);

		if($data['related_domains']->rows){
			foreach($data['related_domains']->record as $k => $d){
				if(count($data['fav']) > 0){
					//if ($this->in_array_r($d->id_domain, $user_domains, 'id_domain'))
					if(in_array($d->id_domain, $data['fav']))
							$data['class'][$d->id_domain] = 'save';
					else	$data['class'][$d->id_domain] = '';
				}
				else
					$data['class'][$d->id_domain] = '';

				$data['related_domains']->record[$k]->price_formatted = $this->format_currency($d->price, true);
				$data['related_domains']->record[$k]->price = $this->format_currency($d->price, false);
			}
		}

		if($data['related_prem_domains']->rows){
			foreach($data['related_prem_domains']->record as  $k => $d){
				if(count($data['fav']) > 0){
					//if ($this->in_array_r($d->id_domain, $user_domains, 'id_domain'))
					if(in_array($d->id_domain, $data['fav']))
							$data['class'][$d->id_domain] = 'save';
					else	$data['class'][$d->id_domain] = '';
				}
				else
					$data['class'][$d->id_domain] = '';

				$data['related_prem_domains']->record[$k]->price_formatted = $this->format_currency($d->price, true);
				$data['related_prem_domains']->record[$k]->price = $this->format_currency($d->price, false);
			}
		}

	
		
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		$data['currency_locale'] = $this->c_locale;
		$data['locale_language'] = $this->c_language;

		$seller_settings = APICall::api_call('/fesettings/getsettings/'.$domainDetails->domain->id_user);
		$data['cnt_seller_settings'] = count($seller_settings);
		if( count($seller_settings) > 0 )	{
			$data['seller_settings'] = $seller_settings;
		}		
		$data['search']  = '';
		$data['hideheader'] = true;
		
		
		
		$data['friendly'] = '';
		$data['id_buyer'] = 0;
		
		// load the buyer info on views including the orders
		if( $this->is_user_login() )	{
			$friendly = $_SESSION['friendly'];
			$id_buyer = $_SESSION['id_buyer'];
			$data['friendly'] = $friendly;
			$data['id_buyer'] = $id_buyer;
			if( $id_buyer > 0 )	{
				
				// get all the order transaction
				$ord_tran = $this->get_all_order_transactions();
				$orders = $ord_tran['orders'];
				$order_domainIDs = $ord_tran['order_domainIDs'];
				$order_domainStatus = $ord_tran['order_domainStatus'];
				$order_IPNstatus_EscrowUniqueIdentifier = $ord_tran['order_IPNstatus_EscrowUniqueIdentifier'];
				$order_IPNstatus_Description = $ord_tran['order_IPNstatus_Description'];
				$order_IPNstatus_Code = $ord_tran['order_IPNstatus_Code'];
				$order_IPNstatus = $ord_tran['order_IPNstatus'];
				$order_IPNstatus_result = $ord_tran['order_IPNstatus_result'];
				
				// overwrite some existing order transaction 
				
				$orders = APICall::api_call('/ordertransactions/'.$id_buyer);
				//$order_domainIDs = array();
				//$order_domainStatus = array();
				//$order_IPNstatus_EscrowUniqueIdentifier = array();
				//$order_IPNstatus_Description = array();
				//$order_IPNstatus_Code = array();
				if( count($orders) > 0 )	{
					foreach($orders as $ind => $order)	{
						$order_domainIDs[$order->id_domain] = $order->id_domain;
						//$order_domainStatus[] = $order->IPNstatus;
						
						$IPNstatus = json_decode($order->IPNstatus);
						if($IPNstatus)	{
							$IPNstatus_EscrowUniqueIdentifier = $IPNstatus->EscrowUniqueIdentifier;
							$IPNstatus_Description = $IPNstatus->Description;
							$IPNstatus_Code = $IPNstatus->Code;
						}
						else	{
							$IPNstatus_EscrowUniqueIdentifier = '';
							$IPNstatus_Description = '';
							$IPNstatus_Code = '';
						}
						if( $IPNstatus_Code == 0 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Cancelled </span>';
						}
						if( $IPNstatus_Code == 5 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Pending </span>';
						}
						if( $IPNstatus_Code == 15 )	{
							$display_status = '<span class="text-danger" style="color:#ff5652;" alt="Your Attention Is Required!" title="Your Attention Is Required!"> Pay Now ! </span>';
						}
						if( $IPNstatus_Code == 20 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Submitted </span>';
						}
						if( $IPNstatus_Code == 25 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Accepted </span>';
						}
						$order_domainStatus[$order->id_domain] = $display_status;
						$order_IPNstatus_EscrowUniqueIdentifier[$order->id_domain] = $IPNstatus_EscrowUniqueIdentifier;
						$order_IPNstatus_Description[$order->id_domain] = $IPNstatus_Description;
						$order_IPNstatus_Code[$order->id_domain] = $IPNstatus_Code;
						
					}
				}
				$data['orders'] = $orders;
				$data['order_domainIDs'] = $order_domainIDs;
				$data['order_domainStatus'] = $order_domainStatus;
				$data['order_IPNstatus_EscrowUniqueIdentifier'] = $order_IPNstatus_EscrowUniqueIdentifier;
				$data['order_IPNstatus_Description'] = $order_IPNstatus_Description;
				$data['order_IPNstatus_Code'] = $order_IPNstatus_Code;
				
				// Favorites Data
				$api_passdata['pageindex'] = 0; 
				$api_passdata['per_page'] = 0;
				$api_passdata['tab'] = 'all';
				$api_passdata['id_buyer'] = $id_buyer;
				$api_passdata['favsearchtxt'] = '';
				$favorites = APICall::api_call('/favorites/getfavdomains', $api_passdata, 'POST');
				
				$fav_domainIDs = array();
				if( count($favorites) > 0 )	{
					foreach($favorites as $fav)	{
						$fav_domainIDs[] = $fav->id_domain;
					}
				}
				$data['fav_domainIDs'] = $fav_domainIDs;

			}
		}
		else	{

			$ord_tran = $this->get_all_order_transactions();

			$data['orders'] = $ord_tran['orders'];
			$data['order_domainIDs'] = $ord_tran['order_domainIDs'];
			$data['order_domainStatus'] = $ord_tran['order_domainStatus'];
			$data['order_IPNstatus_EscrowUniqueIdentifier'] = $ord_tran['order_IPNstatus_EscrowUniqueIdentifier'];
			$data['order_IPNstatus_Description'] = $ord_tran['order_IPNstatus_Description'];
			$data['order_IPNstatus_Code'] = $ord_tran['order_IPNstatus_Code'];
			$data['order_IPNstatus'] = $ord_tran['order_IPNstatus'];
			$data['order_IPNstatus_result'] = $ord_tran['order_IPNstatus_result'];
			
		}
		
		//echo '<pre>';
		//print_r($domainDetails);
		//echo '</pre>';
		
		if($landing_page)	{
				//return view('landing')->with($data);
				return view('detailsnew')->with($data);
		}
		
		return view('details')->with($data);
		//return view('detailsnew')->with($data);
	}
	
	public function save($id_domain)	{
		if( !$this->is_user_login() )	{
			$saved = Cookie::get('BrandEden_favourite_domains');
			$saved = explode(',', $saved);

			$domain_ids = $saved;
			if($id_domain){

				if(!in_array($id_domain, $domain_ids))
						$domain_ids[] = $id_domain;
				else	unset($domain_ids[array_search($id_domain, $domain_ids)]);
				
				$saved = Cookie::forever('BrandEden_favourite_domains', implode(',', $domain_ids));
			}
			
			$response = Response::make('', 200);
			$response->cookie($saved);
			return $response;
		}
		else	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$id_buyer = $_SESSION['id_buyer'];
				$favorites = APICall::api_call('/favorites/getfavoritedomains/'.$id_buyer);
				
				$return_text = '';
				if( count($favorites) > 0 )	{
					$found_domain_data = '';
					$found_domain = false;
					foreach( $favorites as $fav )	{
						$my_fav_domain_ids[] = $fav->id_domain;
						if( $id_domain == $fav->id_domain )	{
							$found_domain = true;
							$found_domain_data = $fav;
						}
					}
					if(!$found_domain)	{	// insert the new favorite domain
						$_data = array('id_domain' => $id_domain, 'id_buyer'=> $id_buyer);
						$res = APICall::api_call('/favorites/savedomain', $_data, 'POST');
						$my_fav_domain_ids[] = $id_domain;
						$return_text = 'added';
					}
					else	{
						$_data = array('id_fav' => $found_domain_data->id_fav, 'id_buyer'=> $id_buyer);
						$res = APICall::api_call('/favorites/removedomain', $_data, 'POST');
						unset($my_fav_domain_ids[array_search($id_domain, $my_fav_domain_ids)]);
						$return_text = 'removed';
					}
					$saved = Cookie::forever('BrandEden_favourite_domains', implode(',', $my_fav_domain_ids));
					$response = Response::make('', 200);
					$response->cookie($saved);
					return $return_text;
				}
				else	{	// just insert or save directly if no favorite domain save
					$_data = array('id_domain' => $id_domain, 'id_buyer'=> $id_buyer);
					$res = APICall::api_call('/favorites/savedomain', $_data, 'POST');
					
					$my_fav_domain_ids[] = $id_domain;
					$saved = Cookie::forever('BrandEden_favourite_domains', implode(',', $my_fav_domain_ids));
					$response = Response::make('', 200);
					$response->cookie($saved);
					$return_text = 'added';
					return $return_text;
				}
			}
		}
	}
	
	public function unsave($id_domain)	{
		if( !$this->is_user_login() )	{
			$saved = Cookie::get('BrandEden_favourite_domains');
			$saved = explode(',', $saved);

			$domain_ids = $saved;
			if($id_domain){

				if(!in_array($id_domain, $domain_ids))
						$domain_ids[] = $id_domain;
				else	unset($domain_ids[array_search($id_domain, $domain_ids)]);
				
				$saved = Cookie::forever('BrandEden_favourite_domains', implode(',', $domain_ids));
			}
			
			$response = Response::make('', 200);
			$response->cookie($saved);
			return $response;
		}
		else	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$id_buyer = $_SESSION['id_buyer'];
				$favorites = APICall::api_call('/favorites/getfavoritedomains/'.$id_buyer);
				if( count($favorites) > 0 )	{
					$found_domain = false;
					foreach( $favorites as $fav )	{
						if( $id_domain == $fav->id_domain )	{
							$found_domain = true;
						}
					}
					if(!$found_domain)	{	// insert the new favorite domain
						$_data = array('id_domain' => $id_domain, 'id_buyer'=> $id_buyer);
						$res = APICall::api_call('/favorites/savedomain', $_data, 'POST');
					}
				}
				else	{	// just insert or save directly if no favorite domain save
					$_data = array('id_domain' => $id_domain, 'id_buyer'=> $id_buyer);
					$res = APICall::api_call('/favorites/savedomain', $_data, 'POST');
				}
			}
		}
	}
	
	function in_array_r($needle, $haystack, $key = '', $strict = false) {
		foreach ($haystack as $item) {
			if (($strict ? $item->$key === $needle : $item->$key == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
				return true;
			}
		}
	
		return false;
	}

	public function enquire_()	{
		return 'processEnquiryJson( '.$this->enquire(false).' )';
    }

	public function generaterandomPassword() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^&*()-=+";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 16; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	
	public function get_payment_info()	{	
		$input = Request::all();
		$json['data'] = $input;
		$result = APICall::api_call('/payments/get_payment_info', $_POST, 'POST');
		return json_encode($result);
	}
	
	public function print_payment_info()	{	
		$input = Request::all();
		$json['data'] = $input;
		$result = APICall::api_call('/payments/export_to_pdf', $_POST, 'POST');
		return json_encode($result);
	}
	
	public function submitpayment($processCaptcha = true)	{	
		$input = Request::all();
		$is_valid = true;
		if(!trim($input['g-recaptcha-response']) && $processCaptcha)	{	
    	    $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfr5xkTAAAAAGjugdzxM1McR1TGWzJWiP9A1GLY&response=".$input['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
	        if($response['success'] == false)	{	
				$is_valid = false;
				$message = "Google captcha response is invalid.";
			}
		}

		if($is_valid){
			$json['data'] = $input;
			$result = APICall::api_call('/payments/submit_payment', $_POST, 'POST');
		}
		else	{
			$result["result"]="ERROR";
			$result["result_code"]=6;
			$result["response_text"]="Submit Payment";
			$result["response_details"]=$message;
		}
		
		return json_encode($result);
	}

	
	public function makeoffer($processCaptcha = true)	{	
		$input = Request::all();
		$is_valid = true;
		if(!trim($input['g-recaptcha-response']) && $processCaptcha)	{	
    	    $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfr5xkTAAAAAGjugdzxM1McR1TGWzJWiP9A1GLY&response=".$input['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
	        if($response['success'] == false)	{	
				$is_valid = false;
				$message = "Google captcha response is invalid.";
			}
		}

		if($is_valid){
			$json['data'] = $input;
			$result = APICall::api_call('/offer/create', $_POST, 'POST');
		}
		else	{
			$result["result"]="ERROR";
			$result["result_code"]=6;
			$result["response_text"]="Enquire";
			$result["response_details"]=$message;
		}
		return json_encode($result);
	}
	
	public function enquire($processCaptcha = true)	{

		$json['status'] = 'DANGER';
		$json['message'] = 'Invalid data.';

		$input = Request::all();

		$is_valid = true;

		if(!trim($input['g-recaptcha-response']) && $processCaptcha){

    	    $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lfr5xkTAAAAAGjugdzxM1McR1TGWzJWiP9A1GLY&response=".$input['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
	        if($response['success'] == false){
				$json['message'] = 'Google captcha response is invalid.';
				$is_valid = false;
			}
		}

		if (!trim($input['enq-domainName'])){
			$json['message'] = 'Domain ID not found.';
			$is_valid = false;
		}
		if(!trim($input['enq-domainOffer'])){ //If not an offer
		        if (!trim($input['enq-message'])){
			        $json['message'] = 'Message must not be empty.';
			        $is_valid = false;
		        }
		}
		if (!filter_var($input['enq-email'], FILTER_VALIDATE_EMAIL) !== false){
			$json['message'] = 'A valid email is required.';
			$is_valid = false;
		}

		if($is_valid){
			if($input['enq-domain-id']){
				$api_response = APICall::api_call('domain/list/'.$input['enq-domain-id']);

				if($api_response->record){
					
					$d = array();
					
					// added by James to add the field id_buyer
					$id_buyer = (int) $input['enq-id_buyer'];
					if( $id_buyer < 1 )	{	// if the user didnt login and make enquiry then will have to find the userid of that email if exist on the database.
						$userinfo = APICall::api_call('users/getbyemail/'.$input['enq-email']);
						if( $userinfo->status == 'success' )	
							$id_buyer = $userinfo->id;
						else
							$id_buyer = 0;
					}

					$domain_price = (int) $input['enq-domain-price'];
					$domain_offer = (int) $input['enq-domainOffer'];
					
					// if buyer have no records then will have to create an account for that buyer and send a password to be able login on the system
					if( $id_buyer < 1 )	{
						
						$randomPassword = $this->generaterandomPassword();
						
						if( $domain_offer > 0 && $domain_price < 1 )	// make offer
							$email_confirm = 0;
						else
							$email_confirm = 1;
						
						$_data = array('first_name' => $input['enq-email'], 'last_name' => $input['enq-email'], 'email' => $input['enq-email'], 'password' => $randomPassword, 'identifier' => '', 'type'=> 3, 'emailconfirm' => $email_confirm);
						$res = APICall::api_call('users', $_data, 'POST');
						
						if($res->error == 0)	{							
							if( $domain_offer > 0 && $domain_price < 1 )	{	// make offer
								$data_user_info = array();
								$data_user_info['url'] = url('/');
								$data_user_info['reg_first_name'] = $input['enq-email'];
								$data_user_info['reg_last_name'] = $input['enq-email'];
								$data_user_info['reg_email'] = $input['enq-email'];								
								$data_user_info['id_user'] = $res->id_user;
								$data_user_info['friendly'] = $res->friendly;
								$data_user_info['user_type'] = $res->user_type;
								$api_res = APICall::api_call('users/send_email_confirmation', $data_user_info, 'POST');
							}
							else	{
								$email_confirm = 1;
								$id_buyer = $res->id_user;
								$data_user_info =  $input;
								$data_user_info['enqemail'] = $input['enq-email'];
								$data_user_info['url'] = url('/');
								$data_user_info['randomPassword'] = $randomPassword;
								$api_res = APICall::api_call('users/send_email_login_info', $data_user_info, 'POST');
							}
						}
						
					}
					

					$d['id_buyer'] = $id_buyer;
					
					$d['domain'] = $api_response->domain->domain;
					$d['offer'] = $input['enq-domainOffer']? $input['enq-domainOffer']: NULL;
					$d['message'] = (string) $input['enq-message'];
					$d['timestamp'] = time();
					$d['status'] = 1;
					$d['sender'] = 'B';
					$d['email'] = $input['enq-email'];
					
					//User stats
					$d['request_ip'] = $this->visitor_stats['ipAddress'];
					$d['request_city'] = $this->visitor_stats['cityName'];
					$d['request_region'] = $this->visitor_stats['regionName'];
					$d['request_country'] = $this->visitor_stats['countryName'];
					$d['request_country_code'] = $this->visitor_stats['countryCode'];
					$d['request_zipcode'] = $this->visitor_stats['zipCode'];
					$d['request_lat'] = $this->visitor_stats['latitude'];
					$d['request_lng'] = $this->visitor_stats['longitude'];
					$d['request_timezone'] = $this->visitor_stats['timeZone'];
					$d['request_local_time'] = $this->visitor_stats['localTime'];
					$d['request_browser'] = $this->visitor_stats['browser'];
					$d['is_system_message'] = 'Y';
					
					$json['api_response']['make_post'] = APICall::api_call('inquiry', $d, 'POST');
                                        $is_stored = true;
					
					$id_inquiry = (int) $json['api_response']['make_post']->id_inquiry;
					/*
					echo '<pre>';
					print_r($json['api_response']['make_post']);
					echo '</pre>';
					exit;
					*/
					
/*
					$enquiry_id = $is_stored = DB::table('inquiries')->insertGetId($d, 'id_inquiry');
					$json['e'] = $enquiry_id;

					$d['text'] = $d['message']; // To fix problems at View ends. It treats $message as object than strings.
					$d['offer'] = $d['offer']? money_format('%.2n', floatval($d['offer'])): 'N/A';
					$d['min_offer'] = $api_response->domain->offer_price? money_format('%.2n', $api_response->domain->offer_price): 'N/A';
					$d['price'] = money_format('%.2n', $api_response->domain->price);
					$d['enquiry_id'] = $enquiry_id;
*/					
					$is_sent = false;
					if($is_stored){

						try{
/*						        $_maildata['to'] = 'sales@brandeden.com';
                                                        $_maildata['from'] = 'sales@brandeden.com';
                                                        $_maildata['subject'] = 'New domain enquiry!';
                                                        $_maildata['message'] = View::make('mail.enquiry', $d)->render();
                                                        $emailer_response1 = APICall::api_call('mailer/send?api_key='.config('app.API_KEY'), $_maildata,'POST');

						        $_maildata['to'] = $d['email'];
                                                        $_maildata['from'] = 'sales@brandeden.com';
                                                        $_maildata['subject'] = 'Order enquiry confirmation!';
                                                        $_maildata['message'] = View::make('mail.confirmation', $d)->render();
                                                        $emailer_response2 = APICall::api_call('mailer/send?api_key='.config('app.API_KEY'), $_maildata,'POST');

							$is_sent = Mail::send('mail.enquiry', $d, function($mail) use ($d){
									//$mail->to('pnkj.btyl@gmail.com')->subject('New domain enquiry!');
									$mail->to('sales@brandeden.com')->subject('New domain enquiry!');
							});

							$is_sent = Mail::send('mail.confirmation', $d, function($mail) use ($d){
									//$mail->to('pnkj.btyl@gmail.com')->subject('Order enquiry confirmation!');
									$mail->to($d['email'])->subject('Order enquiry confirmation!');
							});

                                                        //For debugging
                                                        $json['response1'] = $emailer_response1;
                                                        $json['response2'] = $emailer_response2;

                                                        if($emailer_response1->status != 'success')
                                                                throw new \Exception($emailer_response1->message);

                                                        if($emailer_response2->status != 'success')
                                                                throw new \Exception($emailer_response1->message);

*/
                                                        $is_sent = true;


						}catch(\Exception $e){ $json['message'] = 'Error: '.($e->getMessage()); return json_encode($json); }
					}

					if($is_stored){
						if($is_sent){
						
						        //If sent! make escrow transaction
						        $transaction_d = array();
						        $transaction_d['items'][0]['domain'] = $d['domain'];
						        $transaction_d['items'][0]['description'] = 'Domains purchase via -'.parse_url(config('app.url'), PHP_URL_HOST);
						        $transaction_d['items'][0]['name'] = 'Domain: '.$d['domain'];
						        $transaction_d['transaction_message'] = $d['message'];
						        $transaction_d['buyer'] = $d;
								$transaction_d['buyer']['email'] = $d['email'];
								$transaction_d['id_buyer'] = $id_buyer;
								$transaction_d['id_inquiry'] = $id_inquiry;

							$json['status'] = 'SUCCESS';
							//$json['message'] = 'Enquiry saved. We will contact you soon.';
							$json['message'] = '';
							
						        //Make escrow transaction independent to remaining process (not for offers)
						        if(!trim($input['enq-domainOffer'])){
						        
						                $json['api_response']['escrow_transaction'] = APICall::api_call('/v2/escrow/domains', $transaction_d, 'POST');
										
										/*
										echo '<pre>';
										print_r($transaction_d);
										print_r($json['api_response']['escrow_transaction']);
										echo '</pre>';
										exit;
										*/
										
						                if($json['api_response']['escrow_transaction']->result == 'SUCCESS')	{
											//$json['message'] .= '<br>Please check your mailbox ('.$input['enq-email'].') for completing escrow payment.';
											$json['message'] .= 'Thank you! A transaction has been initiated with Escrow.com. Please check your email for instructions regarding how to complete payment.';
										}
						                else	{
						                    $json['status'] = 'WARNING';
						                    $json['message'] = $json['api_response']['escrow_transaction']->response_details;
						                    if(!$json['message'])
						                        $json['message'] = 'Unable to process escrow transactions right now, please contact support.';
						                }
										
										// update all escrow transaction statuses on our DB
										$update_all_statuses = APICall::api_call('/orders/transactions/update-statuses');
                                                        }							
						}else $json['message'] = 'Unable to send emails. Try again later.';
					}else $json['message'] = 'Unable to save. Try again later.';

				}else $json['message'] = 'Domain ID not found.';
			}else $json['message'] = 'Domain ID not found.';
		}

		return json_encode($json);
	}
	
	public function mycart()	{
		$data = array();
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		
		$data['premiumprice'] = 1000;
		$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
		if( count($usersetting) > 0 )	{
			$data['premiumprice'] = $usersetting->setting->premium_price;
		}
		$data['hideheader'] = false;
		
		return view('mycart')->with($data);
	}
	
	public function billing_old()	{
		$data = array();
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		
		$data['premiumprice'] = 1000;
		$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
		if( count($usersetting) > 0 )	{
			$data['premiumprice'] = $usersetting->setting->premium_price;
		}
		$data['hideheader'] = false;
		return view('billing')->with($data);
	}

	public function billing($id)	{
		$this->is_user_login();
		$data = array();
		$domainDetails = APICall::api_call('domain/list/'.$id);
		$domainDetails->domain->price_formatted = $this->format_currency($domainDetails->domain->price, true);
		$domainDetails->domain->price = $this->format_currency($domainDetails->domain->price, false);
		$data['details'] = $domainDetails;//dd($domainDetails);
		
		$data['friendly'] = '';
		$data['id_buyer'] = 0;
		// load the buyer info on views including the orders
		if( $this->is_user_login() )	{
			$friendly = $_SESSION['friendly'];
			$id_buyer = $_SESSION['id_buyer'];
			$data['friendly'] = $friendly;
			$data['id_buyer'] = $id_buyer;
			if( $id_buyer > 0 )	{
				
				// get all the order transaction
				$ord_tran = $this->get_all_order_transactions();
				$orders = $ord_tran['orders'];
				$order_domainIDs = $ord_tran['order_domainIDs'];
				$order_domainStatus = $ord_tran['order_domainStatus'];
				$order_IPNstatus_EscrowUniqueIdentifier = $ord_tran['order_IPNstatus_EscrowUniqueIdentifier'];
				$order_IPNstatus_Description = $ord_tran['order_IPNstatus_Description'];
				$order_IPNstatus_Code = $ord_tran['order_IPNstatus_Code'];
				$order_IPNstatus = $ord_tran['order_IPNstatus'];
				$order_IPNstatus_result = $ord_tran['order_IPNstatus_result'];
				
				// overwrite some existing order transaction 
				$display_status = '';
				$orders = APICall::api_call('/ordertransactions/'.$id_buyer);
				if( count($orders) > 0 )	{
					foreach($orders as $ind => $order)	{
						$order_domainIDs[$order->id_domain] = $order->id_domain;
						//$order_domainStatus[] = $order->IPNstatus;
						
						$IPNstatus = json_decode($order->IPNstatus);
						if($IPNstatus)	{
							$IPNstatus_EscrowUniqueIdentifier = $IPNstatus->EscrowUniqueIdentifier;
							$IPNstatus_Description = $IPNstatus->Description;
							$IPNstatus_Code = $IPNstatus->Code;
						}
						else	{
							$IPNstatus_EscrowUniqueIdentifier = '';
							$IPNstatus_Description = '';
							$IPNstatus_Code = '';
						}
						if( $IPNstatus_Code == 0 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Cancelled </span>';
						}
						if( $IPNstatus_Code == 5 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Pending </span>';
						}
						if( $IPNstatus_Code == 15 )	{
							$display_status = '<span class="text-danger" style="color:#ff5652;" alt="Your Attention Is Required!" title="Your Attention Is Required!"> Pay Now ! </span>';
						}
						if( $IPNstatus_Code == 20 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Submitted </span>';
						}
						if( $IPNstatus_Code == 25 )	{
							$display_status = '<span class="text-primary" style="color:#3c9ffb;"> Payment Accepted </span>';
						}
						$order_domainStatus[$order->id_domain] = $display_status;
						$order_IPNstatus_EscrowUniqueIdentifier[$order->id_domain] = $IPNstatus_EscrowUniqueIdentifier;
						$order_IPNstatus_Description[$order->id_domain] = $IPNstatus_Description;
						$order_IPNstatus_Code[$order->id_domain] = $IPNstatus_Code;
						
					}
				}
				$data['orders'] = $orders;
				$data['order_domainIDs'] = $order_domainIDs;
				$data['order_domainStatus'] = $order_domainStatus;
				$data['order_IPNstatus_EscrowUniqueIdentifier'] = $order_IPNstatus_EscrowUniqueIdentifier;
				$data['order_IPNstatus_Description'] = $order_IPNstatus_Description;
				$data['order_IPNstatus_Code'] = $order_IPNstatus_Code;
				
				// Favorites Data
				$api_passdata['pageindex'] = 0; 
				$api_passdata['per_page'] = 0;
				$api_passdata['tab'] = 'all';
				$api_passdata['id_buyer'] = $id_buyer;
				$api_passdata['favsearchtxt'] = '';
				$favorites = APICall::api_call('/favorites/getfavdomains', $api_passdata, 'POST');
				
				$fav_domainIDs = array();
				if( count($favorites) > 0 )	{
					foreach($favorites as $fav)	{
						$fav_domainIDs[] = $fav->id_domain;
					}
				}
				$data['fav_domainIDs'] = $fav_domainIDs;

			}
		}
		else	{

			$ord_tran = $this->get_all_order_transactions();

			$data['orders'] = $ord_tran['orders'];
			$data['order_domainIDs'] = $ord_tran['order_domainIDs'];
			$data['order_domainStatus'] = $ord_tran['order_domainStatus'];
			$data['order_IPNstatus_EscrowUniqueIdentifier'] = $ord_tran['order_IPNstatus_EscrowUniqueIdentifier'];
			$data['order_IPNstatus_Description'] = $ord_tran['order_IPNstatus_Description'];
			$data['order_IPNstatus_Code'] = $ord_tran['order_IPNstatus_Code'];
			$data['order_IPNstatus'] = $ord_tran['order_IPNstatus'];
			$data['order_IPNstatus_result'] = $ord_tran['order_IPNstatus_result'];
			
		}
		
		/*
		echo '<pre>';
		print_r($domainDetails);
		echo '</pre>';
		exit;
		*/
		
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		
		$data['premiumprice'] = 1000;
		$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
		if( count($usersetting) > 0 )	{
			$data['premiumprice'] = $usersetting->setting->premium_price;
		}
		$data['hideheader'] = false;
		
		
		$p_data = array();
		$p_data['domain'] = $id;
		//$payment = APICall::api_call('/payments/get_payment_settings');
		$payment_settings = APICall::api_call('/payments/get_payment_settings_by_domain',$p_data,'POST');
		$data['payment_settings'] = $payment_settings;
		return view('billingnew')->with($data);
	}
	
	public function orderconfirm()	{
		$this->is_user_login();
		$data = array();
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		
		$data['premiumprice'] = 1000;
		$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
		if( count($usersetting) > 0 )	{
			$data['premiumprice'] = $usersetting->setting->premium_price;
		}
		$data['hideheader'] = false;
		
		return view('orderconfirmnew')->with($data);
	}
	
	public function orderconfirm_old()	{
		$data = array();
		$data['currency'] = $this->c_symbol;
		$data['currency_tag'] = $this->c_tag;
		
		$data['premiumprice'] = 1000;
		$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
		if( count($usersetting) > 0 )	{
			$data['premiumprice'] = $usersetting->setting->premium_price;
		}
		$data['hideheader'] = false;
		
		return view('orderconfirm')->with($data);
	}
	
	public function get_my_favorites($pageindex=0)	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				
				
				$api_passdata = array();
				
				$tab = 'all';
				if( isset($_POST['tab']) )	{
					$tab = $_POST['tab'];
				}
				
				$per_page = 10;
				if( isset($_POST['tab']) )	{
					$per_page = $_POST['per_page'];
				}
				
				$favsearchtxt = $_POST['favsearchtxt'];
				
				$api_passdata['pageindex'] = $pageindex; 
				$api_passdata['per_page'] = $per_page; 
				$api_passdata['tab'] = $tab;
				$api_passdata['id_buyer'] = $id_buyer;
				$api_passdata['favsearchtxt'] = $favsearchtxt;

				$total_rows_fav_domains = APICall::api_call('/favorites/get_totalrows_favdomains', $api_passdata, 'POST');
				$total_pages = ceil($total_rows_fav_domains/$per_page);
				$data['total_pages'] = $total_pages;
				$data['pageindex'] = $pageindex;
				
				$favorites = APICall::api_call('/favorites/getfavdomains', $api_passdata, 'POST');
				
				//$total_rows_fav_domains = APICall::api_call('/favorites/get_total_rows_favorite_domains/'.$id_buyer);
				//$favorites = APICall::api_call('/favorites/getfavoritedomains/'.$id_buyer);
				$ordertransactions = APICall::api_call('/ordertransactions/'.$id_buyer);
				
				$data['premiumprice'] = 1000;
				$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
				if( count($usersetting) > 0 )	{
					$data['premiumprice'] = $usersetting->setting->premium_price;
				}
				
				$data['favmessage'] = '';
				$data['favorites'] = $favorites;
				$data['ordertransactions'] = $ordertransactions;
				return view('favorites')->with($data);
			}
			else	{
				echo '<h1>Coming soon. For now this is only for Buyer View</h1>';
			}
		}
		else	{
			// return redirect('/');
			echo 'SessionExpire';
		}
	}
	
	public function remove_my_favorite_domain($id)	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				
				$_data = array('id_fav' => $id, 'id_buyer'=> $id_buyer);
				$res = APICall::api_call('/favorites/removedomain', $_data, 'POST');
				
				if( $res->result == 'SUCCESS' )	{
					$message = '<div><p class="status-message text-center bg-success text-success col-lg-12" >'.$res->response_details.' <a class="setting-icon" style="cursor:pointer;float:right;" onclick="$(this).parent().parent().remove();"><i class="icon-close"></i></a></p></div>';
				}
				else	{
					$message = '<div><p class="status-message text-center bg-danger text-danger col-lg-12" >'.$res->response_details.' <a class="setting-icon" style="cursor:pointer;float:right;"  onclick="$(this).parent().parent().remove();"><i class="icon-close"></i></a></p></div>';
				}

				echo $message;
				
				//return $this->get_my_favorites();	// better to use thise call function but doesnt work
				//$this->get_my_favorites();	// better to use thise call function but doesnt work
				
				/*
				$favorites = APICall::api_call('/favorites/getfavoritedomains/'.$id_buyer);
				$ordertransactions = APICall::api_call('/ordertransactions/'.$id_buyer);
				$data['premiumprice'] = 1000;
				$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
				if( count($usersetting) > 0 )	{
					$data['premiumprice'] = $usersetting->setting->premium_price;
				}
				$data['favmessage'] = $message;
				$data['favorites'] = $favorites;
				$data['ordertransactions'] = $ordertransactions;
				return view('favorites')->with($data);
				*/
			}
			else	{
				echo '<h1>Coming soon. For now this is only for Buyer View</h1>';
			}
		}
		else	{
			//return redirect('/');
			echo 'SessionExpire';
		}
	}
	
	public function bulk_delete_favorites_domain()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];

				if( count($_POST['checkboxDomainIDs']) > 0 )	{
					
					$_data = array();
					//$_data['id_buyer'] = $id_buyer; 
					//$_data['favDomainIds'] = $_POST['checkboxDomainIDs'];
					//$res = APICall::api_call('/favorites/bulkremovedomains', $_data, 'POST');

					// better to use the bulk delete above but doesnt work
					foreach( $_POST['checkboxDomainIDs'] as $id )	{
						$_data = array('id_fav' => $id, 'id_buyer'=> $id_buyer);
						$res = APICall::api_call('/favorites/removedomain', $_data, 'POST');
					}
				
					if( $res->result == 'SUCCESS' )	{
						$message = '<div><p class="status-message text-center bg-success text-success col-lg-12" >'.$res->response_details.' <a class="setting-icon" style="cursor:pointer;float:right;" onclick="$(this).parent().parent().remove();"><i class="icon-close"></i></a></p></div>';
					}
					else	{
						$message = '<div><p class="status-message text-center bg-danger text-danger col-lg-12" >'.$res->response_details.' <a class="setting-icon" style="cursor:pointer;float:right;"  onclick="$(this).parent().parent().remove();"><i class="icon-close"></i></a></p></div>';
					}

					echo $message;
					
					//return $this->get_my_favorites($message);	// better to use thise call function but doesnt work
					
					/*
					$favorites = APICall::api_call('/favorites/getfavoritedomains/'.$id_buyer);
					$ordertransactions = APICall::api_call('/ordertransactions/'.$id_buyer);
					$data['premiumprice'] = 1000;
					$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
					if( count($usersetting) > 0 )	{
						$data['premiumprice'] = $usersetting->setting->premium_price;
					}
					$data['favmessage'] = $message;
					$data['favorites'] = $favorites;
					$data['ordertransactions'] = $ordertransactions;
					return view('favorites')->with($data);
					*/
				}
			}
			else	{
				echo '<h1>Coming soon. For now this is only for Buyer View</h1>';
			}
		}
		else	{
			//return redirect('/');
			echo 'SessionExpire';
		}
	}
	
	public function search_favorite_domain_by_tab($tab)	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				
				$_data = array();
				$_data['id_buyer'] = $id_buyer; 
				$_data['tab'] = $tab;
				$favorites = APICall::api_call('/favorites/getbytab', $_data, 'POST');
				$ordertransactions = APICall::api_call('/ordertransactions/'.$id_buyer);
				
				$data['premiumprice'] = 1000;
				$usersetting = APICall::api_call('users/getsetting?&api_key='.config('app.API_KEY'));
				if( count($usersetting) > 0 )	{
					$data['premiumprice'] = $usersetting->setting->premium_price;
				}
				
				$data['favmessage'] = '';
				$data['favorites'] = $favorites;
				$data['ordertransactions'] = $ordertransactions;
				return view('favorites')->with($data);
			}
			else	{
				echo '<h1>Coming soon. For now this is only for Buyer View</h1>';
			}
		}
		else	{
			//return redirect('/');
			echo 'SessionExpire';
		}
	
	}

    public function indexstatic()
	{
		return view('indexstatic');
	}
	
}