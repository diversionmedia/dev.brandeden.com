<?php

namespace App\Http\Controllers;

use Mail;

use App;
use Request;
use Response;
use Session;
use Cookie;
use DB;
use App\Http\Controllers\Controller;
use App\Models\APICall;
use View;

class InquiriesController extends Controller
{

    public function index()	{
	
	}
	
	public function get_order_status()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];

				
				//$update_all_statuses = APICall::api_call('/orders/transactions/update-statuses'); // we need to make sure we have the latest status from escrow
				
				//$orders = APICall::api_call('/orders');
				/*
				$inquiries = APICall::api_call('/buyer/'.$friendly.'/inquiries');
				$inquiries_label = array();
				if( $inquiries->info->inquiries > 0 )	{
					foreach($inquiries->inquiries as $inq)	{
						$inquiries_label[$inq->id_inquiry] = $this->get_status_label($inq->status);
					}
				}
				
				$data['inquiries_label'] = $inquiries_label;
				$data['inquiries'] = $inquiries;
				*/
				$orders = APICall::api_call('/ordertransactions/'.$id_buyer);
				
				/*
				echo '<pre>';
				print_r($orders);
				echo '</pre>';
				*/
				
				$data['orders'] = $orders;
				return view('orderstatus')->with($data);
			}
			else	{
				echo '<h1>Coming soon. For now this is only for Buyer View</h1>';
			}
		}
		else	{
			//return redirect('/');
			echo 'SessionExpire';
		}
	}

	public function get_latest_message_by_domain($domain)	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				
				$data = array();
				
				//$inquiries = APICall::api_call('/inquiries/getlatestmessagefromsellerbydomain/'.$domain.'/'.$id_buyer);
				$inquiries = APICall::api_call('/inquiries/getlatestmessagefromsellerbydomainfilteroffermessage/'.$domain.'/'.$id_buyer.'/0');
				
				/*
				$user_settings = APICall::api_call('/users/getsetting');
				$premium = 1000;
				if( $user_settings->result_code == 0 )	{
					$premium = $user_settings->setting->premium_price;
				}
				*/
				
				if( count($inquiries) > 0 )	{
					foreach( $inquiries as $inq)	{
						$data['unread'] = $inq->unread;
						$data['domain'] = $inq->domain;
						$data['id_logo'] = $inq->id_logo;
						$data['price'] = $inq->price;
					

						if( $inq->default_premium == 'Y' )	{
							$premium = $inq->premium_price;
						}
						else	{
							$premium = 1000;
						}	
					
						$type = ''; 
						if( ((int) $inq->id_logo) > 0 )	{
							$type = 'Brandable';
						}
						if( ($inq->price >= $premium) && (((int) $inq->id_logo) < 1) )	{
							$type = 'Premium';
						}
						if( ($inq->price < $premium) && (((int) $inq->id_logo) < 1) )	{
							$type = 'Under '.$premium;
						}
						$data['type'] = $type;
						
						$data['message'] = $inq->message;
						$data['offer'] = $inq->offer;
						$data['received_on'] = $inq->received_on;
						$data['time'] = date("F d Y g:i a", strtotime($inq->received_on));

						$data['id_inquiry'] = $inq->id_inquiry;
						$data['id_user'] = $inq->id_user;
						$data['id_buyer'] = $inq->id_buyer;
						$data['id_parent'] = $inq->id_parent;
						$data['status'] = $inq->status;
						$data['decided_by'] = $inq->decided_by;
						$data['email'] = $inq->email;
						$data['IPNstatus'] = $inq->IPNstatus;
						$data['transaction_method'] = $inq->transaction_method;
						$data['id_domain'] = $inq->id_domain;

						$IPNstatus_EscrowUniqueIdentifier = '';
						$IPNstatus_Description = '';
						$IPNstatus_Code = '';						
						$IPNstatus = json_decode($inq->IPNstatus);
						//$data['json_IPNstatus'] = json_decode($inq->IPNstatus);
						if($IPNstatus)	{
							$IPNstatus_EscrowUniqueIdentifier = $IPNstatus->EscrowUniqueIdentifier;
							$IPNstatus_Description = $IPNstatus->Description;
							$IPNstatus_Code = $IPNstatus->Code;
						}
						
						$display_status = $IPNstatus_Description;
						if( $IPNstatus_Code == 0 )	{
							$display_status = 'Cancelled';
						}
						if( $IPNstatus_Code == 5 )	{
							$display_status = 'Pending';
						}
						if( $IPNstatus_Code == 15 )	{
							$display_status = 'Ready for payment';
						}
						
						$data['IPNstatus_EscrowUniqueIdentifier'] = $IPNstatus_EscrowUniqueIdentifier;
						$data['IPNstatus_Description'] = $IPNstatus_Description;
						$data['IPNstatus_Code'] = $IPNstatus_Code;
						$data['display_status'] = $display_status;
						
						//$seller = APICall::api_call('users/'.$inq->friendly);
						//$data['fullname'] = $seller->info->first_name.' '.$seller->info->last_name;
						//$data['seller'] = $seller;
						
						$data['fullname'] = $inq->seller_fullname;
						$data['seller_alias'] = $inq->seller_alias;
						$data['seller_avatar'] = $inq->seller_avatar;
						$data['seller_id'] = $inq->seller_id;
						
						//$inquiry_buyer_offer = APICall::api_call('/inquiries/getlatestofferfrombuyerbydomain/'.$inq->domain.'/'.$id_buyer);
						//$data['buyer_offer'] = $inquiry_buyer_offer[0]->offer;
						$data['buyer_offer'] = $inq->buyer_offer;
						$data['count_offer_message'] = $inq->count_offer_message;
					}
				}
				else	{
						$data['domain'] = $domain;
						$data['id_logo'] = 0;
						$data['price'] = 0;
						$data['type'] = '';
						$data['message'] = 'No Message Found';
						$data['offer'] = '';
						$data['received_on'] = '';
						$data['time'] = '';
						$data['fullname'] = '';
						$data['seller_alias'] = '';
						
						$data['id_inquiry'] = '';
						$data['id_user'] = '';
						$data['id_buyer'] = '';
						$data['id_parent'] = '';
						$data['status'] = '';
						$data['decided_by'] = '';
						$data['email'] = '';
						$data['IPNstatus'] = '';
						$data['transaction_method'] = '';
						$data['id_domain'] = '';
						$data['buyer_offer'] = '';
						$data['count_offer_message'] = 0;
				}

				echo json_encode($data);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			//return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function get_order_messages()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
			
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
			
				/*
				$user_settings = APICall::api_call('/users/getsetting');
				$premium = 1000;
				if( $user_settings->result_code == 0 )	{
					$premium = $user_settings->setting->premium_price;
				}
				*/
			
				$data = array();
				//$res_inq = APICall::api_call('/inquiries/getlatestmessagesfromseller/'.$id_buyer);
				$res_inq = APICall::api_call('/inquiries/getlatestmessagesfromsellerbydomainwithfulldata/'.$id_buyer.'/0');
				if( count($res_inq) > 0 )	{
					$cnt = 0;
					foreach($res_inq as $inq)	{
						
						if( $inq->default_premium == 'Y' )	{
							$premium = $inq->premium_price;
						}
						else	{
							$premium = 1000;
						}					
						
						//$seller = APICall::api_call('users/'.$inq->friendly);
						//$data['inquiries'][$cnt]['fullname'] = $seller->info->first_name.' '.$seller->info->last_name;
						$data['inquiries'][$cnt]['fullname'] = $inq->sellerfullname;
						$data['inquiries'][$cnt]['seller_alias'] = $inq->seller_alias;
						
						//$inquiry_domain = APICall::api_call('/inquiries/getlatestmessagefromsellerbydomain/'.$inq->domain.'/'.$id_buyer);
						//$inquiry_domain = APICall::api_call('/inquiries/getlatestmessagefromsellerbydomainfilteroffermessage/'.$inq->domain.'/'.$id_buyer.'/0');
						
						//$data['inquiries'][$cnt]['id_logo'] = $inquiry_domain[0]->id_logo;
						//$data['inquiries'][$cnt]['price'] = $inquiry_domain[0]->price;
						$data['inquiries'][$cnt]['id_logo'] = $inq->id_logo;
						$data['inquiries'][$cnt]['price'] = $inq->price;
						$type = ''; 
						//if( ((int) $inquiry_domain[0]->id_logo) > 0 )	{
						if( ((int) $inq->id_logo) > 0 )	{
							$type = 'Brandable';
						}
						//if( ($inquiry_domain[0]->price >= $premium) && (((int) $inquiry_domain[0]->id_logo) < 1) )	{
						if( ($inq->price >= $premium) && (((int) $inq->id_logo) < 1) )	{
							$type = 'Premium';
						}
						//if( ($inquiry_domain[0]->price < $premium) && (((int) $inquiry_domain[0]->id_logo) < 1) )	{
						if( ($inq->price < $premium) && (((int) $inq->id_logo) < 1) )	{
							$type = 'Under '.$premium;
						}
						$data['inquiries'][$cnt]['type'] = $type;
						
						/*
						$data['inquiries'][$cnt]['domain'] = $inquiry_domain[0]->domain;
						$data['inquiries'][$cnt]['message'] = $inquiry_domain[0]->message;
						$data['inquiries'][$cnt]['offer'] = $inquiry_domain[0]->offer;
						$data['inquiries'][$cnt]['received_on'] = $inquiry_domain[0]->received_on;
						$data['inquiries'][$cnt]['time'] = date("F d Y g:i a", strtotime($inquiry_domain[0]->received_on));
						$data['inquiries'][$cnt]['decided_by'] = $inquiry_domain[0]->decided_by;
						$data['inquiries'][$cnt]['offer'] = $inquiry_domain[0]->offer;
						$data['inquiries'][$cnt]['is_offer_message'] = $inquiry_domain[0]->is_offer_message;
						$data['inquiries'][$cnt]['is_read'] = $inquiry_domain[0]->is_read;
						$data['inquiries'][$cnt]['status'] = $inquiry_domain[0]->status;
						*/
						$data['inquiries'][$cnt]['domain'] = $inq->domain;
						$data['inquiries'][$cnt]['message'] = $inq->message;
						$data['inquiries'][$cnt]['offer'] = $inq->offer;
						$data['inquiries'][$cnt]['received_on'] = $inq->received_on;
						$data['inquiries'][$cnt]['time'] = date("F d Y g:i a", strtotime($inq->received_on));
						$data['inquiries'][$cnt]['decided_by'] = $inq->decided_by;
						$data['inquiries'][$cnt]['is_offer_message'] = $inq->is_offer_message;
						$data['inquiries'][$cnt]['is_read'] = $inq->is_read;
						$data['inquiries'][$cnt]['status'] = $inq->status;						
						$data['inquiries'][$cnt]['unread'] = $inq->unread;
						$data['inquiries'][$cnt]['buyer_offer'] = (int) $inq->buyer_offer;
						
						//$inquiry_buyer_offer = APICall::api_call('/inquiries/getlatestofferfrombuyerbydomain/'.$inq->domain.'/'.$id_buyer);
						//$data['inquiries'][$cnt]['buyer_offer'] = $inquiry_buyer_offer[0]->offer;
						
						
						
						$cnt++;
					}
				}
				else	{
					/*
					// just load the domain that have been enquire even there are no messages
					$inquiries = APICall::api_call('/buyer/'.$friendly.'/inquiries');
					if( $inquiries->info->inquiries > 0 )	{
						$data['inquiries'][] = array();
						$cnt = 0;
						foreach( $inquiries->inquiries as $inq )	{
							$data['inquiries'][$cnt]['domain'] = $inq->offer->domain;
							$data['inquiries'][$cnt]['message'] = $inq->offer->message;
							$data['inquiries'][$cnt]['offer'] = $inq->offer->offer;
							$data['inquiries'][$cnt]['time'] = date("F d Y g:i a", strtotime($inq->request_local_time));
							$data['inquiries'][$cnt]['received_on'] = $inq->request_local_time;

							$buyer = APICall::api_call('users/'.$friendly);
							$data['inquiries'][$cnt]['fullname'] = $buyer->info->first_name.' '.$buyer->info->last_name;
							
							$cnt++;
						}
					}
					*/
				}

				echo json_encode($data);
				//return view('ordermessages')->with($data);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function sendinquirymessage()	{
		
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
			
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
		
				$_POST['request_ip'] = $this->visitor_stats['ipAddress'];
				$_POST['request_city'] = $this->visitor_stats['cityName'];
				$_POST['request_region'] = $this->visitor_stats['regionName'];
				$_POST['request_country'] = $this->visitor_stats['countryName'];
				$_POST['request_country_code'] = $this->visitor_stats['countryCode'];
				$_POST['request_zipcode'] = $this->visitor_stats['zipCode'];
				$_POST['request_lat'] = $this->visitor_stats['latitude'];
				$_POST['request_lng'] = $this->visitor_stats['longitude'];
				$_POST['request_timezone'] = $this->visitor_stats['timeZone'];
				$_POST['request_local_time'] = $this->visitor_stats['localTime'];
				$_POST['request_browser'] = $this->visitor_stats['browser'];
				
				$sendInquiryMessage = APICall::api_call('inquiry',$_POST,'POST');
				$all_messages = APICall::api_call('/inquiries/getallinquiriesmessagesbyidparent/'.$_POST['id_parent'].'/0');
				echo json_encode($all_messages);
				
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function send_offer_message()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				
				$decided_by = $_POST['decided_by'];
				if($_POST['decided_by'] == 'null')	{
					$decided_by = '';
				}

				$data = array(
								'id_parent' => (int) $_POST['id_parent'],
								'domain' => $_POST['domain'],
								'message' => trim(@$_POST['message_offer']),
								'offer' => (int) @$_POST['offer'],
								'offer_message' => (int) @$_POST['offer_message'],
								'sender' => 'B',
								'id_buyer' => (int) $_POST['id_buyer'],
								'id_user' => (int) $_POST['id_user'],
								'decided_by' => $decided_by,
								'status' => 3
								);
								
				$timezone = '+05:30';
				$sign = substr($timezone, 0, 1) == '+'? '+': (substr($timezone, 0, 1) =='-'? '-': null);
				$hour = (int) substr($timezone, 1, 2);
				$minutes = (int) substr($timezone, -2) + ($hour * 60);
				$localtime = gmdate("Y-m-d H:i:s", time());
				if($sign){
						//means no GMT location (Its London my friend)
						$formatter = $sign.$minutes.' minutes';
						$localtime = date("Y-m-d H:i:s", strtotime($formatter));
				}

				$data['request_ip'] = $this->get_client_ip();
				$data['request_local_time'] = $localtime;
				$data['request_browser'] = $_SERVER['HTTP_USER_AGENT'];

				$send_offer_message = APICall::api_call('inquiry', $data, 'POST');
				//echo json_encode($send_offer_message);
				
				$id_parent = (int) $_POST['id_parent'];
				$all_messages = APICall::api_call('/inquiries/getallinquiriesmessagesbyidparent/'.$id_parent.'/1');
				echo json_encode($all_messages);
				
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
		
	}
	
	public function buyer_make_counter_offer()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
		
				//Make counter offer to seller
				$data = array(
								'id_inquiry' => (int) $_POST['mco_id_inquiry'],
								'id_parent' => (int) $_POST['mco_id_parent'],
								'domain' => $_POST['mco_domain'],
								'message' => trim(@$_POST['mco_buyer_message']),
								'offer' => (int) @$_POST['mco_buyer_offer'],
								'sender' => 'B',
								'id_buyer' => (int) $_POST['mco_id_buyer'],
								'status' => 2
								);
								
				$timezone = '+05:30';
				$sign = substr($timezone, 0, 1) == '+'? '+': (substr($timezone, 0, 1) =='-'? '-': null);
				$hour = (int) substr($timezone, 1, 2);
				$minutes = (int) substr($timezone, -2) + ($hour * 60);
				$localtime = gmdate("Y-m-d H:i:s", time());
				if($sign){
						//means no GMT location (Its London my friend)
						$formatter = $sign.$minutes.' minutes';
						$localtime = date("Y-m-d H:i:s", strtotime($formatter));
				}

				$data['request_ip'] = $this->get_client_ip();
				$data['request_local_time'] = $localtime;
				$data['request_browser'] = $_SERVER['HTTP_USER_AGENT'];
				$make_counter_offer = APICall::api_call('inquiry', $data, 'POST');
				echo json_encode($make_counter_offer);
				
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function buyer_confirm_accept_counter_offer()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
		
				$d = array();
				$id_inquiry = $_POST['caco_parent_id_inquiry'];
				$id_buyer = $_POST['caco_id_buyer'];
				$d['id_buyer'] = $_POST['caco_id_buyer'];
				$d['domain'] = trim($_POST['caco_domain']);
				
				$d['message'] = 'Buyer accepted the offer of domain '.$d['domain'].' InquiryID: '.$id_inquiry;
				//$d['message'] = 'Test to Buy from offer';
				$d['timestamp'] = time();
				$d['status'] = 3;
				$d['sender'] = 'B';
				$d['email'] = $_POST['caco_email'];
				
				$d['request_ip'] = $this->visitor_stats['ipAddress'];
				$d['request_city'] = $this->visitor_stats['cityName'];
				$d['request_region'] = $this->visitor_stats['regionName'];
				$d['request_country'] = $this->visitor_stats['countryName'];
				$d['request_country_code'] = $this->visitor_stats['countryCode'];
				$d['request_zipcode'] = $this->visitor_stats['zipCode'];
				$d['request_lat'] = $this->visitor_stats['latitude'];
				$d['request_lng'] = $this->visitor_stats['longitude'];
				$d['request_timezone'] = $this->visitor_stats['timeZone'];
				$d['request_local_time'] = $this->visitor_stats['localTime'];
				$d['request_browser'] = $this->visitor_stats['browser'];

				$transaction_d = array();
				$transaction_d['items'][0]['domain'] = $d['domain'];
				$transaction_d['items'][0]['description'] = 'Domains purchase via -'.parse_url(config('app.url'), PHP_URL_HOST);
				$transaction_d['items'][0]['name'] = 'Domain: '.$d['domain'];
				$transaction_d['transaction_message'] = $d['message'];
				$transaction_d['buyer'] = $d;
				$transaction_d['buyer']['email'] = $d['email'];
				$transaction_d['id_buyer'] = $id_buyer;
				$transaction_d['id_inquiry'] = $id_inquiry;
				$transaction_d['is_from_offer'] = 1; 
				$transaction_d['offer_price'] = $_POST['caco_seller_offer'];

				$result_response = array();
				$escrow_transaction = APICall::api_call('/v2/escrow/domains', $transaction_d, 'POST'); 
				
				if($escrow_transaction->result == 'SUCCESS')	{
					//Accept the latest seller offer inquiry by marking the parent ID
					$data = array(
									'decided_by' => 'B',
									'is_accepted' => 1,
									'status' => 3, 
									'id_inquiry' => (int) $_POST['caco_parent_id_inquiry']
									);
					 $parent_offer_result = APICall::api_call('inquiryupdate', $data, 'POST');
					
					//Accept the latest seller offer inquiry
					$data = array(
									'decided_by' => 'B',
									'is_accepted' => 1,
									'status' => 3, 
									'id_inquiry' => (int) $_POST['caco_seller_latest_id_inquiry']
									);

					 $seller_latest_offer_result = APICall::api_call('inquiryupdate', $data, 'POST');
					 $result_response = $escrow_transaction;
				}
				else	{
					$result_response = $escrow_transaction;
					$result_response->message = $escrow_transaction->result.'  :  '.$escrow_transaction->response_details. ' <br> '.$escrow_transaction->response_text.' Code: '.$escrow_transaction->result_code;
				}
				
				echo json_encode($escrow_transaction);
			
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function buyer_confirm_decline_counter_offer()	{	
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				
				//Decline the latest seller offer inquiry by marking the parent ID
				$data = array(
								'decided_by' => 'B',
								'status' => 6, 
								'id_inquiry' => (int) $_POST['cdco_parent_id_inquiry']
								);
				 $parent_offer_result = APICall::api_call('inquiryupdate', $data, 'POST');
				
				//Decline the latest seller offer inquiry
				$data = array(
								'decided_by' => 'B',
								'status' => 6, 
								'id_inquiry' => (int) $_POST['cdco_seller_latest_id_inquiry']
								);

				 $seller_latest_offer_result = APICall::api_call('inquiryupdate', $data, 'POST');

				echo json_encode($parent_offer_result);
				
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function get_inquiry_message_by_idparent($id_parent)	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
			
				//$all_messages = APICall::api_call('/inquiries/getallinquiriesmessagesbyidparent/'.$id_parent);
				$all_messages = APICall::api_call('/inquiries/getallinquiriesmessagesbyidparent/'.$id_parent.'/0');
				echo json_encode($all_messages);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function get_offer_message_by_idparent($id_parent)	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				$all_messages = APICall::api_call('/inquiries/getallinquiriesmessagesbyidparent/'.$id_parent.'/1');
				echo json_encode($all_messages);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function getunreadmessages($id_buyer)	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				//$unread_messages = APICall::api_call('/inquiries/'.$id_buyer.'/getunreadmessagesforbuyer');
				//$unread_messages = APICall::api_call('/inquiries/'.$id_buyer.'/get_count_unread_messages_for_buyer_by_domain');
				$unread_messages = APICall::api_call('/inquiries/'.$id_buyer.'/get_count_all_messages_for_buyer_by_domain');
				echo json_encode($unread_messages);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			// return redirect('/');
			echo json_encode('SessionExpire');
		}
	}
	
	public function getlivechat()	{
		$livechat = APICall::api_call('/livechat/getmessages');
		echo json_encode($livechat);
	}
	
	public function save_chat_message()	{
		if( $this->is_user_login() )	{
			if( $_SESSION['user_type'] == 'B' )		{	//Buyer
				$data = array();
				$friendly = $_SESSION['friendly'];
				$id_buyer = $_SESSION['id_buyer'];
				
				
				$id_inquiry = (int) $_POST['editChatIDInquiry'];
				$id_parent = (int) $_POST['id_parent-'.$id_inquiry];
				$domain = $_POST['domain-'.$id_inquiry];
				
				$data = array(
								'id_inquiry' => $id_inquiry,
								'id_buyer' => $id_buyer,
								'id_parent' => $id_parent,
								'domain' => $domain,
								'sender' => 'B',
								'message' => trim($_POST['message_'.$id_inquiry]),
								'oldmessage' => trim($_POST['oldmessage-'.$id_inquiry])
								);
				 $result = APICall::api_call('inquiryupdatemessage', $data, 'POST');
				
				echo json_encode($result);
			}
			else	{
				echo json_encode('<h1>Coming soon. For now this is only for Buyer View</h1>');
			}
		}
		else	{
			echo json_encode('SessionExpire');
		}
	}
	
	public function get_client_ip() {
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
	public function helloworld()	{
		// helloworld
	}
	
}
