<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store as Session;
use Illuminate\Contracts\Auth\Guard as Auth;
use Cookie;
use App;
use Response;
use Config;

class LanguagesMiddleware {

   protected $languages = ['en_US','en_GB','nl_NL','zh_CN'];

   public function handle($request, Closure $next){
		
		//Set locale & cookie on each request (set cookie only if not already set)
		$response = $next($request);
		$locale = Cookie::get('locale');
		if(!$locale){
			$locale = App::getLocale();
			$saved = Cookie::forever('locale', $locale);
			$response->cookie($saved);
		}
		App::setLocale($locale);
		setlocale(LC_ALL, $locale.'.UTF8');

		/* Set currency as selected or by locale selected */
		$currencylocale = Cookie::get('currency');
		if(!$currencylocale){
			$currencylocale = Config::get('app.locale');
			$saved = Cookie::forever('currency', $currencylocale);
			$response->cookie($saved);
		}
		setlocale(LC_MONETARY, $currencylocale.'.UTF8');

		$localeconv = localeconv();
		$currency_tag = trim($localeconv['int_curr_symbol']);
		$currecny_symbol = trim($localeconv['currency_symbol']);
		if(!$currecny_symbol || !$currency_tag){
			$currencylocale = Config::get('app.locale');
			$saved = Cookie::forever('currency', $currencylocale);
			$response->cookie($saved);
		}
		setlocale(LC_MONETARY, $currencylocale.'.UTF8');

        return $response;
     }

 }
