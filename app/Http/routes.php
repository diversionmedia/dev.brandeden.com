<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('index');
});*/

Route::get('/', 'DomainsController@index');
Route::get('/currency', 'DomainsController@currency');
Route::get('/static', 'DomainsController@indexstatic');

Route::get('/myaccount', 'AccountController@index');
//Route::post('/myaccount', 'AccountController@index');		// for allowing from submission to redirect
Route::post('/login', 'AccountController@login');			// login submission including social media
Route::get('/login', 'AccountController@mainlogin');		// just redirect to main login page if browsing the login url directly
Route::get('/mainlogin', 'AccountController@mainlogin');	// passing some error message if login info is invalid
Route::get('/logout', 'AccountController@logout');
Route::post('/signup', 'AccountController@signup');
Route::get('/signup', 'AccountController@mainsignup');		// just redirect to main signup page if browsing the signup url directly
Route::any('/industry-sales', 'AccountController@industry_sales');		
Route::get('/get_industry_sales/{topdata}', 'AccountController@get_industry_sales');

Route::get('/get_my_profile/{id_user}', 'AccountController@get_my_profile');
Route::post('/save_account_settings', 'AccountController@save_account_settings');
Route::post('/save_new_password', 'AccountController@save_new_password');
Route::post('/forgot_password', 'AccountController@forgot_password');
Route::get('/process_forgot_password', 'AccountController@process_forgot_password');
Route::post('/upload_avatar', 'AccountController@upload_avatar');
Route::get('/upload_avatar_test', 'AccountController@upload_avatar_test');

Route::get('/process_email', 'AccountController@process_email');
Route::get('/process_email_escrow', 'AccountController@process_email_escrow');
Route::get('/process_email_makeoffer', 'AccountController@process_email_makeoffer');
Route::get('/get_order_status', 'InquiriesController@get_order_status');
Route::get('/get_order_messages', 'InquiriesController@get_order_messages');
Route::get('/get_latest_message_by_domain/{domain}', 'InquiriesController@get_latest_message_by_domain');
Route::post('/sendinquirymessage', 'InquiriesController@sendinquirymessage');
Route::get('/get_inquiry_message_by_idparent/{id_parent}', 'InquiriesController@get_inquiry_message_by_idparent');
Route::get('/get_offer_message_by_idparent/{id_parent}', 'InquiriesController@get_offer_message_by_idparent');
Route::get('/getunreadmessages/{id_buyer}', 'InquiriesController@getunreadmessages');
Route::get('/getlivechat', 'InquiriesController@getlivechat');

Route::post('/save_chat_message', 'InquiriesController@save_chat_message');
Route::post('/buyer_make_counter_offer', 'InquiriesController@buyer_make_counter_offer');
Route::post('/send_offer_message', 'InquiriesController@send_offer_message');
Route::post('/buyer_confirm_accept_counter_offer', 'InquiriesController@buyer_confirm_accept_counter_offer');
Route::post('/buyer_confirm_decline_counter_offer', 'InquiriesController@buyer_confirm_decline_counter_offer');
Route::post('/bulk_delete_favorites_domain', 'DomainsController@bulk_delete_favorites_domain');
Route::get('/search_favorite_domain_by_tab/{tab}', 'DomainsController@search_favorite_domain_by_tab');
Route::any('/get_my_favorites/{pageindex}', 'DomainsController@get_my_favorites');
Route::any('/remove_my_favorite_domain/{id}', 'DomainsController@remove_my_favorite_domain');
Route::any('/mycart', 'DomainsController@mycart');
Route::any('/billing_old', 'DomainsController@billing_old');
//Route::any('/billing', 'DomainsController@billing');
Route::any('/billing/{id}', 'DomainsController@billing');
Route::any('/orderconfirm_old', 'DomainsController@orderconfirm_old');
Route::any('/orderconfirm', 'DomainsController@orderconfirm');

Route::any('/get_payment_info', 'DomainsController@get_payment_info');
Route::any('/print_payment_info', 'DomainsController@print_payment_info');
Route::any('/submitpayment', 'DomainsController@submitpayment');
Route::any('/makeoffer', 'DomainsController@makeoffer');
Route::any('/enquire', 'DomainsController@enquire');
Route::get('/{tab}', 'DomainsController@index');
Route::post('/save/{id}', 'DomainsController@save');
Route::post('/unsave/{id}', 'DomainsController@unsave');
//Route::get('/search', 'DomainsController@showFilter');
Route::get('/more/{tab}/{id}', 'DomainsController@showFilter');
Route::get('/{tab}/{id}/{name}', 'DomainsController@details');


Route::get('/test', 'DomaintestController@index');
Route::get('/test/{tab}', 'DomaintestController@index');
Route::get('/test/{id}', 'DomaintestController@details');



Route::get('/setlocale/{locale}/withcurrency/{currencylocale}', function ($locale, $currencylocale) {

    App::setLocale($locale);
	setlocale(LC_ALL, $locale.'.UTF8');
	setlocale(LC_MONETARY, $currencylocale.'.UTF8');

	$localeconv = localeconv();
	$currency_tag = trim($localeconv['int_curr_symbol']);
	$currency_symbol = trim($localeconv['currency_symbol']);
	if(!$currency_symbol || !$currency_tag)
		$currencylocale = Config::get('app.locale');
	setlocale(LC_MONETARY, $currencylocale.'.UTF8');

	$saved1 = Cookie::forever('locale',App::getLocale());
	$saved2 = Cookie::forever('currency', $currencylocale);

	$response = Response::make('', 200);

	return redirect('/')->withCookie($saved1)->withCookie($saved2);
});
/*Route::controller(Controller::detect()); */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
     //Route::resource('/myaccount', 'AccountController');

     /*Route::resource('/', 'DomainsController@index');
Route::resource('/myaccount', 'AccountController@index');
Route::resource('/login', 'AccountController@login');
Route::resource('/signup', 'AccountController@signup');
Route::resource('/{tab}', 'DomainsController@index');
Route::resource('/save/{id}', 'DomainsController@save');
//Route::get('/search', 'DomainsController@showFilter');
Route::resource('/more/{tab}/{id}', 'DomainsController@showFilter');
Route::resource('/{tab}/{id}/{name}', 'DomainsController@details');*/
});