<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class APICall extends Model
{
    public static function api_call($link, $data = false, $type = false)
	{

		if(strpos($link, '?') !== false) {
			$seperator = '&';
		} else {
			$seperator = '?';
		}
		//echo '<script> console.log("'.config('app.API_URL').$link.$seperator.'api_key='.config('app.API_KEY').'") </script>';

		$curl = curl_init(config('app.API_URL').$link.$seperator.'api_key='.config('app.API_KEY'));
		
		$data['api_key'] = config('app.API_KEY');
		
		$data = json_encode($data);
		
		curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if($type == 'POST') {
			
			if (isset($_FILES['files'])) {
				//if (count($_FILES['files']['tmp_name']) > 0 AND $_POST['multiple'] == "1") {
				if ( count($_FILES['files']['tmp_name']) > 0 ) {
					$count = 0;
					$_data['api_key'] = '33a3d9c20664cce5d39ff8793b5196bb';
					foreach($_FILES["files"]["tmp_name"] as $key => $tmp_name) {
						$_data['files[' . $count . ']'] = $this->getCurlValue($_FILES['files']['tmp_name'][$key], $_FILES['files']['type'][$key], $_FILES['files']['name'][$key]);
						$count++;
					}

					// $_data = array('files[0]' => $files, 'files[1]' => $files, 'api_key' => $api_key);
					// return ($_data);

					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
				}
				else {
					
					
					$filename = $_FILES['file']['name'];
					$filedata = $_FILES['file']['tmp_name'];
					$filesize = $_FILES['file']['size'];

					// $curl_file = getCurlValue($_FILES['files']['tmp_name'], $_FILES['files']['type'], $_FILES['files']['name']);

					$files = array(
						'files' => $this->getCurlValue($_FILES['files']['tmp_name'], $_FILES['files']['type'], $_FILES['files']['name'])
					);

					// return ($files);
					$headers = array("Content-Type:multipart/form-data");
					curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $files);
					curl_setopt($curl, CURLOPT_INFILESIZE, $filesize);
				}
			}
			else {
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			}
			
			//curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}
		$curl_response = curl_exec($curl);
		if ($curl_response === false) {
			$info = curl_getinfo($curl);
			curl_close($curl);
			die('error occured during curl exec. Additioanl info: ' . var_export($info));
		}
	
		curl_close($curl);
		
//		print_r($curl_response);
		return json_decode($curl_response);
	}
	
	public static function getCurlValue($filename, $contentType, $postname)
	{

		// PHP 5.5 introduced a CurlFile object that deprecates the old @filename syntax
		// See: https://wiki.php.net/rfc/curl-file-upload

		if (function_exists('curl_file_create')) {
			return curl_file_create($filename, $contentType, $postname);
		}

		// Use the old style if using an older version of PHP

		$value = "@{$filename};filename=" . $postname;
		if ($contentType) {
			$value.= ';type=' . $contentType;
		}

		return $value;
	}
	
	public static function avatar_upload_curl()
	{

	}

}