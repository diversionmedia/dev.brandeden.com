/*!
	JS functions
 */

function getUnreadMessages(id_buyer)	{
	$.get("/getunreadmessages/"+id_buyer, function( data ) {
		if( data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			var json_data = $.parseJSON(data);
			if( json_data.count.count > 0 )	{
				$('#display_unread_message_count').html('<span class="items-counter chat">'+json_data.count.count+'</span>');
				$('#display_unread_message_count_my_account').text(json_data.count.count);
				$('#display_unread_message_count_my_account').attr('onclick','$(\'#clicktab1\').click();');
				
				if( $('#display_unread_message_count_my_account').attr('class') != undefined )	{
					$('#chatlink').attr('onclick','$(\'#clicktab1\').click();');
				}
				else	{
					$('#chatlink').attr('onclick','window.location.href = "/myaccount";');
				}
			}
			else	{
				$('#display_unread_message_count').html('');
				$('#display_unread_message_count_my_account').text('');
				$('#display_unread_message_count_my_account').removeAttr('onclick');
				if( $('#display_unread_message_count_my_account').attr('class') != undefined )	{
					$('#chatlink').removeAttr('onclick');
				}
				else	{
					//$('#chatlink').removeAttr('onclick');
					$('#chatlink').attr('onclick','window.location.href = "/myaccount";');
				}
			}
			
			if( json_data.count.inquiries != undefined )	{
				if( json_data.count.inquiries.length > 0 )	{
					$.each(json_data.count.inquiries, function (key, val)	{
						var domainLabel = val.domain.replace('.', '_');
						if( val.count > 0 )	{
							$('#marker_' + domainLabel).html('<span class="unreadmessage_marker"> <span id="marker_count_bataba.com">'+val.count+'</span> new message</span>');
							$('#marker_count_'+domainLabel).text(val.count);
						}
						else	{
							$('#marker_' + domainLabel).html('');
						}
					});
				}
			}
			
		}
		
		setTimeout(function(){ getUnreadMessages(id_buyer) }, 15000);	// call every 15 seconds
	});
	return false;
}
	
function saveFavoriteDomain(id_domain)	{
	console.log('/save/'+id_domain);
	//$.post("/save/"+id_domain, function( data ) {
	$.post("/save/"+id_domain, function( data ) {
		console.log(data);
		return false;
	});
	return false;
}	
 
function removeFavoriteDomain(id,domain)	{
	if(confirm("Are you sure you want to remove "+domain+" from your Favorites?"))	{
		//$('#load_favorites').html('<div align="center"><img src="/images/loading.gif"></div>');
		$('#loading_bar_favorite').html('<img src="/images/ajax-loader-bar.gif">');
		$('#loadingbar_pagination').html('<img src="/images/ajax-loader-bar.gif"><br><br>');
		$.get("/remove_my_favorite_domain/"+id, function( data ) {
			//$('#load_favorites').html(data);
			if( data == 'SessionExpire' )	{
				alert('Your session already expire. Click OK and you will be redirected to the main page.');
				window.location.href = "/";
			}
			else	{
				getMyFavorites(0,data);
			}
			return false;
		});	
		return false;
	}
}

function getMyFavorites(page_index=0, message = '')	{	
	//$('#load_favorites').html('<div align="center"><img src="/images/loading.gif"></div>');
	$('#loading_bar_favorite').html('<img src="/images/ajax-loader-bar.gif">');
	$('#loadingbar_pagination').html('<img src="/images/ajax-loader-bar.gif"><br><br>');
	//$.get("/get_my_favorites/"+page_index, function( data ) {
	$.post( "/get_my_favorites/"+page_index, { page_index: page_index, tab: $('#domain_tab').val(), per_page: $('#view_per_page').val(), favsearchtxt: $('#favsearchtxt').val() } ).done(function( data )	{
		
		if( data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			if( message != '' )		{
				$('#result_fav_domains_message_main').html(message);
			}
			$('#loadingbar_pagination').html('');
			$('#loading_bar_favorite').html('');
			$('#load_favorites').html(data);
		}
		return false;
	});
	return false;
}
 
function getOrderStatus(friendly)	{	
	$('#load_order_status').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/get_order_status", function( data ) {
		if( data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			$('#load_order_status').html(data);
		}
		return false;
	});	
	return false;
}

function getIndustrySales(topdata)	{	
	$('#load_industry_sales').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/get_industry_sales/"+topdata, function( data ) {
		if( data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			$('#load_industry_sales').html(data);
		}
		return false;
	});	
	return false;
}

function getLatestMessageByDomain(domain,id='')	{	
	if(id != '')	{
		setActiveMessageList(id);
	}
	
	$('#show_loading_bar').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:50px;">');
	$('#show_loading_bar_latest_message').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:50px;">');
	//$('#load_detail_message').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/get_latest_message_by_domain/"+domain, function( json ) {
		var json_data = $.parseJSON(json);
		
		//console.log( 'latest messages = ' + json_data );
		
		console.log( json_data );
		
		if( json_data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			var strhtml = '';
			//strhtml += '<div><a onclick="$(\'.name-area\').toggle(\'normal\');$(\'.person-detail-area\').toggle(\'normal\');" style="float:right;" class="setting-icon btn btn-default btn-xs"><i class="glyphicon glyphicon-triangle-bottom"></i> </a></div>';
			//strhtml += '<div style="clear:both"></div>';
			
			strhtml += '\
					<div class="name-area">\
						<strong class="name-holder"><a href="#">';
							//<i class="icon"></i><span class="name">'+json_data.fullname+'</span>
			console.log( json_data.seller_avatar );
			if(json_data.seller_avatar != '')	{
				// strhtml += '	<img class="icon img-circle" src="'+api_url+'image.php?id_user='+json_data.seller_id+'&amp;width=60&amp;height=60" alt="'+json_data.fullname+'" title="'+json_data.fullname+'" > \
				// 				<span class="name">'+json_data.fullname+'</span>';
				strhtml += '	<img class="icon img-circle" src="'+api_url+'image.php?id_user='+json_data.seller_id+'&amp;width=60&amp;height=60" alt="'+json_data.seller_alias+'" title="'+json_data.seller_alias+'" > \
								<span class="name">'+json_data.seller_alias+'</span>';
			}
			else	{
				//strhtml += '	<i class="icon"></i><span class="name">'+json_data.fullname+'</span>';
				strhtml += '	<i class="icon"></i><span class="name">'+json_data.seller_alias+'</span>';
			}
							
			strhtml += '\
						</a></strong>\
						<time datetime="'+json_data.received_on+'"> <i class="fa fa-clock-o"></i> '+json_data.time+'</time>\
					</div>\
					<div class="person-detail-area">\
						<div class="person-detail">';
						
					if( json_data.price < 1 )	{
						if( json_data.decided_by != null &&  (json_data.status == 3 || json_data.status == 4) )	{
							var str_header = '';
							var str_latest_offer = '';
							if(json_data.decided_by == 'C')	{
								str_header = 'Seller have accepted your offer';
								str_latest_offer = json_data.buyer_offer
							}
							else	{
								str_header = 'You have accepted seller\'s offer';
								str_latest_offer = json_data.offer
							}
							
							strhtml += '\
								<h2>'+str_header+'</h2>\
								<p class="bg-success text-success" style="padding:5px 10px;border-radius:5px"><i class="fa fa-check-circle fa-2"></i> Final offer price of this domain is <span id="final_latest_offer">'+str_latest_offer+' </span></p>';
						}
						if( json_data.decided_by != null &&  (json_data.status == 6) )	{
							var str_header = '';
							var who_decline = '';
							if(json_data.decided_by == 'C')	{
								str_header = 'Seller have declined your offer';
								who_decline = 'Seller';
								str_latest_offer = json_data.buyer_offer
								str_body = 'Sorry, you\'re offer of '+str_latest_offer+' is being declined by the seller but you can still make a counter offer if that works.';
							}
							else	{
								str_header = 'You have declined seller\'s offer';
								who_decline = 'You';
								str_latest_offer = json_data.offer
								str_body = 'Seller offer of '+str_latest_offer+' is being declined by you but you can still accept the offer if you want to accept.';
							}
							
							strhtml += '\
								<h2 style="color:#a94442;">'+str_header+'</h2>\
								<p class="bg-danger text-danger" style="padding:5px 10px;border-radius:5px"><i class="fa fa-check-circle fa-2"></i>  '+str_body+'  </p>';
						}
					}
					
					var icon_strhtml = '';
					
					if( json_data.price > 0 )	{
						strhtml += '<div style="float:right;">';
						strhtml += '\
							<a class="masterTooltip setting-icon btn btn-default btn-xs" title="Order Info" onclick="$(\'#escrow_msg_info\').toggle(\'normal\');" style="float:right;" ><i class="fa fa-book"></i>  <span class="tooltiptext"></span> </a>';
						strhtml += '\
							<a onclick="printPaymentInfoByID(\'print\', \'approved\', \''+json_data.IPNstatus_EscrowUniqueIdentifier+'\', \''+json_data.transaction_method+'\', \''+json_data.id_domain+'\')" class="masterTooltip setting-icon btn btn-default btn-xs" title="Print Invoice" style="float:right;"><i class="icon-print"></i> <span class="tooltiptext"></span> </a>';
						strhtml += '</div>';
						
					}
					else	{
						if( json_data.status == 3 || json_data.status == 4 )	{
							if( json_data.decided_by == 'B' || json_data.decided_by == 'C' )	{
								strhtml += '<div style="float:right;">';
								strhtml += '\
									<a class="masterTooltip setting-icon btn btn-default btn-xs" title="Order Info" onclick="$(\'#escrow_msg_info\').toggle(\'normal\');" style="float:right;" ><i class="fa fa-book"></i>  <span class="tooltiptext"></span> </a>';
								strhtml += '\
									<a onclick="printPaymentInfoByID(\'print\', \'approved\', \''+json_data.IPNstatus_EscrowUniqueIdentifier+'\', \''+json_data.transaction_method+'\', \''+json_data.id_domain+'\')" class="masterTooltip setting-icon btn btn-default btn-xs" title="Print Invoice" style="float:right;"><i class="icon-print"></i> <span class="tooltiptext"></span> </a>';
								strhtml += '</div>';
							}
						}
					}
					
					strhtml += '\
							<ul class="detail-list list-unstyled">\
								<li>\
									<strong class="description">Domain</strong>\
									<span class="text"><a href="#">'+json_data.domain+'</a></span>\
								</li>';
						
						if( json_data.decided_by != '' || json_data.decided_by != null )	{
							var decided_by = json_data.decided_by;
						}
						else	{
							decided_by = '';
						}
								
						if( json_data.price > 0 )	{
							strhtml += '\
											<li>\
												<strong class="description">Price</strong>\
												<span class="text">'+json_data.price+' \
													<input type="hidden" id="latest_seller_offer" name="latest_seller_offer" value="'+json_data.offer+'"> \
													<input type="hidden" name="for_mco_id_inquiry" id="for_mco_id_inquiry" value="'+json_data.id_inquiry+'" >\
													<input type="hidden" name="for_mco_id_user" id="for_mco_id_user" value="'+json_data.id_user+'" >\
													<input type="hidden" name="for_mco_id_buyer" id="for_mco_id_buyer" value="'+json_data.id_buyer+'" >\
													<input type="hidden" name="for_mco_id_parent" id="for_mco_id_parent" value="'+json_data.id_parent+'" >\
													<input type="hidden" name="for_mco_domain" id="for_mco_domain" value="'+json_data.domain+'" >\
													<input type="hidden" name="for_mco_sender" id="for_mco_sender" value="B" />\
													<input type="hidden" name="for_mco_email" id="for_mco_email" value="'+json_data.email+'" >\
													<input type="hidden" name="for_mco_seller_offer" id="for_mco_seller_offer" value="'+json_data.offer+'" >\
													<input type="hidden" name="for_mco_decided_by" id="for_mco_decided_by" value="'+decided_by+'" >\
												</span>\
											</li>';
						}
						else	{
							strhtml += '\
											<li>\
												<strong class="description">Recent Seller Offer</strong>\
												<span class="text">'+json_data.offer+' \
													<input type="hidden" id="latest_seller_offer" name="latest_seller_offer" value="'+json_data.offer+'"> \
													<input type="hidden" name="for_mco_id_inquiry" id="for_mco_id_inquiry" value="'+json_data.id_inquiry+'" >\
													<input type="hidden" name="for_mco_id_user" id="for_mco_id_user" value="'+json_data.id_user+'" >\
													<input type="hidden" name="for_mco_id_buyer" id="for_mco_id_buyer" value="'+json_data.id_buyer+'" >\
													<input type="hidden" name="for_mco_id_parent" id="for_mco_id_parent" value="'+json_data.id_parent+'" >\
													<input type="hidden" name="for_mco_domain" id="for_mco_domain" value="'+json_data.domain+'" >\
													<input type="hidden" name="for_mco_sender" id="for_mco_sender" value="B" />\
													<input type="hidden" name="for_mco_email" id="for_mco_email" value="'+json_data.email+'" >\
													<input type="hidden" name="for_mco_seller_offer" id="for_mco_seller_offer" value="'+json_data.offer+'" >\
													<input type="hidden" name="for_mco_decided_by" id="for_mco_decided_by" value="'+decided_by+'" >\
												</span>\
											</li>';
							strhtml += '\
											<li>\
												<strong class="description">My Recent Offer</strong>\
												<span class="text">'+json_data.buyer_offer+' \
													<input type="hidden" name="buyer_recent_offer" id="buyer_recent_offer" value="'+json_data.buyer_offer+'" />\
												</span>\
											</li>';
						}
				strhtml += '\
								<li>\
									<strong class="description">Type</strong>\
									<span class="text brand">'+json_data.type+'</span>\
								</li>\
								<li>\
									<strong class="description">Latest Message</strong>\
									<span class="text add">'+json_data.message+'</span>\
								</li>\
							</ul>';
								strhtml += '<div id="escrow_msg_info" style="padding-bottom:20px;display:block;">\
												<ul class="detail-list list-unstyled">';
								
								var status_code = json_data.IPNstatus_Code;
								if( status_code == 0 )	{
									var new_display_status = 'Cancelled';
								}
								else if( status_code == 5 )	{
									var new_display_status = 'Pending';
								}
								else if( status_code == 15 )	{
									var new_display_status = 'Pay Now !';
								}
								else if( status_code == 20 )	{
									var new_display_status = 'Payment Submitted';
								}
								else if( status_code == 25 )	{
									var new_display_status = 'Payment Accepted';
								}
								else if( status_code == 85 )	{
									var new_display_status = 'Transaction Completed';
								}
								else	{
									var new_display_status = json_data.display_status;
								}
								
								if( json_data.price > 0 )	{
									strhtml += '\
													<li>\
														<strong class="description">Transaction ID</strong>\
														<span class="text">'+json_data.IPNstatus_EscrowUniqueIdentifier+' \
														</span>\
													</li\
													<li>\
														<strong class="description">Status</strong>\
														<span class="text"> \
															<a id="escrowStatus" class="setting-icon btn btn-default" onclick="viewStatusInfoMain(\''+json_data.IPNstatus_Code+'\', \''+json_data.IPNstatus_EscrowUniqueIdentifier+'\',\''+json_data.IPNstatus_Description+'\')"><i class="fa fa-book"></i> '+new_display_status+' </a>  \
														</span>\
													</li>';
								}
								else	{
									if( json_data.decided_by != null &&  (json_data.status == 3 || json_data.status == 4) )	{
									strhtml += '\
													<li>\
														<strong class="description">Transaction ID</strong>\
														<span class="text">'+json_data.IPNstatus_EscrowUniqueIdentifier+' \
														</span>\
													</li\
													<li>\
														<strong class="description">Status</strong>\
														<span class="text"> \
															<a id="escrowStatus" class="setting-icon btn btn-default" onclick="viewStatusInfoMain(\''+json_data.IPNstatus_Code+'\', \''+json_data.IPNstatus_EscrowUniqueIdentifier+'\',\''+json_data.IPNstatus_Description+'\')"><i class="fa fa-book"></i> '+new_display_status+' </a>  \
														</span>\
													</li>';
									}
								}
								strhtml += '</ul></div>';
								
				strhtml += '\
						</div>\
					</div>\
				';
				
				if( json_data.price < 1 )	{
					//getPreviousOffers(json_data.id_parent);
					getInquiryMessageByIDParent(json_data.id_parent,json_data.offer);
					if( json_data.decided_by != null &&  (json_data.status == 3 || json_data.status == 4) )	{
						strhtml += '\
						<ul class="btn-list list-unstyled">\
							<li id="loading_send_offer_message"></li>';
							
							strhtml += ' <li><a id="mainbtnSendMessage" onclick="sendOfferMessage()" class="btn btn-primary" style="width:100%;padding-left:20px;padding-right:20px;">Send Message</a></li>';
							
							if(json_data.offer != null)	{
								strhtml += '\
								<li><a id="mainbtnConfirmCounterOffer" class="btn add1" disabled="disabled">Accept</a></li>\
								<li><a id="mainbtnMakeCounterOffer" class="btn add1" disabled="disabled">Counter</a></li>';
								//strhtml += '<li><a id="mainbtnConfirmDeclineCounterOffer" class="btn add1" disabled="disabled">Decline</a></li>';
							}
						strhtml += '\
						</ul>\
						';
					}
					else	{
						strhtml += '\
						<ul class="btn-list list-unstyled">';						
							if(json_data.offer != null)	{
								strhtml += '\
									<li><a id="mainbtnConfirmCounterOffer" onclick="confirmCounterOffer()" class="btn btn-primary">Accept</a></li>\
									<li><a id="mainbtnMakeCounterOffer" onclick="makeCounterOffer()" class="btn add">Counter</a></li>';
							}
						/*
						if( json_data.status == 6  )	{
							strhtml += '\
								<li><a id="mainbtnConfirmDeclineCounterOffer" class="btn add1" disabled="disabled">Decline</a></li>';
						}
						else	{
							strhtml += '\
								<li><a id="mainbtnConfirmDeclineCounterOffer" onclick="confirmDeclineCounterOffer()" class="btn btn-danger">Decline</a></li>';
						}
						*/
						strhtml += '\
						</ul>\
						';
					}
					$('#load_reply_area').html('');
				}
				else	{
					//getInquiryMessageByIDParent(json_data.id_parent);
					getInquiryMessageByIDParent(json_data.id_parent,json_data.offer);
					var str_reply_html = '\
						<div class="reply-area">\
							<div class="field-holder">\
								<textarea name="message" id="message" class="form-control" placeholder="Your message here..."></textarea>\
								<input type="hidden" name="id_inquiry" id="id_inquiry" value="'+json_data.id_inquiry+'" >\
								<input type="hidden" name="id_user" id="id_user" value="'+json_data.id_user+'" >\
								<input type="hidden" name="id_buyer" id="id_buyer" value="'+json_data.id_buyer+'" >\
								<input type="hidden" name="id_parent" id="id_parent" value="'+json_data.id_parent+'" >\
								<input type="hidden" name="domain" id="domain" value="'+json_data.domain+'" >\
								<input type="hidden" name="sender" id="sender" value="B" />\
								<button id="btn_sendInquiry" type="button" onclick="sendInquiryMessage();"><i class="icon-paperplane"></i></button>\
							</div>\
						</div>\
						';
					$('#load_reply_area').html(str_reply_html);
				}
			$('#load_detail_message').html(strhtml);
			$('#show_loading_bar_latest_message').html('');
			runMasterToolTip();
			
			
			if( json_data.count_offer_message > 0 )	{
				setTimeout(function(){ 	sendOfferMessage(); }, 2000);
				setTimeout(function(){	
					$('#mainbtnSendMessage').attr('disabled','disabled');
					$('#mainbtnSendMessage').attr('class','btn add1');
					$('#mainbtnSendMessage').removeAttr('onclick');
				}, 5000);
			}
			
			//return false;
		}
	});
	
	getUnreadMessages(id_buyer);
	return false;
}

function sendOfferMessage()	{
	
	if( $('#for_mco_decided_by').val() == 'B' )	{
		var accpetedBy  = 'You';
	}
	else	{
		var accpetedBy  = 'Seller';
	}
	
	$('#overflow_message_history').hide('slow');
	var id_parent = $('#for_mco_id_parent').val();
	$('#mainbtnSendMessage').attr('disabled','disabled');
	$('#loading_send_offer_message').html('<img src="/images/ajax-loader-bar.gif">');
	$.get("/get_offer_message_by_idparent/"+id_parent, function( json ) {
		var json_data = $.parseJSON(json);
		if( json_data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			var str_reply_html = '\
				<div class="reply-area" style="border:1px solid #ddd; border-radius:20px;margin:10px;">\
					<div>\
					  <span class="hidden" style="float:left;color:#9197af;">Send Message</span>\
					  <span id="loading_bar_offer_send_message"></span>\
					  <a class="setting-icon btn btn-default btn-xs" style="margin-bottom:10px;float:right" onclick="$(\'#messages_text_wrapper\').toggle(\'slow\');">\
						<i class="fa fa-arrow-circle-o-down"></i> hide / show</a>\
					  <a class="setting-icon btn btn-default btn-xs" style="margin-bottom:10px;float:right;margin-right:10px" onclick="sendOfferMessage();">\
						<i class="fa fa-refresh"></i> refresh</a>\
					</div>\
					<div id="offer_messages_history">\
						<div class="clearfix"></div>';
						str_reply_html += '<div class="heading-bar" style="background:none;box-shadow:none;border-bottom:none;">\
												<div>\
													<strong class="name">Message History</strong>\
													<span class="name" style="float:right;font-size:13px;font-weight:normal">\
													  <span style="color:#a2a5ac">Final Offer: </span>  <span onclick="viewFinalOfferDetail();" style="cursor:pointer;"> <i class="fa fa-thumbs-up"></i>  ' + $('#final_latest_offer').text() + '</span>  \
													  <span style="margin-left:20px"> <span style="color:#a2a5ac"> Accepted By:</span>  <span onclick="viewFinalOfferDetail();" style="cursor:pointer;"> '+ accpetedBy +' </span> </span>\
													  <span style="margin-left:20px"> <span style="color:#a2a5ac"> Status: </span> <span onclick="$(\'#escrowStatus\').click();" style="cursor:pointer;"> '+ $('#escrowStatus').text() +' </span> </span>  \
													</span>\
												</div>\
											</div>\
							<div class="clearfix"></div>';

						str_reply_html += '<div id="messages_text_wrapper"> <!-- messages_text_wrapper --> ';
							
						str_reply_html += '<div class="clearfix"></div>';
						str_reply_html += '<div class="container-fluid"><div class="message-wrap">  <div class="msg-wrap" id="offer_message_list_mssgs">';
						var cnt_msg = 0;
						$.each(json_data, function (key, data) {
							console.log( data );

							if(data.sender == 'C')	{
								var user_id = data.id_user;
								var message_buttons = '';
							}
							else	{
								var user_id = data.id_buyer;
								var message_buttons = '\
										<span style="color:#afafaf">\
											<a onclick="editChatMessage(\''+data.id_inquiry+'\');" class="btn btn-primary btn-sm"> <i  class="fa fa-edit"></i> Edit </a>\
										</span>\
									';
							}
							
							str_reply_html += '</form>';
							
							str_reply_html += '\
								<div class="media msg">\
									<div class="media-body">\
										<span id="avatarsender-'+user_id+'" class="pull-left thumb-sm avatar m-r" style="margin-right:10px;">\
											<img class="icon img-circle" src="'+api_url+'image.php?id_user='+user_id+'&width=28&height=28" alt="'+data.senderalias+'" title="'+data.senderalias+'">\
										</span>\
										<div style="margin-left:35px;">\
											<small class="pull-right time"><i class="fa fa-clock-o" datetime="'+data.received_on+'"></i> '+data.time+'</small>\
											<h5 class="media-heading">'+data.senderalias+'  </h5>\
											<span class="">';
											
												//str_reply_html += data.message;
												//str_reply_html += message_buttons;
												
												str_reply_html += '<div id="div_mssg_label_'+data.id_inquiry+'">';
													str_reply_html += '<strong class="description" style="color:#bfbfbf;"> Message : </strong>';
													str_reply_html += '<span class="messgTxt_'+data.id_inquiry+'"> ' + data.message + '</span>' + message_buttons;
												str_reply_html += '</div>';
												
												str_reply_html += '\
															<div id="div_mssg_edit_'+data.id_inquiry+'" style="display:none; padding:5px 5px 10px; border: 1px solid #ddd;">\
																<div class="field-holder1">\
																	<form id="frmEditChatMessage_'+data.id_inquiry+'">\
																		<input value="'+data.id_inquiry+'" type="hidden" id="editChatIDInquiry" name="editChatIDInquiry" >\
																		<input name="id_inquiry-'+data.id_inquiry+'" id="id_inquiry-'+data.id_inquiry+'" value="'+data.id_inquiry+'" type="hidden">\
																		<input name="id_user-'+data.id_inquiry+'" id="id_user-'+data.id_inquiry+'" value="'+data.id_user+'" type="hidden">\
																		<input name="id_buyer-'+data.id_inquiry+'" id="id_buyer-'+data.id_inquiry+'" value="'+data.id_buyer+'" type="hidden">\
																		<input name="id_parent-'+data.id_inquiry+'" id="id_parent-'+data.id_inquiry+'" value="'+data.id_parent+'" type="hidden">\
																		<input name="domain-'+data.id_inquiry+'" id="domain-'+data.id_inquiry+'" value="'+data.domain+'" type="hidden">\
																		<input name="sender-'+data.id_inquiry+'" id="sender-'+data.id_inquiry+'" value="B" type="hidden">\
																		<input name="email-'+data.id_inquiry+'" id="email-'+data.id_inquiry+'" value="'+data.email+'" type="hidden">\
																		<textarea name="message_'+data.id_inquiry+'" id="message_'+data.id_inquiry+'" class="form-control">'+data.message+'</textarea>\
																		<input name="oldmessage-'+data.id_inquiry+'" id="oldmessage-'+data.id_inquiry+'" value="'+data.message+'" type="hidden">\
																	</form>\
																</div>\
																<br>\
																<div>\
																	<a id="btnsaveChatMessage_'+data.id_inquiry+'" onclick="saveChatMessage(\''+data.id_inquiry+'\');" class="btn btn-primary">Save</a>\
																	<a onclick="closeChatMessage(\''+data.id_inquiry+'\');" class="btn add1 btn-default">Cancel</a>\
																	<span id="chat_message_loading_bar_'+data.id_inquiry+'"> </span>\
																</div>\
															</div>\
															';
												
												
												str_reply_html += '\
											</span>\
										</div>\
									</div>\
								</div>\
								<div class="clearfix"></div>\
							';
						});	
						str_reply_html += '</div></div></div>';
						
						str_reply_html += ' \
								<div class="field-holder">\
									<textarea name="message_offer" id="message_offer" class="form-control" placeholder="You message here..."></textarea>\
									<input type="hidden" name="offer_message" id="offer_message" value="1" >\
									<input type="hidden" name="offer" id="offer" value="' + $('#latest_seller_offer').val() + '" >\
									<input type="hidden" name="id_user" id="id_user" value="' + $('#for_mco_id_user').val() + '" >\
									<input type="hidden" name="id_buyer" id="id_buyer" value="' + $('#for_mco_id_buyer').val() + '" >\
									<input type="hidden" name="id_parent" id="id_parent" value="' + $('#for_mco_id_parent').val() + '" >\
									<input type="hidden" name="domain" id="domain" value="' + $('#for_mco_domain').val() + '" >\
									<input type="hidden" name="sender" id="sender" value="B" />\
									<input type="hidden" name="decided_by" id="decided_by" value="' + $('#for_mco_decided_by').val() + '" />\
									<button alt="Send" title="Send" id="btn_sendOfferMessage" type="button" onclick="saveOfferMessage();"><i class="icon-paperplane"></i></button>\
								</div>';
			
				str_reply_html += '</div> <!-- messages_text_wrapper --> ';
				
			str_reply_html += '</div> <!-- offer_messages_history --> ';
			
			$('#load_reply_area').html(str_reply_html);
			$('#mainbtnSendMessage').removeAttr('disabled');
			$('#loading_send_offer_message').html('');
			return false;
		}
	});	
	return false;
}

function saveOfferMessage()	{
	if( $.trim($('#message_offer').val()) == '' )		{
		alert('Please provde a valid message');
		$('.message-block .field-holder').attr('style','border-color:#d9534f;');
		$('#message_offer').focus();
	}
	else	{
		$('.message-block .field-holder').attr('style','border-color:#e2e4eb;');
		//$('#offer_message_list').html('<div align="center"><img src="/images/loading.gif"></div>');
		$('#loading_bar_offer_send_message').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:10px;">');
		$('#btn_sendOfferMessage').attr('style','opacity:0.3');
		$('#btn_sendOfferMessage').attr('disabled','disabled');
		
		$.post( "/send_offer_message", $( "#form_send_message" ).serialize() ).done(function( json ) {
			var json_data = $.parseJSON(json);
			console.log(json_data);

			if( json_data == 'SessionExpire' )	{
				alert('Your session already expire. Click OK and you will be redirected to the main page.');
				window.location.href = "/";
			}
			else	{
				var str_reply_html = '';

				/*
				str_reply_html += '<ul class="tabset list-unstyled" data-type="ajax">';
				$.each(json_data, function (key, data) {
					str_reply_html += '<li>\
							<a>\
								<strong class="name">'+data.senderfullname+' </strong>\
								<time datetime="'+data.received_on+'">'+data.time+'</time>\
								<ul class="info-list list-unstyled">\
									<li>\
										<strong class="description">Message:</strong>\
										<span class="text"> '+data.message+'</span>\
									</li>\
								</ul>\
							</a>\
						</li>';
				});	
				str_reply_html += '</ul>';
				*/
				

				var cnt_msg = 0;
				$.each(json_data, function (key, data) {
					console.log( data );

					if(data.sender == 'C')	{
						var user_id = data.id_user;
						var message_buttons = '';
					}
					else	{
						var user_id = data.id_buyer;
						var message_buttons = '\
								<span style="color:#afafaf">\
									<a onclick="editChatMessage(\''+data.id_inquiry+'\');" class="btn btn-primary btn-sm"> <i  class="fa fa-edit"></i> Edit </a>\
								</span>\
							';
					}

					str_reply_html += '</form>';
					str_reply_html += '\
						<div class="media msg">\
							<div class="media-body">\
								<span id="avatarsender-'+user_id+'" class="pull-left thumb-sm avatar m-r" style="margin-right:10px;">\
									<img class="icon img-circle" src="'+api_url+'image.php?id_user='+user_id+'&width=28&height=28" alt="'+data.senderalias+'" title="'+data.senderalias+'">\
								</span>\
								<div style="margin-left:35px;">\
									<small class="pull-right time"><i class="fa fa-clock-o" datetime="'+data.received_on+'"></i> '+data.time+'</small>\
									<h5 class="media-heading">'+data.senderalias+'  </h5>\
									<span class="">';
										//str_reply_html += data.message;
										
										
										str_reply_html += '<div id="div_mssg_label_'+data.id_inquiry+'">';
											str_reply_html += '<strong class="description" style="color:#bfbfbf;"> Message : </strong>';
											str_reply_html += '<span class="messgTxt_'+data.id_inquiry+'"> ' + data.message + '</span>' + message_buttons;
										str_reply_html += '</div>';
										
										str_reply_html += '\
													<div id="div_mssg_edit_'+data.id_inquiry+'" style="display:none; padding:5px 5px 10px; border: 1px solid #ddd;">\
														<div class="field-holder1">\
															<form id="frmEditChatMessage_'+data.id_inquiry+'">\
																<input value="'+data.id_inquiry+'" type="hidden" id="editChatIDInquiry" name="editChatIDInquiry" >\
																<input name="id_inquiry-'+data.id_inquiry+'" id="id_inquiry-'+data.id_inquiry+'" value="'+data.id_inquiry+'" type="hidden">\
																<input name="id_user-'+data.id_inquiry+'" id="id_user-'+data.id_inquiry+'" value="'+data.id_user+'" type="hidden">\
																<input name="id_buyer-'+data.id_inquiry+'" id="id_buyer-'+data.id_inquiry+'" value="'+data.id_buyer+'" type="hidden">\
																<input name="id_parent-'+data.id_inquiry+'" id="id_parent-'+data.id_inquiry+'" value="'+data.id_parent+'" type="hidden">\
																<input name="domain-'+data.id_inquiry+'" id="domain-'+data.id_inquiry+'" value="'+data.domain+'" type="hidden">\
																<input name="sender-'+data.id_inquiry+'" id="sender-'+data.id_inquiry+'" value="B" type="hidden">\
																<input name="email-'+data.id_inquiry+'" id="email-'+data.id_inquiry+'" value="'+data.email+'" type="hidden">\
																<textarea name="message_'+data.id_inquiry+'" id="message_'+data.id_inquiry+'" class="form-control">'+data.message+'</textarea>\
																<input name="oldmessage-'+data.id_inquiry+'" id="oldmessage-'+data.id_inquiry+'" value="'+data.message+'" type="hidden">\
															</form>\
														</div>\
														<br>\
														<div>\
															<a id="btnsaveChatMessage_'+data.id_inquiry+'" onclick="saveChatMessage(\''+data.id_inquiry+'\');" class="btn btn-primary">Save</a>\
															<a onclick="closeChatMessage(\''+data.id_inquiry+'\');" class="btn add1 btn-default">Cancel</a>\
															<span id="chat_message_loading_bar_'+data.id_inquiry+'"> </span>\
														</div>\
													</div>\
													';
										
										str_reply_html += '\
									</span>\
								</div>\
							</div>\
						</div>\
						<div class="clearfix"></div>\
					';
				});	

		
					
				$('#offer_message_list').html(str_reply_html);
				$('#loading_bar_offer_send_message').html('');
				$('#message_offer').val('');
				$('#btn_sendOfferMessage').removeAttr('style');
				$('#btn_sendOfferMessage').removeAttr('disabled');
				return false;
			}
		});
	}
	return false;
}

function makeCounterOffer()	{
	var latest_seller_offer = $('#latest_seller_offer').val();
	$('#mco_seller_offer').val(latest_seller_offer);
	$('#mco_id_inquiry').val( $('#for_mco_id_inquiry').val() );
	$('#mco_id_user').val( $('#for_mco_id_user').val() );
	$('#mco_id_buyer').val( $('#for_mco_id_buyer').val() );
	$('#mco_id_parent').val( $('#for_mco_id_parent').val() );
	$('#mco_domain').val( $('#for_mco_domain').val() );
	$('#mco_sender').val( $('#for_mco_sender').val() );
	$('#counterOfferModal').modal();
}

function submitMakeCountOffer()	{
	if( $.trim($('#mco_buyer_offer').val()) == '' || parseInt($('#mco_buyer_offer').val()) < 1 )		{
		alert('New Offer should be greater than 0');
		$('#mco_buyer_offer').attr('style','border-color:#d9534f;');
		$('#mco_buyer_offer').focus();
		$('#mco_buyer_message').attr('style','border-color:none;');
	}
	else if( $.trim($('#mco_buyer_message').val()) == '' )		{
		alert('Please provide a valid message to the seller.');
		$('#mco_buyer_message').attr('style','border-color:#d9534f;');
		$('#mco_buyer_message').focus();
		$('#mco_buyer_offer').attr('style','border-color:none;');
	}
	else	{
		$('#mco_buyer_message').attr('style','border-color:none;');
		$('#mco_buyer_offer').attr('style','border-color:none;');
		
		$('#show_loading_image').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:20px;">');
		$('#btnMakeCounterOffer').attr('style','opacity:0.3');
		$('#btnMakeCounterOffer').attr('disabled','disabled');
		
		$.post( "/buyer_make_counter_offer", $( "#frmMakeCounterOffer" ).serialize() ).done(function( json ) {
			var json_data = $.parseJSON(json);
			//console.log(json_data);
			
			if( json_data == 'SessionExpire' )	{
				alert('Your session already expire. Click OK and you will be redirected to the main page.');
				window.location.href = "/";
			}
			else	{
			
				if( json_data.error == 0 )	{
					$('#mco_result_message').html('<p class="status-message text-center bg-success text-success col-lg-12" >Your new offer successfully sent.</p>');
				}
				else	{
					$('#mco_result_message').html('<p class="status-message text-center bg-danger text-danger col-lg-12" >'+json_data.message+'</p>');
				}
				
				$('#mco_buyer_offer').val('');
				$('#mco_buyer_message').val('');
				
				getInquiryMessageByIDParent( $('#mco_id_parent').val(), $('#mco_seller_offer').val() );
				//getLatestMessageByDomain( $('#for_mco_domain').val() );
				
				$('#show_loading_image').html('');
				$('#btnMakeCounterOffer').removeAttr('style');
				$('#btnMakeCounterOffer').removeAttr('disabled');
				return false;
			}
		});
	}
	return false;
}

function confirmCounterOffer()	{
	$('#caco_seller_latest_id_inquiry').val( $('#for_mco_id_inquiry').val() );
	$('#caco_parent_id_inquiry').val( $('#for_mco_id_parent').val() );
	$('#caco_domain').val( $('#for_mco_domain').val() );
	$('#caco_id_buyer').val( $('#for_mco_id_buyer').val() );
	$('#caco_id_parent').val( $('#for_mco_id_parent').val() );
	$('#caco_email').val( $('#for_mco_email').val() );
	$('#caco_seller_offer').val( $('#for_mco_seller_offer').val() );
	
	
	$('.confrim-accepting-price').html( $('#latest_seller_offer').val() );
	$('#caco_result_message').html('');
	$('#confirmCounterOfferModal').modal();
}

function submitConfirmAcceptCounterOffer()	{
		$('#caco_show_loading_image').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:20px;">');
		$('#btnConfirmAcceptCounterOffer').attr('style','opacity:0.3');
		$('#btnConfirmAcceptCounterOffer').attr('disabled','disabled');
		$.post( "/buyer_confirm_accept_counter_offer", $( "#frmConfirmAceeptCounterOffer" ).serialize() ).done(function( json ) {
			var json_data = $.parseJSON(json);
			console.log(json_data);
			
			if( json_data == 'SessionExpire' )	{
				alert('Your session already expire. Click OK and you will be redirected to the main page.');
				window.location.href = "/";
			}
			else	{
				if( json_data.result == 'SUCCESS' )	{
					$('#caco_result_message').html('<p class="status-message text-center bg-success text-success col-lg-12" >You have successfully accepted the seller offer.</p>');
				}
				else	{
					$('#caco_result_message').html('<p class="status-message text-center bg-danger text-danger col-lg-12" >'+json_data.message+'</p>');
				}

				//getInquiryMessageByIDParent( $('#for_mco_id_parent').val(), $('#latest_seller_offer').val() );
				getLatestMessageByDomain( $('#caco_domain').val() );
				
				$('#caco_show_loading_image').html('');
				//$('#btnConfirmAcceptCounterOffer').removeAttr('style');
				//$('#btnConfirmAcceptCounterOffer').removeAttr('disabled');
				return false;
			}
			
		});	
	return false;
}

function confirmDeclineCounterOffer()	{
	$('#cdco_seller_latest_id_inquiry').val( $('#for_mco_id_inquiry').val() );
	$('#cdco_parent_id_inquiry').val( $('#for_mco_id_parent').val() );
	$('#cdco_domain').val( $('#for_mco_domain').val() );
	$('#cdco_id_buyer').val( $('#for_mco_id_buyer').val() );
	$('#cdco_id_parent').val( $('#for_mco_id_parent').val() );
	$('#cdco_email').val( $('#for_mco_email').val() );
	$('#cdco_seller_offer').val( $('#for_mco_seller_offer').val() );
	
	$('.confirm-decline-price').html( $('#latest_seller_offer').val() );
	$('#cdco_result_message').html('');
	$('#confirmDeclineCounterOfferModal').modal();
}

function submitConfirmDeclineCounterOffer()	{
		$('#cdco_show_loading_image').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:20px;">');
		$('#btnConfirmDeclineCounterOffer').attr('style','opacity:0.3');
		$('#btnConfirmDeclineCounterOffer').attr('disabled','disabled');
		$.post( "/buyer_confirm_decline_counter_offer", $( "#frmConfirmDeclineCounterOffer" ).serialize() ).done(function( json ) {
			var json_data = $.parseJSON(json);
			//console.log(json_data);
			
			if( json_data == 'SessionExpire' )	{
				alert('Your session already expire. Click OK and you will be redirected to the main page.');
				window.location.href = "/";
			}
			else	{
				if( json_data.error == 0 )	{
					$('#cdco_result_message').html('<p class="status-message text-danger bg-danger text-warning col-lg-12" >You have successfully declined the seller offer.</p>');
				}
				else	{
					$('#cdco_result_message').html('<p class="status-message text-center bg-danger text-danger col-lg-12" >'+json_data.message+'</p>');
				}
				

				//getInquiryMessageByIDParent( $('#for_mco_id_parent').val(), $('#latest_seller_offer').val() );
				getLatestMessageByDomain( $('#cdco_domain').val() );
				
				$('#cdco_show_loading_image').html('');
				//$('#btnConfirmDeclineCounterOffer').removeAttr('style');
				//$('#btnConfirmDeclineCounterOffer').removeAttr('disabled');
				return false;
			}
			
		});	
	return false;
}

function sendInquiryMessage()	{	
	if( $.trim($('#message').val()) == '' )		{
		alert('Please provde a valid message');
		$('.message-block .field-holder').attr('style','border-color:#d9534f;');
		$('#message').focus();
	}
	else	{
		$('.message-block .field-holder').attr('style','border-color:#e2e4eb;');
		
		//$('#load_all_message_for_domain').html('<div align="center"><img src="/images/loading.gif"></div>');
		$('#show_loading_bar').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:50px;">');
		$('#btn_sendInquiry').attr('style','opacity:0.3');
		$('#btn_sendInquiry').attr('disabled','disabled');
		
		$.post( "/sendinquirymessage", $( "#form_send_message" ).serialize() ).done(function( json ) {
			var json_data = $.parseJSON(json);
			console.log(json_data);

			if( json_data == 'SessionExpire' )	{
				alert('Your session already expire. Click OK and you will be redirected to the main page.');
				window.location.href = "/";
			}
			else	{
					var str_mssg = '';
				
					/*
					str_mssg += '<div class="heading-bar" style="background:none;box-shadow:none;border-bottom:none;">\
						<div>\
							<strong class="name">Escrow | Order Info</strong>\
							<span id="show_loading_bar"></span>';
					str_mssg += '		<a style="cursor:pointer;" onclick="getLatestMessageByDomain(\''+ $( "#for_mco_domain" ).val() +'\')" class="setting-icon" alt="Reload Message History" title="Reload Message History"><i class="fa fa-info"></i></a>';
					str_mssg += '</div>\
					</div>\
					<div class="clearfix"></div>\
					<div style="overflow-x:hidden;overflow-y:auto;height:100px;">';
						str_mssg += '<ul class="tabset list-unstyled" data-type="ajax">';
							str_mssg += '<li>\
									<a>\
										<ul class="info-list list-unstyled">\
											<li>\
												<strong class="description">Transaction ID:</strong>\
												<span class="text"> Hello World </span>\
											</li>\
											<li>\
												<strong class="description">Status:</strong>\
												<span class="text"> Hello World </span>\
											</li>\
										</ul>\
									</a>\
								</li>';
						str_mssg += '</ul>';
					str_mssg += '</div>';
					*/
					
				
					str_mssg += '<div class="heading-bar" style="background:none;box-shadow:none;border-bottom:none;">\
						<div>\
							<strong class="name">Messages History</strong>\
							<span id="show_loading_bar"></span>';
					//str_mssg += '		<a style="cursor:pointer;" onclick="getInquiryMessageByIDParent(\''+ $( "#id_parent" ).val() +'\')" class="setting-icon" alt="Reload Message History" title="Reload Message History"><i class="fa fa-refresh"></i></a>';
					
					str_mssg += '		<a style="cursor:pointer;" onclick="getLatestMessageByDomain(\''+ $( "#for_mco_domain" ).val() +'\')" class="setting-icon" alt="Reload Message History" title="Reload Message History"><i class="fa fa-refresh"></i></a>';
					
					str_mssg += '</div>\
					</div>\
					<div class="clearfix"></div>';
					
					/*
					str_mssg += '<div  style="overflow-x:hidden;overflow-y:auto;height:600px;">';
						str_mssg += '<ul class="tabset list-unstyled" data-type="ajax">';
						$.each(json_data, function (key, data) {
							str_mssg += '<li>\
									<a>\
										<strong class="name">'+data.senderfullname+' </strong>\
										<time datetime="'+data.received_on+'">'+data.time+'</time>\
										<ul class="info-list list-unstyled">\
											<li>\
												<strong class="description">Message:</strong>\
												<span class="text"> '+data.message+'</span>\
											</li>\
										</ul>\
									</a>\
								</li>';
						});	
						str_mssg += '</ul>';
					str_mssg += '</div>';
					*/
					
					
					str_mssg += '<div class="clearfix"></div>';
					str_mssg += '<div class="container-fluid"><div class="message-wrap">  <div class="msg-wrap">';
						$.each(json_data, function (key, data) {
							if(data.sender == 'C')	{
								var user_id = data.id_user;
								var message_buttons = '';
							}
							else	{
								var user_id = data.id_buyer;
								if(data.is_system_message == 'N')	{
									var message_buttons = '\
											<span style="color:#afafaf">\
												<a onclick="editChatMessage(\''+data.id_inquiry+'\');" class="btn btn-primary btn-sm"> <i  class="fa fa-edit"></i> Edit </a>\
											</span>\
										';
								}
								else	{
									var message_buttons = '';
								}
							}
							str_mssg += '\
								<div class="media msg sys_mssg_'+data.is_system_message+' ">\
									<div class="media-body">\
										<span id="avatarsender-'+user_id+'" class="pull-left thumb-sm avatar m-r" style="margin-right:10px;">\
											<img class="icon img-circle" src="'+api_url+'image.php?id_user='+user_id+'&width=28&height=28" alt="'+data.senderalias+'" title="'+data.senderalias+'">\
										</span>\
										<div style="margin-left:35px;">\
											<small class="pull-right time" ><i class="fa fa-clock-o"></i> '+data.time+'</small>\
											<h5 class="media-heading"> <span> '+data.senderalias+' </span> </h5>\
											<span class="">';

												str_mssg += '<div id="div_mssg_label_'+data.id_inquiry+'">';
													str_mssg += '<strong class="description" style="color:#bfbfbf;"> Message : </strong>';
													str_mssg += '<span class="messgTxt_'+data.id_inquiry+'"> ' + data.message + '</span>' + message_buttons;
												str_mssg += '</div>';
												
												str_mssg += '\
															<div id="div_mssg_edit_'+data.id_inquiry+'" style="display:none; padding:5px 5px 10px; border: 1px solid #ddd;">\
																<div class="field-holder1">\
																	<form id="frmEditChatMessage_'+data.id_inquiry+'">\
																		<input value="'+data.id_inquiry+'" type="hidden" id="editChatIDInquiry" name="editChatIDInquiry" >\
																		<textarea name="message_'+data.id_inquiry+'" id="message_'+data.id_inquiry+'" class="form-control">'+data.message+'</textarea>\
																	</form>\
																</div>\
																<br>\
																<div>\
																	<a id="btnsaveChatMessage_'+data.id_inquiry+'" onclick="saveChatMessage(\''+data.id_inquiry+'\');" class="btn btn-primary">Save</a>\
																	<a onclick="closeChatMessage(\''+data.id_inquiry+'\');" class="btn add1 btn-default">Cancel</a>\
																	<span id="chat_message_loading_bar_'+data.id_inquiry+'"> </span>\
																</div>\
															</div>\
															';
												
												str_mssg += '\
											</span>\
										</div>\
									</div>\
								</div>\
								<div class="clearfix"></div>\
							';
						});	
					str_mssg += '</div></div></div>';
					
					
				$('#load_all_message_for_domain').html(str_mssg);
				$('#message').val('');
				$('#btn_sendInquiry').removeAttr('style');
				$('#btn_sendInquiry').removeAttr('disabled');
				return false;
				
			}
		});
	}
	return false;
}

function getPreviousOffers(id_parent)	{	
	$('#show_loading_bar').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:50px;">');
	$.get("/get_inquiry_message_by_idparent/"+id_parent, function( json ) {
		var json_data = $.parseJSON(json);
		
		if( json_data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			var str_mssg = '';
				str_mssg += '<div class="heading-bar" style="background:none;box-shadow:none;border-bottom:none;">\
					<div>\
						<strong class="name">Offers History</strong>\
						<span id="show_loading_bar"></span>\
						<a style="cursor:pointer;" onclick="getInquiryMessageByIDParent(\''+id_parent+'\')" class="setting-icon" alt="Reload Message History" title="Reload Message History"><i class="fa fa-refresh"></i></a>\
					</div>\
				</div>\
				<div class="clearfix"></div>\
				<div  style="overflow-x:hidden;overflow-y:auto;height:600px;">';
				
					str_mssg += '<ul class="tabset list-unstyled" data-type="ajax">';
					$.each(json_data, function (key, data) {
						str_mssg += '<li>\
								<a>\
									<strong class="name">'+data.senderfullname+' </strong>\
									<time datetime="'+data.received_on+'">'+data.time+'</time>\
									<ul class="info-list list-unstyled">\
										<li>\
											<strong class="description">Message:</strong>\
											<span class="text"> '+data.message+'</span>\
										</li>\
									</ul>\
								</a>\
							</li>';
					});	
					str_mssg += '</ul>';
				
				str_mssg += '</div>';
				
			$('#load_all_message_for_domain').html(str_mssg);
			return false;
		}
		
	});	
	return false;
}

function getInquiryMessageByIDParent(id_parent,parentoffer=null)	{
	console.log(parentoffer);
	$('#show_loading_bar').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:50px;">');
	//$.get("/get_inquiry_message_by_idparent/"+id_parent, function( json ) {
	$.get("/get_inquiry_message_by_idparent/"+id_parent, function( json ) {
		var json_data = $.parseJSON(json);
		
		if( json_data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			
			var page_title = 'Message';
			//if( json_data.offer != null )	{
			if( parentoffer != null )	{
				var page_title = 'Offer';
			}
			
			
			var str_mssg = '';
				str_mssg += '<div class="heading-bar" style="background:none;box-shadow:none;border-bottom:none;">\
					<div>\
						<strong class="name">'+page_title+' History</strong>\
						<span id="show_loading_bar"></span>';
				
				if( parentoffer != null )	{
					//str_mssg += '		<a style="cursor:pointer;" onclick="getInquiryMessageByIDParent(\''+id_parent+'\','+parentoffer+')" class="setting-icon" alt="Reload Message History" title="Reload Message History"><i class="fa fa-refresh"></i></a>';
					str_mssg += '		<a onclick="$(\'#overflow_message_history\').toggle(\'slow\');" style="cursor:pointer;margin-right:5px"  class="masterTooltip setting-icon" alt="Hide/View Offer History" title="Hide/View Offers History"><i class="fa fa-arrow-circle-o-down"></i></a>';
					str_mssg += '		<a style="cursor:pointer;" onclick="getLatestMessageByDomain(\''+$('#for_mco_domain').val()+'\')"  class="masterTooltip setting-icon" alt="Reload Offer History" title="Reload Offers History"><i class="fa fa-refresh"></i></a>';
					str_mssg += '		<a style="cursor:pointer;margin-right:5px;" onclick="$(\'.sys_mssg_Y\').toggle(\'slow\')"  class="masterTooltip setting-icon" alt="Show/Hide System Message" title="Show/Hide System Message"> <i class="glyphicon glyphicon-erase"></i> </a>';
				}
				else	{
					str_mssg += '		<a style="cursor:pointer;" onclick="getLatestMessageByDomain(\''+$('#for_mco_domain').val()+'\')" class="masterTooltip setting-icon" alt="Reload Message History" title="Reload Message History"><i class="fa fa-refresh"></i></a>';
					str_mssg += '		<a style="cursor:pointer;margin-right:5px;" onclick="$(\'.sys_mssg_Y\').toggle(\'slow\')" class="masterTooltip setting-icon" alt="Show/Hide System Message" title="Show/Hide System Message"> <i class="glyphicon glyphicon-erase"></i> </a>';
				}
				
				
				
				str_mssg += '	</div>\
				</div>\
				<div class="clearfix"></div>';
				
				/*
				str_mssg += '<div id="overflow_message_history" style="overflow-x:hidden;overflow-y:auto;height:600px;">';
				
					str_mssg += '<ul class="tabset list-unstyled" data-type="ajax">';
					
					var cnt_msg = 0;
					$.each(json_data, function (key, data) {
						console.log( data );
						
						var str_accepted = '';
						var str_border = '';
						
						var final_offer_price = '';
						var final_offer_datetime = '';
						var final_offer_message = '';
						var final_offer_decidedby = '';
						
						if( data.decided_by != data.sender && data.decided_by != null )	{
							cnt_msg++;
							console.log( data.decided_by + ' - ' + data.sender);
						}
						
						if( parentoffer != null && cnt_msg == 1 && (data.status == 3 || data.status == 4) && data.decided_by != null )	{							
							str_accepted = '<span class="btn btn-primary btn-large" disabled="disabled"> <i class="fa fa-thumbs-up"></i> Accepted </span>';
							str_border = 'style="border:1px solid #71be24; border-radius:5px;"';
							
							final_offer_price = ' id="final_offer_price" ';
							final_offer_datetime = ' id="final_offer_datetime" ';
							final_offer_message = ' id="final_offer_message" ';
							final_offer_decidedby = ' id="final_offer_decidedby" ';
						}

						
						if( parentoffer != null && cnt_msg == 1 && (data.status == 6) && data.decided_by != null )	{
							str_accepted = '<span class="btn btn-danger btn-large" disabled="disabled"> <i class="fa fa-thumbs-down"></i> Declined </span>';
							str_border = 'style="border:1px solid #d21724; border-radius:5px;"';
							
							final_offer_price = ' id="final_offer_price" ';
							final_offer_datetime = ' id="final_offer_datetime" ';
							final_offer_message = ' id="final_offer_message" ';
							final_offer_decidedby = ' id="final_offer_decidedby" ';
						}

						if( data.decided_by == data.sender && data.decided_by != null )	{
							str_accepted = '';
							str_border = '';
						}
						
						
						str_mssg += '<li '+str_border+'>\
								<a>\
									<strong class="name" style="font-size:13px; !important;"> \
										<span  '+final_offer_decidedby+' > '+data.senderfullname+' </span> '+str_accepted+'</strong>\
									<time '+final_offer_datetime+' datetime="'+data.received_on+'"> <i class="fa fa-clock-o"></i> '+data.time+'</time>\
									<ul class="info-list list-unstyled">';
							
						if( parentoffer != null )	{
							str_mssg += '	<li>\
												<strong style="font-size:13px; !important;" class="description">Offer:</strong>\
												<span style="font-size:13px;" class="text" '+final_offer_price+' > '+data.offer+'</span>\
											</li>';
						}
						
						str_mssg += '	<li>\
											<strong style="font-size:13px; !important;" class="description">Message:</strong>\
											<span style="font-size:13px;" class="text" '+final_offer_message+' > '+data.message+'</span>\
										</li>\
									</ul>\
								</a>\
							</li>';
					});	
					
					
					str_mssg += '</ul>';
					
				str_mssg += '</div>';
				*/
					var cnt_json_data = 0;
					$.each(json_data, function (key, data) {
						if( data.decided_by != data.sender && data.decided_by != null )	{
							cnt_json_data++;
						}
					});	
					console.log('cnt_json_data' + cnt_json_data);
					
					str_mssg += '<div class="clearfix"></div>';
					str_mssg += '<div class="container-fluid"><div class="message-wrap" id="overflow_message_history">  <div class="msg-wrap">';
					var cnt_msg = 0;
					$.each(json_data, function (key, data) {
						console.log( data );
						
						var str_accepted = '';
						var str_border = '';
						
						var final_offer_price = '';
						var final_offer_datetime = '';
						var final_offer_message = '';
						var final_offer_decidedby = '';
						
						if( data.decided_by != data.sender && data.decided_by != null )	{
							cnt_msg++;
							console.log( data.decided_by + ' - ' + data.sender);
						}
						//if( parentoffer != null && cnt_msg == 1 && (data.status == 3 || data.status == 4) && data.decided_by != null )	{							
						if( parentoffer != null && cnt_msg == cnt_json_data && (data.status == 3 || data.status == 4) && data.decided_by != null )	{							
							str_accepted = '<span class="btn btn-primary btn-large" disabled="disabled"> <i class="fa fa-thumbs-up"></i> Accepted </span>';
							str_border = 'style="border:1px solid #71be24; border-radius:5px;"';
							
							final_offer_price = ' id="final_offer_price" ';
							final_offer_datetime = ' id="final_offer_datetime" ';
							final_offer_message = ' id="final_offer_message" ';
							final_offer_decidedby = ' id="final_offer_decidedby" ';
						}
						//if( parentoffer != null && cnt_msg == 1 && (data.status == 6) && data.decided_by != null )	{
						if( parentoffer != null && cnt_msg == cnt_json_data && (data.status == 6) && data.decided_by != null )	{
							str_accepted = '<span class="btn btn-danger btn-large" disabled="disabled"> <i class="fa fa-thumbs-down"></i> Declined </span>';
							str_border = 'style="border:1px solid #d21724; border-radius:5px;"';
							
							final_offer_price = ' id="final_offer_price" ';
							final_offer_datetime = ' id="final_offer_datetime" ';
							final_offer_message = ' id="final_offer_message" ';
							final_offer_decidedby = ' id="final_offer_decidedby" ';
						}
						if( data.decided_by == data.sender && data.decided_by != null )	{
							str_accepted = '';
							str_border = '';
						}
						if(data.sender == 'C')	{
							var user_id = data.id_user;
							var message_buttons = '';
						}
						else	{
							var user_id = data.id_buyer;
							if(data.is_system_message == 'N')	{
								var message_buttons = '\
										<span style="color:#afafaf">\
											<a onclick="editChatMessage(\''+data.id_inquiry+'\');" class="btn btn-primary btn-sm"> <i  class="fa fa-edit"></i> Edit </a>\
										</span>\
									';
							}
							else	{
								var message_buttons = '';
							}
						}
						str_mssg += '\
							<div class="media msg sys_mssg_'+data.is_system_message+' " '+str_border+'>\
								<div class="media-body">\
									<span id="avatarsender-'+user_id+'" class="pull-left thumb-sm avatar m-r" style="margin-right:10px;">\
										<img class="icon img-circle" src="'+api_url+'image.php?id_user='+user_id+'&width=28&height=28" alt="'+data.senderalias+'" title="'+data.senderalias+'">\
									</span>\
									<div style="margin-left:35px;">\
										<small class="pull-right time" '+final_offer_datetime+' ><i class="fa fa-clock-o"></i> '+data.time+'</small>\
										<h5 class="media-heading"> <span '+final_offer_decidedby+'> '+data.senderalias+' </span> '+str_accepted+' </h5>\
										<span class="">';
										
											if( parentoffer != null )	{
												str_mssg += '	\
																	<strong class="description" style="color:#bfbfbf;"> Offer : </strong>\
																	<span class="text" '+final_offer_price+'> '+data.offer+'</span>\
																	<br>\
																';
											}

											str_mssg += '<div id="div_mssg_label_'+data.id_inquiry+'">';
												str_mssg += '<strong class="description" style="color:#bfbfbf;"> Message : </strong>';
												str_mssg += '<span '+final_offer_message+' class="messgTxt_'+data.id_inquiry+'"> ' + data.message + '</span>' + message_buttons;
											str_mssg += '</div>';
											
											str_mssg += '\
														<div id="div_mssg_edit_'+data.id_inquiry+'" style="display:none; padding:5px 5px 10px; border: 1px solid #ddd;">\
															<div class="field-holder1">\
																<form id="frmEditChatMessage_'+data.id_inquiry+'">\
																	<input value="'+data.id_inquiry+'" type="hidden" id="editChatIDInquiry" name="editChatIDInquiry" >\
																	<input name="id_inquiry-'+data.id_inquiry+'" id="id_inquiry-'+data.id_inquiry+'" value="'+data.id_inquiry+'" type="hidden">\
																	<input name="id_user-'+data.id_inquiry+'" id="id_user-'+data.id_inquiry+'" value="'+data.id_user+'" type="hidden">\
																	<input name="id_buyer-'+data.id_inquiry+'" id="id_buyer-'+data.id_inquiry+'" value="'+data.id_buyer+'" type="hidden">\
																	<input name="id_parent-'+data.id_inquiry+'" id="id_parent-'+data.id_inquiry+'" value="'+data.id_parent+'" type="hidden">\
																	<input name="domain-'+data.id_inquiry+'" id="domain-'+data.id_inquiry+'" value="'+data.domain+'" type="hidden">\
																	<input name="sender-'+data.id_inquiry+'" id="sender-'+data.id_inquiry+'" value="B" type="hidden">\
																	<input name="email-'+data.id_inquiry+'" id="email-'+data.id_inquiry+'" value="'+data.email+'" type="hidden">\
																	<textarea name="message_'+data.id_inquiry+'" id="message_'+data.id_inquiry+'" class="form-control">'+data.message+'</textarea>\
																	<input name="oldmessage-'+data.id_inquiry+'" id="oldmessage-'+data.id_inquiry+'" value="'+data.message+'" type="hidden">\
																</form>\
															</div>\
															<br>\
															<div>\
																<a id="btnsaveChatMessage_'+data.id_inquiry+'" onclick="saveChatMessage(\''+data.id_inquiry+'\');" class="btn btn-primary">Save</a>\
																<a onclick="closeChatMessage(\''+data.id_inquiry+'\');" class="btn add1 btn-default">Cancel</a>\
																<span id="chat_message_loading_bar_'+data.id_inquiry+'"> </span>\
															</div>\
														</div>\
														';
											
											str_mssg += '\
										</span>\
									</div>\
								</div>\
							</div>\
							<div class="clearfix"></div>\
						';
					});	
					str_mssg += '</div></div></div>';
					
				
				
			$('#load_all_message_for_domain').html(str_mssg);
			runMasterToolTip();
			return false;
			
		}
		
	});	
	return false;
}

function editChatMessage(id_inquiry)	{
	$('#div_mssg_label_'+id_inquiry).hide('slow');
	$('#div_mssg_edit_'+id_inquiry).show('slow');
}

function closeChatMessage(id_inquiry)	{
	$('#div_mssg_label_'+id_inquiry).show('slow');
	$('#div_mssg_edit_'+id_inquiry).hide('slow');
}

function saveChatMessage(id_inquiry)	{
	//console.log( $( "#frmEditChatMessage_"+id_inquiry ).serialize() );
	if( $.trim($('#message_'+id_inquiry).val()) == '' )		{
		alert('Please provde a valid message');
		$('#message_'+id_inquiry).attr('style','border-color:#d9534f;');
		$('#message_'+id_inquiry).focus();
	}
	else	{
		$('#message_'+id_inquiry).attr('style','border-color:none;');

		$('#chat_message_loading_bar_'+id_inquiry).html('<img src="/images/ajax-loader-bar.gif" style="margin-left:10px;">');
		$('#btnsaveChatMessage_'+id_inquiry).attr('style','opacity:0.3');
		$('#btnsaveChatMessage_'+id_inquiry).attr('disabled','disabled');
	
		$.post( "/save_chat_message", $( "#frmEditChatMessage_"+id_inquiry ).serialize() ).done(function( json ) {
			var json_data = $.parseJSON(json);
			console.log(json_data);

			if( json_data == 'SessionExpire' )	{
				alert('Your session already expire. Click OK and you will be redirected to the main page.');
				window.location.href = "/";
			}
			else	{
				$('#chat_message_loading_bar_'+id_inquiry).html('');
				$('#btnsaveChatMessage_'+id_inquiry).removeAttr('style');
				$('#btnsaveChatMessage_'+id_inquiry).removeAttr('disabled');
		
				$('.messgTxt_'+id_inquiry).text(json_data.data.message);
				closeChatMessage(id_inquiry);
				return false;
				
			}
		});
	}
}


function getMessages(friendly)	{	
	$('#msglist').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/get_order_messages", function( json ) {
		var json_data = $.parseJSON(json);
		
		if( json_data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
		
			var strhtml = '';
			var stylelink = '';
			var cnt = 0;
			var latest_inq_domain = '';
			if (json_data.inquiries !== undefined)	{

				console.log( json_data );
				
				$.each(json_data.inquiries, function (key, data) {
					cnt++;
					if( cnt == 1)	{	
						stylelink = 'class="active"';	
						latest_inq_domain = data.domain;
					}
					else	{	
						stylelink = '';	
					}
					
					var btnlabeled = '';
					if( data.status == 6 )	{
						btnlabeled = '<span disabled="disabled" class="btn btn-danger btn-xs"> <i class="fa fa-thumbs-down"></i> Declined </span>';
					}
					if( data.status == 3 || data.status == 4 )	{
						btnlabeled = '<span disabled="disabled" class="btn btn-primary btn-xs"> <i class="fa fa-thumbs-up"></i> Accepted </span>';
					}
					
					var domainLabel = data.domain.replace('.', '_');
					strhtml += '\
						<li onclick="getLatestMessageByDomain(\''+data.domain+'\',\''+cnt+'\')">\
							<a href="#tab6" id="msgcnt'+cnt+'" '+stylelink+'>\
								<strong class="name">'+data.seller_alias+' ';
								strhtml += '<span id="marker_'+domainLabel+'">';
									if( data.unread > 0 )	{
										strhtml += '<span class="unreadmessage_marker"> <span id="marker_count_'+domainLabel+'">'+data.unread+'</span> new message</span>';
									}
								strhtml += '</span>';
					strhtml += '\
								</strong>\
								<time datetime="'+data.received_on+'"> <i class="fa fa-clock-o"></i> '+data.time+'</time>\
								<ul class="info-list list-unstyled">\
									<li>\
										<strong class="description">Domain:</strong>\
										<span class="text mail">'+data.domain+'</span>\
									</li>';
						if( data.price > 0 )	{
							strhtml += '\
											<li>\
												<strong class="description">Price:</strong>\
												<span class="text">'+data.price+'</span>\
											</li>';
											
						}
						else	{
							strhtml += '\
									<li>\
										<strong class="description">Recent Seller Offer:</strong>\
										<span class="text"> ' + data.offer;
										
										if( data.decided_by == 'B' )	{
											strhtml += btnlabeled;
										}
										
							strhtml += '\
										</span>\
									</li>\
									<li>\
										<strong class="description">My Recent Offer:</strong>\
										<span class="text">'+data.buyer_offer;
										
										if( data.decided_by == 'C' )	{
											strhtml += btnlabeled;
										}
										
							strhtml += '\
										</span>\
									</li>';
						}
						
					var formatted_message;
					if( data.message.length < 20 )	{
						formatted_message = data.message;
					}
					else	{
						formatted_message = data.message.substr(0, 70) + ' ...';
					}
					
					strhtml += '\
									<li>\
										<strong class="description">Type:</strong>\
										<span class="text">'+data.type+'</span>\
									</li>\
									<li>\
										<strong class="description">Message:</strong>\
										<span class="text"> '+formatted_message+' </span>\
									</li>\
								</ul>\
							</a>\
						</li>\
						';
				});
			}
			else	{
				strhtml += '<li><div align="center">No Messages Found</div></li>';
				$('#load_detail_message').html('');
				$('#load_all_message_for_domain').html('');
			}
			$('#msglist').html(strhtml);
			
			//if( $('#latest_inq_domain').val() != '' )	{
				//getLatestMessageByDomain( $('#latest_inq_domain').val() );
			//}
			
			if( latest_inq_domain != '' )	{
				$('#latest_inq_domain').val( latest_inq_domain );
				getLatestMessageByDomain( latest_inq_domain );
			}
			
			//$('#load_messages').html(data);
			return false;
			
		}
			
	});	
	getUnreadMessages(id_buyer);	// to make sure all the domain count is updated right after viewing the message
	return false;
}

function setActiveMessageList(id)	{
	$('#msglist li a').removeAttr('class');
	$('#msgcnt'+id).attr('class','active');
}

function getMyProfile(buyer_id)	{
	$('#result_account_settings_message').html('');
	$('#loading_bar_accnt_info').html('<img src="/images/ajax-loader-bar.gif" style="margin-left:50px;">');
	$.get("/get_my_profile/"+buyer_id, function( json ) {
		var json_data = $.parseJSON(json);
		console.log(json_data);
		
		if( json_data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			
			$('#first_name').val(json_data.first_name);
			$('#last_name').val(json_data.last_name);
			$('#organization').val(json_data.organization);
			$('#country').val(json_data.country);
			$('#hiddenCountry').val(json_data.country);
			$('#zip').val(json_data.zip);
			$('#city').val(json_data.city);
			$('#state').val(json_data.state);
			$('#hiddenState').val(json_data.state);
			$('#workphone').val(json_data.workphone);
			$('#homephone').val(json_data.homephone);
			$('#mobilephone').val(json_data.mobilephone);
			$('#fax').val(json_data.fax);
			
			if( json_data.birthdate != undefined || json_data.birthdate != null )	{
				var bdate = json_data.birthdate.split('-');
				var newbdate = bdate[2]+'/'+bdate[1]+'/'+bdate[0];
				$('#birthdate').val(newbdate);
			}
			
			$('#gender_'+json_data.gender).prop('checked', true);
			$('#membership_type_'+json_data.membership_type).prop('checked', true);
			$('#currency_pref_'+json_data.currency_pref).prop('checked', true);
			$('#monthly_summary_'+json_data.monthly_summary).prop('checked', true);
			$('#promotional_emails_'+json_data.promotional_emails).prop('checked', true);
			$('#offer_rel_purchase_'+json_data.offer_rel_purchase).prop('checked', true);
			$('#contact_via_us_mail_'+json_data.contact_via_us_mail).prop('checked', true);
			$('#email_pref_'+json_data.email_pref).prop('checked', true);
			$('#lang_pref_'+json_data.lang_pref).prop('checked', true);


			if( json_data.country != undefined || json_data.country != null )	{
				$("#form-account-area").dynamic_location({
					country_filter: ['us','ca','au','nz'],
					default_country: json_data.country,
					default_state: json_data.state,
					default_zip: ""
				},geo_data);
			}
			
			initCustomForms();
			
			$('#loading_bar_accnt_info').html('');
			return false;
			
		}
		
	});
	
	return false;
}

function saveAccountSettings()	{	
	$('#loading_bar_accnt_info').html('<img src="/images/ajax-loader-bar.gif">');
	$('#loading_bar_accnt_info_bottom').html('<div align="center"><img src="/images/ajax-loader-bar.gif"></div><br>');
	$('#btnsaveAccountSettings').attr('disabled','disabled');
	$.post( "/save_account_settings", $( "#form-account-area" ).serialize() ).done(function( json ) {
		var json_data = $.parseJSON(json);
		console.log(json_data);

		if( json_data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			$('#result_account_settings_message').html('<div><span class="status-message text-center bg-success text-success col-lg-12">Account Settings Successfully Saved <a onclick="$(this).parent().parent().remove();" style="cursor:pointer;float:right;" class="setting-icon"><i class="icon-close"></i></a></span></div>');		
			$('#loading_bar_accnt_info').html('');
			$('#loading_bar_accnt_info_bottom').html('');
			$('#btnsaveAccountSettings').removeAttr('disabled');
			return false;
		}
			
	});
	return false;
}

function saveNewPassword()	{	
	if( $.trim($('#password').val()) == '' )		{
		alert('Please provide a valid new password.');
		$('#password').attr('style','border-color:#d9534f;');
		$('#password').focus();
		$('#cnewpassword').attr('style','border-color:none;');
	}
	else if( $.trim($('#cnewpassword').val()) == '' )		{
		alert('Please provide a valid confirm password.');
		$('#cnewpassword').attr('style','border-color:#d9534f;');
		$('#cnewpassword').focus();
		$('#password').attr('style','border-color:none;');
	}
	else if( $.trim($('#password').val()) != $.trim($('#cnewpassword').val()) )		{
		alert('New password doest not match with the confirm password.');
		$('#password').attr('style','border-color:#d9534f;');
		$('#password').focus();
		$('#cnewpassword').attr('style','border-color:none;');
	}
	else	{
		$('#password').attr('style','border-color:none;');
		$('#cnewpassword').attr('style','border-color:none;');
		$('#loading_bar_change_password').html('<img src="/images/ajax-loader-bar.gif">');
		$('#btnSaveNewPassword').attr('disabled','disabled');
		$.post( "/save_new_password", $( "#frmChangePassword" ).serialize() ).done(function( json ) {
			var json_data = $.parseJSON(json);
			console.log(json_data);
			
			if( json_data == 'SessionExpire' )	{
				alert('Your session already expire. Click OK and you will be redirected to the main page.');
				window.location.href = "/";
			}
			else	{	
				var bg_txt = '';
				if( json_data.result == 'SUCCESS' )	{
					bg_txt = 'success';
				}
				else	{
					bg_txt = 'danger';
				}
				
				$('#result_change_password_message').html('<div><span class="status-message text-center bg-'+bg_txt+' text-'+bg_txt+' col-lg-12 btn-lg" style="font-size:14px;"> <b>'+json_data.response_text+'</b> : '+json_data.response_details+' <a onclick="$(this).parent().parent().remove();" style="cursor:pointer;float:right;" class="setting-icon"><i class="icon-close"></i></a></span></div>');		
				$('#loading_bar_change_password').html('');
				$('#btnSaveNewPassword').removeAttr('disabled');
			}
			
		});
	}
	return false;
}

/**** start Forgot Password ********/
function forgotPassword()	{	
	if( !ValidateEmail( $('#email_fp').val() )  )	{
		//alert('Please provide a valid email.');
		$(".notifyjs-wrapper").click();
		notifyUI('email_fp', 'Please provide a valid email.','error',false);
		$('#email_fp').attr('style','border-color:#d9534f;');
		$('#email_fp').focus();
	}
	else	{
		$(".notifyjs-wrapper").click();
		$('#email_fp').attr('style','border-color:none;');
		$('#loading_bar_forgot_password').html('<img src="/images/ajax-loader-bar.gif">');
		$('#btnForgotPassword').attr('disabled','disabled');
		$.post( "/forgot_password", $( "#frmForgotPassword" ).serialize() ).done(function( json ) {
			var json_data = $.parseJSON(json);
			console.log(json_data);
			
			var bg_txt = '';
			if( json_data.result == 'SUCCESS' )	{
				bg_txt = 'success';
			}
			else	{
				bg_txt = 'danger';
			}
			
			$('#result_forgot_password_message').html('<div><span class="status-message text-center bg-'+bg_txt+' text-'+bg_txt+' col-lg-12 btn-lg" style="font-size:14px;">'+json_data.response_details+' <a onclick="$(this).parent().parent().remove();" style="cursor:pointer;float:right;" class="setting-icon"><i class="icon-close"></i></a></span></div>');		
			$('#loading_bar_forgot_password').html('');
			$('#btnForgotPassword').removeAttr('disabled');
			
		});
	}
	return false;
}

$("#email_fp").keyup(function(e){ 
	var code = e.which;
	if(code==13)	{
		e.preventDefault();
		$('#btnForgotPassword').click();
	}
});
/**** end Forgot Password ********/

function applyFavBulkAction()	{
	if( $('#bulkdeletefav').val() == '' ) 	{
		alert('Please select a bulk action');
	}
	else	{
		if( $('#bulkdeletefav').val() == 'delete' ) 	{ 		
			var domainIDS = []
			$('.checkboxDomainID').each(function(i, e)	{
				 if($(e).is(":checked")) {
					  domainIDS.push($(e).attr('value'));
				 }
			});

			if( domainIDS.length < 1 )	{
				alert('Please select from the list.');
			}
			else	{
				if(confirm("Are you sure you want to remove this domains from your Favorites?"))	{
					$('#loading_bar_favorite').html('<img src="/images/ajax-loader-bar.gif">');
					$('#loadingbar_pagination').html('<img src="/images/ajax-loader-bar.gif"><br><br>');
					$('#btnapplyFavBulkAction').attr('disabled','disabled');
					//$('#load_favorites').html('<div align="center"><img src="/images/loading.gif"></div>');
					$.post( "/bulk_delete_favorites_domain", $( "#form-favorite-table-list" ).serialize() ).done(function( data )	{
						
						if( data == 'SessionExpire' )	{
							alert('Your session already expire. Click OK and you will be redirected to the main page.');
							window.location.href = "/";
						}
						else	{
							$('#loading_bar_favorite').html('');
							//$('#load_favorites').html(data);
							
							getMyFavorites(0,data);
							
							//$('#result_fav_domains_message').html('<div><span class="status-message text-center bg-success text-success col-lg-12">Favorite Domains Successfully Deleted <a onclick="$(this).parent().parent().remove();" style="cursor:pointer;float:right;" class="setting-icon"><i class="icon-close"></i></a></span></div>');
							$('#btnapplyFavBulkAction').removeAttr('disabled');
						}
							
					});
				}
			}
		}
	}
	return false;
}

function searchByFavoriteTab(val)	{
	$('#loading_bar_favorite').html('<img src="/images/ajax-loader-bar.gif">');
	$('#btnapplyFavBulkAction').attr('disabled','disabled');
	$('#load_favorites').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/search_favorite_domain_by_tab/"+val, function( data )	{
		if( data == 'SessionExpire' )	{
			alert('Your session already expire. Click OK and you will be redirected to the main page.');
			window.location.href = "/";
		}
		else	{
			$('#loading_bar_favorite').html('');
			$('#load_favorites').html(data);
			$('#btnapplyFavBulkAction').removeAttr('disabled');
		}
	});
	return false;
}

function viewStatusInfoMain(status_code, transactionID, escrow_status)	{
	var title = '';
	var content = '';
	if( status_code == 0 )	{
		title = 'Cancelled';
		content = '<p>Seller have cancelled the transaction. Login to your Escrow account to check the reason why the Seller cancelled this transaction <b>#'+transactionID+'</b>.</p>';
	}
	else if( status_code == 5 )	{
		title = 'Pending';
		content = '<p>Seller haven\'t check or review your inquiry transaction <b>#'+transactionID+'</b>. You can send a reminder by logging-in to your Escrow account.</p>';
	}
	else if( status_code == 15 )	{
		title = 'Pay Now !';
		content = '<p>You can now pay the domain you have enquire. Login to your Escrow account to select a payment method. <b> Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 20 )	{
		title = 'Payment Submitted';
		content = '<p>You have already submitted a payment. Please wait while Escrow securing your payment. <b> Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 25 )	{
		title = 'Payment Accepted';
		content = '<p>Seller have securely received your payment. Seller will release your domain as soon as possible. You can send a message or email to send a reminder. <b>Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 85 )	{
		title = 'Transaction Completed';
		content = '<p>Seller have received your payment and successfully relased your domain to your end. <b>Transaction #'+transactionID+'</b>. </p>';
	}
	else	{
		title = escrow_status;
		content = '<p>You can view the details of this transaction. Login to your Escrow account to check this transaction <b>#'+transactionID+'</b>.</p>';
	}
	
	content += '<div style="color:#6f7c96;font-size:12px;">';
	content += '<br> Transaction ID : ' + transactionID;
	content += '<br> Status : ' + escrow_status;
	content += '</div>';
	
	$('#order_status_title').html(title);
	$('#order_status_content').html(content);
	$('#statusModal').modal();
}

function viewFinalOfferDetail()	{
	var title = 'Final Offer Info';
	var content = '';

	var decided_by = $('#for_mco_decided_by').val();
	if( decided_by == 'C' )	{
		var acceptedby = 'Seller';
		var fullname = $('.name-holder').text();
		var sender = 'Me';
	}
	else	{
		var acceptedby = 'Buyer';
		var fullname = $('#buyer_name').text();
		var sender = 'Seller';
	}
	
	content += '<div>\
					<div class="row">\
						<div class="col-md-3" style="color:#6f7c96;"> Sender </div> <div class="col-md-9"> ' + $('#final_offer_decidedby').text() +' </div>\
						<div class="col-md-3" style="color:#6f7c96;"> Price </div> <div class="col-md-9"> ' + $('#final_offer_price').text() +' </div>\
						<div class="col-md-3" style="color:#6f7c96;"> Time Sent </div> <div class="col-md-9"> ' + $('#final_offer_datetime').text() +' </div>\
						<div class="col-md-3" style="color:#6f7c96;"> Message </div> <div class="col-md-9"> ' + $('#final_offer_message').text() +' </div>\
					</div>\
					<hr>\
					<div class="row">\
						<div class="col-md-3" style="color:#6f7c96;"> Accepted By </div> <div class="col-md-9"> ' + acceptedby +' </div>\
						<div class="col-md-3" style="color:#6f7c96;"> Fullname </div> <div class="col-md-9"> ' + fullname +' </div>\
					</div>\
				</div>\
	';
	
	$('#general_title').html(title);
	$('#general_content').html(content);
	$('#generalModal').modal();
}

$(document).ready(function() {
	var interval;

	function applyAjaxFileUpload(element) {
		$(element).AjaxFileUpload({
			action: "/upload_avatar",
			onChange: function(filename) {
				$('#loading_image_upload_avatar').html('<img src="/images/ajax-loader-bar.gif">');
				// Create a span element to notify the user of an upload in progress
				/*
				var $span = $("<span />")
					.attr("class", $(this).attr("id"))
					.text("Uploading")
					.insertAfter($(this));
				
				//$(this).remove();

				interval = window.setInterval(function() {
					var text = $span.text();
					if (text.length < 13) {
						$span.text(text + ".");
					} else {
						$span.text("Uploading");
					}
				}, 200);
				*/
			},
			onSubmit: function(filename) {
				// Return false here to cancel the upload
				/*var $fileInput = $("<input />")
					.attr({
						type: "file",
						name: $(this).attr("name"),
						id: $(this).attr("id")
					});

				$("span." + $(this).attr("id")).replaceWith($fileInput);

				applyAjaxFileUpload($fileInput);

				return false;*/

				// Return key-value pair to be sent along with the file
				return true;
			},
			onComplete: function(filename, response) {

				console.log( response );
				console.log( filename );
			
				$('#loading_image_upload_avatar').html('');
				
				//$('#avatar_image').attr('src', api_url+'/image.php?id_user='+id_buyer);
				//$('#avatar_image').html('<img src="'+api_url+'/image.php?id_user='+id_buyer+'" >');
				
				
				$('#avatar_image').html('');
				
				
				
				var res_message = '';
				if (typeof(response.error) === "string") {
					//$span.replaceWith($fileInput);
					//applyAjaxFileUpload($fileInput);
					//alert(response.error);
					res_message = '<p class="status-message text-center bg-danger text-danger col-lg-12">'+response.error+'<a onclick="$(this).parent().parent().remove();" style="cursor:pointer;float:right;" class="setting-icon"><i class="icon-close"></i></a></p>';
				}
				
				else	{
					res_message = '<p class="status-message text-center bg-success text-success col-lg-12">Avatar successfully uploaded <a onclick="$(this).parent().parent().remove();" style="cursor:pointer;float:right;" class="setting-icon"><i class="icon-close"></i></a></p>';
				}
				
				$('#loading_image_upload_avatar').html(res_message);
				
				//setTimeout(function()	{	
					var timestamp = new Date().getTime();
					$('#avatar_image').html('');
					$('#avatar_image').html('<img src="'+api_url+'image.php?id_user='+id_user+'&width=150&height=150&timestamp='+timestamp+'" >').fadeIn();
					$('#avatar_image').html('<img src="'+api_url+'image.php?id_user='+id_buyer+'&width=150&height=150&timestamp='+timestamp+'" >').fadeIn();
					console.log('<img src="'+api_url+'image.php?id_user='+id_buyer+'&width=150&height=150&timestamp='+timestamp+'" >');
				//}, 3000);
				
				$('.jcf-file .jcf-fake-input').html('Attach file');
			}
		});
	}

	applyAjaxFileUpload("#uploadAvatarInput");
});

function makeOffer()	{
	$('#buy_now_result').html('');
	if( !ValidateEmail( $('#enq-email').val() )  )	{
		$(".notifyjs-wrapper").click();
		notifyUI('enq-email', 'Please provide a valid email.','error',false);
		$('#enq-email').attr('style','border-color:#d9534f;');
		$('#enq-email').focus();
		$('#enq-message').attr('style','border-color:none;');
		$('#enq-domainOffer').attr('style','border-color:none;');
	}
	else if( (parseInt($('#enq-domainOffer').val()) <= 0 ) || isNaN(parseInt($('#enq-domainOffer').val())) == true ) 	{
		$(".notifyjs-wrapper").click();
		notifyUI('enq-domainOffer', 'Offer price should be greater than 0.','error',false);
		$('#enq-domainOffer').attr('style','border-color:#d9534f;');
		$('#enq-domainOffer').focus();
		$('#enq-email').attr('style','border-color:none;');
		$('#enq-message').attr('style','border-color:none;');
	}
	else	{
		$('#enq-email').attr('style','border-color:#none;');
		$('#enq-domainOffer').attr('style','border-color:#none;');
		$('#enq-message').attr('style','border-color:#none;');

		$('#btn_span_loading_offr').html('<img src="/images/ajax-loader-bar.gif">');
		$('#send-enquiry1').attr('disabled','disabled');
		$.post("/makeoffer", $("#enquiryForm").serialize()).done(function(json){
			console.log(json);
			
			var r = $.parseJSON(json);
			if( r.result != undefined )	{
				if( r.result.toLowerCase().trim() == 'error' )	{
					//alert( r.result + ' : ' + r.response_details );
					var message = r.result + ' : ' + r.response_details;
					$(".notifyjs-wrapper").click();
					notifyUI('buy_now_body', message, 'error', false, pos='top-center', false);
					//$('#buy_now_result').html('<p class="bg-danger">'+message+'</p>');
				}
				else	{
					$(".notifyjs-wrapper").click();
					//notifyUI('buy_now_body', r.response_details, 'success', false, pos='top-left', false);
					$('#buy_now_result').html('<p class="bg-success text-success" style="padding:5px;text-align:center;font-weight:bold;"><i class="glyphicon glyphicon-ok-circle"></i> '+r.response_details+'</p>');
				}
			}
			
			$('#btn_span_loading_offr').html('');
			$('#send-enquiry1').removeAttr('disabled');
		});
	}
	return false;
}

function sendEnquiryTwo()	{
	$('#billing_result').html('');
	if( !ValidateEmail( $('#enq-email2').val() )  )	{
		//alert('Please provide a valid email.');
		$(".notifyjs-wrapper").click();
		notifyUI('enq-email2', 'Please provide a valid email.','error',false);
		$('#enq-email2').attr('style','border-color:#d9534f;');
		$('#enq-email2').focus();
	}
	else if( $('#term_dom_reg').prop('checked') == false )	{
		//alert('You are required to agree the terms of Domain Registration Agreement.');
		$(".notifyjs-wrapper").click();
		notifyUI('terms-list', 'Agree the Domain Registration Agreement?','error', false,  'top-left', false);
		$('#enq-email2').attr('style','border-color:#none;');
	}
	else if( $('#term_universal').prop('checked') == false )	{
		//alert('You are required to agree the terms of Universal Terms of Service.');
		$(".notifyjs-wrapper").click();
		notifyUI('terms-list', 'Agree the Universal Terms of Service?','error', false,  'top-left', false);
		$('#enq-email2').attr('style','border-color:#none;');
	}
	else if( $('#term_refund').prop('checked') == false )	{
		//alert('You are required to agree the terms of Refund Policy.');
		$(".notifyjs-wrapper").click();
		notifyUI('terms-list', 'Agree the Refund Policy?','error', false,  'top-left', false);
		$('#enq-email2').attr('style','border-color:#none;');
	}
	else	{
		$(".notifyjs-wrapper").click();
		$('#enq-email2').attr('style','border-color:#none;');
		
		$('#btn_span_loading').html('<img src="/images/loading.gif">');
		$('#btn_confirm').attr('disabled','disabled');
		$.post("/submitpayment", $("#frmBilling").serialize()).done(function(json){
			console.log(json);
			var r = $.parseJSON(json);
			if( r.result != undefined )	{
				if( r.result.toLowerCase().trim() == 'error' )	{
					//alert( r.result + ' : ' + r.response_details );
					notifyUI('terms-list', r.result + ' : ' + r.response_details,'error', false,  'top-left', false);
				}
				else	{
					//alert( r.result + ' : ' + r.response_details );
					if( r.payment_method == 'paypal' )	{
						window.location.href = r.approval_url;
					}
					else if( r.payment_method == 'escrow' )	{
						//alert(r.response_details);
						if( r.result.toLowerCase().trim() == 'error' )	{
							notifyUI('terms-list', r.result + ' : ' + r.response_details, 'error' , false,  'top-left', false);
						}
						else	{
							$('#billing_result').html('<p class="bg-success text-success" style="padding:10px;">'+r.result + ' : ' + r.response_details+'</p>');
						}
						//notifyUI('terms-list', r.result + ' : ' + r.response_details, r.result.toLowerCase().trim() , false,  'top-left', false);
					}
					else	{
						//alert( r.result + ' : ' + r.response_details );
						//alert('Unknow Payment Method');
						notifyUI('terms-list', 'Unknow Payment Method','error', false,  'top-left', false);
					}
				}
			}
			$('#btn_span_loading').html('');
			$('#btn_confirm').removeAttr('disabled');
		});
	}
	return false;
}

function getPaymentInfo(state, paymentID, method)	{
	if( state.trim() != 'approved' )	{
		alert('Unknow payment status. Press OK and you will be redirected to the main page.');
		window.location.href = "/";
	}
	else	{
		blockpageUI();
		$.post("/get_payment_info",  { payment_status: state, payment_id: paymentID, payment_method: method } ).done(function(json){
			console.log(json);
			var r = $.parseJSON(json);
			if( r.result != undefined )	{
				if( r.result.toLowerCase().trim() == 'error' )	{
					alert(r.response_details);
					window.location.href = "/";
				}
				else	{
					$('#id_domain').text('#'+r.payment_info.id_domain);
					$('#domain_name').text(r.payment_info.domain_name);
					//$('#price').text('$'+r.payment_info.amount);
					
					var fullamount = $.parseJSON(r.payment_info.full_amount_info);
					$('#price').text('$'+fullamount.total);
					$('#total-amount').text('$'+fullamount.total);
					$('#sub_total').text('$'+fullamount.details.subtotal);

					$('#link_export_payment').attr('href',r.link);
					$('#link_print_payment').attr('href',r.link);
					
					var payer = $.parseJSON(r.payment_info.payer);
					var str_payer = '<dl> \
										<dt>Paid with</dt> \
										<dd>Payment Method : '+payer.payment_method.toUpperCase()+'</dd> \
										<dd>Status : '+payer.status+'</dd> \
										<dd>Invoice ID : '+r.payment_info.invoice_number+'</dd> \
										<dt>Billing Info</dt> \
										<dd>Name : '+payer.payer_info.first_name+' '+payer.payer_info.last_name+'</dd> \
										<dd>Email : '+payer.payer_info.email+'</dd> \
										<dd>Phone : '+payer.payer_info.phone+'</dd> \
										<dd>ID : '+payer.payer_info.payer_id+'</dd> \
										<dd>Country Code : '+payer.payer_info.country_code+'</dd> \
										<dt>Shipping Address</dt> \
										<dd>Recipient Name : '+payer.payer_info.shipping_address.recipient_name+'</dd> \
										<dd>Line 1 : '+payer.payer_info.shipping_address.line1+'</dd> \
										<dd>City : '+payer.payer_info.shipping_address.city+'</dd> \
										<dd>State : '+payer.payer_info.shipping_address.state+'</dd> \
										<dd>Postal Code : '+payer.payer_info.shipping_address.postal_code+'</dd> \
										<dd>Country Code : '+payer.payer_info.shipping_address.country_code+'</dd> \
									</dl> \
								';
					$('#payer_section').html(str_payer);
					
				}
			}
			unblockpageUI();
		});
	}
	return false;
}

function printPaymentInfoByID(task, state, paymentID, method, elemID='')	{
	if( state.trim() != 'approved' )	{
		alert('Unknow payment status. Press OK and you will be redirected to the main page.');
		window.location.href = "/";
	}
	else	{
		blockpageUI();
		$.post("/print_payment_info",  { payment_status: state, payment_id: paymentID, payment_method: method } ).done(function(json){
			unblockpageUI();
			console.log(json);
			var r = $.parseJSON(json);
			if( r.result != undefined )	{
				if( r.result.toLowerCase().trim() == 'error' )	{
					alert(r.response_details);
					window.location.href = "/";
				}
				else	{
					if(elemID == '')
						$('#link_print_payment').attr('href',r.link);
					else
						$('#link_print_payment_'+elemID).attr('href',r.link);

					if(elemID == '')
						$('#link_print_payment').trigger('click');
					else
						$('#link_print_payment_'+elemID).click();
					
					//window.location.href = r.link;
					//window.open(r.link,'_blank');

					
					var win = window.open(r.link, '_blank');
					if (win) {
						win.focus();
					} else {
						alert('Please allow popusps for this website to view/open pdf format.');
						//var message = 'Please allow popusps for this website to view/open pdf format.';
						//notifyUI('', message,'error', false,  pos='middle', false);
					}

					
				}
			}
			unblockpageUI();
		});
	}
	return false;
}

function printPaymentInfo(task, state, paymentID, method, elemID='')	{
	if( state.trim() != 'approved' )	{
		alert('Unknow payment status. Press OK and you will be redirected to the main page.');
		window.location.href = "/";
	}
	else	{
		blockpageUI();
		$.post("/print_payment_info",  { payment_status: state, payment_id: paymentID, payment_method: method } ).done(function(json){
			console.log(json);
			var r = $.parseJSON(json);
			if( r.result != undefined )	{
				if( r.result.toLowerCase().trim() == 'error' )	{
					alert(r.response_details);
					window.location.href = "/";
				}
				else	{
					//window.location.href = r.link;
					//window.open(r.link,'_blank');
					if(elemID == '')	{
						$('#link_export_payment').attr('href',r.link);
						$('#link_print_payment').attr('href',r.link);
					}
					else	{
						$('#link_export_payment_'+elemID).attr('href',r.link);
						$('#link_print_payment_'+elemID).attr('href',r.link);
					}
					
					if( task == 'export' )	{
						alert(task);
						if(elemID == '')	{
							$('#link_export_payment').click();
							$('#link_export_payment').trigger('click');
						}
						else	{
							$('#link_export_payment_'+elemID).click();
							$('#link_print_payment_'+elemID).click();
						}
					}
					if( task == 'print' )	{
						alert(task);
						if(elemID == '')	{
							$('#link_export_payment').click();
							$('#link_export_payment').trigger('click');
						}
						else	{
							$('#link_export_payment_'+elemID).click();
							$('#link_print_payment_'+elemID).click();
						}
					}
				}
			}
			unblockpageUI();
		});
	}
	return false;
}

/*** start Form Login ***/
function showLoginModal()	{
	$('.providers').appendTo('#socialLogin');
	$('#socialLogin .providers li').css({	width: '145px'	});
	$('#myModal').modal('show');
}

function submitLogin()	{
	$('#result_message').html('');
	$(".notifyjs-wrapper").click();
	if( !ValidateEmail( $('#email').val() )  )	{
		notifyUI('email', 'Please provide a valid email.','error',false);
		$('#email').attr('style','border-color:#d9534f;');
		$('#email').focus();
		$('#password').attr('style','border-color:none;');
	}
	else if( $('#password').val().trim() == ''  )	{
		notifyUI('password', 'Please provide a valid password.','error',false);
		$('#password').attr('style','border-color:#d9534f;');
		$('#password').focus();
		$('#email').attr('style','border-color:none;');
	}
	else	{
		$('#email').attr('style','border-color:none;');
		$('#password').attr('style','border-color:none;');
		//document.getElementById("loginForm").submit();
		
		$('#btn_login_loading').html('<img src="/images/ajax-loader-bar.gif">');
		$('#btn_login').attr('disabled','disabled');
		
		$.post("/login",  $( "#loginForm" ).serialize() ).done(function(json){
			console.log(json);
			var r = $.parseJSON(json);
			if( r.status.toLowerCase().trim() == 'danger' )	{
				//alert(r.response_details);
				//window.location.href = "/";
				notifyUI('result_message', r.message, 'error', false, 'top-center', false);
			}
			else	{
				notifyUI('result_message', r.message, 'success', false, 'top-center', false);
				window.location.href = "/myaccount";
			}
			$('#btn_login_loading').html('');
			$('#btn_login').removeAttr('disabled');
		});
		
	}
}

$("#email").keyup(function(e){ 
	var code = e.which;
	if(code==13)	{
		e.preventDefault();
		$('#password').focus();
	}
});

$("#password").keyup(function(e){ 
	var code = e.which;
	if(code==13)	{
		e.preventDefault();
		$('#btn_login').click();
	}
});

/*** end Form Login ***/

/*** start Form Registration ***/
function showSignupModal()	{
	$('.providers').appendTo('#socialSignup');
	$('#socialSignup .providers li').css({	width: '145px'	});
	$('#regModal').modal('show');
}

function submitRegistration()	{
	$('#reg_result').html('');
	if( $.trim( $('#reg_first_name').val() ) == '' )	{
		//alert('First name is required.');
		$(".notifyjs-wrapper").click();
		notifyUI('reg_first_name', 'First name is required.','error',false);
		$('#reg_first_name').attr('style','border-color:#d9534f;');
		$('#reg_first_name').focus();
		$('#reg_last_name').attr('style','border-color:none;');
		$('#reg_email').attr('style','border-color:none;');
		$('#reg_password').attr('style','border-color:none;');
	}
	else if( $.trim( $('#reg_last_name').val() ) == '' )	{
		//alert('Last name is required.');
		$(".notifyjs-wrapper").click();
		notifyUI('reg_last_name', 'Last name is required.','error',false);
		$('#reg_last_name').attr('style','border-color:#d9534f;');
		$('#reg_last_name').focus();
		$('#reg_first_name').attr('style','border-color:none;');
		$('#reg_email').attr('style','border-color:none;');
		$('#reg_password').attr('style','border-color:none;');
	}
	else if( !ValidateEmail( $('#reg_email').val() )  )	{
		//alert('Please provide a valid email.');
		$(".notifyjs-wrapper").click();
		notifyUI('reg_email', 'Please provide a valid email.','error',false);
		$('#reg_email').attr('style','border-color:#d9534f;');
		$('#reg_email').focus();
		$('#reg_first_name').attr('style','border-color:none;');
		$('#reg_last_name').attr('style','border-color:none;');
		$('#reg_password').attr('style','border-color:none;');
	}
	else if( $.trim( $('#reg_password').val() ) == '' )	{
		//alert('Password is required.');
		$(".notifyjs-wrapper").click();
		notifyUI('reg_password', 'Password is required.','error',false);
		$('#reg_password').attr('style','border-color:#d9534f;');
		$('#reg_password').focus();
		$('#reg_first_name').attr('style','border-color:none;');
		$('#reg_last_name').attr('style','border-color:none;');
		$('#reg_email').attr('style','border-color:none;');
	}
	else	{
		$(".notifyjs-wrapper").click();
		$('#reg_first_name').attr('style','border-color:none;');
		$('#reg_last_name').attr('style','border-color:none;');
		$('#reg_email').attr('style','border-color:none;');
		$('#reg_password').attr('style','border-color:none;');
		
		$('#btn_reg_loading').html('<img src="/images/loading.gif">');
		$('#btnsignup').attr('disabled','disabled');
		
		//var $btn = $(this);
		//$btn.button('loading');
		$('#regModal .status-message').remove();
		// business logic...
		$.post("/signup", $("#regForm").serialize()).done(function(json){
			//console.log(json);
			var r = $.parseJSON(json);
			var status = r.status.toLowerCase();
			var message = r.message;
			
			$('#reg_result').html('<p class="status-message text-center bg-'+status+' text-'+status+' col-lg-12" >'+message+'</p>');
			//$('<p class="status-message text-center bg-'+status+' text-'+status+' col-lg-12" >'+message+'</p>').prependTo('#regModal .modal-body');
			//$btn.button('reset');
			
			$('#btn_reg_loading').html('');
			$('#btnsignup').removeAttr('disabled');
			return false;
		});
	}
	return false;
}

$("#reg_first_name").keyup(function(e){ 
	var code = e.which;
	if(code==13)	{
		e.preventDefault();
		$('#reg_last_name').focus();
	}
});
$("#reg_last_name").keyup(function(e){ 
	var code = e.which;
	if(code==13)	{
		e.preventDefault();
		$('#reg_email').focus();
	}
});
$("#reg_email").keyup(function(e){ 
	var code = e.which;
	if(code==13)	{
		e.preventDefault();
		$('#reg_password').focus();
	}
});
$("#reg_password").keyup(function(e){ 
	var code = e.which;
	if(code==13)	{
		e.preventDefault();
		$('#btnsignup').click();
	}
});
/*** end Form Registration ***/

function notifyUI(elem_id='', message='',status='error', auto_hide=true,  pos='top-left', arwShw = true)	{
	$("#"+elem_id).notify( message, { position:pos, className:status, autoHide: auto_hide, arrowShow: arwShw  } );
}

function blockpageUI()	{
	$.blockUI({ 
		message: '<h5>Please wait ....</h5>', 
		css: { 
		   border: 'none', 
		   padding: '5px', 
		   backgroundColor: '#000', 
		   '-webkit-border-radius': '10px', 
		   '-moz-border-radius': '10px', 
		   opacity: .6, 
		   color: '#fff' 
		 } 
	});
}

function unblockpageUI()	{
	$.unblockUI();
}

function ValidateEmail(email) {
	var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	return expr.test(email);
};

function runMasterToolTip()	{
	// Tooltip only Text
	$('.masterTooltip').hover(function(){
			// Hover over code
			var title = $(this).attr('title');
			$(this).data('tipText', title).removeAttr('title');
			$('<p class="tooltip"></p>')
			.text(title)
			.appendTo('body')
			.fadeIn('slow');
	}, function() {
			// Hover out code
			$(this).attr('title', $(this).data('tipText'));
			$('.tooltip').remove();
	}).mousemove(function(e) {
			var mousex = e.pageX - 50; //Get X coordinates
			var mousey = e.pageY + 20; //Get Y coordinates
			$('.tooltip').css({ top: mousey, left: mousex })
			
	});
}