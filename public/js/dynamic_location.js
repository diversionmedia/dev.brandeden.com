dynamic_location_geocoder = new google.maps.Geocoder();

(function( $ ) {

	$.fn.dynamic_location = function( custom_options,region_data ) {
		if(!region_data) {
			return;
		}

		var options = $.extend({
			country_selector: "#country",
			default_country: "us",
			state_selector: "#state",
			default_state: "ak",
			zip_selector: "#postalcode",
			default_zip: "",
			country_filter: []
		}, custom_options );

		function country_field_change(country) {
			var zip_field = $(options.zip_selector);
			var state_field = $(options.state_selector);

			state_field.empty();

			//change validators based on country
			if(country == 'us') {
				zip_field.attr('placeholder','5 digit zip code');
				zip_field.attr('maxlength',5);
				zip_field.attr('class','form-control');
				zip_field.attr('pattern','[0-9]*');
				$(".discover, .amex").show();
			}
			else if(country == 'ca') {
				zip_field.attr('placeholder','Enter your postal code');
				zip_field.attr('maxlength',7);
				zip_field.attr('class','form-control');
				//zip_field.attr('pattern','*');
				$(".discover, .amex").show();
			}
			else if(country == 'au') {
				zip_field.attr('placeholder','Enter your postal code');
				zip_field.attr('maxlength',4);
				zip_field.attr('class','form-control');
				zip_field.attr('pattern','[0-9]*');
				$(".discover, .amex").hide();
			}
			else if(country == 'nz') {
				zip_field.attr('placeholder','Enter your postal code');
				zip_field.attr('maxlength',4);
				zip_field.attr('class','form-control');
				zip_field.attr('pattern','[0-9]*');
				$(".discover, .amex").hide();
			}
			
			
			var hiddenCountry = $('#hiddenCountry').val();
			var hiddenState = $('#hiddenState').val();
			console.log(country);
			console.log(hiddenCountry);
			console.log(hiddenState);
			
			if( hiddenCountry == country )	{
				$.each(region_data[country].states,function(index,val) {
					if( val.code == hiddenState )	{
						state_field.append("<option value='" + val.code + "' selected='selected'>" + val.name + "<\/option>");
					}
					else	{
						state_field.append("<option value='" + val.code + "'>" + val.name + "<\/option>");
					}
				});
			}
			else	{
				$.each(region_data[country].states,function(index,val) {
					state_field.append("<option value='" + val.code + "'>" + val.name + "<\/option>");
				});
			}
			
			//$('#state').next().children().first().text('');	// just added since its using a JCF lib
			initCustomForms(); // just added to refresh form JCF from /public/js/jquery.main.js
		}

		function getState(zipcode) {
			dynamic_location_geocoder.geocode( { 'address': zipcode}, function (result, status) {
				if(!result || !result[0]) {
					$(options.state_selector).val('');
					return;
				}

				var state = "N/A";
				for (var component in result[0]['address_components']) {
					for (var i in result[0]['address_components'][component]['types']) {
						if (result[0]['address_components'][component]['types'][i] == "administrative_area_level_1") {
							state = result[0]['address_components'][component]['short_name'];
							$(options.state_selector).val(state);
							return;
						}
					}
				}
			});
		}

		var country_field = $(options.country_selector);

		if($(country_field).length) {
			country_field.change(function() {
				country_field_change($(this).val());
			});

			country_field.empty();

			//only list countries that are not filtered
			$.each(region_data,function(index,val) {
				if($.isArray(options.country_filter)) {
					if($.inArray(val.code,options.country_filter) === -1) {
						return true;
					}
				}

				var opt = $("<option></option>");

				opt.attr('value',val.code);
				opt.text(val.name);

				country_field.append(opt);
			});

			country_field.val(options.default_country);
			country_field.change();
		}
		else {
			country_field_change(options.default_country);
		}

		$(options.zip_selector).keyup(function() {
			var theval = $(this).val();

			if(theval.length >= 5) {
				getState($(this).val());
			}
		});

		$(options.zip_selector).val(options.default_zip);
		$(options.zip_selector).keyup();
	};

}( jQuery ));

geo_data = $.parseJSON("{\"au\": {\"code\":\"au\",\"name\":\"Australia\",\"states\": [{\"code\":\"AU-ACT\", \"name\":\"Australian Capital Territory\"},{\"code\":\"AU-NSW\", \"name\":\"New South Wales\"},{\"code\":\"AU-NT\", \"name\":\"Northern Territory\"},{\"code\":\"AU-QLD\", \"name\":\"Queensland\"},{\"code\":\"AU-SA\", \"name\":\"South Australia\"},{\"code\":\"AU-TAS\", \"name\":\"Tasmania\"},{\"code\":\"AU-VIC\", \"name\":\"Victoria\"},{\"code\":\"AU-WA\", \"name\":\"Western Australia\"}]},\"ca\": {\"name\":\"Canada\",\"code\":\"ca\",\"states\": [{\"code\":\"AB\", \"name\":\"Alberta (AB)\"},{\"code\":\"BC\", \"name\":\"British Columbia (BC)\"},{\"code\":\"MB\", \"name\":\"Manitoba (MB)\"},{\"code\":\"NL\", \"name\":\"Newfoundland (NL)\"},{\"code\":\"NB\", \"name\":\"New Brunswick (NB)\"},{\"code\":\"NS\", \"name\":\"Nova Scotia (NS)\"},{\"code\":\"ON\", \"name\":\"Ontario (ON)\"},{\"code\":\"PE\", \"name\":\"Prince Edwards (PE)\"},{\"code\":\"QC\", \"name\":\"Quebec (QC)\"},{\"code\":\"SK\", \"name\":\"Saskatchewan (SK)\"}]},\"nz\": {\"name\":\"New Zealand\",\"code\":\"nz\",\"states\": [{\"code\":\"NZ-AUK\", \"name\":\"Auckland\"},{\"code\":\"NZ-BOP\", \"name\":\"Bay of Plenty\"},{\"code\":\"NZ-CAN\", \"name\":\"Canterbury\"},{\"code\":\"NZ-CIT\", \"name\":\"Chatham Islands Territory\"},{\"code\":\"NZ-GIS\", \"name\":\"Gisborne District\"},{\"code\":\"NZ-HKB\", \"name\":\"Hawkes\'s Bay\"},{\"code\":\"NZ-MWT\", \"name\":\"Manawatu-Wanganui\"},{\"code\":\"NZ-MBH\", \"name\":\"Marlborough District\"},{\"code\":\"NZ-NSN\", \"name\":\"Nelson City\"},{\"code\":\"NZ-N\", \"name\":\"North Island\"},{\"code\":\"NZ-NTL\", \"name\":\"Northland\"},{\"code\":\"NZ-OTA\", \"name\":\"Otago\"},{\"code\":\"NZ-S\", \"name\":\"South Island\"},{\"code\":\"NZ-STL\", \"name\":\"Southland\"},{\"code\":\"NZ-TKI\", \"name\":\"Taranaki\"},{\"code\":\"NZ-TAS\", \"name\":\"Tasman District\"},{\"code\":\"NZ-WKO\", \"name\":\"Waikato\"},{\"code\":\"NZ-WGN\", \"name\":\"Wellington\"},{\"code\":\"NZ-WTC\", \"name\":\"West Coast\"}]},\"us\": {\"name\":\"United States\",\"code\":\"us\",\"states\":[{\"code\":\"AL\", \"name\":\"Alabama (AL)\"},{\"code\":\"AK\", \"name\":\"Alaska (AK)\"},{\"code\":\"AZ\", \"name\":\"Arizona (AZ)\"},{\"code\":\"AR\", \"name\":\"Arkansas (AR)\"},{\"code\":\"CA\", \"name\":\"California (CA)\"},{\"code\":\"CO\", \"name\":\"Colorado (CO)\"},{\"code\":\"CT\", \"name\":\"Connecticut (CT)\"},{\"code\":\"DE\", \"name\":\"Delaware (DE)\"},{\"code\":\"FL\", \"name\":\"Florida (FL)\"},{\"code\":\"GA\", \"name\":\"Georgia (GA)\"},{\"code\":\"GU\", \"name\":\"Guam (GU)\"},{\"code\":\"HI\", \"name\":\"Hawaii (HI)\"},{\"code\":\"ID\", \"name\":\"Idaho (ID)\"},{\"code\":\"IL\", \"name\":\"Illinois (IL)\"},{\"code\":\"IN\", \"name\":\"Indiana (IN)\"},{\"code\":\"IA\", \"name\":\"Iowa (IA)\"},{\"code\":\"KS\", \"name\":\"Kansas (KS)\"},{\"code\":\"KY\", \"name\":\"Kentucky (KY)\"},{\"code\":\"LA\", \"name\":\"Louisiana (LA)\"},{\"code\":\"ME\", \"name\":\"Maine (ME)\"},{\"code\":\"MD\", \"name\":\"Maryland (MD)\"},{\"code\":\"MA\", \"name\":\"Massachusetts (MA)\"},{\"code\":\"MI\", \"name\":\"Michigan (MI)\"},{\"code\":\"MN\", \"name\":\"Minnesota (MN)\"},{\"code\":\"MS\", \"name\":\"Mississippi (MS)\"},{\"code\":\"MO\", \"name\":\"Missouri (MO)\"},{\"code\":\"MT\", \"name\":\"Montana (MT)\"},{\"code\":\"NE\", \"name\":\"Nebraska (NE)\"},{\"code\":\"NV\", \"name\":\"Nevada (NV)\"},{\"code\":\"NH\", \"name\":\"New Hampshire (NH)\"},{\"code\":\"NJ\", \"name\":\"New Jersey (NJ)\"},{\"code\":\"NM\", \"name\":\"New Mexico (NM)\"},{\"code\":\"NY\", \"name\":\"New York (NY)\"},{\"code\":\"NC\", \"name\":\"North Carolina (NC)\"},{\"code\":\"ND\", \"name\":\"North Dakota (ND)\"},{\"code\":\"OH\", \"name\":\"Ohio (OH)\"},{\"code\":\"OK\", \"name\":\"Oklahoma (OK)\"},{\"code\":\"OR\", \"name\":\"Oregon (OR)\"},{\"code\":\"PA\", \"name\":\"Pennsylvania (PA)\"},{\"code\":\"PR\", \"name\":\"Puerto Rico (PR)\"},{\"code\":\"RI\", \"name\":\"Rhode Island (RI)\"},{\"code\":\"SC\", \"name\":\"South Carolina (SC)\"},{\"code\":\"SD\", \"name\":\"South Dakota (SD)\"},{\"code\":\"TN\", \"name\":\"Tennessee (TN)\"},{\"code\":\"TX\", \"name\":\"Texas (TX)\"},{\"code\":\"UT\", \"name\":\"Utah (UT)\"},{\"code\":\"VT\", \"name\":\"Vermont (VT)\"},{\"code\":\"VA\", \"name\":\"Virginia (VA)\"},{\"code\":\"WA\", \"name\":\"Washington (WA)\"},{\"code\":\"WV\", \"name\":\"West Virginia (WV)\"},{\"code\":\"WI\", \"name\":\"Wisconsin (WI)\"},{\"code\":\"WY\", \"name\":\"Wyoming (WY)\"}]}}");

console.log(geo_data);

$("#opt_in_form").dynamic_location({

	country_filter: ['us','ca','au','nz'],

	default_country: "us",

	default_zip: ""

},geo_data);