//Version 1.0.0
//this will allow only numbers in fields with the class 'onlynums'

 jQuery(document).ready(function($){
	$('.onlynums').keydown(function (e) {
		if (e.shiftKey || e.ctrlKey || e.altKey) {
			e.preventDefault();
		} 
		else {
			var key = e.keyCode;
			if (!((key == 8) || (key == 9) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
				e.preventDefault();
			}
		}
	});
});