/*!
	JS functions
 */

function removeFavoriteDomain(id,domain)	{
	if(confirm("Are you sure you want to remove "+domain+" from your Favorites?"))	{
		$('#load_favorites').html('<div align="center"><img src="/images/loading.gif"></div>');
		$.get("/remove_my_favorite_domain/"+id, function( data ) {
			$('#load_favorites').html(data);
			return false;
		});	
		return false;
	}
}

function getMyFavorites(buyer_id)	{	
	$('#load_favorites').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/get_my_favorites", function( data ) {
		$('#load_favorites').html(data);
		return false;
	});	
	return false;
}
 
function getOrderStatus(friendly)	{	
	$('#load_order_status').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/get_order_status", function( data ) {
		$('#load_order_status').html(data);
		return false;
	});	
	return false;
}

function getLatestMessageByDomain(domain,id='')	{	
	if(id != '')	{
		setActiveMessageList(id);
	}
	
	$('#load_detail_message').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/get_latest_message_by_domain/"+domain, function( json ) {
		var json_data = $.parseJSON(json);
		var strhtml = '\
				<div class="name-area">\
					<strong class="name-holder"><a href="#"><i class="icon"></i><span class="name">'+json_data.fullname+'</span> </a></strong>\
					<time datetime="'+json_data.received_on+'">'+json_data.time+'</time>\
				</div>\
				<div class="person-detail-area">\
					<div class="person-detail">\
						<ul class="detail-list list-unstyled">\
							<li>\
								<strong class="description">Domain:</strong>\
								<span class="text"><a href="#">'+json_data.domain+'</a></span>\
							</li>\
							<li>\
								<strong class="description">Seller Offer:</strong>\
								<span class="text">'+json_data.offer+'</span>\
							</li>\
							<li>\
								<strong class="description">My Offer:</strong>\
								<span class="text">Coming Soon</span>\
							</li>\
							<li>\
								<strong class="description">Type:</strong>\
								<span class="text brand">Brandable - Coming Soon</span>\
							</li>\
							<li>\
								<strong class="description">Message:</strong>\
								<span class="text add">'+json_data.message+'</span>\
							</li>\
						</ul>\
					</div>\
				</div>\
			';
		$('#load_detail_message').html(strhtml);
		
		return false;
	});	
	return false;
}

function getMessages(friendly)	{	
	$('#msglist').html('<div align="center"><img src="/images/loading.gif"></div>');
	$.get("/get_order_messages", function( json ) {
		var json_data = $.parseJSON(json);
		
		var strhtml = '';
		var stylelink = '';
		var cnt = 0;
		if (json_data.inquiries !== undefined)	{
			$.each(json_data.inquiries, function (key, data) {
				cnt++;
				if( cnt == 1)	{	stylelink = 'class="active"';	}
				else	{	stylelink = '';	}
				
				strhtml += '\
					<li onclick="getLatestMessageByDomain(\''+data.domain+'\',\''+cnt+'\')">\
						<a href="#tab6" id="msgcnt'+cnt+'" '+stylelink+'>\
							<strong class="name">'+data.fullname+'</strong>\
							<time datetime="'+data.received_on+'">'+data.time+'</time>\
							<ul class="info-list list-unstyled">\
								<li>\
									<strong class="description">Domain:</strong>\
									<span class="text mail">'+data.domain+'</span>\
								</li>\
								<li>\
									<strong class="description">Seller Offer:</strong>\
									<span class="text">'+data.offer+'</span>\
								</li>\
								<li>\
									<strong class="description">My Offer:</strong>\
									<span class="text"></span>\
								</li>\
								<li>\
									<strong class="description">Message:</strong>\
									<span class="text"> '+data.message+' </span>\
								</li>\
							</ul>\
						</a>\
					</li>\
					';
			});
		}
		else	{
			strhtml += '<li><div align="center">No Messages Found</div></li>';
		}
		$('#msglist').html(strhtml);
		
		if( $('#latest_inq_domain').val() != '' )	{
			getLatestMessageByDomain( $('#latest_inq_domain').val() );
		}
		//$('#load_messages').html(data);
		return false;
	});	
	return false;
}

function setActiveMessageList(id)	{
	$('#msglist li a').removeAttr('class');
	$('#msgcnt'+id).attr('class','active');
}