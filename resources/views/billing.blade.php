@extends('cartheader')
@section('content')
<!-- contain main informative part of the site -->
			<main id="main">
				<section class="tabs-holder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 container-holder add">
								<!-- breadcrumb -->
								<ol class="breadcrumb">
									<li><a href="/">Home</a></li>
									<li class="active">My Cart</li>
								</ol>
								<!-- tabs set -->
								<ul class="tabset cart-tabs list-inline">
									<li><a href="/mycart"><span class="text-holder"><i class="icon cart-icon"></i>Review Shopping</span></a></li>
									<li><a href="#" class="active"><span class="text-holder"><i class="icon dollar-icon"></i>Billing &amp; Payment</span></a></li>
									<li><a href="/orderconfirm"><span class="text-holder"><i class="icon check-icon"></i>Order Confirmation</span></a></li>
								</ul>
							</div>
						</div>
						<form class="form-wrap" action="/orderconfirm" method="post">
							<div class="tab-content-holder add">
								<div class="tab-content">
									<div id="tab21">
										<div class="row">
											<div class="col-md-8 col-xs-12">
												<strong class="info-title">Choose Payment Method</strong>
												<ul class="radio-list">
													<li>
														<label>
															<input type="radio" name="method">
															<span class="fake-input-holder">
																<i class="escrow-icon"></i>
															</span>
														</label>
														<span class="txt">Escrow.com</span>
													</li>
													<li>
														<label>
															<input type="radio" name="method">
															<span class="fake-input-holder">
																<i class="transfer-icon"></i>
															</span>
														</label>
														<span class="txt">Wire Transfer</span>
													</li>
													<li>
														<label>
															<input type="radio" name="method">
															<span class="fake-input-holder">
																<i class="paypal-icon"></i>
															</span>
														</label>
														<span class="txt">Paypal</span>
													</li>
													<li>
														<label>
															<input type="radio" name="method" checked>
															<span class="fake-input-holder">
																<i class="card-icon"></i>
															</span>
														</label>
														<span class="txt">Credit/Debit/Prepaid Card</span>
													</li>
												</ul>
												<div class="card-info">
													<strong class="info-title">Card Information</strong>
													<div class="form-row">
														<div class="form-col">
															<label for="card">Name on Card</label>
															<input type="text" id="card" class="form-control" placeholder="Ex: John Doe">
														</div>
														<div class="form-col">
															<label for="number">Card Number</label>
															<div class="input-col">
																<input type="text" id="number" class="form-control" placeholder="Ex: 0000-0000-0000-0000">
																<i class="icon-card"></i>
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="form-col add">
															<label for="code">CVV Code</label>
															<div class="input-col">
																<input type="number" id="code" class="form-control" placeholder="Ex: 000">
																<i class="code-info"></i>
															</div>
														</div>
														<div class="form-col add2">
															<label>Expire Date</label>
															<div class="input-col add">
																<select class="no-shadow">
																	<option>Month</option>
																	<option>Month</option>
																	<option>Month</option>
																</select>
															</div>
															<div class="input-col add add2">
																<select class="no-shadow">
																	<option>Year</option>
																	<option>Year</option>
																	<option>Year</option>
																</select>
															</div>
														</div>
													</div>
													<strong class="info-title">Card Information</strong>
													<div class="form-row">
														<div class="form-col">
															<label for="f-n">First Name</label>
															<input type="text" id="f-n" class="form-control" placeholder="">
														</div>
														<div class="form-col">
															<label for="number">Last Name</label>
															<input type="text" id="l-n" class="form-control" placeholder="">
														</div>
													</div>
													<div class="form-row">
														<label for="address">Address 1</label>
														<input type="text" id="address" class="form-control" placeholder="">
													</div>
													<div class="form-row">
														<label for="address-2">Address 2</label>
														<input type="text" id="address-2" class="form-control" placeholder="">
													</div>
													<div class="form-row">
														<div class="form-col add">
															<label for="city">City</label>
															<input type="text" id="city" class="form-control" placeholder="">
														</div>
														<div class="form-col add3">
															<label>State</label>
															<select class="no-shadow">
																<option>Select</option>
																<option>Select</option>
																<option>Select</option>
															</select>
														</div>
														<div class="form-col add4">
															<label for="zip">Zip Code</label>
															<input type="text" id="zip" class="form-control" placeholder="">
														</div>
													</div>
													<label class="label">
														<input type="checkbox" name="save-info">
														<span class="fake-input"></span>
														Save my information for future use
													</label>
												</div>
											</div>
											<!--widget-block-->
											<div class="col-md-4 col-xs-12 widget-block">
												<!--widget-->
												<div class="widget">
													<div class="widget-holder">
														<div class="order-holder">
															<strong class="title-order">Order Total</strong>
															<strong class="total-amount">$8,888,888</strong>
														</div>
														<ul class="terms-list">
															<li>
																<label>
																	<input type="checkbox" name="terms">
																	<span class="fake-input"></span>
																	I have read and agree to the terms of the<br> <span>Domain Registration Agreement</span>
																</label>
															</li>
															<li>
																<label>
																	<input type="checkbox" name="terms">
																	<span class="fake-input"></span>
																	I have read and agree to the terms of the<br> <span>Universal Terms of Service</span>
																</label>
															</li>
															<li>
																<label>
																	<input type="checkbox" name="terms">
																	<span class="fake-input"></span>
																	I have read and agree to the<br> <span>refund policy</span>
																</label>
															</li>
														</ul>
													</div>
													<button type="submit" class="btn btn-primary">Confirm &amp; Pay</button>
												</div>
												<h2>Faqs</h2>
												<!--widget-->
												<div class="widget">
													<div class="widget-holder">
														<div class="open-close">
															<a class="slide-openar" href="#">How to buy a domain.</a>
															<div class="slide">
																<p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
															</div>
														</div>
														<div class="open-close">
															<a class="slide-openar" href="#">How to Place Order</a>
															<div class="slide">
																<p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
															</div>
														</div>
														<div class="open-close active">
															<a class="slide-openar" href="#">How to Transfer a Domain Name</a>
															<div class="slide">
																<p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
															</div>
														</div>
													</div>
													<div class="btn-wrap">
														<a class="btn btn-default" href="#">More Faqs</a>
														<a class="btn btn-default" href="#">Contact us</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</section>
			</main>
@stop