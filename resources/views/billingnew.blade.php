@extends('cartheader')
@section('content')
<!-- contain main informative part of the site -->
			<main id="main">
				<?php $d = $details->domain; ?>
				<?php 
					if($details->domain->status == 3)	{	// if dropped redirect to home / main page
						header('Location: /');
						die();
					}
				?>
				<section class="tabs-holder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 container-holder add">
								<!-- breadcrumb -->
								<!--
								<ol class="breadcrumb">
									<li><a href="/">Home</a></li>
									<li class="active">My Cart</li>
								</ol>
								-->
								<!-- tabs set -->
								<!--
								<ul class="tabset cart-tabs list-inline">
									<li><a href="/mycart"><span class="text-holder"><i class="icon cart-icon"></i>Review Shopping</span></a></li>
									<li><a href="#" class="active"><span class="text-holder"><i class="icon dollar-icon"></i>Billing &amp; Payment</span></a></li>
									<li><a href="/orderconfirm"><span class="text-holder"><i class="icon check-icon"></i>Order Confirmation</span></a></li>
								</ul>
								-->
							</div>
						</div>
						<form class="form-wrap" action="#" method="post" id="frmBilling">
							<div class="tab-content-holder add">
								<div class="tab-content">
									<div id="tab21">
									<?php 
									if ( $id_buyer > 0 ) {
										if ( in_array($d->id_domain, $order_domainIDs) ) {	
											if ( $order_IPNstatus_Code[$d->id_domain] == 25 )
												$domain_status = 'Sold';
											else if ( $order_IPNstatus_Code[$d->id_domain] == 0 )
												$domain_status = 'Cancelled';
											else
												$domain_status = 'Pending';
										}
										else	{	
											if($details->domain->status == 3)
												$domain_status = 'Dropped';
											else if($details->domain->status == 2)
												$domain_status = 'Hidden';
											else {
												if($details->domain->price <= 0)
													$domain_status = 'MakeOffer';
												else
													$domain_status = 'BuyNow';
											}
										}
									}
									else	{
										if ( !in_array($d->id_domain, $order_domainIDs) || $order_IPNstatus_Code[$d->id_domain] < 1 ) {	
											if($details->domain->status == 3)
												$domain_status = 'Dropped';
											else if($details->domain->status == 2)
												$domain_status = 'Hidden';
											else {
												if($details->domain->price <= 0)
													$domain_status = 'MakeOffer';
												else
													$domain_status = 'BuyNow';
											}
										}
										else	{
											if ( $order_IPNstatus_Code[$d->id_domain] == 25 )
												$domain_status = 'Sold';
											else if ( $order_IPNstatus_Code[$d->id_domain] == 0 )
												$domain_status = 'Cancelled';
											else
												$domain_status = 'Pending';
										}
									}
									
									if( $domain_status != 'BuyNow' )	{	
										header('Location: /'.$details->domain->domain);	// just redirect domain to the detail page if not for sale
										die();
									}
									else	{
										?>
										<div class="row">
											<div class="col-md-8 col-xs-12">
												<strong class="info-title">Choose Payment Method</strong>
												
												<?php
												if( $payment_settings->result == 'SUCCESS')	{	
													$available_payment_methods = json_decode($payment_settings->payment_settings_info->final_payment_settings);
												}
												if( count($available_payment_methods) > 0 )	{	?>
													<ul class="radio-list">
														<?php foreach( $available_payment_methods as $paymethod )	{	?>
																<li>
																	<label>
																		<input value="<?php echo $paymethod->id_string; ?>" type="radio" name="paymentmethod" checked >
																		<span class="fake-input-holder">
																		<i class="<?php echo $paymethod->id_string; ?>-icon"></i></span>
																	</label>
																	<span class="txt"><?php echo ucfirst($paymethod->id_string); ?></span>
																</li>
														<?php } ?>
													</ul>
												<?php } else  {  ?>
													<h3> No Payment Method Found </h3>
												<?php } ?>
												
												<div class="table-area">
													<strong class="info-title">Order Information</strong>
													<table class="table-holder sec">
														<thead>
															<tr class="gray">
																<th class="col1">Domain Names</th>
																<th class="col2">Price</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class="col1"><?php if($details->record > 0){ echo ucfirst($details->domain->domain); } ?></td>
																<td class="col2"><span class="text-digit">
																	<?php if($details->record > 0) { echo $details->domain->price_formatted; } ?>
																</span></td>
															</tr>
														</tbody>
													</table>
												</div>
												
											</div>
											<!--widget-block-->
											<div class="col-md-4 col-xs-12 widget-block">
												<!--widget-->
												<div class="widget">
													<div class="widget-holder">
														<div class="order-holder">
															<strong class="title-order">Order Total</strong>
															<strong class="total-amount"><?php if($details->record > 0) { echo $details->domain->price_formatted; } ?></strong>
															<?php 
															$s_id_buyer = '';
															if( isset($_SESSION['id_buyer']) )	{	$s_id_buyer = $_SESSION['id_buyer'];	}
															$s_email = '';
															if( isset($_SESSION['email']) )	{	$s_email = $_SESSION['email'];	}
															?>															
															<div class="row">
																<br >
																<input type="hidden" id="url" name="url" value="<?php echo config('app.BASE_URL'); ?>" >
																<input required type="hidden" class="form-control" id="enq-id_buyer2" name="enq-id_buyer" value="<?php echo ($s_id_buyer != '' ? $s_id_buyer : ''); ?> " >
																<div class="inner-addon left-addon">
																	<i class="glyphicon glyphicon-envelope"></i>
																	<input required type="email" class="form-control" id="enq-email2" name="enq-email" placeholder="<?php echo ($s_email != '' ? $s_email : 'E-mail Address'); ?> " value="<?php echo $s_email; ?>" <?php if($s_id_buyer != '' && $s_email != '')	{	echo ''; }  ?> >
																</div>
															</div>
														</div>
														<ul class="terms-list" id="terms-list">
															<li id="billing_result">
															</li>
															<li>
																<label>
																	<input type="checkbox" name="terms" id="term_dom_reg">
																	<span class="fake-input"></span>
																	I have read and agree to the terms of the<br> <span>Domain Registration Agreement</span>
																</label>
															</li>
															<li>
																<label>
																	<input type="checkbox" name="terms" id="term_universal">
																	<span class="fake-input"></span>
																	I have read and agree to the terms of the<br> <span>Universal Terms of Service</span>
																</label>
															</li>
															<li>
																<label>
																	<input type="checkbox" name="terms" id="term_refund">
																	<span class="fake-input"></span>
																	I have read and agree to the<br> <span>refund policy</span>
																</label>
															</li>
															<li>
																<label>
																	<div id="enq-recaptcha" class="g-recaptcha" data-sitekey="6LfseCUTAAAAALVlcV513h29hCq-bCwTq0IUadkV"></div>
																</label>
															</li>
														</ul>
													</div>													
													<textarea required id="enq-message2" name="enq-message" class="form-control hidden" data-required="true">Buying {{ $details->domain->domain }} </textarea>
													<input type="hidden" value="@if($details->record > 0){{$details->domain->domain}}@endif" id="enq-domain-name2" name="enq-domain-name" >
													<input type="hidden" value="@if($details->record > 0){{$details->domain->id_domain}}@endif" id="enq-domain-id2" name="enq-domain-id" >
													<input type="hidden" value="@if($details->record > 0){{$details->domain->price}}@endif" id="enq-domain-price2" name="enq-domain-price" >
													<button id="btn_confirm" onclick="sendEnquiryTwo();" type="button" class="btn btn-primary">
														Confirm &amp; Pay <span id="btn_span_loading"></span> </button>
												</div>
												<h2>Faqs</h2>
												<!--widget-->
												<div class="widget">
													<div class="widget-holder">
														<!-- 
														<div class="open-close">
															<a class="slide-openar" href="#">How to buy a domain.</a>
															<div class="slide">
																<p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
															</div>
														</div>
														-->
														<div class="open-close active">
															<a class="slide-openar" href="#">How to Place Order</a>
															<div class="slide">
																<ol>
																	<li> Find your domain </li>
																	<li> Click Buy Now </li>
																	<li> Select Payment method </li>
																	<li> Accept terms and agreements </li>
																	<li> Pay for domain via Escrow or Paypal </li>
																	<li> Wait for payment verification </li>
																	<li> Receive your domain </li>
																</ol>
															</div>
														</div>
														<!-- 
														<div class="open-close active">
															<a class="slide-openar" href="#">How to Transfer a Domain Name</a>
															<div class="slide">
																<p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
															</div>
														</div>
														-->
													</div>
													<div class="btn-wrap">
														<a class="btn btn-default" href="#">More Faqs</a>
														<a class="btn btn-default" href="#">Contact us</a>
													</div>
												</div>
											</div>
										</div>
										<?php
									}
									?>	
									</div> <!-- end tab21 -->
								</div>
							</div>
						</form>
					</div>
				</section>
			</main>
@stop