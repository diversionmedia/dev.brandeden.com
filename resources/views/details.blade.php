@extends('app')
@section('content')
<script>
    $(document).ready(function(){
		$('#all_cat').click(function(e) {
            $('input[name=category]').removeAttr('checked');
        });
	});
</script>
<!-- contain main informative part of the site -->
<main id="main">
<!-- contain sidebar of the page -->

<aside id="sidebar" class="pull-left hidden">
    <button class="btn btn-primary form-block-opener" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Filter Options <i class="icon-filter"></i></button>
    <form action="/{{$tab}}" class="form-filters collapse" aria-expanded="false" id="collapseExample">
    
        <!-- aside-block -->
        <div class="form-block">
            <h2>Categories</h2>
            <!-- accordion -->
            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                <!-- panel -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="category">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Categories</a>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="category">
                        <div class="panel-body">
                            <ul class="list-unstyled" style="height:250px; overflow-y: scroll;">
                                @foreach($categories AS $c)
                                <li>
                                    <label class="fake-checkbox" for="category{{ $c->id_category }}">
                                        <input type="radio" id="category{{ $c->id_category }}" name="category" value="{{ $c->id_category }}">
                                        <span></span>
                                    </label>
                                    <span class="label"><label for="sub-category1">{{ $c->title }}</label></span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="btn-block text-center">
                    <a href="javascript:" class="btn-all" id="all_cat">All Categories</a>
                </div>
            </div>
        </div>
        <!-- aside-block -->
        <div class="form-block">
            <h2>Price Range</h2>
            <input value="1,10000" id="price" type="range" min="1" max="10000" multiple name="price">
            <div class="values clearfix">
                <span class="pull-left">{{ $currency }} <span class="cur-min" id="price_min">1</span></span>
                <span class="pull-right">{{ $currency }} <span class="cur-max" id="price_max">1000</span></span>
            </div>
        </div>
        <!-- aside-block -->
        <div class="form-block">
            <h2>Domain Length</h2>
            
                <input type="range" value="3,10" min="3" max="10" multiple name="length">
            
            <div class="values clearfix">
                <span class="pull-left"><span class="cur-min" id="length_min">3</span></span>
                <span class="pull-right"><span class="cur-max" id="length_max">10</span></span>
            </div>
        </div>
        <!-- aside-block -->
        <div class="form-block">
            <h2>Domain Extension</h2>
            <!-- list-radios -->
            <ul class="list-inline list-radios">
                @foreach($tlds->record AS $tld)
                <li>
                    <label class="fake-checkbox" for="{{ $tld->tld }}">
                        <input type="checkbox" id="{{ $tld->tld }}" name="tld[]" value="{{ $tld->tld }}">
                        <span></span>
                    </label>
                    <label for="{{ $tld->tld }}">{{ $tld->tld }}</label>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="btn-block text-center">
            <input type="submit" class="btn btn-primary" value="go" id="btnSubmit">
        </div>
    </form>
</aside>

<div class="main-container">
    <!-- section-head -->
    <header class="section-head">
        <div class="head clearfix">
            <a href="#" class="sidebar-opener btn btn-default pull-left"><span class="hidden-xs">Filter Options</span> <i class="icon-filter"></i> <i class="glyphicon glyphicon-remove"></i></a>
            <!-- nav-products -->
            <nav class="nav-products pull-left">
                <a href="#" class="btn-opener"><i class="glyphicon glyphicon-chevron-down"></i></a>
                <ul class="list-inline products-drop">
                    <li @if($tab == 'brandable')class="active"@endif><a href="/brandable">BRANDABLE</a></li>
                    <li @if($tab == 'premium')class="active"@endif><a href="/premium">PREMIUM</a></li>
                    <li @if($tab == 'under1k')class="active"@endif><a href="/under1k">UNDER 1K</a></li>
                    <!--<li><a href="#">WEBSITES</a></li>
                    <li><a href="#">SOLD DOMAINS</a></li>-->
                </ul>
            </nav>
            <!-- form-search -->
            <form action="/{{$tab}}" class="form-search pull-right">
                <label for="search"> </label>
                <div class="form-holder">
                    <input type="search" class="form-control" id="search" name="search" placeholder="Search for domains...">
                    <button type="submit" class="btn-link"><i class="icon-search"></i></button>
                </div>
            </form>
        </div>
    </header>
    <!-- container -->
    <div class="container">
        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/{{$tab}}">{{$tab}}</a></li>
            <li class="active">@if($details->record > 0){{$details->domain->domain}}@endif</li>
        </ol>
        <div class="row">
            <!-- contain the main content of the page -->
            <section id="content" class="col-xs-12 col-sm-8">
                <!-- post -->
                <article class="post item item-list">
                    <h2>Domain</h2>
                    <div class="post-info">
                        <div class="img-holder">
                            <a href="#" class="btn-favorite"><i class="icon-heart-o"></i></a>
                            @if($tab == 'brandable' AND $details->record > 0)
                            <!-- picturefill html structure example -->
                            <span data-picture data-alt="image description">
                                <span data-src="{{ $url }}image.php?id_logo={{ $details->domain->id_logo }}" ></span>
                                <span data-src="{{ $url }}image.php?id_logo={{ $details->domain->id_logo }}" data-media="(-webkit-min-device-pixel-ratio:1.5), (min-resolution:1.5dppx)" ></span>
                                <!--[if (lt IE 9) & (!IEMobile)]>
                                    <span data-src="images/img2-2x.jpg"></span>
                                <![endif]-->
                                <!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
                                <noscript><img src="{{ $url }}image.php?id_logo={{ $details->domain->id_logo }}" alt="image description"></noscript>
                            </span>
                            @endif
                        </div>
                        <h3><a href="#">@if($details->record > 0){{$details->domain->domain}}@endif</a></h3>
                    </div>
                    <h2>Domain Information</h2>
                    <!-- table -->
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Description</td>
                                <td>@if($details->record > 0){{$details->domain->description}}@endif</td>
                            </tr>
                            <tr>
                                <td>Categories</td>
                                <td>@if($details->record > 0){{$details->domain->category_title}}@endif</td>
                            </tr>
                            <tr>
                                <td>Keywords</td>
                                <td>@if($details->record > 0){{$details->domain->keywords}}@endif</td>
                            </tr>
                            <!--<tr>
                                <td>Language</td>
                                <td>@if($details->record > 0){{$details->domain->language}}@endif</td>
                            </tr>-->
                            <tr>
                                <td>TLD</td>
                                <td>@if($details->record > 0){{$details->domain->tld}}@endif</td>
                            </tr>
                            <tr>
                                <td>Reg. Date</td>
                                <td><time datetime="@if($details->record > 0){{$details->domain->reg_date}}@endif">@if($details->record > 0){{date("F d, Y", strtotime($details->domain->reg_date))}}@endif</time></td>
                            </tr>
                            <tr>
                                <td>Length</td>
                                <td>@if($details->record > 0){{$details->domain->length}}@endif</td>
                            </tr>
                        </tbody>
                    </table>
                </article>
            </section>
            <!-- contain sidebar of the page -->
            <aside class="aside col-xs-12 col-sm-4 pull-right">
                <!-- form-order -->
                <form action="mail.php" method="post" class="box form-order">
                    @if($details->record > 0 AND $details->domain->makeoffer == 0)
                    <div class="info">
						<h2 class="hidden" ><a>@if($details->record > 0){{$details->domain->domain}}@endif</a></h2>
                        
                        @if($details->domain->price > 0)
                                <strong class="price">@if($details->record > 0){{ $details->domain->price_formatted }}@endif</strong>
                                <a href="#" class="btn btn-default btn-buy">buy now</a>
                        @else
                                <a href="#" class="btn btn-success btn-buy">Make offer</a>
                        @endif
		                <input type="hidden" name="pricedata" value="@if($details->record > 0){{$details->domain->price}}@endif">
						<input type="hidden" name="offer_pricedata" value="@if($details->record > 0){{$details->domain->offer_price}}@endif">
						<input type="hidden" name="domainId" value="@if($details->record > 0){{$details->domain->id_domain}}@endif">
                    </div>
                    @else
                    <div class="form-holder alert">
                        <a href="#" class="close btn-close" data-dismiss="alert" aria-label="Close"><i class="icon-close"></i></a>
                        <div class="field-row">
                            <span class="label"><label for="offer-buy">New Offer To Buy</label></span>
                            <input type="text" id="offer-buy" name="offer" class="form-control" data-required="true">
                        </div>
                        <div class="field-row">
                            <span class="label"><label for="message">Message to the Seller</label></span>
                            <textarea id="message" name="message" class="form-control" data-required="true"></textarea>
                        </div>
                        <input type="submit" value="Send Message" class="btn btn-primary">
                    </div>
                    @endif
                    <div class="social-area">
                        <!-- social-networks -->
                        <ul class="social-networks list-inline">
                            <li><a href="#"><i class="icon-envelope3"></i></a></li>
                            <li><a href="https://twitter.com/brand_eden" target="_blank"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-linkedin"></i></a></li>
                            <li><a href="#"><i class="icon-google"></i></a></li>
                            <li><a href="https://www.facebook.com/Brand-Eden-330411214041495/" target="_blank"><i class="icon-facebook"></i></a></li>
                        </ul>
                    </div>
                </form>
                <!-- media -->
				<script>
					$(document).ready(function(){
							$('.media iframe').height(($('.media iframe').width() * 9 / 16)+'px');
							$('.media iframe').width($('.media iframe').width()+'px');
							$('.media iframe').removeAttr('width');
					});
				</script>
                <div class="media aspect-ratio">
					<iframe width="100%" src="https://www.youtube.com/embed/xQWRC9Enat4" frameborder="0" allowfullscreen ></iframe>
                </div>
            </aside>
        </div>
        <!-- products-area -->
        @if($related_domains->rows > 0)
	    <section class="products-area">
			<h2>Related Brandable Domains</h2>
            <div class="products-holder row">

			@if($related_domains->rows > 0)
				@foreach($related_domains->record AS $d)
                
		            <!-- item -->
		            <article class="item col-xs-12 col-sm-6 col-md-4 col-lg-4 {{$class[current($d)]}}" data-date="{{date("F d Y", strtotime($d->reg_date))}}" data-price="{{$d->price}}" data-expires="" >
		                <div class="box box-link">
		                    <div class="img-holder">
		                        <!-- picturefill html structure example -->
		                        	<!--<img src="/images/loader.gif" class="loader" />-->
		                            <img src="{{ $url }}image.php?id_logo={{ $d->id_logo }}" alt="image description" class="logo_img">
		                        
		                    </div>
		                    <!-- <h2><a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}">{{$d->domain}}</a></h2> -->
		                    <h2><a href="/{{ $d->domain }}">{{$d->domain}}</a></h2>
		                    <footer class="foot">
		                        <a href="#" data-href="/save/{{ $d->id_domain }}" class="btn btn-info pull-left btn-save">
		                            <span class="text">save</span>
		                            <i class="icon-heart"></i>
		                        </a>
		                        @if($d->price > 0)
		                        <a href="#" class="btn btn-default pull-right btn-buy">
		                            <span class="text">buy now</span>
		                            <i class="icon-shopping-cart"></i>
		                        </a>           
		                        @else
		                        <a href="#" class="btn btn-success pull-right btn-buy">
		                            <span class="text">Make offer</span>
		                            <i class="icon-shopping-cart"></i>
		                        </a> 
		                        @endif            
                                        @if($d->price > 0)
		                                <strong class="price text-center">{{ $d->price_formatted }}</strong>
		                        @else
		                                <strong class="price text-center"> </strong>
		                        @endif

		                        <input type="hidden" name="pricedata" value="{{$d->price}}">
								<input type="hidden" name="offer_pricedata" value="{{$d->offer_price}}">
								<input type="hidden" name="domainId" value="{{ $d->id_domain }}">
		                    </footer>
		                </div>
		            </article>
		            <!-- item -->
				@endforeach
			@endif

            </div>
        </section>
		@endif

        <!-- products-area -->
		@if($related_prem_domains->rows > 0)	        
        <section class="products-area">
			<h2>Related Premium Domains</h2>
            <div class="products-holder row">

			@if($related_prem_domains->rows > 0)
				@foreach($related_prem_domains->record AS $d)

		            <!-- item -->
		            <article class="item item-list col-xs-12 col-sm-6 {{$class[current($d)]}}" data-date="{{date("F d Y", strtotime($d->reg_date))}}" data-price="{{$d->price}}" data-expires="" >

		                <div class="box box-link">
		                    <!-- <h2><a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}">{{$d->domain}}</a></h2> -->
		                    <h2><a href="/{{ $d->domain }}">{{$d->domain}}</a></h2>
		                    <footer class="foot">
		                        @if($d->price > 0)
		                                <strong class="price text-center">{{ $d->price_formatted }}</strong>
		                        @else
		                                <strong class="price text-center"> </strong>
		                        @endif            
		                        @if($d->price > 0)
		                        <a href="#" class="btn btn-default pull-right btn-buy">
		                            <span class="text">buy now</span>
		                            <i class="icon-shopping-cart"></i>
		                        </a>           
		                        @else
		                        <a href="#" class="btn btn-success pull-right btn-buy">
		                            <span class="text">Make offer</span>
		                            <i class="icon-shopping-cart"></i>
		                        </a> 
		                        @endif            
		                        <a href="#" data-href="/save/{{ $d->id_domain }}" class="btn btn-info pull-right btn-save">
		                            <span class="text">save</span>
		                            <i class="icon-heart-o"></i>
		                        </a>
		                        <input type="hidden" name="pricedata" value="{{$d->price}}">
								<input type="hidden" name="offer_pricedata" value="{{$d->offer_price}}">
								<input type="hidden" name="domainId" value="{{ $d->id_domain }}">
		                    </footer>
		                </div>
		            </article>
				@endforeach
			@endif

            </div>
        </section>
		@endif
    </div>
</div>
</main>
@stop
