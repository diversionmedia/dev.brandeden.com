@extends('cartheader')
@section('content')
<script>
    $(document).ready(function(){
		$('#all_cat').click(function(e) {
            $('input[name=category]').removeAttr('checked');
        });
	});
</script>
<!-- contain main informative part of the site -->
			<main id="main">
				<!-- contain sidebar of the page -->

				<div class="main-container">

					<!-- section-head -->
					<header class="section-head hidden">
						<div class="head clearfix">
							<!--
							<a href="#" class="sidebar-opener btn btn-default pull-left"><span class="hidden-xs">{{ trans('messages.Filter Options') }}</span> <i class="icon-filter"></i> <i class="glyphicon glyphicon-remove"></i></a>
							-->
							<!-- nav-products -->
							<nav class="nav-products pull-left">
								<a href="#" class="btn-opener"><i class="glyphicon glyphicon-chevron-down"></i></a>
								<ul class="list-inline products-drop">
									<li><a href="/brandable">{{ trans('messages.BRANDABLE') }}</a></li>
									<li><a href="/premium">{{ trans('messages.PREMIUM') }}</a></li>
									<li><a href="/under1k">{{ trans('messages.UNDER 1K') }}</a></li>
								</ul>
							</nav>
						</div>
					</header>

					<!-- container -->
					<div class="container">

						<br>
						<div class="row">
							<!-- contain the main content of the page -->
							<section id="content" class="col-xs-12">
								<!-- post -->
								<article class="post">
									<h2>Domain</h2>
									<div align="center">
										<h1 class="bg-danger1 text-danger">Error : Domain(<?php echo substr($_SERVER['REQUEST_URI'],1); ?> ) Not Found.</h1>
									</div>
									<div align="left">
										<p> <a href="/"> &laquo; Back to Home Page</a> </p>
									</div>
								</article>
							</section>
						</div>
					</div>
				</div>

				
			</main>
@stop