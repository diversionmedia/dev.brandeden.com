@extends('cartheader')
@section('content')
<script>
    $(document).ready(function(){
		$('#all_cat').click(function(e) {
            $('input[name=category]').removeAttr('checked');
        });
	});
</script>
<!-- contain main informative part of the site -->
			<main id="main">
				<!-- contain sidebar of the page -->
				<?php $d = $details->domain; ?>
				<?php 
					if($details->domain->status == 3)	{	// if dropped redirect to home / main page
						header('Location: /');
						die();
					}
				?>
				<div class="main-container">

					<!-- section-head -->
					<header class="section-head hidden">
						<div class="head clearfix">
							<!--
							<a href="#" class="sidebar-opener btn btn-default pull-left"><span class="hidden-xs">{{ trans('messages.Filter Options') }}</span> <i class="icon-filter"></i> <i class="glyphicon glyphicon-remove"></i></a>
							-->
							<!-- nav-products -->
							<nav class="nav-products pull-left">
								<a href="#" class="btn-opener"><i class="glyphicon glyphicon-chevron-down"></i></a>
								<ul class="list-inline products-drop">
								<?php if( $cnt_seller_settings > 0 ) 	{	?>
										<?php // if( $seller_settings->is_vis_brandable > 0 ) {	?>
											<li @if($tab == 'brandable')class="active"@endif><a href="/brandable">{{ trans('messages.BRANDABLE') }}</a></li>
										<?php // }	?>
										<?php // if( $seller_settings->is_vis_premium > 0 ) {	?>
											<li @if($tab == 'premium')class="active"@endif><a href="/premium">{{ trans('messages.PREMIUM') }}</a></li>
										<?php // }	?>
										<?php // if( $seller_settings->is_vis_under1k > 0 ) {	?>
											<li @if($tab == 'under1k')class="active"@endif><a href="/under1k">UNDER $<?php echo $premiumprice; ?></a></li>
										<?php // }	?>
										<!--<li><a href="#">WEBSITES</a></li>-->
										<!--<li><a href="#">SOLD DOMAINS</a></li>-->
								<?php }	else	{	 ?>
										<li @if($tab == 'brandable')class="active"@endif><a href="/brandable">{{ trans('messages.BRANDABLE') }}</a></li>
										<li @if($tab == 'premium')class="active"@endif><a href="/premium">{{ trans('messages.PREMIUM') }}</a></li>
										<li @if($tab == 'under1k')class="active"@endif><a href="/under1k">{{ trans('messages.UNDER 1K') }}</a></li>
								<?php }	?>
								</ul>
							</nav>
							<!-- form-search -->
							<form action="/{{$tab}}?<?php echo urldecode(http_build_query($_GET)) ?>" class="form-search pull-right">
								<label for="search"> </label>
								<div class="form-holder">
									<input type="search" class="form-control" id="search" name="search" placeholder="{{ trans('messages.Search for domains') }}..." value="{{$search}}">
									<button type="submit" class="btn-link"><i class="icon-search"></i></button>
								</div>
							</form>
						</div>
					</header>

					<!-- container -->
					<div class="container">
						<!-- breadcrumb -->
						<ol class="breadcrumb hidden">
							<li><a href="/">Home</a></li>
							<li><a style="cursor:pointer;">Domains</a></li>
							<li><a href="/<?php echo ($tab); ?>"><?php echo ucfirst($tab); ?></a></li>
							<li class="active">@if($details->record > 0){{ucfirst($details->domain->domain)}}@endif</li>
						</ol>
						<br>
						<div class="row">
							<!-- contain the main content of the page -->
							<section id="content" class="col-xs-12 col-sm-8">
								<!-- post -->
								<article class="post">
									<h2>Domain</h2>
									<div class="post-info">
										<div class="img-holder">
											<?php if ( $id_buyer > 0 ) {	?>
													<?php if ( !in_array($d->id_domain, $fav_domainIDs) ) {	?>
															<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" href="#" class="btn-favorite btn-save3"><i class="icon-heart-o"></i></a>
													<?php }	else	{	?>
															<a style="color: #ff5652;" id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" href="#" class="btn-favorite btn-save3"><i class="icon-heart-o"></i></a>
													<?php }	?>
											<?php }	else	{	?>
													<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" href="#" class="btn-favorite btn-save3"><i class="icon-heart-o"></i></a>
											<?php }	?>
											@if($tab == 'brandable' AND $details->record > 0)
											<!-- picturefill html structure example -->
											<span data-picture data-alt="image description">
												<span data-src="{{ $url }}image.php?id_logo={{ $details->domain->id_logo }}" ></span>
												<span data-src="{{ $url }}image.php?id_logo={{ $details->domain->id_logo }}" data-media="(-webkit-min-device-pixel-ratio:1.5), (min-resolution:1.5dppx)" ></span>
												<!--[if (lt IE 9) & (!IEMobile)]>
													<span data-src="images/img2-2x.jpg"></span>
												<![endif]-->
												<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
												<noscript><img src="{{ $url }}image.php?id_logo={{ $details->domain->id_logo }}" alt="{{ $details->domain->domain }}" title="{{ $details->domain->domain }}"></noscript>
											</span>
											@endif
										</div>
										
										<!-- <h3><a href="#">@if($details->record > 0){{ucfirst($details->domain->domain)}}@endif</a></h3> -->
										
										<br>
										<?php if($details->record > 0){	?>
											<div class="row">
												<p class="status-message text-center bg-success text-success col-lg-12">
													<button type="button" class="close" onclick="$(this).parent().parent().remove();">
														<!--<span>×</span>-->
														<span class="sr-only" onclick="$(this).parent().parent().remove();">Close</span>
													</button>
													<span style="cursor:pointer;font-size:31px;word-break:break-all;text-align:center;margin:0;"> 
														@if($details->record > 0){{ucfirst($details->domain->domain)}}@endif 
													</span>
												</p>
												<div class="clearfix"></div>
											</div>
										<?php }	?>
										
										
										<p class="text-muted" >"@if($details->record > 0){{ucfirst($details->domain->domain)}}@endif" is for sale. A smooth and common combination of words, which suggests upward progress, growth and promotion. A category killer brand name that will garner instant attraction and quick credibility</p>
										<br>
									</div>
									<h2>Domain Information</h2>
									<!-- table -->
									<table class="table">
										<tbody>
											<tr>
												<td>Description</td>
												<td>@if($details->record > 0){{$details->domain->description}}@endif</td>
											</tr>
											<tr>
												<td>Categories</td>
												<td>@if($details->record > 0){{$details->domain->category_title}}@endif</td>
											</tr>
											<tr>
												<td>Keywords</td>
												<td>@if($details->record > 0){{$details->domain->keywords}}@endif</td>
											</tr>
											<!--<tr>
												<td>Language</td>
												<td>@if($details->record > 0){{$details->domain->language}}@endif</td>
											</tr>-->
											<tr>
												<td>TLD</td>
												<td>@if($details->record > 0){{$details->domain->tld}}@endif</td>
											</tr>
											<tr>
												<td>Reg. Date</td>
												<td>
													<time datetime="<?php echo $details->domain->reg_date; ?>">
														<?php echo date("F d, Y", strtotime($details->domain->reg_date)); ?>
													</time>
												</td>
												<?php /*
												<td><time datetime="@if($details->record > 0){{$details->domain->reg_date}}@endif">@if($details->record > 0){{date("F d, Y", strtotime($details->domain->reg_date))}}@endif</time></td>-->
												<!--<td><time datetime="@if($details->record > 0){{$details->domain->whois->domain_created}}@endif">@if($details->record > 0){{date("F d, Y", strtotime($details->domain->whois->domain_created))}}@endif</time></td>
												*/ ?>
											</tr>
											<tr>
												<td>Length</td>
												<td>@if($details->record > 0){{$details->domain->length}}@endif</td>
											</tr>
										</tbody>
									</table>
								</article>
							</section>
							<!-- contain sidebar of the page -->
							<aside class="aside col-xs-12 col-sm-4 pull-right">

								<?php if ( $id_buyer > 0 ) {	?>
								
										<?php	if ( in_array($d->id_domain, $order_domainIDs) ) {	?>
													<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
														<div class="info">
															
															<?php /*
															<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
																<span id="btn_<?php echo $d->id_domain; ?>" onclick="viewStatusInfo('<?php echo $order_IPNstatus_Code[$d->id_domain]; ?>', '<?php echo $order_IPNstatus_EscrowUniqueIdentifier[$d->id_domain]; ?>','<?php echo $order_IPNstatus_Description[$d->id_domain]; ?>')" alt="Click to view more info" title="Click to view more info">
																	<?php echo $order_domainStatus[$d->id_domain]; ?>
																</span>
															</a>
															*/ ?>
															
															<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																		<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
																			<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																		</a>
															<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																		<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
																			<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																		</a>																		
															<?php }	else	{	?>
																		<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
																			<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																		</a>
															<?php }	 ?>	
															
														</div>
													</form>
										<?php }	else	{	?>	
										
											<?php if($details->domain->status == 3)	{	?>
												<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
													<div class="info">
														<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
															<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" style="color:#3c9ffb;"> DROPPED </span>
														</a>
													</div>
												</form>
											<?php }	else if($details->domain->status == 2)	{	?>
												<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
													<div class="info">
														<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
															<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" style="color:#3c9ffb;"> HIDDEN </span>
														</a>
													</div>
												</form>
											<?php }	else {	?>
										
													<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
														<div id="buyNowModal2"><div class="modal-body"></div></div>
														<div class="info <?php if($details->record > 0) {  if($details->domain->price <= 0)	{	echo '';	} 	}	?>">
															<?php if( $details->domain->price > 0 ) {	?>
																	<strong class="price"><?php if($details->record > 0) { echo $details->domain->price_formatted; } ?></strong>
																	<input name="pricedata" value="<?php echo $details->domain->price; ?>" type="hidden">
															<?php }	?>
															<?php if($details->domain->price <= 0) {	?>
																<a onclick="$('#send-enquiry2').click();" style="cursor:pointer;" class="btn btn-success">{{ trans('messages.Make offer') }}</a>
															<?php }	else	{	?>
																<a onclick="$('#send-enquiry2').click();" style="cursor:pointer;" class="btn btn-default">{{ trans('messages.Buy now') }}</a>
															<?php }	?>
														</div>
														<div class="info hidden">
															<strong>@if($details->record > 0){{$details->domain->domain}}@endif</strong>
															<input value="@if($details->record > 0){{$details->domain->domain}}@endif" type="url" class="form-control hidden" id="enq-domainName2" name="enq-domainName" placeholder="Brandeden.com" readonly >
															<?php if($details->domain->price <= 0) {	?>
																<a onclick="$('#send-enquiry2').click();" style="cursor:pointer;" class="btn btn-success">{{ trans('messages.Make offer') }}</a>
															<?php }	else	{	?>
																<a onclick="$('#send-enquiry2').click();" style="cursor:pointer;" class="btn btn-default">{{ trans('messages.Buy now') }}</a>
															<?php }		?>
														</div>
														<?php 
														$s_id_buyer = '';
														if( isset($_SESSION['id_buyer']) )	{	$s_id_buyer = $_SESSION['id_buyer'];	}
														$s_email = '';
														if( isset($_SESSION['email']) )	{	$s_email = $_SESSION['email'];	}
														?>
														<div class="form-holder alert">
															<div class="field-row">
																<span class="label"><label for="{{ trans('messages.Your Email') }}">{{ trans('messages.Your Email') }}</label></span>
																<input required type="hidden" class="form-control" id="enq-id_buyer2" name="enq-id_buyer" value="<?php echo ($s_id_buyer != '' ? $s_id_buyer : ''); ?> " >
																<input required type="email" class="form-control" id="enq-email2" name="enq-email" placeholder="<?php echo ($s_email != '' ? $s_email : 'Xyz@internet.com'); ?> " value="<?php echo $s_email; ?>" <?php if($s_id_buyer != '' && $s_email != '')	{	echo 'readonly'; }  ?> >
															</div>
															<div class="field-row">
																<span class="label"><label for="{{ trans('messages.Message') }}">{{ trans('messages.Message') }}</label></span>
																<textarea required id="enq-message2" name="enq-message" class="form-control" data-required="true"></textarea>
															</div>
															<div class="field-row @if($details->record > 0) @if($details->domain->price > 0) hidden @endif @endif">
																<span class="label"><label for="enq-domainOffer">{{ trans('messages.Your offer price (in USD)') }}</label></span>
																<input type="number" id="enq-domainOffer2" name="enq-domainOffer" placeholder="{{ trans('messages.Your offer price (in USD)') }}" class="form-control" data-required="true">
															</div>
															  <div class="form-group col-sm-12">
																<label for="enq-recaptcha" class="control-label"> </label>
																<div id="enq-recaptcha" class="g-recaptcha" data-sitekey="6LfseCUTAAAAALVlcV513h29hCq-bCwTq0IUadkV"></div>
															  </div>
															  <div class="clearfix" ></div>
															  <input type="hidden" value="@if($details->record > 0){{$details->domain->id_domain}}@endif" id="enq-domain-id2" name="enq-domain-id" >
															  <input type="hidden" value="@if($details->record > 0){{$details->domain->price}}@endif" id="enq-domain-price2" name="enq-domain-price" >
															<?php if($details->domain->price <= 0)	{	?>
																<button type="button" class="btn btn-primary" id="send-enquiry2" data-loading-text="{{ trans('messages.Sending') }}..." autocomplete="off" >{{ trans('messages.Make offer') }}</button>
															<?php }	else	{	?>
																<button type="button" class="btn btn-primary" id="send-enquiry2" data-loading-text="{{ trans('messages.Sending') }}..." autocomplete="off" >{{ trans('messages.Buy now') }}</button>
															<?php }		?>
														</div>
													</form>
												<?php }	?>
										<?php }	?>
								<?php }	else	{	?>

										<?php	if ( !in_array($d->id_domain, $order_domainIDs) || $order_IPNstatus_Code[$d->id_domain] < 1 ) {		?>
										
											<?php if($details->domain->status == 3)	{	?>
												<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
													<div class="info">
														<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
															<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" style="color:#3c9ffb;"> DROPPED </span>
														</a>
													</div>
												</form>
											<?php }	else if($details->domain->status == 2)	{	?>
												<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
													<div class="info">
														<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
															<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" style="color:#3c9ffb;"> HIDDEN </span>
														</a>
													</div>
												</form>
											<?php }	else {	?>
												<!-- form-order -->
												<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
													<div id="buyNowModal2"><div class="modal-body"></div></div>
													<div class="info <?php if($details->record > 0) {  if($details->domain->price <= 0)	{	echo '';	} 	}	?>">
														<?php if( $details->domain->price > 0 ) {	?>
																<strong class="price"><?php if($details->record > 0) { echo $details->domain->price_formatted; } ?></strong>
																<input name="pricedata" value="<?php echo $details->domain->price; ?>" type="hidden">
														<?php }	?>
														<?php if($details->domain->price <= 0) {	?>
															<a onclick="$('#send-enquiry2').click();" style="cursor:pointer;" class="btn btn-success">{{ trans('messages.Make offer') }}</a>
														<?php }	else	{	?>
															<a onclick="$('#send-enquiry2').click();" style="cursor:pointer;" class="btn btn-default">{{ trans('messages.Buy now') }}</a>
														<?php }	?>
													</div>
													<div class="info hidden">
														<strong>@if($details->record > 0){{$details->domain->domain}}@endif</strong>
														<input value="@if($details->record > 0){{$details->domain->domain}}@endif" type="url" class="form-control hidden" id="enq-domainName2" name="enq-domainName" placeholder="Brandeden.com" readonly >
														<?php if($details->domain->price <= 0) {	?>
															<a onclick="$('#send-enquiry2').click();" style="cursor:pointer;" class="btn btn-success">{{ trans('messages.Make offer') }}</a>
														<?php }	else	{	?>
															<a onclick="$('#send-enquiry2').click();" style="cursor:pointer;" class="btn btn-default">{{ trans('messages.Buy now') }}</a>
														<?php }		?>
													</div>
													<?php 
													$s_id_buyer = '';
													if( isset($_SESSION['id_buyer']) )	{	$s_id_buyer = $_SESSION['id_buyer'];	}
													$s_email = '';
													if( isset($_SESSION['email']) )	{	$s_email = $_SESSION['email'];	}
													?>
													<div class="form-holder alert">
														<div class="field-row">
															<span class="label"><label for="{{ trans('messages.Your Email') }}">{{ trans('messages.Your Email') }}</label></span>
															<input required type="hidden" class="form-control" id="enq-id_buyer2" name="enq-id_buyer" value="<?php echo ($s_id_buyer != '' ? $s_id_buyer : ''); ?> " >
															<input required type="email" class="form-control" id="enq-email2" name="enq-email" placeholder="<?php echo ($s_email != '' ? $s_email : 'Xyz@internet.com'); ?> " value="<?php echo $s_email; ?>" <?php if($s_id_buyer != '' && $s_email != '')	{	echo 'readonly'; }  ?> >
														</div>
														<div class="field-row">
															<span class="label"><label for="{{ trans('messages.Message') }}">{{ trans('messages.Message') }}</label></span>
															<textarea required id="enq-message2" name="enq-message" class="form-control" data-required="true"></textarea>
														</div>
														<div class="field-row @if($details->record > 0) @if($details->domain->price > 0) hidden @endif @endif">
															<span class="label"><label for="enq-domainOffer">{{ trans('messages.Your offer price (in USD)') }}</label></span>
															<input type="number" id="enq-domainOffer2" name="enq-domainOffer" placeholder="{{ trans('messages.Your offer price (in USD)') }}" class="form-control" data-required="true">
														</div>
														  <div class="form-group col-sm-12">
															<label for="enq-recaptcha" class="control-label"> </label>
															<!--<div id="enq-recaptcha" class="g-recaptcha" data-sitekey="6Lfr5xkTAAAAAGjugdzxM1McR1TGWzJWiP9A1GLY"></div>-->
															<div id="enq-recaptcha" class="g-recaptcha" data-sitekey="6LfseCUTAAAAALVlcV513h29hCq-bCwTq0IUadkV"></div>
														  </div>
														  <div class="clearfix" ></div>
														  <input type="hidden" value="@if($details->record > 0){{$details->domain->id_domain}}@endif" id="enq-domain-id2" name="enq-domain-id" >
														  <input type="hidden" value="@if($details->record > 0){{$details->domain->price}}@endif" id="enq-domain-price2" name="enq-domain-price" >
														<?php if($details->domain->price <= 0)	{	?>
															<button type="button" class="btn btn-primary" id="send-enquiry2" data-loading-text="{{ trans('messages.Sending') }}..." autocomplete="off" >{{ trans('messages.Make offer') }}</button>
														<?php }	else	{	?>
															<button type="button" class="btn btn-primary" id="send-enquiry2" data-loading-text="{{ trans('messages.Sending') }}..." autocomplete="off" >{{ trans('messages.Buy now') }}</button>
														<?php }		?>
													</div>
													<!--
													<div class="social-area">
														<ul class="social-networks list-inline">
															<li><a href="#"><i class="icon-envelope3"></i></a></li>
															<li><a href="https://twitter.com/brand_eden" target="_blank"><i class="icon-twitter"></i></a></li>
															<li><a href="#"><i class="icon-linkedin"></i></a></li>
															<li><a href="#"><i class="icon-google"></i></a></li>
															<li><a href="https://www.facebook.com/Brand-Eden-330411214041495/" target="_blank"><i class="icon-facebook"></i></a></li>
														</ul>
													</div>
													-->
												</form>
											<?php }	?>
										<?php }	else	{	?>
										
													<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
													
															<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
																<div class="info">
																	<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" style="color:#3c9ffb;"> SOLD </span>
																	</a>
																</div>
															</form>
													<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
													
															<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
																<div class="info">
																	<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" style="color:#3c9ffb;"> CANCELLED </span>
																	</a>
																</div>
															</form>
													
													<?php }	else	{	?>
													
															<form id="enquiryForm2" action="/enquire" method="post" class="form-order">
																<div class="info">
																	<a style="cursor:pointer;background-color:#fff;border:none;" class="btn btn-success">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" style="color:#3c9ffb;"> PENDING </span>
																	</a>
																</div>
															</form>
													
													
													<?php }	?>
													
										<?php }	?>
										
								<?php }	?>
							
								

								
								<!-- media -->
								<!--
								<div class="media">
									<img src="images/img3.jpg" alt="video description" class="media-img">
								</div>
								-->
								
								<!-- media -->
								
								<script>
									$(document).ready(function(){
											$('.media iframe').height(($('.media iframe').width() * 9 / 16)+'px');
											$('.media iframe').width($('.media iframe').width()+'px');
											$('.media iframe').removeAttr('width');
									});
								</script>
								<div class="media aspect-ratio">
									<iframe width="100%" src="https://www.youtube.com/embed/xQWRC9Enat4" frameborder="0" allowfullscreen ></iframe>
								</div>
								
							</aside>
						</div>
						<!-- products-area -->
						
						 @if($related_domains->rows > 0)
							<section class="products-area">
								<h2>Related Brandable Domains</h2>
								<div class="products-holder row">
								
									@if($related_domains->rows > 0)
										@foreach($related_domains->record AS $d)
									
											<!-- item -->
											<article class="item save col-xs-12 col-sm-6 col-md-4 col-lg-4 {{$class[current($d)]}}" data-date="{{date("F d Y", strtotime($d->reg_date))}}" data-price="{{$d->price}}" data-expires="" >
												<div class="box">
													<div class="img-holder">
														<!-- picturefill html structure example -->
														<span data-picture data-alt="image description">
															<span data-src="{{ $url }}image.php?id_logo={{ $d->id_logo }}" ></span>
															<span data-src="{{ $url }}image.php?id_logo={{ $d->id_logo }}" data-media="(-webkit-min-device-pixel-ratio:1.5), (min-resolution:1.5dppx)" ></span>
															<!--[if (lt IE 9) & (!IEMobile)]>
																<span data-src="images/img1-2x.jpg"></span>
															<![endif]-->
															<!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
															<noscript><img src="{{ $url }}image.php?id_logo={{ $d->id_logo }}" alt="image description" class="logo_img"></noscript>
														</span>
													</div>
													<h2><a href="/{{ $d->domain }}">{{$d->domain}}</a></h2>
													<footer class="foot">
													
														<?php if ( $id_buyer > 0 ) {	?>
															<?php if ( !in_array($d->id_domain, $fav_domainIDs) ) {	?>
																<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="btn btn-info pull-left btn-save2" style="background-color: #fff;border-color: #ff5652;color: #ff5652;">
																	<span class="text" style="display:block;">save</span>
																	<i class="icon-heart" style="display:none;"></i>
																</a>
															<?php }	else	{	?>
																<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="btn btn-info pull-left btn-save2" style="background: #ff5652 none repeat scroll 0 0; color: #fff;">
																	<span class="text" style="display:none;">save</span>
																	<i class="icon-heart" style="display:block;"></i>
																</a>
															<?php }	?>
														<?php }	else	{	?>
															<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="btn btn-info pull-left btn-save2" style="background-color: #fff;border-color: #ff5652;color: #ff5652;">
																<span class="text" style="display:block;">save</span>
																<i class="icon-heart" style="display:none;"></i>
															</a>
														<?php }	?>
														
														<?php if($d->price > 0)	{	?>
															
															<?php if ( $id_buyer > 0 ) {	?>
																	<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
																	
																		<?php if($d->status == 3)	{	?>
																				<a href="#" class="btn pull-right">
																					<span class="text">DROPPED</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	else if($d->status == 2)	{	?>
																				<a href="#" class="btn pull-right">
																					<span class="text">HIDDEN</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	else {	?>
																				<a href="#" class="btn btn-default pull-right btn-buy">
																					<span class="text">buy now</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	?>
																		
																	<?php }	else	{	?>
																	
																			<?php /*
																			<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary pull-right" onclick="viewStatusInfo('<?php echo $order_IPNstatus_Code[$d->id_domain]; ?>', '<?php echo $order_IPNstatus_EscrowUniqueIdentifier[$d->id_domain]; ?>','<?php echo $order_IPNstatus_Description[$d->id_domain]; ?>')" alt="Click to view more info" title="Click to view more info" style="cursor:pointer;">
																				<?php echo $order_domainStatus[$d->id_domain]; ?>
																			</span>
																			
																			*/ ?>
																			<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																						<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary pull-right"> Sold </span>
																			<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																						<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary pull-right"> Cancelled </span>																	
																			<?php }	else	{	?>
																						<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																			<?php }	 ?>	
																		
																		
																	<?php }	?>
															<?php }	else	{	?>
																		<?php	if ( !in_array($d->id_domain, $order_domainIDs) || $order_IPNstatus_Code[$d->id_domain] < 1 ) {	?>
																				<?php if($d->status == 3)	{	?>
																						<a href="#" class="btn pull-right">
																							<span class="text">DROPPED</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	else if($d->status == 2)	{	?>
																						<a href="#" class="btn pull-right">
																							<span class="text">HIDDEN</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	else {	?>
																						<a href="#" class="btn btn-default pull-right btn-buy">
																							<span class="text">buy now</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	?>
																		<?php }	else	{	?>
																		
																					<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																								<a href="#" class="btn pull-right">
																									<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																									<i class="icon-shopping-cart"></i>
																								</a>
																					<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																								<a href="#" class="btn pull-right">
																									<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																									<i class="icon-shopping-cart"></i>
																								</a>																		
																					<?php }	else	{	?>
																								<a href="#" class="btn pull-right">
																									<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																									<i class="icon-shopping-cart"></i>
																								</a>
																					<?php }	 ?>	
																		
																		<?php }	?>
																	
															<?php }	?>
															
														<?php }	else	{	?>														
																<?php if($d->status == 3)	{	?>
																		<a href="#" class="btn pull-right">
																			<span class="text">DROPPED</span>
																			<i class="icon-shopping-cart"></i>
																		</a>
																<?php }	else if($d->status == 2)	{	?>
																		<a href="#" class="btn pull-right">
																			<span class="text">HIDDEN</span>
																			<i class="icon-shopping-cart"></i>
																		</a>
																<?php }	else {	?>
																		<a href="#" class="btn btn-success pull-right btn-buy">
																			<span class="text">Make offer</span>
																			<i class="icon-shopping-cart"></i>
																		</a> 
																<?php }	?>
														<?php }	?>
														
														
														<?php	if ( $id_buyer > 0 ) {	?>
																<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
																		@if($d->price > 0)
																				<strong class="price text-center">{{ $d->price_formatted }}</strong>
																		@else
																				<strong class="price text-center"> </strong>
																		@endif
																<?php }	?>
														<?php }	else	{	?>
																@if($d->price > 0)
																		<strong class="price text-center">{{ $d->price_formatted }}</strong>
																@else
																		<strong class="price text-center"> </strong>
																@endif
														<?php }	?>
														
														<input type="hidden" name="pricedata" value="{{$d->price}}">
														<input type="hidden" name="offer_pricedata" value="{{$d->offer_price}}">
														<input type="hidden" name="domainId" value="{{ $d->id_domain }}">
														
													</footer>
												</div>
											</article>
											
										@endforeach
									@endif
									
								</div>
							</section>
						@endif
						
						<!-- products-area -->
						@if($related_prem_domains->rows > 0)
							<section class="products-area">
								<h2>Related Premium Domains</h2>
								<div class="products-holder row">
								
									@if($related_prem_domains->rows > 0)
										@foreach($related_prem_domains->record AS $d)
								
											<!-- item -->
											<article class="item-list col-xs-12 col-sm-6 {{$class[current($d)]}}" data-date="{{date("F d Y", strtotime($d->reg_date))}}" data-price="{{$d->price}}" data-expires="">
												<div class="box">
													<h2><a href="/{{ $d->domain }}">{{$d->domain}}</a></h2>
													<footer class="foot">
														<?php if($d->price > 0) {	?>
																<?php if ( $id_buyer > 0 ) {	?>
															
																		<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
																				<?php if($d->status == 3)	{	?>
																						<a href="#" class="btn pull-right">
																							<span class="text">DROPPED</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	else if($d->status == 2)	{	?>
																						<a href="#" class="btn pull-right">
																							<span class="text">HIDDEN</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	else {	?>
																						<strong class="price text-center">{{ $d->price_formatted }}</strong>
																						<a href="#" class="btn btn-default pull-right btn-buy">
																							<span class="text">buy now</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	?>
																		<?php }	else	{	?>																
																				<?php /*
																				<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary pull-right" onclick="viewStatusInfo('<?php echo $order_IPNstatus_Code[$d->id_domain]; ?>', '<?php echo $order_IPNstatus_EscrowUniqueIdentifier[$d->id_domain]; ?>','<?php echo $order_IPNstatus_Description[$d->id_domain]; ?>')" alt="Click to view more info" title="Click to view more info" style="cursor:pointer;">
																					<?php echo $order_domainStatus[$d->id_domain]; ?>
																				</span>
																				<i class="icon-shopping-cart"></i>
																				*/ ?>
																				<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																							
																								<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary pull-right" style="cursor:pointer;"> Sold </span>
																								<i class="icon-shopping-cart"></i>
																							
																				<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																							
																								<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary pull-right" style="cursor:pointer;"> Cancelled </span>
																								<i class="icon-shopping-cart"></i>
																																					
																				<?php }	else	{	?>
																							
																								<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary pull-right" style="cursor:pointer;">> Pending </span>
																								<i class="icon-shopping-cart"></i>
																							
																				<?php }	 ?>	
																		<?php }	?>
																
																<?php }	else	{	?>
																		<?php	if ( !in_array($d->id_domain, $order_domainIDs) || $order_IPNstatus_Code[$d->id_domain] < 1 ) {	?>
																				<?php if($d->status == 3)	{	?>
																						<a href="#" class="btn pull-right">
																							<span class="text">DROPPED</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	else if($d->status == 2)	{	?>
																						<a href="#" class="btn pull-right">
																							<span class="text">HIDDEN</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	else {	?>
																						<strong class="price text-center">{{ $d->price_formatted }}</strong>
																						<a href="#" class="btn btn-default pull-right btn-buy">
																							<span class="text">buy now</span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																				<?php }	?>
																		<?php }	else	{	?>
																		
																		
																					<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																								<a href="#" class="btn pull-right">
																									<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																									<i class="icon-shopping-cart"></i>
																								</a>
																					<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																								<a href="#" class="btn pull-right">
																									<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																									<i class="icon-shopping-cart"></i>
																								</a>																		
																					<?php }	else	{	?>
																								<a href="#" class="btn pull-right">
																									<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																									<i class="icon-shopping-cart"></i>
																								</a>
																					<?php }	 ?>	

																		<?php }	?>
																	
																<?php }	?>
															
														<?php }	else	{	?>
																<?php if($d->status == 3)	{	?>
																		<a href="#" class="btn pull-right">
																			<span class="text">DROPPED</span>
																			<i class="icon-shopping-cart"></i>
																		</a>
																<?php }	else if($d->status == 2)	{	?>
																		<a href="#" class="btn pull-right">
																			<span class="text">HIDDEN</span>
																			<i class="icon-shopping-cart"></i>
																		</a>
																<?php }	else {	?>
																		<strong class="price text-center"> </strong>
																		<a href="#" class="btn btn-success pull-right btn-buy">
																			<span class="text">Make offer</span>
																			<i class="icon-shopping-cart"></i>
																		</a>
																<?php }	?>
														<?php }	?>
														
														
														
														<?php if ( $id_buyer > 0 ) {	?>
															<?php if ( !in_array($d->id_domain, $fav_domainIDs) ) {	?>
																<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="btn btn-info pull-right btn-save4">
																	<span class="text">save</span>
																	<i class="icon-heart-o"></i>
																</a>
															<?php }	else	{	?>
																<a id="btn-love-{{ $d->id_domain }}" style="background: #ff5652 none repeat scroll 0 0; color: #fff;" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="btn btn-info pull-right btn-save4">
																	<span class="text">save</span>
																	<i class="icon-heart-o"></i>
																</a>
															<?php }	?>
															
														<?php }	else	{	?>
														
															<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="btn btn-info pull-right btn-save4">
																<span class="text">save</span>
																<i class="icon-heart-o"></i>
															</a>
															
														<?php }	?>
														
														<input type="hidden" name="pricedata" value="{{$d->price}}">
														<input type="hidden" name="offer_pricedata" value="{{$d->offer_price}}">
														<input type="hidden" name="domainId" value="{{ $d->id_domain }}">
													</footer>
												</div>
											</article>
											
										@endforeach
									@endif
											
								</div>
							</section>
						@endif
					</div>
				</div>
				
	

<!--Modal-->
    <div class="modal fade" id="statusModal" role="dialog" onmouseover="$('.modal-backdrop').remove();">
    <div class="modal-dialog">
		<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="order_status_title">Status Info</h4>
			  </div>
			  <div class="modal-body">
				<div id="order_status_content"></div>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" id="send-close" data-dismiss="modal">{{ trans('messages.Close') }}</button>
			  </div>
		</div>
	</div>
	</div>
	
<script>
function viewStatusInfo(status_code, transactionID, escrow_status)	{
	var title = '';
	var content = '';
	if( status_code == 0 )	{
		title = escrow_status;
		content = '<p>Seller have cancelled the transaction. Login to your Escrow account to check the reason why the Seller cancelled this transaction <b>#'+transactionID+'</b>.</p>';
	}
	else if( status_code == 5 )	{
		title = 'Pending';
		content = '<p>Seller haven\'t check or review your enquiry transaction <b>#'+transactionID+'</b>. You can send a reminder by logging-in to your Escrow account.</p>';
	}
	else if( status_code == 15 )	{
		title = 'Pay Now !';
		content = '<p>You can now pay the domain you have enquire. Login to your Escrow account to select a payment method. <b> Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 20 )	{
		title = 'Payment Submitted';
		content = '<p>You have already submitted a payment. Please wait while Escrow securing your payment. <b> Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 25 )	{
		title = 'Payment Accepted';
		content = '<p>Seller have securely received your payment. Seller will release your domain as soon as possible. You can send a message or email to send a reminder. <b>Transaction #'+transactionID+'</b>. </p>';
	}
	else	{
		title = escrow_status;
		content = '<p>You can view the details of this transaction. Login to your Escrow account to check this transaction <b>#'+transactionID+'</b>.</p>';
	}
	
	content += '<div style="color:#6f7c96;font-size:12px;">';
	content += '<br> Escrow Transaction ID : ' + transactionID;
	content += '<br> Escrow Status : ' + escrow_status;
	content += '</div>';
	
	$('#order_status_title').html(title);
	$('#order_status_content').html(content);
	$('#statusModal').modal();
	setTimeout(function(){ 
		$('.modal-backdrop').remove();
	}, 1000);
}
</script>
				
			</main>
@stop