<?php
header('Location: http://'.$_SERVER['SERVER_NAME'].'/');
exit;
?>
<!DOCTYPE html>
<html>
<head>
	<!-- set the encoding of your site -->
	<meta charset="utf-8">
	<!-- set the viewport width and initial-scale on mobile devices -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Brandeden</title>
	<link rel="icon" href="<?php echo asset('images/icon.ico')?>" type="image/x-icon">
	<!-- include the Montserrat Font stylesheet -->
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<!-- include the bootstrap stylesheet -->
	<link rel="stylesheet" href="<?php echo asset('css/bootstrap.css')?>">
	<!-- include the site stylesheet -->
	<link rel="stylesheet" href="<?php echo asset('css/font-awesome/css/font-awesome.min.css')?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo asset('css/main-cart.css')?>">
    <!-- include the datapicker stylesheet -->
	<link rel="stylesheet" type="text/css" href="/css/jquery-ui-cart.min.css">
   	<!-- include jQuery library -->
	<script type="text/javascript" src="<?php echo (isset($_SERVER["HTTPS"]) ? 'https://' : 'http://'); ?>ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript">window.jQuery || document.write('<script src="/js/jquery-1.11.2.min.js"><\/script>')</script>
	<!-- include Bootstrap JavaScript -->
	<script src="/js/bootstrap.min.js"></script>
	<!-- include custom JavaScript -->
	<script src="/js/jquery.main.js"></script>
    <!-- include datepicker JavaScript -->
	<script type="text/javascript" src="/js/jq.ui.datepicker.js"></script>
	<!-- include custom JavaScript for sidebar -->
	<script src="/js/jquery.main.sidebar.js"></script>
	<!-- recaptcha -->
	<script src='https://www.google.com/recaptcha/api.js'></script>

    <!--include money.js-->
    <script src="/js/money.js"></script>
    <script src="/js/accounting.min.js"></script>
	<!-- include utility JavaScript -->
	<script type="text/javascript">
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
			document.createTextNode(
			  '@-ms-viewport{width:auto!important}'
			)
			)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>

    <script type="text/javascript">
		
		(function() {
			if (typeof window.janrain !== 'object') window.janrain = {};
			if (typeof window.janrain.settings !== 'object') window.janrain.settings = {};
			
			janrain.settings.tokenUrl = '<?php echo config('app.BASE_URL'); ?>/login';
			janrain.settings.type = 'modal';
			janrain.settings.appId = 'idbpadndmkmpcnggmojf';
			janrain.settings.appUrl = 'https://zendomain.rpxnow.com/';
			//janrain.settings.providers = ['googleplus', 'yahoo'];//['facebook', 'googleplus', 'blogger', 'aol', 'yahoo', 'openid'];
			//janrain.settings.providers = ['facebook', 'googleplus', 'blogger', 'aol', 'yahoo', 'openid'];
			janrain.settings.providers = ['facebook', 'googleplus', 'twitter', 'amazon', 'microsoftaccount','linkedin'];
			janrain.settings.actionText = 'Choose a provider!';
			janrain.settings.showAttribution = false;
			janrain.settings.fontColor = '#a020f0';
			janrain.settings.fontFamily = 'impact, playbill, fantasy';		
			function isReady() { janrain.ready = true; };
			if (document.addEventListener) {
			  document.addEventListener("DOMContentLoaded", isReady, false);
			} else {
			  window.attachEvent('onload', isReady);
			}
		
			var e = document.createElement('script');
			e.type = 'text/javascript';
			e.id = 'janrainAuthWidget';
		
			if (document.location.protocol === 'https:') {
			  e.src = 'https://rpxnow.com/js/lib/zendomain/engage.js';
			} else {
			  e.src = 'http://widget-cdn.rpxnow.com/js/lib/zendomain/engage.js';
			}
		
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(e, s);
		})();
		
		$(document).ready(function(){			

			//Trigger a click on load to adjust screen heights.
			if($('html').width() > 1399)
				$('.sidebar-opener').click();

			//Click boxes and letsgo
			$('a.btn-save, a.btn-buy').hover(
			  	function() {
					$(this).parent().parent().removeClass('box-link');
			  	},
			  	function() {
					$(this).parent().parent().addClass('box-link');
			  	}
			);
			$('body').on('click', 'div.box-link', function(){
			  	var link = $(this).find('h2>a').attr('href');
				if($(this).hasClass('box-link'))
					window.location.href = link;
			});

			$('body').on("click", 'a.btn-save', function(e){
			
				$link = $(this);
				$.ajax({
					type: "POST",
					url: $link.data('href'),
					success: function(r) {
						/*if($link.closest('article').hasClass('save'))
							$link.closest('article').removeClass('save');
						else
							$link.closest('article').addClass('save');*/
					},
					error: function(xhr, exception){
						alert('Something went wrong!! Please try later.');
					}
				});
				return false;
			});

			$('body').on("click", 'a.btn-save3', function(e)	{
				$link = $(this);
				var id = $link.data('id');
				console.log(id);
				$.ajax({
					type: "POST",
					url: $link.data('href'),
					success: function(r) {
						console.log(r);
						if(r == 'added')	{	$('#btn-love-'+id).attr('style','color: #ff5652;');	}
						else	{	$('#btn-love-'+id).attr('style','color: #caccd8;');	}
					},
					error: function(xhr, exception)	{	alert('Something went wrong!! Please try later.');	}
				});
				return false;
			});
			
			$('body').on("click", 'a.btn-save2', function(e){
				$link = $(this);
				var id = $link.data('id');
				console.log(id);
				$.ajax({
					type: "POST",
					url: $link.data('href'),
					success: function(r) {
						console.log(r);
						if(r == 'added')	{
							$('#btn-love-'+id).attr('style','background: #ff5652 none repeat scroll 0 0; color: #fff;');
							$('#btn-love-'+id).html('<i class="icon-heart" style="display:block;"></i>');
						}
						else	{
							$('#btn-love-'+id).attr('style','background-color: #fff;border-color: #ff5652;color: #ff5652;');
							$('#btn-love-'+id).html('<span class="text" style="display:block;">save</span>');
						}
					},
					error: function(xhr, exception){
						alert('Something went wrong!! Please try later.');
					}
				});
				return false;
			});
			
			$('body').on("click", 'a.btn-save4', function(e){
				$link = $(this);
				var id = $link.data('id');
				console.log(id);
				$.ajax({
					type: "POST",
					url: $link.data('href'),
					success: function(r) {
						console.log(r);
						if(r == 'added')	{
							$('#btn-love-'+id).attr('style','background: #ff5652 none repeat scroll 0 0; color: #fff;');
						}
						else	{
							$('#btn-love-'+id).attr('style','background-color: #fff;border-color: #ff5652;color: #ff5652;');
						}
					},
					error: function(xhr, exception){
						alert('Something went wrong!! Please try later.');
					}
				});
				return false;
			});
			
			$('body').on("click", 'a.btn-buy', function(){
				$('#buyNowModal').modal();
				$('.box').removeClass('buyNowActiveBox');
				$(this).parent().parent().addClass('buyNowActiveBox');
				return false;
			});
			
			$('#buyNowModal').on('shown.bs.modal', function (event) {
				$('#enq-domainName').val($('.buyNowActiveBox h2>a').text());
				$('#enq-domainPrice').val($('.buyNowActiveBox strong.price').text());
				$('#enq-domain-price').val($('.buyNowActiveBox [name=pricedata]').val());
				$('#enq-domainOffer').val($('.buyNowActiveBox input[name=offer_pricedata]').val());
				$('#enq-domain-id').val($('.buyNowActiveBox input[name=domainId]').val());
				//$('#enq-email').val('');
				//$('#enq-message').val('');

				if($('#enq-domain-price').val() <= 0){
					$('#enq-domainOffer').parent().show();
			                $('#send-enquiry').removeClass('btn-primary');
			                $('#send-enquiry').addClass('btn-success');
			                $('#send-enquiry').addClass('btn-make-offer');
			                $('#send-enquiry').text('<?php echo trans('messages.Make offer') ?>');
			                $('#send-close').removeClass('btn-default').addClass('btn-success');
			                $('#enq-domainOffer').parent().show();
			                $('#enq-domainPrice').parent().hide();
				}else{
				
				        $('#enq-domainOffer').parent().hide();
			                $('#send-enquiry').addClass('btn-primary');
			                $('#send-enquiry').removeClass('btn-success');
				        $('#send-enquiry').removeClass('btn-make-offer');
				        $('#send-enquiry').text('<?php echo trans('messages.Send enquiry') ?>');
				        $('#send-close').removeClass('btn-success').addClass('btn-default');
			                $('#enq-domainOffer').parent().hide();
			                $('#enq-domainPrice').parent().show();
			        }

				$('#enq-email').focus();
			});

			$('#buyNowModal').on('hidden.bs.modal', function (event) {
				$('#buyNowModal .status-message').remove();
			});

			$('#send-enquiry').click(function()	{
				if( !ValidateEmail( $('#enq-email').val() )  )	{
					alert('Please provide a valid email.');
					$('#enq-email').attr('style','border-color:#d9534f;');
					$('#enq-email').focus();
					$('#enq-message').attr('style','border-color:none;');
					$('#enq-domainOffer').attr('style','border-color:none;');
				}
				else if( $.trim( $('#enq-message').val() ) == '' )	{
					alert('Please provide a message.');
					$('#enq-message').attr('style','border-color:#d9534f;');
					$('#enq-message').focus();
					$('#enq-email').attr('style','border-color:none;');
					$('#enq-domainOffer').attr('style','border-color:none;');
				}
				else if( ($.trim( $('#send-enquiry').text() ) == 'Make offer') && ($('#enq-domainOffer').val() <= 0) ) 	{
					alert('Offer price should be greater than 0.');
					$('#enq-domainOffer').attr('style','border-color:#d9534f;');
					$('#enq-domainOffer').focus();
					$('#enq-email').attr('style','border-color:none;');
					$('#enq-message').attr('style','border-color:none;');
				}
				else	{
					$('#enq-email').attr('style','border-color:#none;');
					$('#enq-domainOffer').attr('style','border-color:#none;');
					$('#enq-message').attr('style','border-color:#none;');
					
					var $btn = $(this);
					$btn.button('loading');
					$('#buyNowModal .status-message').remove();
					// business logic...
					$.post("/enquire", $("#enquiryForm").serialize()).done(function(json){
						var r = $.parseJSON(json);
						var status = r.status.toLowerCase();
						var message = r.message;
						$('<p class="status-message text-center bg-'+status+' text-'+status+' col-lg-12" >'+message+'</p>').prependTo('#buyNowModal .modal-body');
						$btn.button('reset');
						
						<?php
						$s_email = '';
						if( isset($_SESSION['email']) )	{	$s_email = $_SESSION['email'];	}
						if( $s_email == '' )	{	?>	$('#enq-email').val('');	<?php	} ?>
						$('#enq-message').val('');					
						
						$('#enq-domainOffer').val(0);
						return false;
					});
				}
				return false;
			});
			
		});

</script>
<script type="text/javascript">
var fxMoney;
var $currency_symbol = '$';
$(document).ready(function() {
    $('.currency').click(function(e){
		// Load exchange rates data via AJAX:
		var $currency = $(this).text();
		$('#change_flag').html('<span class="currency-flag currency-flag-'+$currency.toLowerCase()+'"></span>');
		console.log( $currency );
		$currency_symbol = $(this).data('symbol');
		console.log( $currency_symbol );
		$('#btn_currency').text($currency_symbol + ' ' + $currency);
		//$('#btn_currency').text($currency);
		$('#overlay-wrapper').show();
		$.ajax({
				type: "GET",
				url: "/currency",
				dataType: 'json',
				success: function(data) {
					if(data.result_code == 0)
					{
						// Check money.js has finished loading:
						if ( typeof fx !== "undefined" && fx.rates ) {
							fx.rates = data.rates;
							fx.base = data.base;
						} else {
							// If not, apply to fxSetup global:
							var fxSetup = {
								rates : data.rates,
								base : data.base
							}
						}
						fx.settings = { from: 'USD', to: $currency };
						fxMoney = fx;
						_changeprice(fxMoney, $currency_symbol);
					}
					else
						alert(data.response_details);
					$('#overlay-wrapper').hide();
				}
		});	
		e.preventDefault();	
	});	
});
function _changeprice(fx, $currency_symbol)
{
	
	var check_current = $('#current_currency').val();
	//if( check_current != $currency_symbol )	{
		
		$('#current_currency').val( $currency_symbol );
	
		$('input[name=pricedata]').each(function(index, element) {
			var amount = fx.convert($(this).val());
			if( amount > 0 )	{
				amount = accounting.formatMoney(amount, {
					symbol: $currency_symbol,
					format: "%s%v"
				});
				
				var str_amt = amount.split('.');
				
				//$(this).siblings('.price').text(amount);
				$(this).siblings('.price').text(str_amt[0]);
			}
		});
}
</script>
    <style>
	.close {
    color: #000;
    float: right;
    font-size: 21px;
    font-weight: 700;
    line-height: 1;
    opacity: 0.2;
    text-shadow: 0 1px 0 #fff;
}
.close:focus, .close:hover {
    color: #000;
    cursor: pointer;
    opacity: 0.5;
    text-decoration: none;
}
button.close {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: 0 none;
    cursor: pointer;
    padding: 0;
}
.modal-open {
    overflow: hidden;
}
.modal {
    bottom: 0;
    display: none;
    left: 0;
    outline: 0 none;
    overflow: hidden;
    position: fixed;
    right: 0;
    top: 0;
    z-index: 1050;
}
.modal.fade .modal-dialog {
    transform: translate(0px, -25%);
    transition: transform 0.3s ease-out 0s;
}
.modal.in .modal-dialog {
    transform: translate(0px, 0px);
}
.modal-open .modal {
    overflow-x: hidden;
    overflow-y: auto;
}
.modal-dialog {
    margin: 10px;
    position: relative;
    width: auto;
}
.modal-content {
    background-clip: padding-box;
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 6px;
    box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
    outline: 0 none;
    position: relative;
}
.modal-backdrop {
    background-color: #000;
    bottom: 0;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
    z-index: 1040;
}
.modal-backdrop.fade {
    opacity: 0;
}
.modal-backdrop.in {
    opacity: 0.5;
}
.modal-header {
    border-bottom: 1px solid #e5e5e5;
    padding: 15px;
}
.modal-header .close {
    margin-top: -2px;
}
.modal-title {
    line-height: 1.42857;
    margin: 0;
}
.modal-body {
    padding: 15px;
    position: relative;
}
.modal-footer {
    border-top: 1px solid #e5e5e5;
    padding: 15px;
    text-align: right;
}
.modal-footer .btn + .btn {
    margin-bottom: 0;
    margin-left: 5px;
}
.modal-footer .btn-group .btn + .btn {
    margin-left: -1px;
}
.modal-footer .btn-block + .btn-block {
    margin-left: 0;
}
.modal-scrollbar-measure {
    height: 50px;
    overflow: scroll;
    position: absolute;
    top: -9999px;
    width: 50px;
}
@media (min-width: 768px) {
.modal-dialog {
    margin: 30px auto;
    width: 600px;
}
.modal-content {
    box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);
}
.modal-sm {
    width: 300px;
}
}
@media (min-width: 992px) {
.modal-lg {
    width: 900px;
}
}
.box-link{
	cursor:pointer;
}
.btn-make-offer{
        background-color:#B85EF7;
        color:#FFF;
}
.btn-success.disabled, .btn-success.disabled:hover, .btn-success.disabled:focus, .btn-success.disabled.focus, .btn-success.disabled:active, .btn-success.disabled.active, .btn-success[disabled], .btn-success[disabled]:hover, .btn-success[disabled]:focus, .btn-success.focus[disabled], .btn-success[disabled]:active, .btn-success.active[disabled], fieldset[disabled] .btn-success, fieldset[disabled] .btn-success:hover, fieldset[disabled] .btn-success:focus, fieldset[disabled] .btn-success.focus, fieldset[disabled] .btn-success:active, fieldset[disabled] .btn-success.active {
  background-color:auto;
  border-color: #b85ef7;
}
.form-order .info .btn-default,
.form-order .info .btn-success {
  display: block;
  font-size: 36px;
  font-weight: 700;
  line-height: 1.25;
  padding: 31px 10px;
}
</style>
</head>
<body>

	<!-- main container of all the page elements -->
	<div id="wrapper">
		<!-- inner wrapper -->
		<div class="w1">
			<!-- topbar -->
			<div class="topbar clearfix container-fluid add">
				<a href="#" class="topbar-opener"><i class="glyphicon glyphicon-remove"></i></a>
				<!-- user-info -->
				<div class="user-info pull-right">
					<!-- nav-signup -->
<?php

					
					if( !isset($_SESSION['id_buyer']) )	{
						?>
						<nav class="nav-signup pull-left">
							<ul class="list-inline">
								<!-- 
								<li><a href="#" data-toggle="modal" data-target="#regModal">Signup</a></li> 
								<li><a href="#" data-toggle="modal" data-target="#myModal">Login</a></li>
								-->
								<li><a href="#" onclick="showSignupModal()">Signup</a></li> 
								<li><a href="#" onclick="showLoginModal()">Login</a></li>
							</ul>
						</nav>
						<?php
					}
					else	{
						?>
						<nav class="nav-signup pull-left">
							<ul class="list-inline">
								<li><a href="/myaccount">My Account <i class="icon-user"></i></a></li>
								<li><a href="/logout">Logout</a></li>
							</ul>
						</nav>
						<?php
					}
					
/*
                    @if(Session::has('id_buyer'))
                    <nav class="nav-signup pull-left">
						<ul class="list-inline">
							<li><a href="/myaccount">My Account <i class="icon-user"></i></a></li>
						</ul>
					</nav>
                    @else
					<nav class="nav-signup pull-left">
						<ul class="list-inline">
							<li><a href="#" data-toggle="modal" data-target="#regModal">Signup</a></li>
							<li><a href="#" data-toggle="modal" data-target="#myModal">Login</a></li>
						</ul>
					</nav>
                    @endif
*/
?>
					<ul class="side-menu info-links list-inline pull-left">
					 <?php if( isset($_SESSION['id_buyer']) )	{ ?>
						<li class="" >
							<a id="chatlink" href="#"><i class="icon-chat"></i></a>
							<span id="display_unread_message_count"></span>
						</li>
					 <?php } ?>
						<!--
						<li class="" >
							<a href="#"><i class="icon-cart"></i></a>
							<span class="items-counter">2</span>
						</li>
						-->
						<!--<li><a href="#"><img src="/images/flag.jpg" alt="country description"></a></li>-->
						<li class="">
							<a href="#"><i class="icon-cart"></i></a>
							<?php if ( strpos($_SERVER['REDIRECT_URL'], '/billing/') !== false ) {	?>
								<span class="items-counter">1</span>
							<?php }	?>
						</li>
						<li><a href="#" id="change_flag"><span class="currency-flag currency-flag-usd"></span></a></li>

						<li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span id="btn_currency">$ USD</span><span class="caret"></span> </a> 
							<ul class="dropdown-menu" role="menu" style="padding:0px;margin:2px;">
							  <li style="border:none;" class="dropdown_menu_li"><a class="currency" data-symbol="$" >USD</a></li>
							  <li style="border:none;" class="dropdown_menu_li"><a class="currency" data-symbol="€">EUR</a></li>
							  <li style="border:none;" class="dropdown_menu_li"><a class="currency" data-symbol="£">GBP</a></li>
							  <li style="border:none;" class="dropdown_menu_li"><a class="currency" data-symbol="¥">CNY</a></li>
							</ul>
						</li>
					</ul>
					<input type="hidden" id="current_selected_currency" name="current_selected_currency" value="$">
<?php /*
					<ul class="info-links list-inline pull-left">
						<li><a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$locale_language}}</a>
                        <ul class="dropdown-menu" role="menu" >
                          <li role="presentation"><a href="/setlocale/en_US/withcurrency/{{$currency_locale}}" >English</a></li>
                          <li role="presentation"><a href="/setlocale/de_DE/withcurrency/{{$currency_locale}}" >German</a></li>
                          <li role="presentation"><a href="/setlocale/es_ES/withcurrency/{{$currency_locale}}" >Espanol</a></li>
                          <li role="presentation"><a href="/setlocale/fr_FR/withcurrency/{{$currency_locale}}" >French</a></li>
                          <li role="presentation"><a href="/setlocale/it_IT/withcurrency/{{$currency_locale}}" >Italian</a></li>
                          <li role="presentation"><a href="/setlocale/zh_CN/withcurrency/{{$currency_locale}}" >Chinese</a></li>
                          <li role="presentation"><a href="/setlocale/ru_RU/withcurrency/{{$currency_locale}}" >Russian</a></li>
                        </ul>
					</ul>
*/ ?>
				</div>

				<!-- social-networks -->
				<ul class="social-networks list-inline pull-left">
					<li><a href="https://www.facebook.com/Brand-Eden-330411214041495/" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a href="https://twitter.com/brand_eden" target="_blank"><i class="icon-twitter"></i></a></li>
					<!--
					<li><a href="#"><i class="icon-linkedin"></i></a></li>
					<li><a href="#"><i class="icon-google"></i></a></li>
					<li><a href="#"><i class="icon-youtube"></i></a></li>
					-->
				</ul>
<?php //				<a href="tel:18001231234" class="helpline pull-left"><i class="icon-phone"></i> +1 800 123 1234</a> ?>
			</div>
			<!-- header of the page -->
			<header id="header">
				<div class="container-fluid">
					<div class="header-holder">
						<!-- topbar-opener -->
						<a href="#" class="topbar-opener"><i class="icon-list"></i></a>
						<!-- navbar-toggle -->
						<button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- page logo -->
						<strong class="logo pull-left"><a href="/"><img src="/images/logo.png" alt="Brandeden"></a></strong>
					</div>
					<nav class="navbar navbar-default">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<i class="glyphicon glyphicon-remove"></i>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li <?php echo ($_SERVER['REQUEST_URI'] == '/' ? 'class="active"' : ''); ?> >
									<a href="/">{{ trans('messages.HOME') }}</a></li>
								<!--
								<li><a href="#">{{ trans('messages.BRANDING BASICS') }}</a></li>
								<li><a href="#">{{ trans('messages.BUYING PROCESS') }}</a></li>
								-->
								<li <?php echo ($_SERVER['REQUEST_URI'] == '/industry-sales' ? 'class="active"' : ''); ?> >
									<a href="/industry-sales">{{ trans('messages.INDUSTRY SALES') }}</a></li>
								<li><a href="#">{{ trans('messages.BLOG') }}</a></li>
							</ul>
						</div>
					</nav>
				</div>
			</header>
			
			
			<!-- page-header -->
			<div class="page-header text-center">
				<div class="container">
					<h1 style="margin:auto;" >{{ trans('messages.BETTER BRANDS, BETTER BRANDING, BETTER BUSINESS') }}</h1>
					<!-- <p>{{ trans('messages.Brandable domain names give you instant credibility') }}</p> -->
				</div>
			</div>
			
			
			<script>
				$(document).ready(function(){
					$('#all_cat').click(function(e) {
						$('input[name=category]').removeAttr('checked');
					});
				});
			</script>
			<!-- contain main informative part of the site -->
			<main id="main">
				<!-- contain sidebar of the page -->

				<div class="main-container">

					<!-- section-head -->
					<header class="section-head hidden">
						<div class="head clearfix">
							<!--
							<a href="#" class="sidebar-opener btn btn-default pull-left"><span class="hidden-xs">{{ trans('messages.Filter Options') }}</span> <i class="icon-filter"></i> <i class="glyphicon glyphicon-remove"></i></a>
							-->
							<!-- nav-products -->
							<nav class="nav-products pull-left">
								<a href="#" class="btn-opener"><i class="glyphicon glyphicon-chevron-down"></i></a>
								<ul class="list-inline products-drop">
									<li><a href="/brandable">{{ trans('messages.BRANDABLE') }}</a></li>
									<li><a href="/premium">{{ trans('messages.PREMIUM') }}</a></li>
									<li><a href="/under1k">{{ trans('messages.UNDER 1K') }}</a></li>
								</ul>
							</nav>
						</div>
					</header>

					<!-- container -->
					<div class="container">

						<br>
						<div class="row">
							<!-- contain the main content of the page -->
							<section id="content" class="col-xs-12">
								<!-- post -->
								<article class="post">
									<div align="center">
										<h1 class="bg-danger1 text-danger">Error : 404 Page Not Found.</h1>
									</div>
									<div align="left">
										<p> <a href="/"> &laquo; Back to Home Page</a> </p>
									</div>
								</article>
							</section>
						</div>
					</div>
				</div>

				
			</main>
			
		</div>
		<!-- footer of the page -->
		<div class="bottom-container">
			<!-- footer-aside -->
			<aside class="footer-aside">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12 col-lg-4">
							<strong class="logo"><a href="#"><img src="/images/logo2.png" alt="Brandeden"></a></strong>
							<div class="text-holder">
								<p><!-- Sed ornare cras donec litora integer curabit ur orci at nullam aliquam libero nam himen aeos amet massa amet. ...<a href="#">More</a> --></p>
							</div>
							<!-- social-area -->
							<div class="social-area">
								<h3>{{ trans('messages.Follow Us') }}</h3>
								<!-- social-networks -->
								<ul class="social-networks list-inline">
									<li><a href="https://www.facebook.com/Brand-Eden-330411214041495/" target="_blank"><i class="icon-facebook"></i></a></li>
									<li><a href="https://twitter.com/brand_eden" target="_blank"><i class="icon-twitter"></i></a></li>
									<!--
									<li><a href="#"><i class="icon-linkedin"></i></a></li>
									<li><a href="#"><i class="icon-google"></i></a></li>
									<li><a href="#"><i class="icon-youtube"></i></a></li>
									-->
								</ul>
							</div>
						</div>
						<nav class="col-xs-12 col-sm-6 col-lg-2">
							<h3>{{ trans('messages.Product & Services') }}</h3>
							<ul class="list-unstyled">
								<li><a href="/brandable">{{ trans('messages.Brandable Domains') }}</a></li>
								<li><a href="/premium">{{ trans('messages.Premium Domains') }}</a></li>
								<li><a href="/under1k">Under $1000 Domains</a></li>
								<!--<li><a href="/under1k">{{ trans('messages.Under $1,000 Domains') }}</a></li>-->
								<!-- <li><a href="#">{{ trans('messages.Websites') }}</a></li> -->
							</ul>
						</nav>
						<nav class="col-xs-12 col-sm-6 col-lg-2">
							<h3>{{ trans('messages.Resources') }}</h3>
							<ul class="list-unstyled">
								<!--
								<li><a href="#">{{ trans('messages.Branding Basics') }}</a></li>
								<li><a href="#">{{ trans('messages.Buying Process') }}</a></li>
								-->
								<li><a href="/industry-sales">{{ trans('messages.Industry Sales') }}</a></li>
								<li><a href="#">{{ trans('messages.Blog') }}</a></li>
							</ul>
						</nav>
						<nav class="col-xs-12 col-sm-6 col-lg-2">
							<h3>{{ trans('messages.Company Info') }} </h3>
							<ul class="list-unstyled">
								<li><a href="#">{{ trans('messages.About Us') }}</a></li>
								<li><a href="#">{{ trans('messages.Privacy Policy') }}</a></li>
								<li><a href="#">{{ trans('messages.Terms of Service') }}</a></li>
								<li><a href="#">{{ trans('messages.TradeMark Issues') }}</a></li>
							</ul>
						</nav>
						<div class="col-xs-12 col-sm-6 col-lg-2">
							<h3>{{ trans('messages.Contact Us') }} </h3>
							<!-- social-links -->
							<ul class="social-links list-unstyled">
<!--								<li><a href="#"><i class="icon-question"></i>{{ trans('messages.FAQ') }}</a></li> 
								<li><a href="#"><i class="icon-chat_bubble_outline"></i>{{ trans('messages.Live chat') }}</a></li> -->
								<li><a href="mailto:sales@brandeden.com"><i class="icon-envelope2"></i>sales@brandeden.com</a></li> 
<!--								<li><a href="tel:18001231234"><i class="icon-phone"></i> +1 800 123 1234</a></li> -->
							</ul>
						</div>
					</div>
				</div>
			</aside>
			<!-- footer of the page -->
			<footer id="footer">
				<div class="container-fluid text-center">
					<p>&copy; <?php echo date('Y', time()) ?> <a href="#">BrandEden</a>. {{ trans('messages.All Rights Reserved') }}.</p>
				</div>
			</footer>
		</div>
	</div>
    <!--Registration Modal-->
    <div class="modal fade" id="regModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Up</h4>
        </div>
        <div class="modal-body">
		  <div id="reg_result"></div>
          <form id="regForm" method="POST" action="/signup" validate class="form-horizontal">
              <div class="form-group">
				<label for="first_name" class="control-label col-sm-2">Firstname</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="reg_first_name" name="reg_first_name" placeholder="Your Firstname">
					<span class="help-block"></span>
				</div>
              </div>
              <div class="form-group">
				<label for="password" class="control-label col-sm-2">Lastname</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="reg_last_name" name="reg_last_name" placeholder="Your Lastname">
					<span class="help-block"></span>
				 </div>
              </div>
              <div class="form-group">
				<label for="email" class="control-label col-sm-2">Email</label>
				<div class="col-sm-10">
					<input type="email" class="form-control" id="reg_email" name="reg_email" placeholder="Your Email">
					<span class="help-block"></span>
				</div>
              </div>
              <div class="form-group">
				<label for="password" class="control-label col-sm-2">Password</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" id="reg_password" name="reg_password" placeholder="Your Password">
					<span class="help-block"></span>
				</div>
              </div>
			  
              <div class="form-group">
				<label class="control-label col-sm-2">&nbsp;</label>
				<div class="col-sm-10">
					<div id="enq-recaptcha" class="g-recaptcha" data-sitekey="6LfseCUTAAAAALVlcV513h29hCq-bCwTq0IUadkV"></div>
					
					<button onclick="submitRegistration()" type="button" class="btn btn-primary pull-right" id="btnsignup">Sign Up <span id="btn_reg_loading"></span> </button>
					<input type="hidden" name="action" value="signup" />
				</div>
			  </div>
			  
			  <?php /*
			  <div class="form-group">
				<label class="control-label col-sm-2">&nbsp;</label>
				<div class="col-sm-10">
					<button onclick="submitRegistration()" type="button" class="btn btn-primary pull-right" id="btnsignup">Sign Up <span id="btn_reg_loading"></span> </button>
					<!--<a href="#" class="btn btn-default janrainEngage"  data-dismiss="modal" onclick="$('.janrainHeader div').html('Sign up using your account with');">{{ trans('messages.Social Sign-Up') }}</a>-->
					<input type="hidden" name="action" value="signup" />
				</div>
			  </div>
			  */ ?>
			  
          </form>
				
			  <div style="clear:both;"></div>
			  <div class="form-actions">
				<hr>
				<h4>Social Signup</h4>
				<div id="socialSignup"></div>
				<br/>
			  </div>
			  <div style="clear:both;"></div>
			  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
    <!--Login Modal-->
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('messages.Login') }}</h4>
        </div>
        <div class="modal-body">
          <form id="loginForm" method="POST" action="/login" validate>
			<div id="result_message"></div>
                              <div class="form-group">
                                  <input type="email" class="form-control" id="email" name="email" value="" title="{{ trans('messages.Please enter your email') }}" placeholder="example@gmail.com" >
                                  <span class="help-block"></span>
                              </div>
                              <div class="form-group">
                                  <label for="password" class="control-label">{{ trans('messages.Password') }}</label>
                                  <input type="password" class="form-control" id="password" name="password" value="" title="{{ trans('messages.Please enter your password') }}">
                                  <span class="help-block"></span>
                              </div>
                              <div id="loginErrorMsg" class="alert alert-error hide">{{ trans('messages.Wrong username og password') }}</div>
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember" id="remember"> Remember me
                                  </label>
                                  <!-- <p class="help-block">({{ trans('messages.if this is a private computer') }})</p> -->
								  
								  <button style="float:right;" id="btn_login" onclick="submitLogin()" type="button" class="btn btn-primary">Login <i class="fa fa-sign-in" aria-hidden="true"></i> <span id="btn_login_loading"></span> </button>
                              </div>
                              
                              <!-- <a href="#" class="btn btn-default janrainEngage"  data-dismiss="modal">{{ trans('messages.Social Login') }}</a> -->
							 <!--  <button onclick="$('#forgotPasswordModal').modal();" type="button" class="btn btn-default"> Forgot Password ? </button> -->
                              <input type="hidden" name="action" value="login" />
                          </form>
						  
			  <div style="clear:both;"></div>
			  <div class="form-actions">
				<hr>
				<h4>Social login</h4>
				<div id="socialLogin"></div>
				<br/>
			  </div>
			  <div style="clear:both;"></div>
				<div class="form-actions">
					<hr>
					<h4>Forgot your password ?</h4>
					<p> Click <a href="javascript:;" id="forget-password" onclick="$('#forgotPasswordModal').modal();"> here </a> to reset your password. </p>
				</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  
  <div class="modal fade" id="forgotPasswordModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Forgot Password ? <span id="loading_bar_forgot_password"> </span> </h4>
        </div>
        <div class="modal-body">
			  <div id="result_forgot_password_message"></div>
			  <form id="frmForgotPassword" method="POST" action="/login" validate>
				  <div class="form-group">
					  <label for="password" class="control-label">Your Email</label>
					  <input type="email" class="form-control" id="email_fp" name="email_fp" value="" title="{{ trans('messages.Please enter your email') }}" placeholder="example@gmail.com" >
				  </div>

				  <button id="btnForgotPassword" onclick="forgotPassword()" type="button" class="btn btn-default"> Submit </button>
			  </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('messages.Close') }}</button>
        </div>
      </div>
    </div>
  </div>
  
<!--Buy Now Modal-->
    <div class="modal fade" id="buyNowModal" role="dialog">
    <div class="modal-dialog">
    
		<div class="modal-content">
          <form id="enquiryForm" method="POST" action="/enquire" validate>
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<!-- <h4 class="modal-title" id="exampleModalLabel">{{ trans('messages.Buy now') }}</h4> -->
				<h4 class="modal-title" id="exampleModalLabel">Make Offer</h4>
			  </div>
			  <div class="modal-body" id="buy_now_body">
				  <div id="buy_now_result"></div>
				  <div class="form-group col-sm-12">
				    <label for="enq-email" class="control-label">{{ trans('messages.Your Email') }}</label>
					<?php 
					$s_id_buyer = '';
					if( isset($_SESSION['id_buyer']) )	{	$s_id_buyer = $_SESSION['id_buyer'];	}
					$s_email = '';
					if( isset($_SESSION['email']) )	{	$s_email = $_SESSION['email'];	}
					?>
					
					<input required type="hidden" class="form-control" id="enq-id_buyer" name="enq-id_buyer" value="<?php echo ($s_id_buyer != '' ? $s_id_buyer : ''); ?> " >
				    <input required type="email" class="form-control" id="enq-email" name="enq-email" placeholder="<?php echo ($s_email != '' ? $s_email : 'Xyz@internet.com'); ?> " value="<?php echo $s_email; ?>" <?php if($s_id_buyer != '' && $s_email != '')	{	echo 'readonly'; }  ?> >
					 
				  </div>
				  <div class="form-group col-sm-12 hidden">
				    <label for="enq-message" class="control-label">{{ trans('messages.Message') }}</label>
				    <textarea required class="form-control" id="enq-message" name="enq-message" >Buying a Domain</textarea>
				  </div>
				  <div class="form-group">
					 <div class="col-sm-6">
						<label for="enq-domainName" class="control-label">{{ trans('messages.Selected domain') }}</label>
						<input type="url" class="form-control" id="enq-domainName" name="enq-domainName" placeholder="Brandeden.com" readonly >
					  </div>
					  <div class="col-sm-6">
						<label for="enq-domainPrice" class="control-label">{{ trans('messages.Price') }}</label>
						<input type="text" class="form-control" id="enq-domainPrice" name="enq-domainPrice" placeholder="" readonly >
					  </div>
				          <div class="col-sm-6">
				            <label for="enq-domainOffer" class="control-label">{{ trans('messages.Your offer price (in USD)') }}</label>
				            <input type="number" class="form-control" id="enq-domainOffer" name="enq-domainOffer" placeholder="{{ trans('messages.Your offer price (in USD)') }}" >
				          </div>

					  <div class="clearfix"></div>
				  </div>
				  <div class="form-group col-sm-12">
				    <label for="enq-recaptcha" class="control-label"> </label>
					<!--<div id="enq-recaptcha" class="g-recaptcha" data-sitekey="6Lfr5xkTAAAAAGjugdzxM1McR1TGWzJWiP9A1GLY"></div>-->
					<div id="enq-recaptcha" class="g-recaptcha" data-sitekey="6LfseCUTAAAAALVlcV513h29hCq-bCwTq0IUadkV"></div>
				  </div>

					<div class="clearfix" ></div>
				  <input type="hidden" value="" id="enq-domain-id" name="enq-domain-id" >
				  <input type="hidden" value="" id="enq-domain-price" name="enq-domain-price" >
				  <input type="hidden" id="url" name="url" value="<?php echo config('app.BASE_URL'); ?>" >
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" id="send-close" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-success" id="send-enquiry1" onclick="makeOffer()" data-loading-text="{{ trans('messages.Sending') }}..." autocomplete="off" >
				Make Offer <span id="btn_span_loading_offr"></span></button>
				<!-- <button type="button" class="btn btn-primary" id="send-enquiry" data-loading-text="{{ trans('messages.Sending') }}..." autocomplete="off" >{{ trans('messages.Send enquiry') }}</button> -->
			  </div>
			</form>
		</div>
	</div>
	</div>

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>

<link href="/css/currency-flags.css" rel="stylesheet" type="text/css" />

<!-- <script type="text/javascript" src="/js/bootstrap.min.js"></script> -->
<script type="text/javascript" src="/js/jquery.ajaxfileupload.js"></script>

<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="/js/notify.js"></script>

<link href="/css/bootstrapprogressbar.css" rel="stylesheet" type="text/css" />
<link href="/js/StrongPass.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/StrongPass.js" ></script>

<link href="/js/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery.validationEngine-en.js" ></script>
<script type="text/javascript" src="/js/jquery.validationEngine.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="/js/dynamic_location.js"></script>

<?php if( isset($_SESSION['id_buyer']) )	{ ?>
	<script type="text/javascript">
		var id_buyer = <?php echo $_SESSION['id_buyer']; ?>;
		getUnreadMessages(id_buyer);
	</script>
<?php }	?>

<style>
/* chat */
    .conversation-wrap
    {
        box-shadow: -2px 0 3px #ddd;
        padding:0;
        max-height: 400px;
        overflow: auto;
    }
    .conversation
    {
        padding:5px;
        border-bottom:1px solid #ddd;
        margin:0;

    }

    .message-wrap
    {
        /* box-shadow: 0 0 3px #ddd; */
        padding:0;

    }
    .msg
    {
        padding:5px;
        /*border-bottom:1px solid #ddd;*/
        margin:0;
    }
    .msg-wrap
    {
        padding:10px;
		/*
        max-height: 250px;
        overflow: auto;
		*/
    }

    .time
    {
        color:#bfbfbf;
    }

    .send-wrap
    {
        border-top: 1px solid #eee;
        border-bottom: 1px solid #eee;
        padding:10px;
        /*background: #f8f8f8;*/
    }

    .send-message
    {
        resize: none;
    }

    .highlight
    {
        background-color: #f7f7f9;
        border: 1px solid #e1e1e8;
    }

    .send-message-btn
    {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-left-radius: 0;

        border-bottom-right-radius: 0;
    }
    .btn-panel
    {
        background: #f7f7f9;
    }

    .btn-panel .btn
    {
        color:#b8b8b8;

        transition: 0.2s all ease-in-out;
    }

    .btn-panel .btn:hover
    {
        color:#666;
        background: #f8f8f8;
    }
    .btn-panel .btn:active
    {
        background: #f8f8f8;
        box-shadow: 0 0 1px #ddd;
    }

    .btn-panel-conversation .btn,.btn-panel-msg .btn
    {

        background: #f8f8f8;
    }
    .btn-panel-conversation .btn:first-child
    {
        border-right: 1px solid #ddd;
    }

    .msg-wrap .media-heading
    {
        color:#55a119;
        /* font-weight: 700; */
		font-size: 12px;
    }


    .msg-date
    {
        background: none;
        text-align: center;
        color:#aaa;
        border:none;
        box-shadow: none;
        border-bottom: 1px solid #ddd;
    }
</style>

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-86403411-1', 'auto');
 ga('send', 'pageview');

</script>

</body>
</html>
