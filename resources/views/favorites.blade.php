					<div class="table-area">		
							<?php	if( trim($favmessage) != '' )	{	echo $favmessage;	}	?>
							<form method="post" class="form-favorite-table-list" id="form-favorite-table-list">
							<div id="result_fav_domains_message"></div>
							<table class="table-holder">
                                <thead>
                                    <tr>
                                        <th class="col1">
                                            <div class="checkbox-holder">
                                                <label>
                                                    <input type="checkbox" id="checkAll" name="checkAll" >
                                                    <span class="fake-input"></span>
                                                    Domain Names
                                                </label>
                                            </div>
                                        </th>
                                        <th class="col2">Price</th>
                                        <th class="col2">Expires</th>
                                        <th class="col3">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php
									if( count($favorites) > 0 )	{
										$order_domainIDs = array();
										if( count($ordertransactions) > 0 )	{
											foreach($ordertransactions as $order)	{
												$order_domainIDs[$order->id_domain] = $order->id_domain;
												
												$IPNstatus = json_decode($order->IPNstatus);
												if($IPNstatus)	{
													$IPNstatus_EscrowUniqueIdentifier = $IPNstatus->EscrowUniqueIdentifier;
													$IPNstatus_Description = $IPNstatus->Description;
													$IPNstatus_Code = $IPNstatus->Code;
												}
												else	{
													$IPNstatus_EscrowUniqueIdentifier = '';
													$IPNstatus_Description = '';
													$IPNstatus_Code = '';
												}

												$style='';
												$new_style='';
												$display_status = $IPNstatus_Description;
												if( $IPNstatus_Code == 0 )	{
													$display_status = 'Cancelled';
													$style='style="background-color:#5bb402 !important;color:#fff !important;"';
												}
												if( $IPNstatus_Code == 5 )	{
													$display_status = 'Pending';
													$style='style="background-color:#5bb402 !important;color:#fff !important;"';
												}
												if( $IPNstatus_Code == 15 )	{
													$display_status = 'Pay Now !';
													$tooltip = ' title="Your Attention is Required!"';
													$style='style="background-color:#d9534f !important;color:#fff !important;"';
													$new_style = 'btn-cancelled-new';
												}
												if( $IPNstatus_Code == 20 )	{
													$display_status = 'Payment Submitted';
													$style='style="background-color:#5bb402 !important;color:#fff !important;"';
												}
												if( $IPNstatus_Code == 25 )	{
													$display_status = 'Payment Accepted <i class="glyphicon glyphicon-thumbs-up"></i>';
													$style='style="background-color:#5bb402 !important;color:#fff !important;"';
												}

												$order_domainIDs['displayStatus'][$order->id_domain] = $display_status;
												$order_domainIDs['EscrowUniqueIdentifier'][$order->id_domain] = $IPNstatus_EscrowUniqueIdentifier;
												$order_domainIDs['Description'][$order->id_domain] = $IPNstatus_Description;
												$order_domainIDs['Code'][$order->id_domain] = $IPNstatus_Code;
												$order_domainIDs['new_style'][$order->id_domain] = $new_style;
												$order_domainIDs['style'][$order->id_domain] = $style;
												
											}
										}

										foreach($favorites as $fav)	{
											?>
											<tr>
												<td class="col1">
													<div class="checkbox-holder">
														<label>
															<input type="checkbox" name="checkboxDomainIDs[]" id="checkboxDomainID" class="checkboxDomainID" value="<?php echo $fav->id_fav; ?>">
															<span class="fake-input"></span>
															<?php echo $fav->domain; ?>
														</label>
													</div>
													<?php	if( $fav->id_logo > 0 )	{ ?> <span class="title">Brandable</span> <?php 	}	?>
													<?php	if( $fav->price >= $premiumprice && $fav->id_logo == 0 )	{	?>  <span class="title add">Premium</span> <?php 	}	?>
													<!--
													<span class="title">Brandable</span>
													 <span class="title add">Premium</span>
													 -->
												</td>
												<td class="col2">
													$<?php echo $fav->price; ?>
												</td>
												<td class="col2">
													<?php if($fav->expires != '0000-00-00' && trim($fav->expires) != '' && trim($fav->expires) != null )	{	?>
													<time data-date="<?php echo $fav->expires; ?>" datetime="<?php echo date("F d Y H:i:s", strtotime($fav->expires)); ?> "><?php echo date("F d, Y", strtotime($fav->expires)); ?></time>
													<?php }	?>
												</td>
												<td class="col3 box">
													<div class="del-area">
														<?php if (!in_array($fav->id_domain, $order_domainIDs)) {	?>
																<?php if( $fav->price > 0 )	{	?>
																			<a href="#" class="btn btn-buy">Buy Now</a>
																<?php }	else	{ ?>
																			<a href="#" class="btn btn-makeoffer-new btn-buy">Make Offer</a>
																<?php } ?>
																<h2 style="display:none;"><a href="/beaast.com"><?php echo $fav->domain; ?></a></h2>
																<strong class="price text-center" style="display:none;"><?php echo $fav->price; ?></strong>
																<input type="hidden" value="<?php echo $fav->price; ?>" name="pricedata">
																<input type="hidden" value="<?php echo $fav->offer_price; ?>" name="offer_pricedata">
																<input type="hidden" value="<?php echo $fav->id_domain; ?>" name="domainId">
														<?php } else	{	?>
															<?php 
															$IPNstatus_Code = $order_domainIDs['Code'][$fav->id_domain];
															$IPNstatus_EscrowUniqueIdentifier = $order_domainIDs['EscrowUniqueIdentifier'][$fav->id_domain];
															$IPNstatus_Description = $order_domainIDs['Description'][$fav->id_domain];
															$style = $order_domainIDs['style'][$fav->id_domain];
															$new_style = $order_domainIDs['new_style'][$fav->id_domain];
															
															if( $IPNstatus_Code == 25 )	{
																$dstatus = '<i class="fa fa-money"></i> Accepted';
															}
															else if( $IPNstatus_Code == 20 )	{
																$dstatus = '<i class="fa fa-money"></i> Submitted';
															}
															else	{
																$dstatus = $order_domainIDs['displayStatus'][$fav->id_domain];
															}
															
															?>
															<a <?php echo $style; ?> class="btn <?php echo $new_style; ?>" onclick="viewStatusInfo('<?php echo $IPNstatus_Code; ?>', '<?php echo $IPNstatus_EscrowUniqueIdentifier; ?>','<?php echo $IPNstatus_Description; ?>')"><?php echo $dstatus; ?></a>
															<!--<a <?php echo $style; ?> class="btn btn-<?php echo strtolower($order_domainIDs['displayStatus'][$fav->id_domain]); ?>-new" onclick="viewStatusInfo('<?php echo $IPNstatus_Code; ?>', '<?php echo $IPNstatus_EscrowUniqueIdentifier; ?>','<?php echo $IPNstatus_Description; ?>')"><?php echo $order_domainIDs['displayStatus'][$fav->id_domain]; ?></a>-->
														<?php } 	?>
														<a onclick="removeFavoriteDomain('<?php echo $fav->id_fav; ?>','<?php echo $fav->domain; ?>')" style="cursor:pointer" class="del-icon"><i class="icon-bin"></i></a>
													</div>
												</td>
											</tr>
											<?php
										}
									}
									else	{
										?> <tr> <td colspan="4"> No Domains Found </td> </tr> <?php
									}
									?>
                                </tbody>
                            </table>
							</form>
					</div>		

							<?php  if( $total_pages > 1 )	{ ?>
								<div id="loadingbar_pagination"></div>
								<ul class="pagniation list-unstyled">
									<?php for( $i=0; $i<$total_pages; $i++ )	{ ?>
											<?php 
												$page_display = $i + 1; 
											?>
											<li <?php echo ($pageindex == $i ? 'class="active"' : ''); ?> >
												<?php if($pageindex == $i) 	{	?>
													<span onclick="getMyFavorites(<?php echo $i; ?>)" style="cursor:pointer;"> <?php echo $page_display; ?> </span>
												<?php }	else {	?>
													<a onclick="getMyFavorites(<?php echo $i; ?>)" style="cursor:pointer;"> <?php echo $page_display; ?> </a>
												<?php }		?>
											</li>
									<?php } ?>
								</ul>
							<?php } ?>
							


							
							
<script>
function viewStatusInfo(status_code, transactionID, escrow_status)	{
	var title = '';
	var content = '';
	if( status_code == 0 )	{
		title = 'Cancelled';
		content = '<p>Seller have cancelled the transaction. Login to your Escrow account to check the reason why the Seller cancelled this transaction <b>#'+transactionID+'</b>.</p>';
	}
	else if( status_code == 5 )	{
		title = 'Pending';
		content = '<p>Seller haven\'t check or review your enquiry transaction <b>#'+transactionID+'</b>. You can send a reminder by logging-in to your Escrow account.</p>';
	}
	else if( status_code == 15 )	{
		title = 'Pay Now !';
		content = '<p>You can now pay the domain you have enquire. Login to your Escrow account to select a payment method. <b> Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 20 )	{
		title = 'Payment Submitted';
		content = '<p>You have already submitted a payment. Please wait while Escrow securing your payment. <b> Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 25 )	{
		title = 'Payment Accepted';
		content = '<p>Seller have securely received your payment. Seller will release your domain as soon as possible. You can send a message or email to send a reminder. <b>Transaction #'+transactionID+'</b>. </p>';
	}
	else	{
		title = escrow_status;
		content = '<p>You can view the details of this transaction. Login to your Escrow account to check this transaction <b>#'+transactionID+'</b>.</p>';
	}
	
	content += '<div style="color:#6f7c96;font-size:12px;">';
	content += '<br> Escrow Transaction ID : ' + transactionID;
	content += '<br> Escrow Status : ' + escrow_status;
	content += '</div>';
	
	$('#order_status_title').html(title);
	$('#order_status_content').html(content);
	$('#statusModal').modal();
}
$("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
</script>