@extends('app')
@section('content')
<main id="main" class="main-active">
<script>
    $(document).ready(function(){
		var category = {{$category}};
		var tld = $('#tld').val();
		var price = '{{$price}}';
		var length = '{{$length}}';
		var search_key = '';
		var order = '';
		var order_by = '';
		$('img.logo_img').one('load', function(){
			$(this).parent().parent().siblings('.imgloader').hide();
			$(this).parent().parent().show();
			/*$(this).siblings('img.loader').hide();
			$(this).removeClass('logo_img');*/
		});
		$('#all_cat').click(function(e) {
			$(".cat_list input:checkbox").prop('checked', true);
        });
		$('#none_cat').click(function(e) {
			$(".cat_list input:checkbox").prop('checked', false);
        });
		
		$('.load-more').click(function(e) {
            e.preventDefault();
			// var $currency_symbol = $('#current_currency').val();
			var curr_page = $('#curr_page').val();
			var total_page = $('#total_page').val();
			curr_page++;
			$('#curr_page').val(curr_page);
			//category = $('[name=category]').val();
			//tld = $('#tld').val();
			
			var category = '';
			if( $('[name=category]').length > 0 )	{
				$('[name=category]').each(function (i, e) {
					if( $(e).prop('checked') )	{
						category += $(e).val();
						category += ',';
					}
				});
				category = category.substring(0, category.length - 1);
			}
			
			var tld = '';
			if( $('[name=tld]').length > 0 )	{
				$('[name=tld]').each(function (i, e) {
					//$(e).val()
					//console.log( $(e).val() );
					//console.log( $(e).prop('checked') );
					if( $(e).prop('checked') )	{
						tld += $(e).val();
						tld += ',';
					}
				});
				tld = tld.substring(0, tld.length - 1);
			}
			
			var price = $('[id=price_range_slider]').val(); //$('#price').val();
			//console.log(price);
			length = $('[name=length]').val(); //$('#length').val();
			search_key = $('#search').val();
			//console.log(search_key);
			
			console.log(curr_page);
			console.log(total_page);
			if(curr_page == total_page)	{
				$(this).hide();
			}
			else	{
				$(this).show();
			}
			$('#overlay-wrapper').show();
 			$.get($(this).attr('href') + '/' + curr_page, { 'search': search_key,'category': category, 'tld': tld, 'price': price, 'length': length, 'order': order, 'order_by': order_by}, function(source) { 
				//$('#productlist').append(source);
				$('#container').append(source);
				if($currency_symbol != '$')	{
					_changeprice(fxMoney, $currency_symbol);
				}
				//$('.currency').click();
				$('#overlay-wrapper').hide(); 
				$('#container').find('img.logo_img').on('load', function(){	
					$(this).parent().parent().siblings('.imgloader').hide();
					$(this).parent().parent().show();
					//$(this).siblings('img.loader').hide();
					//$(this).removeClass('logo_img');
				});
				
			});	
			return false;
        });
		
		$("#search").keyup(function(e){ 
			var code = e.which;
			if(code==13)	{
				e.preventDefault();
				$('#btnSearch').click();
			}
		});
		
		$('#btnSearch').click(function(e)	{
            e.preventDefault();
			
			//var $currency_symbol = $('#current_currency').val();
			
			var current_tab_url = $('#current_tab_url').val();
			
			$('#current_tab_url').val( current_tab_url );
			
			//console.log(current_tab_url);
			
			//var curr_page = $('#curr_page').val();
			var curr_page = 1;
			//var total_page = $('#total_page').val();
			var total_page = $('#total_page_search').val();
			//curr_page++;
			$('#curr_page').val(curr_page);
			//category = $('[name=category]').val();
			//tld = $('#tld').val();
			
			console.log(curr_page);
			console.log(total_page);
			if(curr_page == total_page)	{
				$('.load-more').hide();
			}
			else	{
				$('.load-more').show();
			}
			
			var category = '';
			if( $('[name=category]').length > 0 )	{
				$('[name=category]').each(function (i, e) {
					if( $(e).prop('checked') )	{
						category += $(e).val();
						category += ',';
					}
				});
				category = category.substring(0, category.length - 1);
			}
			
			var tld = '';
			if( $('[name=tld]').length > 0 )	{
				$('[name=tld]').each(function (i, e) {
					//$(e).val()
					//console.log( $(e).val() );
					//console.log( $(e).prop('checked') );
					if( $(e).prop('checked') )	{
						tld += $(e).val();
						tld += ',';
					}
				});
				tld = tld.substring(0, tld.length - 1);
			}
			
			price = $('[id=price_range_slider]').val(); //$('#price').val();
			length = $('[name=length]').val(); //$('#length').val();
			search_key = $('#search').val();
			//console.log(search_key);
			//console.log(total_page);

			
			var invalid_characters = [ '#','%', '&' ];
			var found_invalid_character = false;
			var invalid_character = '';
			for( var m=0; m < search_key.length; m++ )	{
				var res_check_input = jQuery.inArray( search_key[m], invalid_characters );
				if( res_check_input > -1 )	{
					found_invalid_character = true;
					invalid_character = search_key[m];
				}
			}
			
			var res_check_input = jQuery.inArray( search_key, invalid_characters );
			if( found_invalid_character == true )	{
				alert(' Invalid input character ('+invalid_character+') found. Please remove this '+invalid_character+' from your search.');
				$('.form-search .form-holder').attr('style','border-color:#d9534f;');
				$('#search').focus();
			}
			else	{
				$('.form-search .form-holder').attr('style','border-color:#e4e6ec;');
				$('#overlay-wrapper').show();
				$.get($('#current_tab_url').val() + '/' + curr_page, { 'search': search_key,'category': category, 'tld': tld, 'price': price, 'length': length, 'order': order, 'order_by': order_by}, function(source) { 
					//$('#productlist').append(source);
					//$('#container').append(source);
					$('#container').html(source);
					if($currency_symbol != '$')	{
						_changeprice(fxMoney, $currency_symbol);
					}
					//$('.currency').click();
					$('#overlay-wrapper').hide(); 
					$('#container').find('img.logo_img').on('load', function(){	
						$(this).parent().parent().siblings('.imgloader').hide();
						$(this).parent().parent().show();
						//$(this).siblings('img.loader').hide();
						//$(this).removeClass('logo_img');
					});

					var $result = $(source).find('#total_page_search');
					$('#total_page').val( $result.val() );
					
					if( !isNaN(parseInt($result.val())) )	{
						if( parseInt($result.val()) < 2 )	{
							$('.section-footer').hide();
							$('.load-more').hide();
						}
						else	{
							$('.section-footer').show();
							$('.load-more').show();
						}
					}
					else	{
						$('.section-footer').hide();
						$('.load-more').hide();
					}
				});	

			}
			
			return false;
        });
		
		$('#btnSubmitGoFilter').click(function(e)	{
            e.preventDefault();
			
			//var $currency_symbol = $('#current_currency').val();
			
			var current_tab_url = $('#current_tab_url').val();
			
			$('#current_tab_url').val( current_tab_url );
			
			//console.log(current_tab_url);
			
			//var curr_page = $('#curr_page').val();
			var curr_page = 1;
			//var total_page = $('#total_page').val();
			var total_page = $('#total_page_search').val();
			//curr_page++;
			$('#curr_page').val(curr_page);
			//category = $('[name=category]').val();
			//tld = $('#tld').val();
			
			console.log(curr_page);
			console.log(total_page);
			if(curr_page == total_page)	{
				$('.load-more').hide();
			}
			else	{
				$('.load-more').show();
			}
			
			var category = '';
			if( $('[name=category]').length > 0 )	{
				$('[name=category]').each(function (i, e) {
					if( $(e).prop('checked') )	{
						category += $(e).val();
						category += ',';
					}
				});
				category = category.substring(0, category.length - 1);
			}
			
			var tld = '';
			if( $('[name=tld]').length > 0 )	{
				$('[name=tld]').each(function (i, e) {
					//$(e).val()
					//console.log( $(e).val() );
					//console.log( $(e).prop('checked') );
					if( $(e).prop('checked') )	{
						tld += $(e).val();
						tld += ',';
					}
				});
				tld = tld.substring(0, tld.length - 1);
			}
			
			//console.log(tld);
			
			
			price = $('[id=price_range_slider]').val(); //$('#price').val();
			length = $('[name=length]').val(); //$('#length').val();
			search_key = $('#search').val();
			//console.log(search_key);
			//console.log(total_page);

			$('#overlay-wrapper').show();
 			$.get($('#current_tab_url').val() + '/' + curr_page, { 'search': search_key,'category': category, 'tld': tld, 'price': price, 'length': length, 'order': order, 'order_by': order_by}, function(source) { 
				//$('#productlist').append(source);
				//$('#container').append(source);
				$('#container').html(source);
				if($currency_symbol != '$')	{
					_changeprice(fxMoney, $currency_symbol);
				}
				//$('.currency').click();
				$('#overlay-wrapper').hide(); 
				$('#container').find('img.logo_img').on('load', function(){	
					$(this).parent().parent().siblings('.imgloader').hide();
					$(this).parent().parent().show();
					//$(this).siblings('img.loader').hide();
					//$(this).removeClass('logo_img');
				});

				var $result = $(source).find('#total_page_search');
				$('#total_page').val( $result.val() );
				
				//console.log( parseInt($result.val()) );
				
				if( !isNaN(parseInt($result.val())) )	{
					if( parseInt($result.val()) < 2 )	{
						$('.section-footer').hide();
						$('.load-more').hide();
					}
					else	{
						$('.section-footer').show();
						$('.load-more').show();
					}
				}
				else	{
					$('.section-footer').hide();
					$('.load-more').hide();
				}
				
			});	
			return false;
        });
		
		
		$('.sorting-list a').click(function(e) {
            e.preventDefault();
			
			var curr_page = 1;
			var total_page = $('#total_page').val();
			$('#curr_page').val(curr_page);
			
			console.log(curr_page);
			console.log(total_page);
			if(curr_page == total_page)	{
				$('.load-more').hide();
			}
			else	{
				$('.load-more').show();
			}
			
			var btn = $('.sorting-area').find('.btn-link');
			var btnText = btn.find('.type');
			$('#container article').remove();
			$link = $(this);
			btnText.text($link.data('short-name'));
			order = $link.data('sort-order');
			order_by = $link.data('sort-by');
			price = $('[id=price_range_slider]').val(); //$('#price').val();
			length = $('[name=length]').val(); //$('#length').val();
			
			var category = '';
			if( $('[name=category]').length > 0 )	{
				$('[name=category]').each(function (i, e) {
					if( $(e).prop('checked') )	{
						category += $(e).val();
						category += ',';
					}
				});
				category = category.substring(0, category.length - 1);
			}
			
			var tld = '';
			if( $('[name=tld]').length > 0 )	{
				$('[name=tld]').each(function (i, e) {
					//$(e).val()
					//console.log( $(e).val() );
					//console.log( $(e).prop('checked') );
					if( $(e).prop('checked') )	{
						tld += $(e).val();
						tld += ',';
					}
				});
				tld = tld.substring(0, tld.length - 1);
			}
			
			$('#sort_order').val(order);
			$('#sort_by').val(order_by);
			$('#overlay-wrapper').show();
 			$.get($link.attr('href') + '/1', { 'search': search_key,'category': category, 'tld': tld, 'price': price, 'length': length, 'order': order, 'order_by': order_by}, function(source) { 
				$('#container').html(source);
				if($currency_symbol != '$')	{
					_changeprice(fxMoney, $currency_symbol);
				}
				$('#overlay-wrapper').hide(); 
				$('#container').find('img.logo_img').on('load', function(){	
					$(this).parent().parent().siblings('.imgloader').hide();
					$(this).parent().parent().show();
					/*$(this).siblings('img.loader').hide();
					$(this).removeClass('logo_img');*/
				});
				
				var $result = $(source).find('#total_page_search');
				$('#total_page').val( $result.val() );
				
				//console.log( parseInt($result.val()) );
				
				if( !isNaN(parseInt($result.val())) )	{
					if( parseInt($result.val()) < 2 )	{
						$('.section-footer').hide();
						$('.load-more').hide();
					}
					else	{
						$('.section-footer').show();
						$('.load-more').show();
					}
				}
				else	{
					$('.section-footer').hide();
					$('.load-more').hide();
				}
				
//				$('.sidebar-opener').click();
			});
        });

		$(".collapse").on('shown.bs.collapse', function() {
			//$('.main-container').css('min-height', ($('#sidebar').height()+50)+'px');
			if($('html').width() > 1023){
			        $('.sidebar-opener').click();
			        if($('html').width() < 1399)
				        $('.sidebar-opener').click();
                        }
		});
		$(".collapse").on('hidden.bs.collapse', function() {
			//$('.main-container').css('min-height', ($('#sidebar').height()+50)+'px');
                        if($('html').width() > 1023){
			        $('.sidebar-opener').click();
			        if($('html').width() < 1399)
				        $('.sidebar-opener').click();
                        }
		});

	});
</script>
<style>
.logo_img{
	/*display:none !important;*/
}
.imgloader
{
	position:relative;
	background: #fff;
	width:100% /* 90% */;
	height:auto /*58%*/;
	/*display:none;*/
}
#overlay-wrapper{
    position: absolute;
    top: 0;
    left:0;
    bottom:0;
    right: 0;
    background: #fff;
	opacity: 0.8;
    filter: alpha(opacity=80);
	z-index:9999;
	display:none;
}

#overlay-content-box{
    width: 80%;
    height:50%;
    position: absolute;
    left: 45%;
    top: 45%;
}
/* Loading Effect Three */
.loading-effect-3 {
  width: 100px;
  height: 100px;
}
.loading-effect-3 div {
  display: block;
  width: 100%;
  height: 100%;
  border: 1px solid #000;
  border-radius: 50%;
}
.loading-effect-3 div > span {
  background: #000;
  width: 10px;
  height: 10px;
  display: block;
  border-radius: 50%;
  position: relative;
  top: -5px;
  left: 50%;
  -webikt-transform-origin: 0 55px;
  -moz-transform-origin: 0 55px;
  -o-transform-origin: 0 55px;
  transform-origin: 0 55px;
  -webkit-animation: effect-3 2s infinite linear;
  -moz-animation: effect-3 2s infinite linear;
  -ms-animation: effect-3 2s infinite linear;
  -o-animation: effect-3 2s infinite linear;
  animation: effect-3 2s infinite linear;
}
@-webkit-keyframes effect-3 {
  from {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes effect-3 {
  from {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  to {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
</style>
<!-- contain main informative part of the site -->
<!-- <main id="main" class="main-active"> -->
<!-- contain sidebar of the page -->
<aside id="sidebar" class="pull-left">
    <button class="btn btn-primary form-block-opener" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample">{{ trans('messages.Filter Options') }} <i class="icon-filter"></i></button>

    <form action="/{{$tab}}" class="form-filters collapse in" aria-expanded="true" id="collapseExample" method="GET" >

        <!-- aside-block -->
        <div class="form-block">
            <h2>{{ trans('messages.Categories') }}</h2>
            <!-- accordion -->
            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                
                <!-- panel -->
                
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="category">
                        <a id="catheader" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">{{ trans('messages.Categories') }}</a>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="category">
                        <div class="panel-body">
                            <ul class="list-unstyled cat_list" style="height:250px; overflow-y: scroll;">
                                @foreach($categories AS $c)
                                <li>
                                    <label class="fake-checkbox" for="category{{ $c->id_category }}">
                                        <input type="checkbox" {{ $cats_selected[$c->id_category] }} id="category{{ $c->id_category }}" name="category" value="{{ $c->id_category }}">
                                        <span></span>
                                    </label>
                                    <span class="label"><label for="sub-category1">{{ $c->title }}</label></span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="btn-block text-center">
                    <a href="javascript:" class="btn-all" id="all_cat"> All {{ trans('messages.Categories') }} </a> | 
                    <a href="javascript:" class="btn-all" id="none_cat"> None </a>
                </div>
            </div>
        </div>
        <!-- aside-block -->
        <div class="form-block">
            <h2>{{ trans('messages.Price Range') }}</h2>
			<input name="price" value="{{$min_price_selected}},{{$max_price_selected}}" type="hidden" id="price_range_slider" />
            <input value="{{$min_price}},{{$max_price}}" id="price" type="range" min="{{$min_price}}" max="{{$max_price}}" multiple >
            <div class="values clearfix hidden">
                <span class="pull-left"  >
					{{$currency}}<span class="cur-min" id="price_min">{{$min_price}}</span> 
					
				</span>
                <span class="pull-right" >
					{{$currency}}<span class="cur-max" id="price_max">{{$max_price}}</span>
					
				</span>
            </div>
			<div class="clearfix" id="custom_price_ranges">
				
				<!--
				<span class="pull-left"><span class="cur-min price" id="left_price" >{{$currency}} {{$min_price}}</span></span>
				<span class="pull-right"><span class="cur-max price" id="right_price">{{$currency}} {{$max_price}}</span></span>
				-->
				
				<span class="pull-left" >
					<input type="hidden" value="{{$currency}}" name="current_currency" id="current_currency">
					<span class="cur-min price" id="leftPriceLabel">{{$min_price}}</span> 
					<input type="hidden" value="{{$min_price}}" name="pricedata" id="leftPrice">
					<input type="hidden" value="{{$min_price}}" name="converted_pricedata" id="converted_leftPrice">
				</span>
				<span class="pull-right" >
					<span class="cur-max price" id="rightPriceLabel">{{$max_price}}</span> 
					<input type="hidden" value="{{$max_price}}" name="pricedata" id="rightPrice">
					<input type="hidden" value="{{$min_price}}" name="converted_pricedata" id="converted_rightPrice">
				</span>
				
				<!--
  				<span class="pull-left">$<input type="text" id="cur-min-input" size="7" value="{{$min_price_selected}}" class="cur-min price_box" data-pricebox-index="first" placeholder="{{$min_price}}" ></span>
                <span class="pull-right">$<input type="text" id="cur-max-input" size="7" value="{{$max_price_selected}}" class="cur-max price_box" data-pricebox-index="last" placeholder="{{$max_price}}" ></span>
				-->
				
            </div>
        </div>
		<script>
			$(document).ready(function(){

				$('#custom_price_ranges .price_box').keyup(function(){

					var min_range = {{$min_price}};
					var max_range = {{$max_price}};
					var val = $(this).val();
					if(isNaN(val) || val <= 0)
						$(this).val(val.substr(0, val.length-1));

					var v1, v2;
					var $price = jcf.getInstance('#price');
					$price.refresh();
					v1 = $price.values[0];
					v2 = $price.values[1];
					if(val < min_range || val > max_range){
						return;
					}
					else{
						if($(this).attr('data-pricebox-index') == 'first')
								v1 = val;
						else 	v2 = val;
					}
					console.log(v1, v2);

					//update prices
					$price.setSliderValue([v1,v2]);
					$price.values = [v1,v2];
					$price.updateValues();
					$price.updateRanges();

					console.log(v1, v2);
					$('#values .cur-min').text(v1);
					$('#values .cur-max').text(v2);
					
					$('#price').attr('value', v1+','+v2).attr('min', v1).attr('max', v2);

					$('#cur-min-input').val(v1);
					$('#cur-max-input').val(v2);
					
					$price.refresh();
					
					console.clear();
					console.log($price.getSliderValue());
				});
			});
		</script>
        <!-- aside-block -->
        <div class="form-block">
            <h2>{{ trans('messages.Domain Length') }}</h2>

				<input name="length" value="{{$min_length_selected}},{{$max_length_selected}}" type="hidden" />
                <input type="range" value="{{$min_length}},{{$max_length}}" min="{{$min_length}}" max="{{$max_length}}" multiple >
            
            <div class="values clearfix">
                <span class="pull-left"><span class="cur-min" id="length_min">{{$min_length}}</span></span>
                <span class="pull-right"><span class="cur-max" id="length_max">{{$max_length}}</span></span>
            </div>
        </div>
        <!-- aside-block -->
        <div class="form-block">
            <h2>{{ trans('messages.Domain Extension') }}</h2>
            <!-- list-radios -->
            <ul class="list-inline list-radios">
                @foreach($tlds->record AS $tld)
                <li>
                    <label class="fake-checkbox" for="{{ $tld->tld }}">
                        <!-- <input type="checkbox" {{ $tld_selected[$tld->tld] }} id="{{ $tld->tld }}" name="tld[]" value="{{ $tld->tld }}" id="tld"> -->
						
						<input type="checkbox" {{ $tld_selected[$tld->tld] }} id="{{ $tld->tld }}" name="tld" value="{{ $tld->tld }}" id="tld">
                        <span></span>
                    </label>
                    <label for="{{ $tld->tld }}">{{ $tld->tld }}</label>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="btn-block text-center">
            <!-- <input type="submit" class="btn btn-primary" value="{{ trans('messages.go') }}" id="btnSubmit"> -->
			<input type="button" class="btn btn-primary" value="{{ trans('messages.go') }}" id="btnSubmitGoFilter">
        </div>
    </form>
</aside>
<!-- main-container -->
<div class="main-container">
    <div class="container-fluid">
        <!-- products-area -->
        <section class="products-area sorting-area">
            <header class="section-head">
                <div class="head clearfix">
                    <a href="#" class="sidebar-opener btn btn-default pull-left"><span class="hidden-xs">{{ trans('messages.Filter Options') }}</span> <i class="icon-filter"></i> <i class="glyphicon glyphicon-remove"></i></a>
                    <!-- nav-products -->
                    <nav class="nav-products pull-left">
                        <a href="#" class="btn-opener"><i class="glyphicon glyphicon-chevron-down"></i></a>
                        <ul class="list-inline products-drop">
						<?php if( $cnt_seller_settings > 0 ) 	{	?>
								<?php // if( $seller_settings->is_vis_brandable > 0 ) {	?>
									<li @if($tab == 'brandable')class="active"@endif><a href="/brandable">{{ trans('messages.BRANDABLE') }}</a></li>
								<?php // }	?>
								<?php // if( $seller_settings->is_vis_premium > 0 ) {	?>
									<li @if($tab == 'premium')class="active"@endif><a href="/premium">{{ trans('messages.PREMIUM') }}</a></li>
								<?php // }	?>
								<?php // if( $seller_settings->is_vis_under1k > 0 ) {	?>
									<li @if($tab == 'under1k')class="active"@endif><a href="/under1k">UNDER <span class="price"><?php echo $premiumprice_formatted; ?></span><input type="hidden" value="<?php echo $premiumprice; ?>" name="pricedata"></a></li>
									<!--<li @if($tab == 'under1k')class="active"@endif><a href="/under1k">UNDER <span class="price"><?php echo $currency; ?><?php echo $premiumprice; ?></span><input type="hidden" value="<?php echo $premiumprice; ?>" name="pricedata"></a></li>-->
								<?php // }	?>
								<!--<li><a href="#">WEBSITES</a></li>-->
								<!--<li><a href="#">SOLD DOMAINS</a></li>-->
						<?php }	else	{	 ?>
								<li @if($tab == 'brandable')class="active"@endif><a href="/brandable">{{ trans('messages.BRANDABLE') }}</a></li>
								<li @if($tab == 'premium')class="active"@endif><a href="/premium">{{ trans('messages.PREMIUM') }}</a></li>
								<li @if($tab == 'under1k')class="active"@endif><a href="/under1k">UNDER <span class="price"><?php echo $premiumprice_formatted; ?></span><input type="hidden" value="<?php echo $premiumprice; ?>" name="pricedata"></a></li>
								<!--<li @if($tab == 'under1k')class="active"@endif><a href="/under1k">{{ trans('messages.UNDER 1K') }}</a></li>-->
						<?php }	?>
                        </ul>
                    </nav>
                    <!-- form-search -->
                    <form action="/{{$tab}}?<?php echo urldecode(http_build_query($_GET)) ?>" class="form-search pull-right">
                        <label for="search"> </label>
                        <div class="form-holder">
                            <input type="search" class="form-control" id="search" name="search" placeholder="{{ trans('messages.Search for domains') }}..." value="{{$search}}">
                            <button id="btnSearch" type="button" class="btn-link"><i class="icon-search"></i></button>
                        </div>
                    </form>
                </div>
                <!-- sort-type -->
                <div class="sort-type clearfix">
                    <div class="sort-holder pull-right">
                        <!-- sort-select -->
                        <div class="sort-select">
                            <button class="btn-link" type="button" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop">
                                <span class="title pull-left">{{ trans('messages.Sort by') }}:</span>
                                <span class="type pull-right">{{ trans('messages.Default') }}</span>
                                <span class="caret"></span>
                            </button>
                            <div class="collapse" id="sortDrop">
                                <ul class="list-unstyled sorting-list">
									<li><a class="active" data-sort-by="asc" data-short-name="{{ trans('messages.Default')}}" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="order_no">{{ trans('messages.Default')}}</a></li>
                                	<li><a data-sort-by="asc" data-short-name="{{ trans('messages.Name A to Z')}}" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="domain">{{ trans('messages.Name A to Z')}}</a></li>
                                    <li><a data-sort-by="desc" data-short-name="{{ trans('messages.Name Z to A')}}" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="domain">{{ trans('messages.Name Z to A') }}</a></li>
<!--													<li><a data-sort-by="asc" data-short-name="register asc" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="reg_date">{{ trans('messages.by registered') }} asc</a></li>
                                                    <li><a data-sort-by="desc" data-short-name="register desc" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="reg_date">{{ trans('messages.by registered') }} desc</a></li> 
                                                    <li><a data-sort-by="asc" data-short-name="expires asc" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="expires">{{ trans('messages.by expires') }} asc</a></li>
                                    <li><a data-sort-by="desc" data-short-name="expires desc" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="expires">{{ trans('messages.by expires') }} desc</a></li> -->
                                                    <li><a data-sort-by="asc" data-short-name="{{ trans('messages.Price Asc') }}" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="price">{{ trans('messages.Price Asc') }}</a></li>
                                                    <li><a data-sort-by="desc" data-short-name="{{ trans('messages.Price Desc') }}" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="price">{{ trans('messages.Price Desc') }}</a></li>
                                                    <!--<li><a class="active" data-sort-by="symbol" data-short-name="A-Z asc" href="#" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="1">by alphabet asc</a></li>
                                    <li><a data-sort-by="desc" data-short-name="A-Z asc" href="/more/{{$tab}}" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="domain">by alphabet desc</a></li>
													<li><a data-sort-by="date" data-short-name="register asc" href="#" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="1">by registered asc</a></li>
                                                    <li><a data-sort-by="date" data-short-name="register desc" href="#" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="0">by registered desc</a></li>
                                                    <li><a data-sort-by="expires" data-short-name="expires asc" href="#" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="1">by expires asc</a></li>
                                    <li><a data-sort-by="expires" data-short-name="expires desc" href="#" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="0">by expires desc</a></li> 
                                                    <li><a data-sort-by="price" data-short-name="low price" href="#" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="1">by price low</a></li>
                                                    <li><a data-sort-by="price" data-short-name="high price" href="#" data-toggle="collapse" data-target="#sortDrop" aria-expanded="false" aria-controls="sortDrop" data-sort-order="0">by price high</a></li> -->                                                                                     
                                </ul>
                            </div>
                        </div>
                        <!-- grid-view -->
                        <a href="#" class="btn-grid active"><i class="icon-grid"></i></a>
                        <a href="#" class="btn-list"><i class="icon-bars"></i></a>
                    </div>
                </div>
            </header>
            <!-- products-holder -->
            <div id="overlay-wrapper">
                <div id="overlay-content-box">
                    <div class="clear-loading loading-effect-3">
                        <div><span></span></div>
                    </div>
                </div>
            </div>
            <div class="products-holder row" id="container">
				
					<!-- item -->
					@if($domainData->rows > 0)
						@foreach($domainData->record AS $d)
							@if($tab == 'brandable')
								<article class="isotope-item item col-xs-12 col-sm-6 col-md-4 col-lg-3 {{$class[current($d)]}}" data-date="{{date("F d Y", strtotime($d->reg_date))}}" data-price="{{$d->price}}" data-expires="{{date("F d Y", strtotime($d->expires))}}">
									<div class="box box-link">
										<div class="imgloader">
												<img src="/images/loader1.gif" />
										</div>
										<div class="img-holder" style="display:none;" >
											<!-- picturefill html structure example -->
												<!--<img src="/images/loader.gif" class="loader" />-->
											<!-- <a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}"> -->
											<a href="/{{ $d->domain }}">
												<img src="{{ $url }}image.php?id_logo={{ $d->id_logo }}" alt="Some logo text for SEO" class="logo_img">
											</a>                            
										</div>
										<!-- <h2><a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}">{{ $d->domain }}</a></h2> -->
										<h2><a href="/{{ $d->domain }}">{{ $d->domain }}</a></h2>
										<footer class="foot">
											<?php if ( $id_buyer > 0 ) {	?>
												<?php if ( !in_array($d->id_domain, $fav_domainIDs) ) {	?>
													<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="fav btn btn-info pull-left btn-save2">
														<span class="text">{{ trans('messages.save') }}</span>
														<i class="icon-heart"></i>
													</a>
												<?php }	else	{	?>
													<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="fav btn btn-info pull-left btn-save2" style="background: #ff5652 none repeat scroll 0 0; color: #fff;">
														<span class="text" style="display:none;">{{ trans('messages.save') }}</span>
														<i class="icon-heart" style="display:block;"></i>
													</a>
												<?php }	?>
											
											<?php }	else	{	?>
												<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="fav btn btn-info pull-left btn-save">
													<span class="text">{{ trans('messages.save') }}</span>
													<i class="icon-heart"></i>
												</a>
											<?php }	?>

											
											<?php if($d->price <= 0)	{	?>
													<?php if($d->status == 3)	{	?>
															<a href="#" class="btn pull-right">
																<span class="text">DROPPED</span>
																<i class="icon-shopping-cart"></i>
															</a>
													<?php }	else if($d->status == 2)	{	?>
															<a href="#" class="btn pull-right">
																<span class="text">HIDDEN</span>
																<i class="icon-shopping-cart"></i>
															</a>
													<?php }	else {	?>
															<a href="#" class="btn btn-success pull-right btn-buy">
																<span class="text">{{ trans('messages.Make offer') }}</span>
																<i class="icon-shopping-cart"></i>
															</a>
													<?php }	?>
											<?php }	else	{	?>						
													<?php	if ( $id_buyer > 0 ) {	?>
																<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
																			<?php if($d->status == 3)	{	?>
																					<a href="#" class="btn pull-right">
																						<span class="text">DROPPED</span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																			<?php }	else if($d->status == 2)	{	?>
																					<a href="#" class="btn pull-right">
																						<span class="text">HIDDEN</span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																			<?php }	else {	?>
																					<a href="#" class="btn btn-default pull-right btn-buy">
																						<span class="text">{{ trans('messages.buy now') }}</span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																			<?php }	?>
																<?php }	else	{	?>
																			<?php /*
																			<a href="#" class="btn pull-right">
																				<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" onmouseover="viewStatusInfo('<?php echo $order_IPNstatus_Code[$d->id_domain]; ?>', '<?php echo $order_IPNstatus_EscrowUniqueIdentifier[$d->id_domain]; ?>','<?php echo $order_IPNstatus_Description[$d->id_domain]; ?>')" alt="Click to view more info" title="Click to view more info">
																					<?php echo $order_domainStatus[$d->id_domain]; ?>
																				</span>
																				<i class="icon-shopping-cart"></i>
																			</a>
																			*/ ?>
																			
																			<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																						<a href="#" class="btn pull-right">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																			<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																						<a href="#" class="btn pull-right">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																							<i class="icon-shopping-cart"></i>
																						</a>																		
																			<?php }	else	{	?>
																						<a href="#" class="btn pull-right">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																			<?php }	 ?>	
																	
																<?php }	 ?>
													<?php }	else	{	 ?>
													
															<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
																		<?php if($d->status == 3)	{	?>
																				<a href="#" class="btn pull-right">
																					<span class="text">DROPPED</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	else if($d->status == 2)	{	?>
																				<a href="#" class="btn pull-right">
																					<span class="text">HIDDEN</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	else {	?>
																				<a href="#" class="btn btn-default pull-right btn-buy">
																					<span class="text">{{ trans('messages.buy now') }}</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	?>
															<?php }	else	{	?>
															
																<?php	if ( $order_IPNstatus_Code[$d->id_domain] < 1 ) {	?>
																		<?php if($d->status == 3)	{	?>
																				<a href="#" class="btn pull-right">
																					<span class="text">DROPPED</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	else if($d->status == 2)	{	?>
																				<a href="#" class="btn pull-right">
																					<span class="text">HIDDEN</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	else {	?>
																				<a href="#" class="btn btn-default pull-right btn-buy">
																					<span class="text">{{ trans('messages.buy now') }}</span>
																					<i class="icon-shopping-cart"></i>
																				</a>
																		<?php }	?>
																<?php }	else	{	?>
																
																			<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																						<a href="#" class="btn pull-right">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																			<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																						<a href="#" class="btn pull-right">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																							<i class="icon-shopping-cart"></i>
																						</a>																		
																			<?php }	else	{	?>
																						<a href="#" class="btn pull-right">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																			<?php }	 ?>	

																	
																<?php }	 ?>	
															
															<?php }	 ?>
															
													<?php }	 ?>
											<?php }	?>
											<?php	if ( $id_buyer > 0 ) {	?>
														<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
															<?php if($d->price <= 0)	{	?>
																	<strong class="price text-center"> </strong>
															<?php }	else	{	?>
																	<strong class="price text-center">{!! $d->price_formatted !!}</strong>
															<?php }	?>
														<?php }	?>
											<?php }	else	{	?>
														<?php if($d->price <= 0)	{	?>
																<strong class="price text-center"> </strong>
														<?php }	else	{	?>
																<strong class="price text-center">{!! $d->price_formatted !!}</strong>
														<?php }	?>
											<?php }	?>
											
											
											<input type="hidden" name="pricedata" value="{{$d->price}}">
											<input type="hidden" name="offer_pricedata" value="{{$d->offer_price}}">
											<input type="hidden" name="domainId" value="{{ $d->id_domain }}">
										</footer>
									</div>
								</article>
							@else
								<article class="isotope-item item item-list col-xs-12 col-sm-6 col-md-4 col-lg-3 {{$class[current($d)]}}" data-date="{{date("F d Y", strtotime($d->reg_date))}}" data-price="{{$d->price}}" data-expires="{{date("F d Y", strtotime($d->expires))}}">
									<div class="box box-link">
										<!-- <h2><a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}">{{ $d->domain }}</a></h2> -->
										<h2><a href="/{{ $d->domain }}">{{ $d->domain }}</a></h2>
										<footer class="foot">
										
											<?php /*
											<?php if($d->price <= 0)	{	?>
													<strong class="price text-center"> </strong>
											<?php }	else	{	?>
													<strong class="price text-center">{{ $d->price_formatted }}</strong>
											<?php }	?>
											*/ ?>
											
											<input type="hidden" name="pricedata" value="{{$d->price}}">
											<input type="hidden" name="offer_pricedata" value="{{$d->offer_price}}">
											<input type="hidden" name="domainId" value="{{ $d->id_domain }}">
											
											<div class="pull-right">
											<?php if($d->price <= 0)	{	?>
													<?php if($d->status == 3)	{	?>
															<a href="#" class="btn">
																<span class="text">DROPPED</span>
																<i class="icon-shopping-cart"></i>
															</a>
													<?php }	else if($d->status == 2)	{	?>
															<a href="#" class="btn ">
																<span class="text">HIDDEN</span>
																<i class="icon-shopping-cart"></i>
															</a>
													<?php }	else {	?>
															<a href="#" class="btn btn-success btn-buy">
																<span class="text">{{ trans('messages.Make offer') }}</span>
																<i class="icon-shopping-cart"></i>
															</a>
													<?php }	?>
											<?php }	else	{	?>
													<?php	if ( $id_buyer > 0 ) {	?>
																<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
																			<?php if($d->status == 3)	{	?>
																					<a href="#" class="btn ">
																						<span class="text">DROPPED</span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																			<?php }	else if($d->status == 2)	{	?>
																					<a href="#" class="btn ">
																						<span class="text">HIDDEN</span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																			<?php }	else {	?>
																					<a href="#" class="btn btn-default  btn-buy">
																						<span class="text">{{ trans('messages.buy now') }}</span>
																						<i class="icon-shopping-cart"></i>
																					</a> 
																			<?php }	?>
																<?php }	else	{	?>
																			<?php /*
																			<a href="#" class="btn pull-right">
																				<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" onmouseover="viewStatusInfo('<?php echo $order_IPNstatus_Code[$d->id_domain]; ?>', '<?php echo $order_IPNstatus_EscrowUniqueIdentifier[$d->id_domain]; ?>','<?php echo $order_IPNstatus_Description[$d->id_domain]; ?>')" alt="Click to view more info" title="Click to view more info">
																					<?php echo $order_domainStatus[$d->id_domain]; ?>
																				</span>
																				<i class="icon-shopping-cart"></i>
																			</a>
																			*/ ?>
																		
																			<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																						<a href="#" class="btn ">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																			<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																						<a href="#" class="btn ">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																							<i class="icon-shopping-cart"></i>
																						</a>																		
																			<?php }	else	{	?>
																						<a href="#" class="btn ">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																			<?php }	 ?>	
																		
																<?php }	?>
													<?php }	else	{	?>
													
																<?php	if ( !in_array($d->id_domain, $order_domainIDs) || $order_IPNstatus_Code[$d->id_domain] < 1 ) {	?>
																			<?php if($d->status == 3)	{	?>
																					<a href="#" class="btn">
																						<span class="text">DROPPED</span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																			<?php }	else if($d->status == 2)	{	?>
																					<a href="#" class="btn">
																						<span class="text">HIDDEN</span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																			<?php }	else {	?>
																					<a href="#" class="btn btn-default btn-buy">
																						<span class="text">{{ trans('messages.buy now') }}</span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																			<?php }	?>
																<?php }	else	{	?>
																
																			<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 ) {	?>
																						<a href="#" class="btn ">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																			<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																						<a href="#" class="btn">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																							<i class="icon-shopping-cart"></i>
																						</a>																		
																			<?php }	else	{	?>
																						<a href="#" class="btn ">
																							<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																							<i class="icon-shopping-cart"></i>
																						</a>
																			<?php }	 ?>	
																

																<?php }	?>

													<?php }	?>
													
											<?php }	?>
	
											</div>
											<div class="pull-left">
											
											
											<?php if ( $id_buyer > 0 ) {	?>
													<?php if ( !in_array($d->id_domain, $fav_domainIDs) ) {	?>
														<a href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="fav btn btn-info btn-save2">
															<span class="text">{{ trans('messages.save') }}</span>
															<i class="icon-heart-o"></i>
														</a>
													<?php }	else	{	?>
														<a href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="fav btn btn-info btn-save2" style="background: #ff5652 none repeat scroll 0 0; color: #fff;">
															<span class="text" style="display:none;" >{{ trans('messages.save') }}</span>
															<i class="icon-heart-o" style="display:block;" ></i>
														</a>
													<?php }	?>
											<?php }	else	{	?>
												<a href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="fav btn btn-info btn-save">
													<span class="text">{{ trans('messages.save') }}</span>
													<i class="icon-heart-o"></i>
												</a>
											<?php }	?>
											</div>
											
											<div class="pull-left" style="padding-left:40px;">
												<?php if($d->price <= 0)	{	?>
														<strong class="price text-center"> </strong>
												<?php }	else	{	?>
														<strong class="price text-center">{{ $d->price_formatted }}</strong>
												<?php }	?>
											</div>
											
										</footer>
									</div>
								</article>
							@endif 
						@endforeach
					@else
						<div align="center">No Domain Found</div>
					@endif
					
				<div id="productlist">
				</div>
				
				<?php /*
				@if($domainData->total_rows > $records_per_page && $domainData->rows > 0)
				<footer class="section-footer text-center">
					<a href="/more/{{$tab}}" class="btn btn-default load-more" >{{ trans('messages.Load More')}}</a>
					<input type="hidden" id="curr_page" value="1">
					<input type="hidden" id="total_page" value="{{$total_page}}">
				</footer>
				@endif
				<input type="hidden" value="<?php echo $domainData->total_rows; ?>" id="total_rows" name="total_rows" />
			
				*/
				?>
				
            </div>
			
				@if($domainData->total_rows > $records_per_page && $domainData->rows > 0)
				<footer class="section-footer text-center">
					<a href="/more/{{$tab}}" class="btn btn-default load-more" >{{ trans('messages.Load More')}}</a>
					<input type="hidden" id="curr_page" value="1">
					<input type="hidden" id="total_page" value="{{$total_page}}">
				</footer>
				@endif
				<input type="hidden" value="<?php echo $domainData->total_rows; ?>" id="total_rows" name="total_rows" />
			
			
			
			<input type="hidden" value="/more/{{$tab}}" id="current_tab_url" name="current_tab_url" />
			
			
        </section>
    </div>
</div>




<script>
function viewStatusInfo(status_code, transactionID, escrow_status)	{
	var title = '';
	var content = '';
	if( status_code == 0 )	{
		title = 'Cancelled';
		content = '<p>Seller have cancelled the transaction. Login to your Escrow account to check the reason why the Seller cancelled this transaction <b>#'+transactionID+'</b>.</p>';
	}
	else if( status_code == 5 )	{
		title = 'Pending';
		content = '<p>Seller haven\'t check or review your enquiry transaction <b>#'+transactionID+'</b>. You can send a reminder by logging-in to your Escrow account.</p>';
	}
	else if( status_code == 15 )	{
		title = 'Pay Now !';
		content = '<p>You can now pay the domain you have enquire. Login to your Escrow account to select a payment method. <b> Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 20 )	{
		title = 'Payment Submitted';
		content = '<p>You have already submitted a payment. Please wait while Escrow securing your payment. <b> Transaction #'+transactionID+'</b>. </p>';
	}
	else if( status_code == 25 )	{
		title = 'Payment Accepted';
		content = '<p>Seller have securely received your payment. Seller will release your domain as soon as possible. You can send a message or email to send a reminder. <b>Transaction #'+transactionID+'</b>. </p>';
	}
	else	{
		title = escrow_status;
		content = '<p>You can view the details of this transaction. Login to your Escrow account to check this transaction <b>#'+transactionID+'</b>.</p>';
	}
	
	content += '<div style="color:#6f7c96;font-size:12px;">';
	content += '<br> Escrow Transaction ID : ' + transactionID;
	content += '<br> Escrow Status : ' + escrow_status;
	content += '</div>';
	
	$('#order_status_title').html(title);
	$('#order_status_content').html(content);
	$('#statusModal').modal();
}
</script>

</main>
@stop 