@extends('app')
@section('content')
<!-- contain main informative part of the site -->
<main id="main">
<section class="tabs-holder">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 container-holder">
                <ul class="tabset list-inline">
                    <li><a id="topA" onclick="getIndustrySales('top20');" style="cursor:pointer;"><span class="text-holder"><i class="icon fav-icon"></i>Top 20 This Week</span></a></li>
                    <li><a id="topB" onclick="getIndustrySales('top100');" style="cursor:pointer;"><span class="text-holder"><i class="icon status-icon"></i>Top 100 Year To Date</span></a></li>
                    <li><a id="topC" onclick="getIndustrySales('top500');" style="cursor:pointer;"><span class="text-holder"><i class="icon history-icon"></i>Top 500 All Time</span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="tab-content-holder">
            <div class="tab-content">
                <div id="tab1">
					<h2>Industry Sales</h2>
					<div id="load_industry_sales"></div>
				</div>
			</div>
		</div>
    </div>
</section>
<script>
var api_url = '<?php echo config('app.API_URL') ?>';
$( document ).ready(function() {
	$('#topC').click();
});	
</script>
</main>
@stop