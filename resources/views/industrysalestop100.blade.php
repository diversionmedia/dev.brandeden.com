						<div class="table-area">
                            <table class="table-holder add">
                                <thead>
                                    <tr>
										<th class="col66" style="width:1%"></th>
										<th class="col6">Domain</th>
										<th class="col6">Sold For</th>
										<th class="col7">Where Sold</th>
										<th class="col7">Date *</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php
									$url = 'http://www.dnjournal.com/ytd-sales-charts.htm';
									$html = file_get_contents($url); 
									$pokemon_doc = new DOMDocument();
									libxml_use_internal_errors(TRUE);
									if(!empty($html))	{
										$pokemon_doc->loadHTML($html);
										libxml_clear_errors(); 
										$pokemon_xpath = new DOMXPath($pokemon_doc);
										$pokemon_row = $pokemon_xpath->query('//td[@width="37%"]');	// for Domain Column
										$domain_cont = array();
										if($pokemon_row->length > 0)	{
											$cnt = 0;
											foreach($pokemon_row as $row){
												$domain_cont[] = $row->nodeValue;		
											}
										}
										$pokemon_row = $pokemon_xpath->query('//td[@width="34%"]'); // for Solf For(Top 100 Sales Chart)
										$domain_sold_for = array();
										if($pokemon_row->length > 0)	{
											$cnt = 0;
											foreach($pokemon_row as $row){
												$check_str = str_replace(' ', '', $row->nodeValue);
												$check_str = str_replace(array("\r","\n"), '', $check_str);
												if( trim($check_str) != 'SoldFor' && trim($check_str) != '' )	{
													$domain_sold_for[] = $row->nodeValue;
												}
											}
										}
										$pokemon_row = $pokemon_xpath->query('//td[@width="24%"]'); // for Where Sold(Top 100 Sales Chart)
										$domain_where_sold = array();
										if($pokemon_row->length > 0)	{
											$cnt = 0;
											foreach($pokemon_row as $row){
												$check_str = str_replace(' ', '', $row->nodeValue);
												$check_str = str_replace(array("\r","\n"), '', $check_str);
												if( trim($check_str) != 'WhereSold' && trim($check_str) != '' )	{
													$domain_where_sold[] = $row->nodeValue;
												}
											}
										}

										$pokemon_row = $pokemon_xpath->query('//td[@width="10%"]'); // for Date(Top 100 Sales Chart)
										$domain_date = array();
										if($pokemon_row->length > 0)	{
											$cnt = 0;
											foreach($pokemon_row as $row){
												$check_str = str_replace(' ', '', $row->nodeValue);
												$check_str = str_replace(array("\r","\n"), '', $check_str);
												if( trim($check_str) != 'Date*' && trim($check_str) != '' )	{
													$domain_date[] = $row->nodeValue;
												}
											}
										}
										
										if( count($domain_cont) > 0 )	{
											$cnt = 0; 
											foreach($domain_cont as $ind => $val)	{ 
												if( isset($domain_sold_for[$ind]) && isset($domain_where_sold[$ind]) && isset($domain_date[$ind]) )	{
													$cnt++; 
													?>
													<tr>
														<td> <?php echo $cnt; ?> . </td>
														<td> <?php echo $val; ?> </td>
														<td> <?php echo $domain_sold_for[$ind]; ?> </td>
														<td> <?php echo $domain_where_sold[$ind]; ?> </td>
														<td> <?php echo $domain_date[$ind]; ?> </td>
													</tr>
													<?php 
												}	
											}
										}
										else	{
											?>
											<tr>
												<td colspan="5"> No Domain Found. </td>
											</tr>
											<?php
										}
									}
									else	{
										?>
										<tr>
											<td colspan="5"> Can't  render the HTML. </td>
										</tr>
										<?php
									}
									?>
                                </tbody>
                            </table>
                        </div>
						<div>* DNJournal.com: <a href="http://www.dnjournal.com/ytd-sales-charts.htm" target="_blank">Year To Date Sales Charts</a></div>