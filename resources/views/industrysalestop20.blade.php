						<div class="table-area">
                            <table class="table-holder add">
                                <thead>
                                    <tr>
										<th class="col66" style="width:1%"></th>
										<th class="col6">Domain</th>
										<th class="col6">Sold For</th>
										<th class="col7">Where Sold</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php
									$url = 'http://www.dnjournal.com/domainsales.htm';
									$html = file_get_contents($url); 
									$pokemon_doc = new DOMDocument();
									libxml_use_internal_errors(TRUE); 
									if(!empty($html))	{ 
										$pokemon_doc->loadHTML($html);
										libxml_clear_errors();
										$pokemon_xpath = new DOMXPath($pokemon_doc);
										$pokemon_row = $pokemon_xpath->query('//td[@width="37%"]');	// for Domain Column
										$domain_cont = array();
										if($pokemon_row->length > 0)	{
											$cnt = 0;
											foreach($pokemon_row as $row){
												$domain_cont[] = $row->nodeValue;		
											}
										}
										$pokemon_row = $pokemon_xpath->query('//td[@width="30%"]'); // for Solf For(DN Journal Top 20)
										$domain_sold_for = array();
										if($pokemon_row->length > 0)	{
											$cnt = 0;
											foreach($pokemon_row as $row){
												$check_str = str_replace(' ', '', $row->nodeValue);
												$check_str = str_replace(array("\r","\n"), '', $check_str);
												if( trim($check_str) != 'SoldFor' && trim($check_str) != '' )	{
													$domain_sold_for[] = $row->nodeValue;
												}
											}
										}
										
										$pokemon_row = $pokemon_xpath->query('//td[@width="28%"]'); // for Where Sold(DN Journal Top 20)
										$domain_where_sold = array();
										if($pokemon_row->length > 0)	{
											$cnt = 0;
											foreach($pokemon_row as $row){
												$check_str = str_replace(' ', '', $row->nodeValue);
												$check_str = str_replace(array("\r","\n"), '', $check_str);
												if( trim($check_str) != 'WhereSold' && trim($check_str) != '' )	{
													$domain_where_sold[] = $row->nodeValue;
												}
											}
										}
										
										if( count($domain_cont) > 0 )	{
											$cnt = 0; 
											foreach($domain_cont as $ind => $val)	{ 
												if( isset($domain_sold_for[$ind]) && isset($domain_where_sold[$ind]) )	{
													$cnt++; 
													?>
													<tr>
														<td> <?php echo $cnt; ?> . </td>
														<td> <?php echo $val; ?> </td>
														<td> <?php echo $domain_sold_for[$ind]; ?> </td>
														<td> <?php echo $domain_where_sold[$ind]; ?> </td>
													</tr>
													<?php 
												}	
											}
										}
										else	{
											?>
											<tr>
												<td colspan="4"> No Domain Found. </td>
											</tr>
											<?php
										}
									}
									else	{
										?>
										<tr>
											<td colspan="4"> Can't  render the HTML. </td>
										</tr>
										<?php
									}
									?>
                                </tbody>
                            </table>
                        </div>
						<div>* DNJournal.com: <a href="http://www.dnjournal.com/domainsales.htm" target="_blank">Top 20 This Week</a></div>