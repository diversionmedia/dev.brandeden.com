						<div class="table-area">
                            <table class="table-holder add">
                                <thead>
                                    <tr>
										<th class="col66" style="width:1%"></th>
										<th class="col6">Domain</th>
										<th class="col6">Sold For</th>
										<th class="col7">Where Sold</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php
									$url = 'https://www.domaining.com/topsales/';
									$stream_opts = ["ssl"=>["verify_peer"=>false, "verify_peer_name"=>false, ]];
									$html = file_get_contents($url, false, stream_context_create($stream_opts));
									//$html = file_get_contents($url); 
									$pokemon_doc = new DOMDocument();
									libxml_use_internal_errors(TRUE); 
									if(!empty($html))	{ 
										$pokemon_doc->loadHTML($html);
										libxml_clear_errors(); 
										$pokemon_xpath = new DOMXPath($pokemon_doc);
										$pokemon_row = $pokemon_xpath->query('//tr[starts-with(@id, "cd_")]');
										$domain_cont = array();
										if($pokemon_row->length > 0)	{
											$cnt = 0;
											foreach($pokemon_row as $row){
												$check_str = str_replace(array("\r","\n"), '**--**', $row->nodeValue);
												$domain_cont[] = $check_str;
											}
										}
										
										if( count($domain_cont) > 0 )	{
											foreach( $domain_cont as $ind => $val )	{
												$result = explode('**--**',$val);	?>
												<tr>
													<?php 
													foreach($result as $i => $col)	{
														if( trim($col != '') )	{	?> <td id="<?php echo $i; ?>"> <?php echo $col; ?> </td> <?php	}
													}
													?>
												</tr>
												<?php 
											}
										}
										else	{
											?>
											<tr>
												<td colspan="4"> No Domain Found. </td>
											</tr>
											<?php
										}
									}
									else	{
										?>
										<tr>
											<td colspan="4"> Can't  render the HTML. </td>
										</tr>
										<?php
									}
									?>
                                </tbody>
                            </table>
                        </div>
						<div>* Domaining.com: <a href="https://www.domaining.com/topsales/" target="_blank">Top Sales</a></div>