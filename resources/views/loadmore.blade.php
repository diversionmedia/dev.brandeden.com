<?php if($domainData->result == 'SUCCESS')	{	?>

	
	<?php /* if($domainData->response_details_main_result != '')	{	?>
		<article class="isotope-item item col-xs-12 col-sm-6 col-md-4 col-lg-12"><div align="center"><?php echo $domainData->response_details_main_result ; ?></div></article>
	<?php }	*/?>

	<?php if($domainData->rows > 0)	{	?>
		<?php foreach($domainData->record AS $ind => $d)	{	?>
			<?php if($tab == 'brandable')	{	?>
				<article class="isotope-item item col-xs-12 col-sm-6 col-md-4 <?php echo ($d->id_logo > 0 ? 'col-lg-3' : 'col-lg-4'); ?> {{$class[current($d)]}}" data-date="{{date("F d Y", strtotime($d->reg_date))}}" data-price="{{$d->price}}" data-expires="{{date("F d Y", strtotime($d->expires))}}">
							<div class="box box-link">
								<?php if(  $d->id_logo > 0 ) {	?>
									<!--
									<div class="imgloader">
											<img src="/images/loader1.gif" />
									</div>
									<div class="img-holder" style="display:none;" >
									-->
									<div class="img-holder">
										<!-- picturefill html structure example -->
											<!--<img src="/images/loader.gif" class="loader" />-->
										<!-- <a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}"> -->
										<a href="/{{ $d->domain }}">
											<img src="{{ $url }}image.php?id_logo={{ $d->id_logo }}" alt="Some logo text for SEO" class="logo_img">
										</a>
									</div>
								<?php } ?>	
								<!-- <h2><a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}">{{ $d->domain }}</a></h2> -->
								<h2><a href="/{{ $d->domain }}">{{ $d->domain }}</a></h2>
								<footer class="foot">
									<?php if ( $id_buyer > 0 ) {	?>
									
										<?php if ( !in_array($d->id_domain, $fav_domainIDs) ) {	?>
												<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="fav btn btn-info pull-left btn-save2">
													<span class="text">{{ trans('messages.save') }}</span>
													<i class="icon-heart"></i>
												</a>
										<?php }	else	{	?>
												 <a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}" class="fav btn btn-info pull-left btn-save2" style="background: #ff5652 none repeat scroll 0 0; color: #fff;">
													<span style="display:none;" class="text">{{ trans('messages.save') }}</span>
													<i style="display:block;" class="icon-heart"></i>
												</a>
										<?php }	?>
										
									<?php }	else	{	?>
									
										<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" class="fav btn btn-info pull-left btn-save">
											<span class="text">{{ trans('messages.save') }}</span>
											<i class="icon-heart"></i>
										</a>
										
									<?php }	?>
									
									<?php if($d->price <= 0)	{	?>
										<?php if($d->status == 3)	{	?>
												<a href="#" class="btn pull-right">
													<span class="text">DROPPED</span>
													<i class="icon-shopping-cart"></i>
												</a>
										<?php }	else if($d->status == 2)	{	?>
												<a href="#" class="btn pull-right">
													<span class="text">HIDDEN</span>
													<i class="icon-shopping-cart"></i>
												</a>
										<?php }	else {	?>
												<a href="#" class="btn btn-success pull-right btn-buy">
													<span class="text">{{ trans('messages.Make offer') }}</span>
													<i class="icon-shopping-cart"></i>
												</a>
										<?php }	?>
									<?php }	else	{	?>
											<?php	if ( $id_buyer > 0 ) {	?>
														<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
															<?php if($d->status == 3)	{	?>
																	<a href="#" class="btn pull-right">
																		<span class="text">DROPPED</span>
																		<i class="icon-shopping-cart"></i>
																	</a>
															<?php }	else if($d->status == 2)	{	?>
																	<a href="#" class="btn pull-right">
																		<span class="text">HIDDEN</span>
																		<i class="icon-shopping-cart"></i>
																	</a>
															<?php }	else {	?>
																	<!--<a href="#" class="btn btn-default pull-right btn-buy">-->
																	<a href="/billing/{{ $d->domain }}" class="btn btn-default pull-right">
																		<span class="text">{{ trans('messages.buy now') }}</span>
																		<i class="icon-shopping-cart"></i>
																	</a>
															<?php }	?>
														<?php }	else	{	?>
														
															<?php /*
															<a href="#" class="btn pull-right">
																<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" onmouseover="viewStatusInfo('<?php echo $order_IPNstatus_Code[$d->id_domain]; ?>', '<?php echo $order_IPNstatus_EscrowUniqueIdentifier[$d->id_domain]; ?>','<?php echo $order_IPNstatus_Description[$d->id_domain]; ?>')" alt="Click to view more info" title="Click to view more info">
																	<?php echo $order_domainStatus[$d->id_domain]; ?>
																</span>
																<i class="icon-shopping-cart"></i>
															</a>
															*/ ?>
															<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 || $order_IPNstatus_Code[$d->id_domain] == 85 ) {	?>
																		<a href="#" class="btn pull-right">
																			<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																			<i class="icon-shopping-cart"></i>
																		</a>
															<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																		<a href="#" class="btn pull-right">
																			<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																			<i class="icon-shopping-cart"></i>
																		</a>																		
															<?php }	else	{	?>
																		<a href="#" class="btn pull-right">
																			<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																			<i class="icon-shopping-cart"></i>
																		</a>
															<?php }	 ?>	
																		
														<?php }	 ?>
											<?php }	 else	{	?>
														<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
															<?php if($d->status == 3)	{	?>
																	<a href="#" class="btn pull-right">
																		<span class="text">DROPPED</span>
																		<i class="icon-shopping-cart"></i>
																	</a>
															<?php }	else if($d->status == 2)	{	?>
																	<a href="#" class="btn pull-right">
																		<span class="text">HIDDEN</span>
																		<i class="icon-shopping-cart"></i>
																	</a>
															<?php }	else {	?>
																	<!-- <a href="#" class="btn btn-default pull-right btn-buy"> -->
																	<a href="/billing/{{ $d->domain }}" class="btn btn-default pull-right">
																		<span class="text">{{ trans('messages.buy now') }}</span>
																		<i class="icon-shopping-cart"></i>
																	</a>
															<?php }	?>
														<?php }	else	{	?>

																<?php	if ( $order_IPNstatus_Code[$d->id_domain] < 1 ) {	?>
																		<!-- <a href="#" class="btn btn-default pull-right btn-buy"> -->
																		<a href="/billing/{{ $d->domain }}" class="btn btn-default pull-right">
																			<span class="text">{{ trans('messages.buy now') }}</span>
																			<i class="icon-shopping-cart"></i>
																		</a>
																<?php }	else	{	?>
																
																		<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 || $order_IPNstatus_Code[$d->id_domain] == 85 ) {	?>
																					<a href="#" class="btn pull-right">
																						<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																		<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																					<a href="#" class="btn pull-right">
																						<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																						<i class="icon-shopping-cart"></i>
																					</a>																		
																		<?php }	else	{	?>
																					<a href="#" class="btn pull-right">
																						<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																						<i class="icon-shopping-cart"></i>
																					</a>
																		<?php }	 ?>	
																	
																<?php }	 ?>	
																
														<?php }	 ?>	
											<?php }	 ?>											
									<?php }	?>

									<?php	if ( $id_buyer > 0 ) {	?>
												<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
													<?php if($d->price <= 0)	{	?>
														<strong class="price text-center"> </strong>
													<?php }	else	{	?>
														<strong class="price text-center">{!! $d->price_formatted !!}</strong>
													<?php }	?>
												<?php }	?>
									<?php }	else	{	?>
												<?php if($d->price <= 0)	{	?>
													<strong class="price text-center"> </strong>
												<?php }	else	{	?>
													<strong class="price text-center">{!! $d->price_formatted !!}</strong>
												<?php }	?>
									<?php }	?>
									
									<input type="hidden" name="pricedata" value="{{$d->price}}">
									<input type="hidden" name="offer_pricedata" value="{{$d->offer_price}}">
									<input type="hidden" name="domainId" value="{{ $d->id_domain }}">
								</footer>
							</div>
				</article>
		<?php }	else	{	?>
                <article class="isotope-item item item-list col-xs-12 col-sm-6 col-md-4 <?php echo ($d->id_logo > 0 ? 'col-lg-3' : 'col-lg-4'); ?>  {{$class[current($d)]}}" data-date="{{date("F d Y", strtotime($d->reg_date))}}" data-price="{{$d->price}}" data-expires="{{date("F d Y", strtotime($d->expires))}}">
                    <div class="box box-link">
					
							<?php if(  $d->id_logo > 0 ) {	?>
								<!--
								<div class="imgloader">
										<img src="/images/loader1.gif" />
								</div>
								<div class="img-holder" style="display:none;" >
								-->
								<div class="img-holder">
									<!-- picturefill html structure example -->
										<!--<img src="/images/loader.gif" class="loader" />-->
									<!-- <a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}"> -->
									<a href="/{{ $d->domain }}">
										<img src="{{ $url }}image.php?id_logo={{ $d->id_logo }}" alt="Some logo text for SEO" class="logo_img">
									</a>
								</div>
								
							<?php } ?>	
					
                        <!-- <h2><a href="/{{$tab}}/{{ $d->id_domain }}/{{ $d->domain }}">{{ $d->domain }}</a></h2> -->
						<h2>
							<a href="/{{ $d->domain }}">{{ $d->domain }}</a>
							 <!-- <div align="center"><a href="/{{ $d->domain }}">{{ $d->domain }}</a></div>-->
						</h2>
                        <footer class="foot">
							
							<?php if($d->price <= 0)	{	?>
                                <strong class="price text-center"> </strong>
							<?php }	else	{	?>
                                <strong class="price text-center">{{ $d->price_formatted }}</strong>
							<?php }	?>
							
                            <input type="hidden" name="pricedata" value="{{$d->price}}">
							<input type="hidden" name="offer_pricedata" value="{{$d->offer_price}}">
							<input type="hidden" name="domainId" value="{{ $d->id_domain }}">
							<?php if($d->price <= 0)	{	?>
                                    <a href="#" class="btn btn-success pull-right btn-buy">
										<span class="text">{{ trans('messages.Make offer') }}</span>
										<i class="icon-shopping-cart"></i>
                                    </a>
                            <?php }	else	{	?>
									<?php	if ( $id_buyer > 0 ) {	?>
									
												<?php	if ( !in_array($d->id_domain, $order_domainIDs) ) {	?>
														<?php if($d->status == 3)	{	?>
																<a href="#" class="btn pull-right">
																	<span class="text">DROPPED</span>
																	<i class="icon-shopping-cart"></i>
																</a>
														<?php }	else if($d->status == 2)	{	?>
																<a href="#" class="btn pull-right">
																	<span class="text">HIDDEN</span>
																	<i class="icon-shopping-cart"></i>
																</a>
														<?php }	else {	?>
																<!-- <a href="#" class="btn btn-default pull-right btn-buy"> -->
																<a href="/billing/{{ $d->domain }}" class="btn btn-default pull-right">
																	<span class="text">{{ trans('messages.buy now') }}</span>
																	<i class="icon-shopping-cart"></i>
																</a>
														<?php }	?>
												<?php }	else	{	?>
														<?php /*
														<a href="#" class="btn pull-right">
															<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary" onmouseover="viewStatusInfo('<?php echo $order_IPNstatus_Code[$d->id_domain]; ?>', '<?php echo $order_IPNstatus_EscrowUniqueIdentifier[$d->id_domain]; ?>','<?php echo $order_IPNstatus_Description[$d->id_domain]; ?>')" alt="Click to view more info" title="Click to view more info">
																<?php echo $order_domainStatus[$d->id_domain]; ?>
															</span>
															<i class="icon-shopping-cart"></i>
														</a>
														*/ ?>
														<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 || $order_IPNstatus_Code[$d->id_domain] == 85 ) {	?>
																	<a href="#" class="btn pull-right">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																		<i class="icon-shopping-cart"></i>
																	</a>
														<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																	<a href="#" class="btn pull-right">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																		<i class="icon-shopping-cart"></i>
																	</a>																		
														<?php }	else	{	?>
																	<a href="#" class="btn pull-right">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																		<i class="icon-shopping-cart"></i>
																	</a>
														<?php }	 ?>	
													
													
												<?php }	?>
												
									<?php }	else	{	?>
												<?php	if ( !in_array($d->id_domain, $order_domainIDs) || $order_IPNstatus_Code[$d->id_domain] < 1 ) {	?>
														<?php if($d->status == 3)	{	?>
																<a href="#" class="btn pull-right">
																	<span class="text">DROPPED</span>
																	<i class="icon-shopping-cart"></i>
																</a>
														<?php }	else if($d->status == 2)	{	?>
																<a href="#" class="btn pull-right">
																	<span class="text">HIDDEN</span>
																	<i class="icon-shopping-cart"></i>
																</a>
														<?php }	else {	?>
																<!-- <a href="#" class="btn btn-default pull-right btn-buy"> -->
																<a href="/billing/{{ $d->domain }}" class="btn btn-default pull-right">
																	<span class="text">{{ trans('messages.buy now') }}</span>
																	<i class="icon-shopping-cart"></i>
																</a>
														<?php }	?>
												<?php }	else	{	?>
														<?php	if ( $order_IPNstatus_Code[$d->id_domain] == 25 || $order_IPNstatus_Code[$d->id_domain] == 85 ) {	?>
																	<a href="#" class="btn pull-right">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Sold </span>
																		<i class="icon-shopping-cart"></i>
																	</a>
														<?php	}	else if ( $order_IPNstatus_Code[$d->id_domain] == 0 ) {	?>
																	<a href="#" class="btn pull-right">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Cancelled </span>
																		<i class="icon-shopping-cart"></i>
																	</a>																		
														<?php }	else	{	?>
																	<a href="#" class="btn pull-right">
																		<span id="btn_<?php echo $d->id_domain; ?>" class="text-primary"> Pending </span>
																		<i class="icon-shopping-cart"></i>
																	</a>
														<?php }	 ?>
																
												<?php }	?>
												
									<?php }	?>
                            <?php }	?>
							
							<?php if ( $id_buyer > 0 ) {	?>
								<?php if ( !in_array($d->id_domain, $fav_domainIDs) ) {	?>
									<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}"  class="fav btn btn-info pull-right btn-save2">
										<span class="text">{{ trans('messages.save') }}</span>
										<i class="icon-heart-o"></i>
									</a>
								<?php }	else	{	?>
									<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}"  class="fav btn btn-info pull-right btn-save2" style="background: #ff5652 none repeat scroll 0 0; color: #fff;">
										<span class="text" style="display:none;" >{{ trans('messages.save') }}</span>
										<i class="icon-heart-o" style="display:block;" ></i>
									</a>
								<?php }	?>
							<?php }	else	{	?>
								<a id="btn-love-{{ $d->id_domain }}" href="#" data-href="/save/{{ $d->id_domain }}" data-id="{{ $d->id_domain }}"  class="fav btn btn-info pull-right btn-save">
									<span class="text">{{ trans('messages.save') }}</span>
									<i class="icon-heart-o"></i>
								</a>
							<?php }	?>
							
							<?php /*
							<?php if($d->price <= 0)	{	?>
                                <strong class="price text-center"> </strong>
							<?php }	else	{	?>
                                <strong class="price text-center">{{ $d->price_formatted }}</strong>
							<?php }	?>
							*/ ?>
							
                        </footer>
                    </div>
				</article>
			<?php }	?>
		<?php } // end foreach	?>
    <?php }	else	{	?>
		<div align="center"><?php echo $domainData->response_details ; ?></div>
    <?php }	?>
	<div><input type="hidden" id="total_page_search" value="{{$total_page}}"></div>
@if($show_more == true)
<!--<a class="load-more btn btn-primary" href="/more/{{$tab}}/{{ $curr_page }}">load more</a>-->
@endif

<?php }	else	{	?>
	<article><div align="center"><?php echo $domainData->response_details ; ?> <input type="hidden" id="total_page_search" value="{{$total_page}}"></div></article>
<?php }	?>

<?php if( isset($domainData->response_details_main_result) && $domainData->response_details_main_result != '' )	{	?>
	<script>
		$('#sublabel_result').html("<div align='center'><?php echo $domainData->response_details_main_result ; ?></div> <br> <div align='left'>Related Results</div> <br> ");
	</script>
<?php }	?>

