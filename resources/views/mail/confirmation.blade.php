<h2>Hi, Your order enquiry is successfully placed</h2>
<p>Your enquiry for domain - {{$domain}} is placed. We will check and respond to your request in short time.</p>
<h4>Order details -</h4>
<table border="0" >
	<tr>
		<td><b>Domain </b> </td>
		<td><b>-</b> {{$domain}}</td>
	</tr>
	<tr>
		<td><b>Domain price (actual) </b> </td>
		<td><b>-</b> {{$price}}</td>
	</tr>
	<tr>
		<td><b>Proposed offer price </b> </td>
		<td><b>-</b> {{$offer}}</td>
	</tr>
	<tr>
		<td colspan="2" ><b>Included cover message -</b> </td>
	</tr>
</table>
	<p><i>{!! nl2br($text) !!}</i></p>

<br>
<p>Thanks,
<br>BrandEden.com</p>
