<h2>Hi, You have recieved a new domain enquiry</h2>
<br>
<table border="0" >
	<tr>
		<td><b>User email </b> </td>
		<td><b>-</b> {{$email}}</td>
	</tr>
	<tr>
		<td><b>Domain </b> </td>
		<td><b>-</b> {{$domain}}</td>
	</tr>
	<tr>
		<td><b>Domain price (actual) </b> </td>
		<td><b>-</b> {{$price}}</td>
	</tr>
	<tr>
		<td><b>Minimum acceptable offer price </b> </td>
		<td><b>-</b> {{$min_offer}}</td>
	</tr>
	<tr>
		<td><b>Enquiry link </b> </td>
		<td><b>-</b> <a href="http://app.zendomains.com/inquiries/{{$enquiry_id}}">{{$domain}}</a></td>
	</tr>
	<tr>
		<td><b>User's offer price </b> </td>
		<td><b>-</b> {{$offer}}</td>
	</tr>
	<tr>
		<td colspan="2" ><b>User's message </b> </td>
	</tr>
</table>
	<p><i>{!! nl2br($text) !!}</i></p>
<br>
<p>Thanks,
<br>BrandEden.com</p>


