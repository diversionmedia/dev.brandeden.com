@extends('app')
@section('content')
<!-- contain main informative part of the site -->
<main id="main">
<section class="tabs-holder">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 container-holder">
                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">My Account</li>
                </ol>
                <!-- tabs set -->
                <ul class="tabset list-inline">
                    <li><a id="clicktab1" href="#tab1" onclick="getMessages('<?php echo $friendly; ?>')" class="active"><span class="text-holder"><i class="icon message-icon"></i>Messages</span></a></li>
                    <li><a href="#tab2" onclick="getMyFavorites(0)"><span class="text-holder"><i class="icon fav-icon"></i>Favorites</span></a></li>
                    <li><a href="#tab3" onclick="getOrderStatus('<?php echo $friendly; ?>')"><span class="text-holder"><i class="icon status-icon"></i>Order Status</span></a></li>
                    <!--<li><a href="#tab4"><span class="text-holder"><i class="icon history-icon"></i>Order History</span></a></li>-->
                    <li><a href="#tab5" onclick="getMyProfile('<?php echo $buyer_id; ?>')"><span class="text-holder"><i class="icon settig-icon"></i>Account Settings</span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="tab-content-holder">
            <div class="tab-content">
                <div id="tab1">
					<h2>Messages</h2>
					<div class="message-block">
                        <div class="message-area">
                            <div class="heading-bar">
                                <strong class="title">New Messages</strong>
								<a title="Reload New Messages" alt="Reload New Messages" class="setting-icon" onclick="getMessages('<?php echo $friendly; ?>')" style="cursor:pointer;"><i class="fa fa-refresh"></i></a>
                                <span class="number" id="display_unread_message_count_my_account"></span>
                            </div>
							<!--
                            <form action="#" class="search-field">
                                <input type="search" class="form-control" placeholder="Search">
                            </form>
							-->
                            <div class="new-message-area jcf-scrollable1">
                                <ul class="tabset list-unstyled" data-type="ajax" id="msglist">
									<li><div align="center"><img src="/images/loading.gif"></div></li>
                                </ul>
                            </div>
                        </div>
                        <div class="detail-block">
                            <div id="tab6">
								<div class="heading-bar">
									<strong id="buyer_name" class="name"><?php echo $first_name; ?>  <?php echo $last_name; ?></strong>
									<span id="show_loading_bar_latest_message"></span>
									<a alt="Toggle Detail Message" title="Toggle Detail Message" style="cursor:pointer" onclick="$('.name-area').toggle('normal');$('.person-detail-area').toggle('normal');" class="setting-icon"><i class="glyphicon glyphicon-triangle-bottom"></i></a>
									<!--<a class="setting-icon"><i class="icon-gear"></i></a>-->
									<input type="hidden" value="<?php echo $inq_domain; ?>" id="latest_inq_domain">
								</div>
									<form action="#" class="person-info">
									<!--<div id="load_detail_message" align="center"><img src="/images/loading.gif"></div>-->
									<!--<div id="load_detail_message" style="min-height:100px;">-->
									<div id="load_detail_message">
									
										<?php /*
										<div class="name-area">
											<strong id="seller_name" class="name-holder"><a href="#"><i class="icon"></i><span class="name"><?php echo $inq_fullname; ?></span> </a></strong>
											<time datetime="<?php echo $inq_time; ?>"><?php echo $inq_time; ?></time>
										</div>
										<div class="person-detail-area">
											<div class="person-detail">
												<ul class="detail-list list-unstyled">
													<li>
														<strong class="description">Domain:</strong>
														<span class="text"><a href="#"><?php echo $inq_domain; ?></a></span>
													</li>
													<li>
														<strong class="description">Seller Offer:</strong>
														<span class="text"><?php echo $inq_offer; ?></span>
													</li>
													<li>
														<strong class="description">My Offer:</strong>
														<span class="text">Coming Soon</span>
													</li>
													<li>
														<strong class="description">Type:</strong>
														<span class="text brand">Brandable - Coming Soon</span>
													</li>
													<li>
														<strong class="description">Message:</strong>
														<span class="text add"><?php echo $inq_message; ?></span>
													</li>
												</ul>
											</div>
										</div>
										*/ ?>
									
									</div>
									</form>
									<!--
									<ul class="btn-list list-unstyled">
										<li><a href="#" class="btn btn-primary">Accept</a></li>
										<li><a href="#" class="btn add">Counter</a></li>
										<li><a href="#" class="btn add1">Decline</a></li>
									</ul>
									-->

									<!--
									<form action="#" id="form_send_message">
									<div id="load_reply_area">
									</div>
									</form>
									-->

									
									<!--
									<div class="reply-area">
										<div class="field-holder">
											<textarea class="form-control" placeholder="Reply..."></textarea>
											<button type="submit"><i class="icon-paperplane"></i></button>
										</div>
										<div class="file-holder">
											<input type="file">
										</div>
									</div>
									-->

								<div style="border:0px solid #e2e4eb;">
									<div style="border:1px solid #e2e4eb;margin:10px;border-radius:20px;" id="load_all_message_for_domain">
									</div>
								
									<form action="#" id="form_send_message">
									<div id="load_reply_area">
									</div>
									</form>
								
								</div>
									
								<!--
								<div style="border:1px solid #e2e4eb;">
									<div class="heading-bar" style="background:none;box-shadow:none;border-bottom:none;">
										<div>
											<strong class="name">Message History</strong>
											<a class="setting-icon" alt="Reload Message History" title="Reload Message History"><i class="fa fa-refresh"></i></a>
										</div>
									</div>
									<div class="clearfix"></div>
									<div id="load_all_message_for_domain" style="overflow-x:hidden;overflow-y:auto;height:600px;"></div>
								</div>
								-->		
												<!--
												<ul class="tabset list-unstyled" data-type="ajax" >
													<li>
														<a href="#tab9">
															<strong class="name">John Doe </strong>
															<time datetime="2016-02-14 02:50">2:50 PM</time>
															<ul class="info-list list-unstyled">
																<li>
																	<strong class="description">Message:</strong>
																	<span class="text"> Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat...</span>
																</li>
															</ul>
														</a>
													</li>
												</ul>
												-->
											
									
									
								
						
								
								
								
								
								
								
								
								
								

							</div>
                            <div id="tab7" data-url="inc/tab02.html"></div>
                            <div id="tab8" data-url="inc/tab03.html"></div>
                            <div id="tab9" data-url="inc/tab04.html"></div>
                            <div id="tab10" data-url="inc/tab05.html"></div>
                        </div>
                    </div>
                </div>
                <div id="tab2">
                    <h2>Favorites</h2>
                    <form action="inc/filtered-content.html" method="post" class="form-area">
					 </form>
                        <div class="apply-area">
                            <div class="apply-holder">
                                <div class="apply-select">
                                    <select id="bulkdeletefav" id="bulkdeletefav">
										<option value="">Bulk Action</option>
                                        <option value="delete">Delete</option>
                                    </select>
                                </div>
                                <input id="btnapplyFavBulkAction" type="button" class="btn btn-primary" value="Apply" onclick="applyFavBulkAction()">
								<span style="margin-left:50px;" id="loading_bar_favorite"></span>
                            </div>
                            <div class="view-area" style="float:left;margin-left:30px;">
                                <div class="category-select">
                                    
									<!--<select id="domain_tab" onchange="searchByFavoriteTab($(this).val())">-->
									<select id="domain_tab" name="domain_tab" onchange="getMyFavorites(0)">
                                        <option value="all">All</option>
                                        <option value="brandable">Brandable</option>
                                        <option value="premium">Premium</option>
                                        <option value="underprice">Under <?php echo $premiumprice; ?></option>
                                    </select>
                                </div>
                                <div class="view-select">
                                    <select id="view_per_page" name="view_per_page" onchange="getMyFavorites(0)">
                                        <option value="5">View:  5</option>
                                        <option value="10" selected="selected">View:  10</option>
                                        <option value="20">View:  20</option>
                                        <option value="50">View:  50</option>
                                    </select>
                                </div>
                            </div>
							
							<form class="form-search pull-right">
								<!--<label for="search">Advanced Search</label>-->
								<div class="form-holder">
									<input type="search" class="form-control" id="favsearchtxt" name="favsearchtxt" placeholder="Search for domain name">
									<button type="button" class="btn-link" onclick="getMyFavorites(0)"><i class="icon-search"></i></button>
								</div>
							</form>
							
                        </div>
						<div id="result_fav_domains_message_main"></div>
						<div class="clearfix"></div>
                        <!--<div class="table-area">-->
							<div id="load_favorites"><div align="center"><img src="/images/loading.gif"></div></div>
                        <!--</div>-->
                   
                </div>
                <div id="tab3">
                    <form action="inc/filtered-content2.html" method="post" class="form-area">
                        <div class="heading-area">
                            <h2>Order Status</h2>
                            <div class="view-area">
                                <div class="view-select">
                                    <select>
                                        <option>View:  10 </option>
                                        <option>View:  10 </option>
                                        <option>View:  10 </option>
                                        <option>View:  10 </option>
                                    </select>
                                </div>
                                <div class="data-picker-area">
                                    <input type="text" placeholder="Start" data-datepicker="">
                                </div>
                                <div class="data-picker-area">
                                    <input type="text" placeholder="End" data-datepicker="">
                                </div>
                                <div class="month-select">
                                    <select>
                                        <option>Month to Date</option>
                                        <option>Week to Date</option>
                                        <option>Last week</option>
                                        <option>Last Year</option>
                                        <option>Custom</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">GO</button>
                            </div>
                        </div>
						<div id="load_order_status">
							<div align="center"><img src="/images/loading.gif"></div>
						</div>
                    </form>
                </div>
                <div id="tab4">
					<!--
                    <form action="inc/filtered-content3.html" method="post" class="form-area">
                        <div class="heading-area">
                            <h2>Order History</h2>
                            <div class="view-area">
                                <div class="view-select">
                                    <select>
                                        <option>View:  10 </option>
                                        <option>View:  10 </option>
                                        <option>View:  10 </option>
                                        <option>View:  10 </option>
                                    </select>
                                </div>
                                <div class="data-picker-area">
                                    <input type="text" placeholder="Start" data-datepicker="">
                                </div>
                                <div class="data-picker-area">
                                    <input type="text" placeholder="End" data-datepicker="">
                                </div>
                                <div class="month-select">
                                    <select>
                                        <option>Month to Date</option>
                                        <option>Week to Date</option>
                                        <option>Last week</option>
                                        <option>Last Year</option>
                                        <option>Custom</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">GO</button>
                            </div>
                        </div>
                        <div class="table-area">
                            <table class="table-holder">
                                <thead>
                                    <tr>
                                        <th class="col9">Date</th>
                                        <th class="col10">#Reciept</th>
                                        <th class="col11">Domain Name</th>
                                        <th class="col13">Details</th>
                                        <th class="col12">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="col9"><time datetime="2016-12-31">31-12-2016</time></td>
                                        <td class="col9">#23659945</td>
                                        <td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
                                        <td class="col13">Lorem ipsum dolor sit amet.</td>
                                        <td class="col12">$8888888</td>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="pagniation-holder">
                            <ul class="pagniation list-unstyled">
                                <li class="active">1</li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                            </ul>
                            <div class="btn-holder">
                                <input type="file">
                                <a href="#" class="print-btn"><i class="icon-print"></i>Print</a>
                            </div>
                        </div>
                    </form>
					-->
                </div>
                <div id="tab5">
				
					<div id="change_pass_section" style="display: none;">
						<form class="form-area" id="frmChangePassword">
							
							<div class="account-form-holder"><div class="acount-area"><div class="account-info same-height-left" style="width:100%;">
							
								<div id="result_change_password_message"></div> 
								<div class="heading">
									<h2>
										Change Password 
										<span id="loading_bar_change_password"></span> 
										<span onclick="$('#change_pass_section').hide('slow');" style="float:right;" class="btn btn-default btn-sm"> <i class="icon-close"></i> Close </span>
									</h2>
								</div>
								<div class="fields-area" >
									<div class="field-holder">
										<label>New Password</label>
										<div class="holder">
											<input type="hidden" name="id_user" id="id_user" value="<?php echo $buyer_id; ?>" />
											<input type="hidden" name="action" id="action" value="update_account" />
											<input type="password" class="form-control" id="password" name="password">
										</div>
									</div>
									<div class="field-holder">
											<label style="color:#ccc;">&nbsp;</label>
											<div class="holder">
												<div style="display:none;float:left;">
													Strength:
													<div class="figure" id="strength_human"></div>    
													(<div class="figure" id="strength_score">0</div>)
												</div>
												
												<div style="color:#ccc;float:left;margin-right:20px;">Strength:</div>
												<div class="progress" style="float:left;width: 60%;">
												  <div id="progressbar_1" class="progress-bar progress-bar-danger " role="progressbar"
												  aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%;display:none;">
													Very Weak
												  </div>
												  <div id="progressbar_2" class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar"
												  aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%;display:none;">
													Weak
												  </div>
												  <div id="progressbar_3" class="progress-bar progress-bar-info progress-bar-striped" role="progressbar"
												  aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%;display:none;">
													Good
												  </div>
												  <div id="progressbar_4" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
												  aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width:25%;display:none;">
													Strong
												  </div>
												</div>
											</div>
									</div>
									<div class="field-holder">
										<label>Confirm Password</label>
										<div class="holder">
											<input type="password" class="form-control" id="cnewpassword" name="cnewpassword">
										</div>
									</div>
									<button type="button" id="btnSaveNewPassword" onclick="saveNewPassword()" class="btn btn-primary btn-sm"> <i class="icon-checkmark"></i> Save </button>
									<span onclick="$('#change_pass_section').hide('slow');" class="btn btn-primary btn-sm"> <i class="icon-close"></i> Close </span>
								</div>
							</div></div></div>
						</form>
					</div>
				
					<div id="upload_avatar_section" style="display: none;">
						<form class="form-area" id="frmChangePassword">
							
							<div class="account-form-holder"><div class="acount-area"><div class="account-info same-height-left" style="width:100%;">
							
								<div id="result_change_password_message"></div> 
								<div class="heading">
									<h2>
										Upload Avatar 
										<span id="loading_bar_change_password"></span> 
										<span onclick="$('#upload_avatar_section').hide('slow');" style="float:right;" class="btn btn-default btn-sm"> <i class="icon-close"></i> Close </span>
									</h2>
								</div>
								<div class="fields-area" >
									<div class="field-holder">
										<div class="holder">
										
										<form method="post" action="" enctype="multipart/form-data">
											<div><input type="file" name="file" id="uploadAvatarInput" /></div>
											<div id="loading_image_upload_avatar"></div>
											<div id="avatar_image">
												<img src="<?php echo config('app.API_URL') ?>/image.php?id_user=<?php echo $_SESSION['id_buyer']; ?>&width=150&height=150" >
											</div>
										</form>
											
										</div>
									</div>
									<span onclick="$('#upload_avatar_section').hide('slow');" class="btn btn-primary btn-sm"> <i class="icon-close"></i> Close </span>
								</div>
							</div></div></div>
						</form>
					</div>
				
                    <form action="#" class="form-area" id="form-account-area">
                        <div class="account-form-holder">
                            <div class="acount-area">
								<div id="result_account_settings_message"></div>
                                <div class="account-info">
                                    <div class="heading">
                                        <h2>
											Account Information 
											<span id="loading_bar_accnt_info"></span> 
											<span onclick="$('#change_pass_section').show('slow');" style="float:right;" class="btn btn-default btn-sm">Change Password</span> 
											<span onclick="$('#upload_avatar_section').show('slow');" style="float:right;margin-right:5px;" class="btn btn-default btn-sm">Upload Avatar</span> 
										</h2>
                                    </div>
                                    <div class="fields-area">
									
                                        <div class="fields-block">
                                            <div class="field-holder">
                                                <label for="name">First Name</label>
                                                <div class="holder">
                                                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="<?php echo $first_name; ?>" value="<?php echo $first_name; ?>">
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="name1">Last Name</label>
                                                <div class="holder">
                                                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="<?php echo $last_name; ?>" value="<?php echo $last_name; ?>">
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="organization">Organization</label>
                                                <div class="holder">
                                                    <input type="text" name="organization" id="organization" class="form-control" placeholder="ABC Multimedia">
                                                </div>
                                            </div>
											<!--
					<input class="validate[required]" type="text" id="phonenum" name="phonenum" placeholder="Phone 10 digit #">
					<input class="validate[required]" type="text" id="address1" name="address1" placeholder="Address">
					<input class="validate[required]" type="text" id="city" name="city" placeholder="City" >
					<input class="validate[required]" type="text" id="postalCode" name="postalCode" placeholder="5 digit zip code">
					<select id="state" class="validate[required]" name="state">
					</select>
					<select class="field_input" id="country" name="country">
						<option value="ca">Canada</option>
						<option value="us">United States</option>
					</select>											
											-->
											
                                            <div class="field-holder">
                                                <label for="country">Country/Region</label>
                                                <div class="holder">
													<input type="hidden" name="hiddenCountry" id="hiddenCountry">
                                                    <!--<select name="country" id="country" class="no-shadow"></select>-->
                                                    <select name="country" id="country" class="form-control"></select>
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="state">State</label>
                                                <div class="holder">
													<input type="hidden" name="hiddenState" id="hiddenState">
													<!--<select name="state" id="state" class="no-shadow"></select>-->
													<select name="state" id="state" class="form-control"></select>
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="zip">Zip/Postal</label>
                                                <div class="holder">
                                                    <input class="form-control" type="text" name="zip" id="zip"  placeholder="Enter Zip">
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="city">City</label>
                                                <div class="holder">
													<input type="text" name="city" id="city" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="work">Work Phone</label>
                                                <div class="holder">
                                                    <input type="tel" name="workphone" id="workphone" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fields-block">
                                            <div class="field-holder">
                                                <label for="phone">Home Phone</label>
                                                <div class="holder">
                                                    <input type="tel" name="homephone" id="homephone" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="mobile">Mobile Phone</label>
                                                <div class="holder">
                                                    <input type="tel" name="mobilephone" id="mobilephone" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="fax">Fax</label>
                                                <div class="holder">
                                                    <input type="tel" name="fax" id="fax" class="form-control" placeholder="">
                                                </div>
                                            </div>
                                            <div class="field-holder">
                                                <label for="Birthdate">Birthdate</label>
                                                <div class="holder">
                                                    <input type="text" name="birthdate" id="birthdate" class="form-control" placeholder="DD/MM/YY" data-datepicker="">
                                                </div>
                                            </div>
                                            <div class="field-holder add1">
                                                <strong class="title">Gender</strong>
                                                <div class="holder">
                                                    <ul class="checkbox-list list-unstyled">
                                                        <li>
                                                            <div class="checkbox-holder">
                                                                <label>
                                                                    <input type="radio" name="gender" id="gender_1" value="1">
                                                                    <span class="fake-input"></span>
                                                                    Male
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="checkbox-holder">
                                                                <label>
                                                                    <input type="radio" name="gender" id="gender_2" value="2">
                                                                    <span class="fake-input"></span>
                                                                    Female
                                                                </label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="field-holder add">
                                                <strong class="title">Account Type</strong>
                                                <div class="holder">
                                                    <ul class="checkbox-list list-unstyled">
                                                        <li>
                                                            <div class="checkbox-holder">
                                                                <label>
                                                                    <input type="radio" name="membership_type" id="membership_type_1" value="1">
                                                                    <span class="fake-input"></span>
                                                                    Basic
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="checkbox-holder">
                                                                <label>
                                                                    <input type="radio"name="membership_type" id="membership_type_2" value="2">
                                                                    <span class="fake-input"></span>
                                                                    Standard
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="checkbox-holder">
                                                                <label>
                                                                    <input type="radio" name="membership_type" id="membership_type_3" value="3">
                                                                    <span class="fake-input"></span>
                                                                    Premium
                                                                </label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="field-holder add">
                                                <strong class="title">Currency Preference</strong>
                                                <div class="holder">
                                                    <ul class="checkbox-list list-unstyled">
                                                        <li>
                                                            <div class="checkbox-holder">
                                                                <label>
                                                                    <input type="radio" name="currency_pref" id="currency_pref_1" value="1">
                                                                    <span class="fake-input"></span>
                                                                    USD
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="checkbox-holder">
                                                                <label>
                                                                    <input type="radio" name="currency_pref" id="currency_pref_2" value="2">
                                                                    <span class="fake-input"></span>
                                                                    EUR
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="checkbox-holder">
                                                                <label>
                                                                    <input type="radio" name="currency_pref" id="currency_pref_3" value="3">
                                                                    <span class="fake-input"></span>
                                                                    GBP
                                                                </label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-info">
                                    <div class="heading">
                                        <h2>Contact Preferences</h2>
                                    </div>
                                    <div class="checkbox-farme">
                                        <div class="checkbox-block">
                                            <strong class="title">Monthly Account Summary:</strong>
                                            <ul class="checkbox-list list-unstyled">
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="monthly_summary" id="monthly_summary_1" value="1">
                                                            <span class="fake-input"></span>
                                                            Yes
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="monthly_summary" id="monthly_summary_0" value="0">
                                                            <span class="fake-input"></span>
                                                            No
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="checkbox-block">
                                            <strong class="title">Promotional Emails: </strong>
                                            <ul class="checkbox-list list-unstyled">
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="promotional_emails" id="promotional_emails_1" value="1">
                                                            <span class="fake-input"></span>
                                                            Yes
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="promotional_emails" id="promotional_emails_0" value="0">
                                                            <span class="fake-input"></span>
                                                            No
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="checkbox-block">
                                            <strong class="title">Offers related to purchases: </strong>
                                            <ul class="checkbox-list list-unstyled">
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="offer_rel_purchase" id="offer_rel_purchase_1" value="1">
                                                            <span class="fake-input"></span>
                                                            Yes
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="offer_rel_purchase" id="offer_rel_purchase_0" value="0">
                                                            <span class="fake-input"></span>
                                                            No
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="checkbox-block">
                                            <strong class="title">Contact via US Mail for products, services, or special</strong>
                                            <ul class="checkbox-list list-unstyled">
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="contact_via_us_mail" id="contact_via_us_mail_1" value="1">
                                                            <span class="fake-input"></span>
                                                            Yes
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="contact_via_us_mail" id="contact_via_us_mail_0" value="0">
                                                            <span class="fake-input"></span>
                                                            No
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="checkbox-block">
                                            <strong class="title">Email Preference:</strong>
                                            <ul class="checkbox-list list-unstyled">
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="email_pref" id="email_pref_0" value="0">
                                                            <span class="fake-input"></span>
                                                            HTML
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="email_pref" id="email_pref_1" value="1">
                                                            <span class="fake-input"></span>
                                                            Plain Text
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="checkbox-block">
                                            <strong class="title">Language Preferences:</strong>
                                            <ul class="checkbox-list list-unstyled">
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="lang_pref" id="lang_pref_0"  value="0">
                                                            <span class="fake-input"></span>
                                                            ENG US
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="lang_pref" id="lang_pref_1"  value="1">
                                                            <span class="fake-input"></span>
                                                            ENG UK
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="lang_pref" id="lang_pref_2"  value="2">
                                                            <span class="fake-input"></span>
                                                            Chinese
                                                        </label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="checkbox-holder">
                                                        <label>
                                                            <input type="radio" name="lang_pref" id="lang_pref_3"  value="3">
                                                            <span class="fake-input"></span>
                                                            Japanese
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<span id="loading_bar_accnt_info_bottom"></span>
                            <input id="btnsaveAccountSettings" type="button" class="btn btn-primary" value="Save" onclick="saveAccountSettings()">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
<script>

var api_url = '<?php echo config('app.API_URL') ?>';
var id_buyer = '<?php echo $_SESSION['id_buyer']; ?>';

$( document ).ready(function() {
	
	var api_url = '<?php echo config('app.API_URL') ?>';
	var id_buyer = '<?php echo $_SESSION['id_buyer']; ?>';
	
	$('#clicktab1').click();
	
	$("#password").on("keypress keyup keydown", function() {
        var pass = $(this).val();
        $("#strength_human").text(checkPassStrength(pass));
        $("#strength_score").text(scorePassword(pass));
		
		if( $("#strength_human").text() == 'very weak' )	{
			$("#progressbar_1").show('slow');
			$("#progressbar_2").hide();
			$("#progressbar_3").hide();
			$("#progressbar_4").hide();
		}
		if( $("#strength_human").text() == 'weak' )	{
			$("#progressbar_1").show('slow');
			$("#progressbar_2").show('slow');
			$("#progressbar_3").hide();
			$("#progressbar_4").hide();
		}

		if( $("#strength_human").text() == 'good' )	{
			$("#progressbar_1").show('slow');
			$("#progressbar_2").show('slow');
			$("#progressbar_3").show('slow');
			$("#progressbar_4").hide();
		}

		if( $("#strength_human").text() == 'strong' )	{
			$("#progressbar_1").show('slow');
			$("#progressbar_2").show('slow');
			$("#progressbar_3").show('slow');
			$("#progressbar_4").show('slow');
		}
		
		if( $("#strength_human").text() == 'weak' || $("#strength_human").text() == 'very weak' )	{
			$('#password').attr('style','border-color:#d9534f;');
			$('#btnSaveNewPassword').attr('disabled','disabled');
		}
		else	{
			$('#password').attr('style','border-color:#5bb402;');
			$('#btnSaveNewPassword').removeAttr('disabled');
		}
    });

});	
</script>
<style>

.unreadmessage_marker {
    background: #ff5555 none repeat scroll 0 0;
    border-radius: 0 4px 4px 0;
    color: #fff;
    display: inline-block;
    font-size: 10px;
    font-weight: 400;
    height: 20px;
    line-height: 13px;
    margin: 0 0 0 20px;
    padding: 3px 5px 1px;
    position: relative;
    text-transform: uppercase;
    vertical-align: middle;
}
.unreadmessage_marker::after {
    border-color: transparent #ff5555 transparent transparent;
    border-style: solid;
    border-width: 10px 10px 10px 0;
    content: "";
    left: -10px;
    position: absolute;
    top: 0;
}

/* chat */
    .conversation-wrap
    {
        box-shadow: -2px 0 3px #ddd;
        padding:0;
        max-height: 400px;
        overflow: auto;
    }
    .conversation
    {
        padding:5px;
        border-bottom:1px solid #ddd;
        margin:0;

    }

    .message-wrap
    {
        /* box-shadow: 0 0 3px #ddd; */
        padding:0;

    }
    .msg
    {
        padding:5px;
        /*border-bottom:1px solid #ddd;*/
        margin:0;
    }
    .msg-wrap
    {
        padding:10px;
        max-height: 450px;
        overflow: auto;

    }

    .time
    {
        color:#bfbfbf;
    }

    .send-wrap
    {
        border-top: 1px solid #eee;
        border-bottom: 1px solid #eee;
        padding:10px;
        /*background: #f8f8f8;*/
    }

    .send-message
    {
        resize: none;
    }

    .highlight
    {
        background-color: #f7f7f9;
        border: 1px solid #e1e1e8;
    }

    .send-message-btn
    {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-left-radius: 0;

        border-bottom-right-radius: 0;
    }
    .btn-panel
    {
        background: #f7f7f9;
    }

    .btn-panel .btn
    {
        color:#b8b8b8;

        transition: 0.2s all ease-in-out;
    }

    .btn-panel .btn:hover
    {
        color:#666;
        background: #f8f8f8;
    }
    .btn-panel .btn:active
    {
        background: #f8f8f8;
        box-shadow: 0 0 1px #ddd;
    }

    .btn-panel-conversation .btn,.btn-panel-msg .btn
    {

        background: #f8f8f8;
    }
    .btn-panel-conversation .btn:first-child
    {
        border-right: 1px solid #ddd;
    }

    .msg-wrap .media-heading
    {
        color:#55a119;
        font-weight: 700;
    }


    .msg-date
    {
        background: none;
        text-align: center;
        color:#aaa;
        border:none;
        box-shadow: none;
        border-bottom: 1px solid #ddd;
    }


    body::-webkit-scrollbar {
        width: 12px;
    }
 
    
    /* Let's get this party started */
    ::-webkit-scrollbar {
        width: 6px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); 
/*        -webkit-border-radius: 10px;
        border-radius: 10px;*/
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
/*        -webkit-border-radius: 10px;
        border-radius: 10px;*/
        background:#ddd; 
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
    }
    ::-webkit-scrollbar-thumb:window-inactive {
        background: #ddd; 
    }

	/*
	.media-body, .msg-wrap .media-heading	{
		font-size: 14px !important;
	}
	*/
</style>
</main>
@stop