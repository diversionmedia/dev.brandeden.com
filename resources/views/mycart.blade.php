@extends('cartheader')
@section('content')
<!-- contain main informative part of the site -->
			<main id="main">
				<section class="tabs-holder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 container-holder add">
								<!-- breadcrumb -->
								<ol class="breadcrumb">
									<li><a href="/">Home</a></li>
									<li class="active">My Cart</li>
								</ol>
								<!-- tabs set -->
								<ul class="tabset cart-tabs list-inline">
									<li><a href="#" class="active"><span class="text-holder"><i class="icon cart-icon"></i>Review Shopping</span></a></li>
									<li><a href="/billing"><span class="text-holder"><i class="icon dollar-icon"></i>Billing &amp; Payment</span></a></li>
									<li><a href="/orderconfirm"><span class="text-holder"><i class="icon check-icon"></i>Order Confirmation</span></a></li>
								</ul>
							</div>
						</div>
						<form class="form-wrap" action="/billing" method="post">
							<div class="tab-content-holder add">
								<div class="tab-content">
									<div id="tab20">
										<div class="row">
											<div class="col-md-8 col-xs-12 wrap-table">
												<div class="table-area">
													<table class="table-holder sec">
														<thead>
															<tr class="gray">
																<th class="col1">
																	<div class="checkbox-holder">
																		<label>
																			<input type="checkbox" data-check-pattern="[name^='some-key']" >
																			<span class="fake-input"></span>
																			Domain Names
																		</label>
																	</div>
																</th>
																<th class="col2">Price</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class="col1">
																	<div class="checkbox-holder">
																		<label>
																			<input type="checkbox" name="some-key2">
																			<span class="fake-input"></span>
																			CustomerDevelopmentMethodology.com
																		</label>
																	</div>
																</td>
																<td class="col2"><span class="text-digit">$8,888,8888</span>
																	<div class="del-area">
																		<a href="#" class="del-icon"><i class="icon-bin"></i></a>
																	</div>
																</td>
															</tr>
															<tr>
																<td class="col1">
																	<div class="checkbox-holder">
																		<label>
																			<input type="checkbox" name="some-key2">
																			<span class="fake-input"></span>
																			CustomerDevelopmentMethodology.com
																		</label>
																	</div>
																</td>
																<td class="col2"><span class="text-digit">$8,888,8888</span>
																	<div class="del-area">
																		<a href="#" class="del-icon"><i class="icon-bin"></i></a>
																	</div>
																</td>
															</tr>
															<tr>
																<td class="col1">
																	<div class="checkbox-holder">
																		<label>
																			<input type="checkbox" name="some-key2">
																			<span class="fake-input"></span>
																			CustomerDevelopmentMethodology.com
																		</label>
																	</div>
																</td>
																<td class="col2"><span class="text-digit">$8,888,8888</span>
																	<div class="del-area">
																		<a href="#" class="del-icon"><i class="icon-bin"></i></a>
																	</div>
																</td>
															</tr>
															<tr>
																<td class="col1">
																	<div class="checkbox-holder">
																		<label>
																			<input type="checkbox" name="some-key2">
																			<span class="fake-input"></span>
																			CustomerDevelopmentMethodology.com
																		</label>
																	</div>
																</td>
																<td class="col2"><span class="text-digit">$8,888,8888</span>
																	<div class="del-area">
																		<a href="#" class="del-icon"><i class="icon-bin"></i></a>
																	</div>
																</td>
															</tr>
															<tr>
																<td class="col1">
																	<div class="checkbox-holder">
																		<label>
																			<input type="checkbox" name="some-key2">
																			<span class="fake-input"></span>
																			CustomerDevelopmentMethodology.com
																		</label>
																	</div>
																</td>
																<td class="col2"><span class="text-digit">$8,888,8888</span>
																	<div class="del-area">
																		<a href="#" class="del-icon"><i class="icon-bin"></i></a>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<a class="continue" href="/">Continue Shopping</a>
											</div>
											<!--widget-block-->
											<div class="col-md-4 col-xs-12 widget-block">
												<!--widget-->
												<div class="widget">
													<div class="widget-holder">
														<div class="counter-holder">
															<div class="counting-text">
																<div class="counting-row">
																	<span class="sub-title">Sub Total</span>
																	<span class="digits">$8,888,888</span>
																</div>
																<div class="counting-row">
																	<span class="sub-title">Tax (10%)</span>
																	<span class="digits">$888</span>
																</div>
															</div>
															<div class="total-holder">
																<span class="sub-title">Total</span>
																<span class="total-digits">$8,888,888</span>
															</div>
														</div>
														<ul class="domain-info list-unstyled">
															<li>The domain is delivered instantly upon purchase.</li>
															<li>Once purchased, the domain is your property.</li>
															<li>You may transfer the domain to any registrar at any time.</li>
														</ul>
													</div>
													<button type="submit" class="btn btn-primary">PROCEED TO PAYMENT</button>
												</div>
												<h2>Faqs</h2>
												<!--widget-->
												<div class="widget">
													<div class="widget-holder">
														<div class="open-close">
															<a class="slide-openar" href="#">How to buy a domain.</a>
															<div class="slide">
																<p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
															</div>
														</div>
														<div class="open-close">
															<a class="slide-openar" href="#">How to Place Order</a>
															<div class="slide">
																<p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
															</div>
														</div>
														<div class="open-close active">
															<a class="slide-openar" href="#">How to Transfer a Domain Name</a>
															<div class="slide">
																<p>consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam</p>
															</div>
														</div>
													</div>
													<div class="btn-wrap">
														<a class="btn btn-default" href="#">More Faqs</a>
														<a class="btn btn-default" href="#">Contact us</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</section>
			</main>
@stop