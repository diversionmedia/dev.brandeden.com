@extends('cartheader')
@section('content')
<!-- contain main informative part of the site -->
			<main id="main">
				<section class="tabs-holder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 container-holder add">
								<!-- breadcrumb -->
								<ol class="breadcrumb">
									<li><a >Home</a></li>
									<li class="active">My Cart</li>
								</ol>
								<!-- tabs set -->
								<ul class="tabset cart-tabs list-inline">
									<li><a href="/mycart" class="done"><span class="text-holder"><i class="icon cart-icon"></i>Review Shopping</span></a></li>
									<li><a href="/billing" class="done"><span class="text-holder"><i class="icon dollar-icon"></i>Billing &amp; Payment</span></a></li>
									<li><a href="#" class="active"><span class="text-holder"><i class="icon check-icon"></i>Order Confirmation</span></a></li>
								</ul>
							</div>
						</div>
						<form class="form-wrap" action="#">
							<div class="tab-content-holder add">
								<div class="tab-content">
									<div id="tab22">
										<div class="row">
											<div class="col-xs-12">
												<div class="payment-holder">
													<div class="confirmation-box">
														<div class="icon-holder">
															<i class="icon-checked">
																<img src="images/icon-checked.png" alt="image description">
															</i>
														</div>
														<strong class="payment-title">Payment Confirmed</strong>
														<p>Thanks you, your payment has been confirmed. A confirmation email has been sent to you. </p>
													</div>
													<div class="table-block">
														<table class="table-holder thrd">
															<thead>
																<tr>
																	<th class="th-heading" colspan="3">Order Summary</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td class="col9">#23659945</td>
																	<td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
																	<td class="col12">$8888888</td>
																	
																</tr>
																<tr>
																	<td class="col9">#23659945</td>
																	<td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
																	<td class="col12">$8888888</td>
																	
																</tr>
																<tr>
																	<td class="col9">#23659945</td>
																	<td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
																	<td class="col12">$8888888</td>
																	
																</tr>
																<tr>
																	<td class="col9">#23659945</td>
																	<td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
																	<td class="col12">$8888888</td>
																	
																</tr>
																<tr>
																	<td class="col9">#23659945</td>
																	<td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
																	<td class="col12">$8888888</td>
																	
																</tr>
																<tr>
																	<td class="col9">#23659945</td>
																	<td class="col11"><a href="#">CustomerDevelopmentMethodology.com</a></td>
																	<td class="col12">$8888888</td>
																	
																</tr>
															</tbody>
														</table>
														<div class="calculation-holder">
															<dl>
																<dt>Card Holder Name</dt>
																<dd>John Doe</dd>
																<dt>Payment Type</dt>
																<dd>VISA  **** ****  ****  2345</dd>
																<dt>Payment Date</dt>
																<dd>5/01/2016  at 2:05PM</dd>
															</dl>
															<ul class="amount-detail">
																<li>
																	<span class="amount-title">Sub Total</span>
																	<span class="amount">$8,888,888</span>
																</li>
																<li>
																	<span class="amount-title">Tax (10%)</span>
																	<span class="amount">$888</span>
																</li>
																<li>
																	<span class="amount-title">Total</span>
																	<span class="total-amount">$8,888,888</span>
																</li>
															</ul>
														</div>
													</div>
													<div class="pagniation-holder">
														<a class="back" href="/">Back to Homepage</a>
														<div class="btn-holder">
															<input type="file">
															<a href="#" class="print-btn"><i class="icon-print"></i>Print</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</section>
			</main>
@stop