@extends('cartheader')
@section('content')
<!-- contain main informative part of the site -->
			<main id="main">
				<section class="tabs-holder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 container-holder add">
								<!--
								<ol class="breadcrumb">
									<li><a >Home</a></li>
									<li class="active">My Cart</li>
								</ol>
								
								<ul class="tabset cart-tabs list-inline">
									<li><a href="/mycart" class="done"><span class="text-holder"><i class="icon cart-icon"></i>Review Shopping</span></a></li>
									<li><a href="/billing" class="done"><span class="text-holder"><i class="icon dollar-icon"></i>Billing &amp; Payment</span></a></li>
									<li><a href="#" class="active"><span class="text-holder"><i class="icon check-icon"></i>Order Confirmation</span></a></li>
								</ul>
								-->
							</div>
						</div>
						<form class="form-wrap" action="#">
							<div class="tab-content-holder add">
								<div class="tab-content">
									<div id="tab22">
										<div class="row">
											<div class="col-xs-12">
												<div class="payment-holder">
													<div class="confirmation-box">
														<div class="icon-holder">
															<i class="icon-checked">
																<img src="images/icon-checked.png" alt="image description">
															</i>
														</div>
														<strong class="payment-title">Payment Received</strong>
														<p>Thank you, your payment has been received. </p>
													</div>
													<div class="table-block">
														<table class="table-holder thrd">
															<thead>
																<tr>
																	<th class="th-heading" colspan="3">Order Summary</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td class="col9" id="id_domain"></td>
																	<td class="col11"><a id="domain_name"></a></td>
																	<td class="col12" id="price"></td>
																	
																</tr>
															</tbody>
														</table>
														<div class="calculation-holder">
															<dl id="payer_section">
																<!--
																<dt>Card Holder Name</dt>
																<dd>John Doe</dd>
																<dt>Payment Type</dt>
																<dd>VISA  **** ****  ****  2345</dd>
																<dt>Payment Date</dt>
																<dd>5/01/2016  at 2:05PM</dd>
																-->
															</dl>
															<ul class="amount-detail">
																<li>
																	<span class="amount-title">Sub Total</span>
																	<span class="amount" id="sub_total"></span>
																</li>
																<li>
																	<span class="amount-title">Tax (0%)</span>
																	<span class="amount">0</span>
																</li>
																<li>
																	<span class="amount-title">Total</span>
																	<span class="total-amount" id="total-amount"></span>
																</li>
															</ul>
														</div>
													</div>
													<div class="pagniation-holder">
														<a class="back" href="/">Back to Homepage</a>
														<div class="btn-holder">
															<!-- <input type="file"> -->
															<a id="link_export_payment" download style="margin-right:5px;" class="btn btn-primary"> <i class="glyphicon glyphicon-download-alt"></i> Download</a>
															<a id="link_print_payment" target="_blank" class="btn btn-primary"> <i class="icon-print"></i> Print </a>
															<!-- <a href="#" class="print-btn"><i class="icon-print"></i>Print</a> -->
															<span id="btnreload" class="print-btn hidden" onclick="getPaymentInfo('<?php echo $_GET['state']; ?>','<?php echo $_GET['paymentId']; ?>','<?php echo $_GET['method']; ?>');">Reload</span>
															
															<div class="hidden">
																<a href="" class="hidden"  download>Export</a>
																<a href="" class="hidden"  target="_blank">Print</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</section>
				
<script>
$(document).ready(function(){
	getPaymentInfo('<?php echo $_GET['state']; ?>','<?php echo $_GET['paymentId']; ?>','<?php echo $_GET['method']; ?>');
});
</script>
<style>
.table-block .calculation-holder dl	{
	width: 555px;
}
.table-block .calculation-holder dl dd	{
	font: 200 12px/8px "Montserrat",Arial,Helvetica,sans-serif !important;
}
</style>
			</main>
@stop