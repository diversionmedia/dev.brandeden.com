						<?php 
						/*
						echo '<pre>';
						print_r($orders);
						echo '</pre>';
						*/
						?>
						
						<div class="table-area">
                            <table class="table-holder add">
                                <thead>
                                    <tr>
                                        <th class="col4">Domain Name</th>
                                        <th class="col6">Order Date</th>
                                        <!--<th class="col5">Order Expires</th>-->
                                        <th class="col44">Transaction #</th>
                                        <th class="col6">Payment Type</th>
										<th class="col7">Status</th>
										<th class="col66">Invoice</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php 
								if( count($orders) > 0 )	{
									foreach($orders as $order)	{	
										$IPNstatus = json_decode($order->IPNstatus);
										if($IPNstatus)	{
											$IPNstatus_EscrowUniqueIdentifier = $IPNstatus->EscrowUniqueIdentifier;
											$IPNstatus_Description = $IPNstatus->Description;
											$IPNstatus_Code = $IPNstatus->Code;
											
										}
										else	{
											$IPNstatus_EscrowUniqueIdentifier = '';
											$IPNstatus_Description = '';
											$IPNstatus_Code = '';
										}
										
										$display_status = $IPNstatus_Description;
										$bg_status = '';
										$tooltip = '';
										$style = 'style="cursor:pointer;"';
										if( $IPNstatus_Code == 0 )	{
											$display_status = 'Cancelled';
											//$bg_status = 'bg-danger';
											//$style = 'style="cursor:pointer;background-color:#d9534f;color:#fff;"';
											$style = 'style="cursor:pointer;"';
										}
										if( $IPNstatus_Code == 5 )	{
											$display_status = 'Pending';
											//$bg_status = 'bg-warning';
											//$style = 'style="cursor:pointer;background-color:#f0ad4e;color:#fff;"';
										}
										if( $IPNstatus_Code == 15 )	{
											$display_status = 'Pay Now !';
											$bg_status = 'text-danger';
											$style = 'style="cursor:pointer;background-color:#d9534f;color:#fff;"';
											//$style = 'style="cursor:pointer;"';
											$tooltip = ' title="Your Attention is Required!"';
											//$tooltip = 'data-toggle="tooltip" data-placement="top" title="Your Attention is Required!"';
										}
										if( $IPNstatus_Code == 20 )	{
											$display_status = 'Payment Submitted';
											//$bg_status = 'bg-primary';
											//$style = 'style="cursor:pointer;background-color:#428bca;color:#fff;"';
											$style = 'style="cursor:pointer;"';
										}
										if( $IPNstatus_Code == 25 )	{
											$display_status = 'Payment Accepted <i class="glyphicon glyphicon-thumbs-up"></i>';
											$bg_status = 'text-success';
											//$bg_status = 'bg-success';
											//$style = 'style="cursor:pointer;background-color:#5bb402;color:#fff;"';
											$style = 'style="cursor:pointer;"';
										}
										if( $IPNstatus_Code == 85 )	{
											$display_status = 'Transaction Completed <i class="glyphicon glyphicon-ok"></i>';
											$bg_status = 'text-success';
											//$bg_status = 'bg-success';
											//$style = 'style="cursor:pointer;background-color:#5bb402;color:#fff;"';
											$style = 'style="cursor:pointer;"';
										}
										?>
										<tr>
											<td class="col4"><a href="#"><?php echo $order->domain; ?></a></td>
											<td class="col6"><time datetime="<?php echo date("F d Y H:i:s", $order->ordertimestamp); ?>"><?php echo date("F d, Y", $order->ordertimestamp); ?></time></td>
											<!--<td class="col5"><time datetime=""></time></td>-->
											<td class="col44"><?php echo $IPNstatus_EscrowUniqueIdentifier; ?></td>
											<td class="col6">
												<div class="holder">
													<span class="text"><?php echo $order->transaction_method; ?></span>
													<!-- <a href="#" class="setting-icon"><i class="icon-gear"></i></a> -->
												</div>
											</td>
											<td class="col7 <?php echo $bg_status; ?>" <?php echo $style; ?> <?php echo $tooltip; ?> onclick="viewStatusInfoMain('<?php echo $IPNstatus_Code; ?>', '<?php echo $IPNstatus_EscrowUniqueIdentifier; ?>','<?php echo $IPNstatus_Description; ?>')" alt="Click to view more info" title="Click to view more info">
												<div class="holder">
													<span><?php echo $display_status; ?></span>
													<?php 
													// sample API response
													/*
													{"Code":0,"Description":"Transaction Cancelled","EscrowUniqueIdentifier":857476}
													{"Code":5,"Description":"Buyer Accepted","EscrowUniqueIdentifier":857535}
													{"Code":15,"Description":"Both parties accepted","EscrowUniqueIdentifier":857615}
													{"Code":20,"Description":"Buyer paid Escrow.com","EscrowUniqueIdentifier":857616}
													*/
													//echo $IPNstatus_Description; 
													?>
													
													<?php 
													/*
													$btn_class = '';
													if($inq->status == 2)	{	$btn_class = 'declined'; }
													if($inq->status == 3)	{	$btn_class = 'cancel'; }
													?>
													<a href="#" class="btn <?php echo $btn_class; ?>"><?php echo $inquiries_label[$inq->id_inquiry]; ?></a>
													<!-- <a href="#" class="setting-icon"><i class="icon-gear"></i></a> -->
													*/
													?>
												</div>
											</td>
											<td class="col66">
												<!-- <a id="link_print_payment_<?php echo $order->id_domain; ?>" target="_blank" > <i class="icon-print"></i>  </a> -->
												<i style="cursor:pointer;" class="icon-print" onclick="printPaymentInfoByID('print', 'approved', '<?php echo $IPNstatus_EscrowUniqueIdentifier; ?>', '<?php echo strtolower($order->transaction_method); ?>', '<?php echo $order->id_domain; ?>')"></i>
													
											</td>
										</tr>
										<?php
									}
								}
								else	{
									?><tr><td colspan="6">No Order Found</td></tr><?php
								}
								?>
                                </tbody>
                            </table>
                        </div>
						<?php /*
                        <ul class="pagniation list-unstyled">
                            <li class="active">1</li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                        </ul>
						*/ ?>
						
<script>
$('[data-toggle=tooltip]').tooltip();
</script>